1. kubectl installieren     \
2. virtual box installieren  |-> kann da nur bei manjaro weiterhelfen
3. minikube installieren    /
4. (Konsole)> minikube start -p cchat --memory 4096
     -> Startet/Erstellt die VM (das memory flag ist wichtig da sich die Services sonst selbst abschießen)
5. (Konsole)> kubectl apply --all -f kubernetes/
     -> Startet alle Services/Pods/Deplyments unserer Container
6. (Konsole)> kubectl get all
     -> Zeigt alles an was gerade rennt (mit Ports und IP adressen)
7. (Konsole)> kubectl delete --all -f kubernetes/
     -> Entfernet wieder alle Services/Pods/Deployments


Weitere Commands:
 > kubectl cluster-info
    -> Zeit die IP Adresse des Clusters an
 > kubectl logs -f service/[serviceName]
    -> Zeigt alle Logs eines Services an (eg."service/user")
 > curl -X POST "http://[ClusterAddress]:30100/api/register" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"password\": \"user1\", \"username\": \"user1\"}"
    -> Schickt ein register Request ans User Service
 > kubectl exec -it [PodName] /bin/bash
    -> root console für den ausgewählten Pod (z.b. für mongodb) (Pod namen sind nach jedem start anders)


