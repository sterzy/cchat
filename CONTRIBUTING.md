# How to Contribute to this Project
Please don't at this stage. Actually, we are surprised you found this and if you actually want to contribute that is very nice of you. However, this is a project for a university course at this stage and we cannot accept any of your contributions at this stage as it would potentially affect our evaluation for the course.

Thank you for your interest in the project though and we hope that you stay interested in it until the end of this early stage.
