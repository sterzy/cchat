Feature: Interacting with the chat list

    Background:
        Given the browser is open

    Scenario Outline: Add a new chat to the list.
        Given user is registered
        And user is at chat list
        When a new chat is added with name "<name>"
        Then the new chat "<name>" is listed in the chat list

        Examples:
            | name      |
            | Gerhard   |
            | Magdalena |
            | Niki      |
            | Simon     |
            | Stefan    |


    Scenario: Chat list shows last message date.
        Given user is registered
        And user has a chat
        When user has sent a message
        Then the last message and date are displayed in list
