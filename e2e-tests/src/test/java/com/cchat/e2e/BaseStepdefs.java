package com.cchat.e2e;

import com.cchat.e2e.browser.Browser;
import com.cchat.e2e.browser.BrowserFactory;
import com.cchat.e2e.util.E2EWebElementFactory;
import com.cchat.e2e.util.E2eWebElement;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public abstract class BaseStepdefs {
    public static boolean HEADLESS;

    public static String HOST;
    public static String USERNAME;
    public static String PASSWORD;
    public static Browser BROWSER;

    public static RemoteWebDriver driver;
    public static WebDriverWait wait;

    public static E2eWebElement root;
    private static E2EWebElementFactory factory;



    @BeforeClass
    public static void setup() {

        if (driver != null) {
            return;
        }

        // Lokal
        HOST = "localhost:4200";
        // Mit Docker
        //HOST = "localhost:8080";
        HEADLESS = false;

        BROWSER = BrowserFactory.byName("chrome", HEADLESS);
        if (BROWSER == null) {
            throw new IllegalArgumentException("Browser not found");
        }

        driver = BROWSER.getRemoteWebDriver();
        wait = new WebDriverWait(driver, 10);
        factory = new E2EWebElementFactory(driver);
        root = factory.getRootE2EWebElement();

    }



    public void register() {

        this.setup();

        driver.get(HOST);

        List<WebElement> elements = driver.findElements(By.cssSelector("*[data-e2e=\"new-user-id\"]"));

        if (elements.size() == 0) {
            return;
        }

        assertThat(elements.size(), is(1));

        E2eWebElement input = root.findChild("new-user-id");

        input.input("test-user");

        root.findChild("register-new-user").click();
    }


    @AfterClass
    public static void tearDown() {
        if (driver != null) {
            driver.close();
        }

        driver = null;
        root = null;
    }

}
