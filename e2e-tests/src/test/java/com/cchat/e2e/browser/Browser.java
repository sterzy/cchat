package com.cchat.e2e.browser;

import org.openqa.selenium.remote.RemoteWebDriver;

public abstract class Browser {
    private boolean headless;

    public Browser(boolean headless) {
        System.setProperty(getPropertyKey(), getPropertyValue());
        this.headless = headless;
    }

    public boolean isHeadless() {
        return headless;
    }

    public abstract String getPropertyKey();

    public String getPropertyValue() {
        String fileRelative;
        if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
            // Windows
            fileRelative = "/windows/" + getDriverFile() + ".exe";
        } else {
            // Linux
            fileRelative = "/linux/" + getDriverFile();
        }

        return this.getClass().getResource(fileRelative).getFile();
    }

    public abstract String getDriverFile();

    public abstract RemoteWebDriver getRemoteWebDriver();
}
