package com.cchat.e2e.browser;

public class BrowserFactory {
    public static final Browser chrome(boolean headless) {
        return new Chrome(headless);
    }

    public static final Browser firefox(boolean headless) {
        return new Firefox(headless);
    }

    public static final Browser byName(String name, boolean headless) {
        if (name.equalsIgnoreCase("chrome")) {
            return chrome(headless);
        } else if (name.equalsIgnoreCase("firefox")) {
            return firefox(headless);
        }

        return null;
    }
}
