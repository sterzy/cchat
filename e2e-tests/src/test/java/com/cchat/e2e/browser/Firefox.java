package com.cchat.e2e.browser;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Firefox extends Browser {
    public Firefox(boolean headless) {
        super(headless);
    }

    @Override
    public final String getPropertyKey() {
        return "webdriver.gecko.driver";
    }

    @Override
    public final String getDriverFile() {
        return "geckodriver";
    }

    @Override
    public RemoteWebDriver getRemoteWebDriver() {
        FirefoxOptions options = new FirefoxOptions();
        options.setCapability("marionette", true);
        options.setHeadless(isHeadless());

        return new FirefoxDriver(options);
    }
}
