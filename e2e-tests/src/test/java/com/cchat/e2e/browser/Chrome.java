package com.cchat.e2e.browser;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Chrome extends Browser {
    public Chrome(boolean headless) {
        super(headless);
    }

    @Override
    public final String getPropertyKey() {
        return "webdriver.chrome.driver";
    }

    @Override
    public final String getDriverFile() {
        return "chromedriver";
    }

    @Override
    public RemoteWebDriver getRemoteWebDriver() {
        ChromeOptions options = new ChromeOptions();
        options.setCapability("marionette", false);
        options.addArguments("window-size=1920,1200");
        options.addArguments("--no-sandbox");
        options.setHeadless(isHeadless());

        return new ChromeDriver(options);
    }
}
