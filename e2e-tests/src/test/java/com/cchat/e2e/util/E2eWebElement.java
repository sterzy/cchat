package com.cchat.e2e.util;

import org.openqa.selenium.*;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.IntStream;

public class E2eWebElement {

    public static int DEFAULT_WAIT_IN_SECONDS = 4;
    public static int RETRY_SLEEP = 500;
    public static int MAX_RETRY = 5;

    private WebElement webElement;
    private RemoteWebDriver webDriver;
    private WebDriverWait wait;
    private Supplier<WebElement> refreshFunction;
    private E2eWebElement parent;

    public E2eWebElement(WebElement webElement, RemoteWebDriver webDriver, Supplier<WebElement> refreshFunction) {
        this.webDriver = webDriver;
        this.wait = new WebDriverWait(webDriver, DEFAULT_WAIT_IN_SECONDS);
        this.webElement = webElement;
        this.refreshFunction = refreshFunction;
    }

    public E2eWebElement(WebElement webElement, RemoteWebDriver webDriver, Supplier<WebElement> refreshFunction, E2eWebElement parent) {
        this(webElement, webDriver, refreshFunction);
        this.parent = parent;
    }

    /**
     * Diese methode ändert für alle nachfolgenden aufrufe von wait, wie lange auf eine Kondition gewartet wird
     * @param timeoutInSeconds
     */
    public void setTimeout(int timeoutInSeconds) {
        this.wait = new WebDriverWait(webDriver, timeoutInSeconds);
    }

    /**
     * refreshed rekursiv die WebElemente der Parents und dann das eigene WebElement.
     */
    protected void refresh() {
        /* Hier wird rekursiv von parent nach child jedes Element neu geladen werden.
         Das ist sehr langsam und fängt nur ein paar edge cases ab, die wir derzeit nicht verwenden
         Falls es mal notwendig wird würde ich nur dann rekursiv laden wenn eine Exception fliegt die das notwendig macht.
*/
        if (this.parent != null) {
            this.parent.refresh();
        }

        this.webElement = refreshFunction.get();
    }
    public E2eWebElement findChild(String e2eId) {
        return findChild(getByFromE2e(e2eId));
    }

    public E2eWebElement findChild(By by) {
        return retry(() -> {
            return new E2eWebElement(wait.until(ExpectedConditions.presenceOfNestedElementLocatedBy(webElement, by)), webDriver, () -> this.findChild(by).getWebElement(), this);
        });
    }

    public List<E2eWebElement> findChildren(String e2eId) {
        return findChildren(getByFromE2e(e2eId));
    }

    public List<E2eWebElement> findChildren(By by) {
        refresh();
        AtomicInteger atomicInteger = new AtomicInteger(0);
        List<E2eWebElement> result = new ArrayList<>();
        List<WebElement> webElements = wait.until(presenceOfNestedElementsLocatedBy(webElement, by));

        IntStream.range(0, webElements.size()).forEach(
                i -> {
                    result.add(
                            new E2eWebElement(
                                    webElements.get(i),
                                    webDriver,
                                    () -> this.findChildren(by).get(i).getWebElement(),
                                    this));
                }
        );
        return result;
    }

    public void click() {
        retry(() -> {
            // force staleness check
            wait.until(ExpectedConditions.elementToBeClickable(webElement));
            webElement.click();
        });
    }

    public void scrollIntoView() {
        retry(() -> {
            webDriver.executeScript("arguments[0].scrollIntoView(true);", webElement);
        });
    }

    public void input(String text) {
        retry(() -> {
            webElement.sendKeys(text);
        });
    }

    public void typeahead(String text, int selectPosition) {
        retry(() -> {
            if (text != null) {
                clear();
                webElement.sendKeys(text);
            }
            click();
            findChild(By.xpath("//ngb-typeahead-window/button[" + selectPosition + "]")).click();
        });
    }

    public void sharedTypeahead(String text, int selectPosition) {
        retry(() -> {
            if (text != null) {
                clear();
                webElement.sendKeys(text);
            }
            click();
            findChild(By.xpath("//slpshared-typeahead-dropdown-menu/button[" + selectPosition + "]")).click();
        });
    }


    public void dropdown(String text, int selectPosition) {
        retry(() -> {
            if (text != null) {
                clear();
                refresh();
                webElement.sendKeys(text);
            }
            findChild(By.xpath("//ngb-highlight[" + selectPosition + "]")).click();
        });
    }

    public void select(String text) {
        retry(() -> {
            webElement.click();
            findChild(By.xpath(".//option[normalize-space(text())=\"" + text + "\"]")).click();
        });
    }

    public void absentChild(String e2eId) {
        absentChild(getByFromE2e(e2eId));
    }

    public void absentChild(By by) {
        retry(() -> {
            WebDriverWait wait = new WebDriverWait(webDriver, 1);
            refresh();
            wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
        });
    }

    public void clear() {
        retry(() -> {
            while (!webElement.getAttribute("value").equals("")) {
                webElement.sendKeys(Keys.BACK_SPACE);
            }
        });
    }

    protected WebElement getWebElement() {
        return retry(() -> {
            webElement.getText();
            return webElement;
        });
    }


    public E2eWebElement getParent() {
        return parent;
    }


    public String getAttribute(String s) {
        return retry(() -> {
            refresh();
            webElement.getText();
            return webElement.getAttribute(s);
        });
    }

    public String getText() {
        return retry(() -> {
            // force staleness check
            refresh();
            webElement.getText();
            return webElement.getText();
        });
    }

    public String getCSSValue(String s) {
        return retry(() -> {
            refresh();
            webElement.getText();
            return webElement.getCssValue(s);
        });
    }

    public boolean isEnabled() {
        return retry(() -> {
            refresh();
            webElement.getText();
            return webElement.isEnabled();
        });
    }

    public boolean isSelected() {
        return retry(() -> {
            refresh();
            webElement.getText();
            return webElement.isSelected();
        });
    }

    public boolean isDisplayed() {
        return retry(() -> {
            // force staleness check
            refresh();
            webElement.getText();
            return webElement.isDisplayed();
        });
    }

    public String getTagName() {
        return retry(() -> {
            refresh();
            webElement.getText();
            return webElement.getTagName();
        });
    }

    public Rectangle getRect() {
        return retry(() -> {
            refresh();
            webElement.getText();
            return webElement.getRect();
        });
    }

    public Point getLocation() {
        return retry(() -> {
            // force staleness check
            refresh();
            webElement.getText();
            return webElement.getLocation();
        });

    }

    public Dimension getSize() {
        return retry(() -> {
            // force staleness check
            refresh();
            webElement.getText();
            return webElement.getSize();
        });
    }

    // Helper Functions

    private void sleep(int time) {
        try {
            TimeUnit.MILLISECONDS.sleep(time);
        } catch (InterruptedException e) {
            // Ist uns egal
        }
    }

    private <T> T retry(Supplier<T> sup) {
        int i = 0;
        while (i < MAX_RETRY) {
            try {
                return sup.get();
            } catch (Exception e) {
                refresh();
                i++;
                if(i >= MAX_RETRY){
                    throw new RetryException(e);
                }
                sleep(RETRY_SLEEP);
            }
        }
        throw new RetryException();
    }

    private void retry(Runnable sup) {
        boolean retry = true;
        int i = 0;
        while (retry) {
            try {
                sup.run();
                retry = false;
            } catch (Exception e) {
                refresh();
                i++;
                if(i >= MAX_RETRY){
                    throw new RetryException(e);
                }
                sleep(RETRY_SLEEP);
            }
        }
    }

    private By getByFromE2e(String e2eId) {
        return By.cssSelector("*[data-e2e='" + e2eId + "']");
    }

    private ExpectedCondition<List<WebElement>> presenceOfNestedElementsLocatedBy(final WebElement parent,
                                                                                  final By childLocator) {
        return new ExpectedCondition<List<WebElement>>() {
            public List<WebElement> apply(WebDriver driver) {
                List<WebElement> allChildren = parent.findElements(childLocator);
                return allChildren.isEmpty() ? null : allChildren;
            }

            public String toString() {
                return String.format("visibility of element located by %s -> %s", parent, childLocator);
            }
        };
    }

    private class RetryException extends RuntimeException {
        public RetryException() {
        }

        public RetryException(String message) {
            super(message);
        }

        public RetryException(String message, Throwable cause) {
            super(message, cause);
        }

        public RetryException(Throwable cause) {
            super(cause);
        }

        public RetryException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
        }
    }
}
