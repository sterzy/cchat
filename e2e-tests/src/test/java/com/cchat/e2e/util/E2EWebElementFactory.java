package com.cchat.e2e.util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class E2EWebElementFactory {

    private RemoteWebDriver webDriver;
    private WebDriverWait wait;

    public E2EWebElementFactory(RemoteWebDriver webDriver){
        this.webDriver = webDriver;
        wait = new WebDriverWait(webDriver, 10);
    }

    public E2eWebElement getE2EWebElement(String e2eId){
        WebElement we = wait.until(ExpectedConditions.presenceOfElementLocated(
                By.cssSelector("*[data-e2e='" + e2eId + "']")
        ));

        return new E2eWebElement(we, webDriver, () -> (getE2EWebElement(e2eId).getWebElement()));
    }

    public E2eWebElement getE2EWebElement(By by){
        WebElement we = wait.until(ExpectedConditions.presenceOfElementLocated(by));
        return new E2eWebElement(we, webDriver, () -> getE2EWebElement(by).getWebElement());
    }

    /**
     *
     * @return root WebElement
     */
    public E2eWebElement getRootE2EWebElement(){
        WebElement we = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/*")));
        return new E2eWebElement(we, webDriver, () -> getRootE2EWebElement().getWebElement());
    }
}
