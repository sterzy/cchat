package com.cchat.e2e.junit;

import com.cchat.e2e.browser.Browser;
import com.cchat.e2e.browser.BrowserFactory;
import com.cchat.e2e.util.E2EWebElementFactory;
import com.cchat.e2e.util.E2eWebElement;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.*;

public class BaseTests {


    public static boolean HEADLESS;

    public static String HOST;
    public static Browser BROWSER;

    public static RemoteWebDriver driver;
    public static WebDriverWait wait;

    public static E2eWebElement root;
    public static E2EWebElementFactory factory;

    @BeforeClass
    public static void setup() {

        if (driver != null) {
            return;
        }

        // Local
        HOST = "http://localhost:4200";
        HEADLESS = false;

        BROWSER = BrowserFactory.byName("chrome", HEADLESS);
        if (BROWSER == null) {
            throw new IllegalArgumentException("Browser not found");
        }

        driver = BROWSER.getRemoteWebDriver();
        driver.get(HOST);
        wait = new WebDriverWait(driver, 10);
        factory = new E2EWebElementFactory(driver);
        root = factory.getRootE2EWebElement();


    }


    @AfterClass
    public static void tearDown() {
        if (driver != null) {
            driver.close();
        }

        driver = null;
        root = null;

    }

    public static void registerUser(String username) {
        if (!driver.getCurrentUrl().equals(HOST+"/register")) {
            driver.navigate().to(HOST+"/register");
        }

        // find the input buttons
        E2eWebElement registerUsernameField = root.findChild("register-username");
        E2eWebElement registerPasswordField = root.findChild("register-password");
        E2eWebElement registerConfirmPasswordField = root.findChild("register-confirm-password");

        // fill out the input buttons
        registerUsernameField.input(username);
        assertEquals(username, registerUsernameField.getAttribute("value"));
        registerPasswordField.input(username);
        assertEquals(username, registerPasswordField.getAttribute("value"));
        registerConfirmPasswordField.input(username);
        assertEquals(username, registerConfirmPasswordField.getAttribute("value"));

        // click the register button
        E2eWebElement registerButtonOnRegisterSite = root.findChild("register-button-on-register-site");
        registerButtonOnRegisterSite.click();

        // das ist nur damit selenium darauf wartet das die login seite geladen wurde
        E2eWebElement settingsButton = root.findChild("settings-button");

        // logout
        logoutUser(username);

    }

    public static void unregisterUser(String username) {
        E2eWebElement settingsButton = root.findChild("settings-button");
        assertNotNull(settingsButton);
        settingsButton.click();

        E2eWebElement deleteAccount = root.findChild("delete-account");
        assertNotNull(deleteAccount);
        deleteAccount.click();

        E2eWebElement confirmDeleteAccount = root.findChild("proceed-delete-session");
        assertNotNull(confirmDeleteAccount);
        confirmDeleteAccount.click();

        // Only to slow selenium a bit down, else selenium breaks the unregister routine
        E2eWebElement loginUsernameField = root.findChild("login-username");
    }

    public static void loginUser(String username) {
        if (!driver.getCurrentUrl().equals(HOST+"/login")) {
            driver.navigate().to(HOST+"/login");
        }
        // find the input buttons
        E2eWebElement loginUsernameField = root.findChild("login-username");
        E2eWebElement loginPasswordField = root.findChild("login-password");

        // fill out the input buttons
        loginUsernameField.input(username);
        assertEquals(username, loginUsernameField.getAttribute("value"));
        loginPasswordField.input(username);
        assertEquals(username, loginPasswordField.getAttribute("value"));

        // click the login button
        E2eWebElement loginButtonOnLoginSite = root.findChild("login-button-on-login-site");
        loginButtonOnLoginSite.click();

        if (driver.getCurrentUrl().contains("confirm")) {
            E2eWebElement proceedButton = root.findChild(By.xpath("//button[span/text()='Proceed']"));
            assertNotNull(proceedButton);
            proceedButton.click();
        }

        // das ist nur damit selenium darauf wartet das die login seite geladen wurde
        E2eWebElement settingsButton = root.findChild("settings-button");


    }

    public static void logoutUser(String username) {
        if (!driver.getCurrentUrl().contains("login")) {
            driver.navigate().to(HOST + "/user/api/logout");
        }
    }
}
