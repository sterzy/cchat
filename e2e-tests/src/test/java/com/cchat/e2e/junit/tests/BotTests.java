package com.cchat.e2e.junit.tests;

import com.cchat.e2e.junit.BaseTests;
import com.cchat.e2e.util.E2eWebElement;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import static org.junit.Assert.*;

public class BotTests extends BaseTests {

    @Test
    public void registerBotAndUseItTest() {
        // start the bot interface
        E2eWebElement settingsButton = root.findChild("settings-button");
        assertNotNull(settingsButton);
        settingsButton.click();
        E2eWebElement botButton = root.findChild("bot-interface");
        assertNotNull(botButton);
        botButton.click();

        // escape to the normal chat interface
        Actions action = new Actions(driver);
        action.sendKeys(Keys.ESCAPE).perform();


        // add the example-bot
        E2eWebElement addNewBot = root.findChild("add-new-bot");
        assertNotNull(addNewBot);
        addNewBot.click();

        E2eWebElement registerBotForm = root.findChild("register-bot-form");
        assertNotNull(registerBotForm);

        // Fill out the form
        E2eWebElement name = registerBotForm.findChild(By.xpath("//*[@formcontrolname='name']"));
        assertNotNull(name);
        name.input("example-bot");
        assertEquals("example-bot", name.getAttribute("value"));

        E2eWebElement description = registerBotForm.findChild(By.xpath("//*[@formcontrolname='description']"));
        assertNotNull(description);
        description.input("What a wonderful example bot!");
        assertEquals("What a wonderful example bot!", description.getAttribute("value"));

        E2eWebElement webHookProtocol = registerBotForm.findChild(By.xpath("//*[@formcontrolname='webHookProtocol']"));
        assertNotNull(webHookProtocol);
        webHookProtocol.click();

        E2eWebElement HTTP = webHookProtocol.findChild(By.xpath("//mat-option[@value='http://']"));
        assertNotNull(HTTP);
        HTTP.click();

        E2eWebElement botUrl = registerBotForm.findChild(By.xpath("//*[@formcontrolname='webHook']"));
        assertNotNull(botUrl);
        botUrl.input("localhost:10099/api");
        assertEquals("localhost:10099/api", botUrl.getAttribute("value"));

        E2eWebElement isPublic = registerBotForm.findChild(By.xpath("//*[@formcontrolname='isPublic']"));
        assertNotNull(isPublic);
        isPublic.click();

        // send the register request
        E2eWebElement  botRegisterButton = registerBotForm.findChild("register-button");
        assertNotNull(botRegisterButton);
        botRegisterButton.click();


        // check if bot was really registered
        E2eWebElement botCard = root.findChild(By.xpath("//mat-card-title[text()='example-bot']"));
        assertNotNull(botCard);
        botCard.click();

        E2eWebElement manageBotWindow = root.findChild(By.xpath("//mat-slide-toggle[@id='mat-slide-toggle-1']"));
        assertNotNull(manageBotWindow);
        manageBotWindow.click();

        E2eWebElement addToChatsButton = root.findChild(By.xpath("//button[span/text()='Add to chats']"));
        assertNotNull(addToChatsButton);
        addToChatsButton.click();

        E2eWebElement inputTestMessage = root.findChild(By.xpath("//input[@placeholder='Enter a test message']"));
        assertNotNull(inputTestMessage);
        inputTestMessage.input("test message");

        E2eWebElement testMessageButton= root.findChild(By.xpath("//button[span/text()='Send test message']"));
        assertNotNull(testMessageButton);
        testMessageButton.click();

        E2eWebElement successMessage = root.findChild(By.xpath("//strong[contains(text(),'Decrypted message')]"));
        assertNotNull(successMessage);

        E2eWebElement closeRegisterDialog = root.findChild("cancel-register-button");
        assertNotNull(closeRegisterDialog);
        closeRegisterDialog.click();

        //#######################
        // Use the bot
        //#######################
        // Open bot chat
        E2eWebElement botChat = root.findChild(By.xpath("//*[@data-e2e='chat-name' and text()='example-bot']"));
        assertNotNull(botChat);
        botChat.click();


        // send wrong formatted message
        E2eWebElement chatField = root.findChild("message-input");
        assertNotNull(chatField);
        while (!chatField.getText().contains("formatted")) {
            chatField.input("wrong formatted message");
        }
        E2eWebElement sendMessageButton = root.findChild("send-message-button");
        assertNotNull(sendMessageButton);

        // we need that because selenium is too fast so that the send button is not really active although selenium think it is.
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        sendMessageButton.click();


        // look if usage message was returned
        E2eWebElement chatMessageWrongFormat = root.findChild(By.xpath("//div[contains(text(),'format was wrong')]"));
        assertNotNull(chatMessageWrongFormat);

        // send right message
        chatField.input("time:5,message:Wohoo, everything is working");
        sendMessageButton.click();

        // look if correct response was send
        E2eWebElement chatMessageResponse = root.findChild(By.xpath("//div[contains(text(),'I will send you the message')]"));
        assertNotNull(chatMessageResponse);

        // look if we got the correct message back from the bot
        E2eWebElement chatMessageText = root.findChild(By.xpath("//div[text()='Wohoo, everything is working']"));
        assertNotNull(chatMessageText);




        // to see something before the test finishes. ONLY for demonstration purpose.
        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
    

    @BeforeClass
    public static void beforeClassBotTests() {
        registerUser("botowner");
    }

    @AfterClass
    public static void afterClassBotTests() {
        loginUser("botowner");
        unregisterUser("botowner");
    }

    @Before
    public void before() {
        loginUser("botowner");
    }

    @After
    public void after() {
        logoutUser("botowner");
    }


}
