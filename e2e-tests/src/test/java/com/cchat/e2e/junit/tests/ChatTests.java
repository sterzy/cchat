package com.cchat.e2e.junit.tests;

import com.cchat.e2e.browser.Browser;
import com.cchat.e2e.browser.BrowserFactory;
import com.cchat.e2e.junit.BaseTests;
import com.cchat.e2e.util.E2EWebElementFactory;
import com.cchat.e2e.util.E2eWebElement;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.*;

public class ChatTests extends BaseTests {

    public static Browser BROWSER2;

    public static RemoteWebDriver driver2;
    public static WebDriverWait wait2;

    public static E2eWebElement root2;
    public static E2EWebElementFactory factory2;


    @Test
    public void findFriendsAndAddThemToChatListTest() {

        // Add test-user1 as friend
        E2eWebElement friendSearchBar = root.findChild("friend-search-bar");
        assertNotNull(friendSearchBar);

        friendSearchBar.input("test-us");
        E2eWebElement test_user1_search = root.findChild(By.xpath("//mat-option[@data-e2e='friend-search-bar-dropdown' and span/text()=' test-user1 ']"));
        assertNotNull(test_user1_search);
        test_user1_search.click();

        E2eWebElement addNewFriend = root.findChild("save-new-chat");
        assertNotNull(addNewFriend);
        addNewFriend.click();

        // check if test-user1 was registered as a chat
        E2eWebElement test_user1_chat = root.findChild(By.xpath("//*[@data-e2e='chat-name' and text()='test-user1']"));
        assertNotNull(test_user1_chat);
        test_user1_chat.click();

        // Add test-user2 as friend
        friendSearchBar.input("test-us");
        E2eWebElement test_user2_search = root.findChild(By.xpath("//mat-option[@data-e2e='friend-search-bar-dropdown' and span/text()=' test-user2 ']"));
        assertNotNull(test_user2_search);
        test_user2_search.click();

        addNewFriend = root.findChild("save-new-chat");
        assertNotNull(addNewFriend);
        addNewFriend.click();

        // check if test-user2 was registered as a chat
        E2eWebElement test_user2_chat = root.findChild(By.xpath("//*[@data-e2e='chat-name' and text()='test-user2']"));
        assertNotNull(test_user2_chat);
        test_user2_chat.click();

        // ist drinnen damit selenium am schluss nicht so schnell weggeht und man was sieht (z.B. fuers mr2 ganz gut)
        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void chatWithFriend() {

        // add chat for test-user2 (we are test-user1)
        E2eWebElement friendSearchBar = root.findChild("friend-search-bar");
        assertNotNull(friendSearchBar);

        friendSearchBar.input("test-user2");

        E2eWebElement test_user2_search = root.findChild(By.xpath("//mat-option[@data-e2e='friend-search-bar-dropdown' and span/text()=' test-user2 ']"));
        assertNotNull(test_user2_search);
        test_user2_search.click();

        E2eWebElement addNewFriend = root.findChild("save-new-chat");
        assertNotNull(addNewFriend);
        addNewFriend.click();

        E2eWebElement test_user2_chat = root.findChild(By.xpath("//*[@data-e2e='chat-name' and text()='test-user2']"));
        assertNotNull(test_user2_chat);
        test_user2_chat.click();

        //#######################
        // Login test-user2
        if (!driver2.getCurrentUrl().equals(HOST+"/login")) {
            driver2.navigate().to(HOST+"/login");
        }
        // find the input buttons
        E2eWebElement loginUsernameField = root2.findChild("login-username");
        E2eWebElement loginPasswordField = root2.findChild("login-password");

        // fill out the input buttons
        loginUsernameField.input("test-user2");
        assertEquals("test-user2", loginUsernameField.getAttribute("value"));
        loginPasswordField.input("test-user2");
        assertEquals("test-user2", loginPasswordField.getAttribute("value"));

        // click the login button
        E2eWebElement loginButtonOnLoginSite = root2.findChild("login-button-on-login-site");
        loginButtonOnLoginSite.click();

        // for the proceed window
        E2eWebElement proceedButton = root2.findChild(By.xpath("//button[span/text()='Proceed']"));
        assertNotNull(proceedButton);
        proceedButton.click();

        // das ist nur damit selenium darauf wartet das die login seite geladen wurde
        E2eWebElement settingsButton = root2.findChild("settings-button");
        //########################

        // add chat for test-user1 (we are test-user2)
        E2eWebElement friendSearchBarSecondWindow = root2.findChild("friend-search-bar");
        assertNotNull(friendSearchBarSecondWindow);

        friendSearchBarSecondWindow.input("test-user1");

        E2eWebElement test_user2_searchSecondWindow = root2.findChild(By.xpath("//mat-option[@data-e2e='friend-search-bar-dropdown' and span/text()=' test-user1 ']"));
        assertNotNull(test_user2_searchSecondWindow);
        test_user2_searchSecondWindow.click();

        E2eWebElement addNewFriendSecondWindow = root2.findChild("save-new-chat");
        assertNotNull(addNewFriendSecondWindow);
        addNewFriendSecondWindow.click();

        E2eWebElement test_user2_chatSecondWindow = root2.findChild(By.xpath("//*[@data-e2e='chat-name' and text()='test-user1']"));
        assertNotNull(test_user2_chatSecondWindow);
        test_user2_chatSecondWindow.click();

        // send message from test-user2 to test-user1
        E2eWebElement chatFieldSecondWindow = root2.findChild("message-input");
        assertNotNull(chatFieldSecondWindow);
        chatFieldSecondWindow.input("Test");
        E2eWebElement sendMessageButtonSecondWindow = root2.findChild("send-message-button");
        assertNotNull(sendMessageButtonSecondWindow);
        sendMessageButtonSecondWindow.click();

        // look if message was send
        E2eWebElement chatMessageFirstWindow = root.findChild(By.xpath("//div[contains(text(),'Test')]"));
        assertNotNull(chatMessageFirstWindow);

        // ist drinnen damit selenium am schluss nicht so schnell weggeht und man was sieht (z.B. fuers mr2 ganz gut)
        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @BeforeClass
    public static void beforeClassChatTests() {

        Dimension dimension = driver.manage().window().getSize();

        driver.manage().window().setSize(new Dimension(dimension.width/2, dimension.height));
        driver.manage().window().setPosition(new Point(0,0));

        registerUser("test-user1");
        registerUser("test-user2");

        BROWSER2 = BrowserFactory.byName("chrome", HEADLESS);
        if (BROWSER2 == null) {
            throw new IllegalArgumentException("Browser not found");
        }

        driver2 = BROWSER2.getRemoteWebDriver();
        driver2.get(HOST);
        wait2 = new WebDriverWait(driver2, 10);
        factory2 = new E2EWebElementFactory(driver2);
        root2 = factory2.getRootE2EWebElement();

        driver2.manage().window().setSize(new Dimension(dimension.width/2, dimension.height));
        driver2.manage().window().setPosition(new Point(dimension.width/2,0));

    }

    @AfterClass
    public static void afterClassChatTests() {
        loginUser("test-user1");
        unregisterUser("test-user1");
        loginUser("test-user2");
        unregisterUser("test-user2");

        if (driver2 != null) {
            driver2.close();
        }

        driver2 = null;
        root2 = null;

    }

    @Before
    public void before() {
        loginUser("test-user1");
    }

    @After
    public void after() {
        logoutUser("test-user1");
    }


}
