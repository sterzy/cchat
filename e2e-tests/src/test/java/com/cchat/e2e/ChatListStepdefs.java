package com.cchat.e2e;

import com.cchat.e2e.util.E2eWebElement;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

public class ChatListStepdefs extends BaseStepdefs {

    @Given("the browser is open")
    public void theBrowserIsOpen() {
        super.setup();
    }

    @Given("user is registered")
    public void userIsRegistered() {
        this.register();
    }

    @Given("user is at chat list")
    public void user_is_at_chat_list() {
    }

    @When("a new chat is added with name {string}")
    public void a_new_chat_is_added_with_name(String string) {

        root.findChild("add-chat-button").click();

        root.findChild("new-chat-name").input(string);

        root.findChild("save-new-chat").click();
    }

    @Then("the new chat {string} is listed in the chat list")
    public void the_new_chat_is_listed_in_the_chat_list(String string) {
        // Write code here that turns the phrase above into concrete actions

        List<String> chatNames = root.findChild("chat-list").findChildren("chat-name")
            .stream()
            .map(elem -> elem.getText())
            .collect(Collectors.toList());

        assertThat(chatNames, hasItem(string));
    }

    @And("user has a chat")
    public void userHasAChat() {
        root.findChild("add-chat-button").click();

        root.findChild("new-chat-name").input("chat");

        root.findChild("save-new-chat").click();
    }

    @When("user has sent a message")
    public void userHasSentAMessage() {
        E2eWebElement chat = root.findChild("chat-list").findChildren("chat-name")
            .stream()
            .filter(elem -> elem.getText().equals("chat"))
            .findFirst()
            .orElse(null);

        assertThat(chat, is(notNullValue()));

        chat.click();

        root.findChild("message-input").input("text\n");

    }

    @Then("the last message and date are displayed in list")
    public void theLastMessageAndDateUpdate() {
        E2eWebElement chat = root.findChild("chat-list").findChildren("chat-list-entry")
            .stream()
            .filter(elem -> elem.findChild("chat-name").getText().equals("chat"))
            .findFirst()
            .orElse(null);

        assertThat(chat, is(notNullValue()));

        assertThat(chat.findChild("chat-lastmsg").getText(), is("text"));
        assertThat(chat.findChild("chat-lastmgs-date").getText(), is(notNullValue()));

    }
}
