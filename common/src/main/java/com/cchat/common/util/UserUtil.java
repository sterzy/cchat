package com.cchat.common.util;

import com.cchat.common.security.SecurityUser;
import org.springframework.security.authentication.RememberMeAuthenticationToken;

import java.security.Principal;

public class UserUtil {
    public static SecurityUser getUser(Principal principal) {
        if (principal == null) {
            return null;
        }

        return (SecurityUser) ((RememberMeAuthenticationToken) principal).getPrincipal();
    }
}
