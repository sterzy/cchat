package com.cchat.common.util;

public final class Constants {

    // RabbitMQ Predefined Destinations
    public static final String TOPIC_EP         = "/topic/";
    public static final String AMQ_QUEUES       = "/amq/queue/";
    public static final String EXCHANGE_EP      = "/exchange/amq.direct/";
    public static final String USER_DESTINATION = TOPIC_EP + "user-destination";
    public static final String USER_REGISTRY    = TOPIC_EP + "user-registry";

    // Web Socket Endpoints
    public static final String WEB_SOCKET_EP = "/ws";
    public static final String CHAT_EP       = EXCHANGE_EP + "conversation";
    public static final String ERROR_EP      = EXCHANGE_EP + "error";
    public static final String APP_PREFIX    = "/chat";
    public static final String USER_PREFIX   = "/user";

    // App Routes
    public static final String SEND_ROUTE     = "/send";
    public static final String BOT_SEND_ROUTE = "/bot/send";

    // JWT Token
    public static final String JWT_COOKIE  = "jwt-access";

    // Messaging Response Payloads
    public static final String MESSAGE_SENT = "SENT";

    // Bot Related Queues
    public static final String BOT_QUEUE  = "bot-incoming";

}
