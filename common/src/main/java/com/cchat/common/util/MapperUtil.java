package com.cchat.common.util;

import com.cchat.common.dto.IdNamePojo;
import com.cchat.common.dto.UserDto;
import com.cchat.common.security.SecurityUser;
import org.dozer.DozerBeanMapper;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MapperUtil {
    private DozerBeanMapper mapper = new DozerBeanMapper();

    public <DTO, DOMAIN> DTO toDto(DOMAIN domain, Class<DTO> clazz) {
        return mapper.map(domain, clazz);
    }

    public <DTO, DOMAIN> DOMAIN toDomain(DTO dto, Class<DOMAIN> clazz) {
        return mapper.map(dto, clazz);
    }

    public <DTO, DOMAIN> List<DTO> toDto(List<DOMAIN> list, Class<DTO> clazz) {
        return list
                .stream()
                .map(p -> toDto(p, clazz))
                .collect(Collectors.toList());
    }

    public <DTO, DOMAIN> List<DOMAIN> toDomain(List<DTO> list, Class<DOMAIN> clazz) {
        return list
                .stream()
                .map(p -> toDomain(p, clazz))
                .collect(Collectors.toList());
    }

    public <T> List<IdNamePojo> toKeyValueDto(List<T> list, Function<T, Integer> keyResolver, Function<T, String> valueResolver) {
        return list.stream()
                .map(p -> new IdNamePojo(keyResolver.apply(p), valueResolver.apply(p)))
                .collect(Collectors.toList());
    }

    public UserDto toUserDto(SecurityUser user) {
        if (user == null) {
            return null;
        }

        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setUsername(user.getUsername());
        Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
        if (authorities != null) {
            userDto.setGroups(authorities.stream().map(p -> p.getAuthority()).collect(Collectors.toList()));
        }

        return userDto;
    }
}
