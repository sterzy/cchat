package com.cchat.common.config;

import com.cchat.common.util.Constants;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.*;
import org.springframework.messaging.simp.config.*;
import org.springframework.web.socket.config.annotation.*;

@Log4j2
@Configuration
@EnableWebSocketMessageBroker
@ConditionalOnProperty(name = "server.enable-websocket")
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    // RabbitMQ Configuration Variables

    @Value("${rabbitmq.enabled:false}")
    private boolean enableBroker;

    @Value("${rabbitmq.port:}")
    private String brokerPort;

    @Value("${rabbitmq.host:}")
    private String brokerHost;

    @Value("${rabbitmq.client.user:}")
    private String brokerUser;

    @Value("${rabbitmq.client.pass:}")
    private String brokerPassword;

    @Value("${rabbitmq.system.user:}")
    private String brokerSystemUser;

    @Value("${rabbitmq.system.pass:}")
    private String brokerSystemPassword;

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint(Constants.WEB_SOCKET_EP);
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        
        if (this.enableBroker) {

            int parsedPort;

            // This is a not so ideal work-around, because kubernetes appears to
            // be picky when it comes to the type of an environment variable
            try {
                parsedPort = Integer.parseInt(this.brokerPort);
            } catch (NumberFormatException e) {
                parsedPort = 61613;
                log.warn("could not parse broker port, falling back to stomp default port!");
            }

            registry.enableStompBrokerRelay(Constants.TOPIC_EP, Constants.AMQ_QUEUES, Constants.EXCHANGE_EP)
                    .setAutoStartup(true) // Default anyway, just making it explicit
                    .setClientLogin(this.brokerUser)
                    .setClientPasscode(this.brokerPassword)
                    .setSystemLogin(this.brokerSystemUser)
                    .setSystemPasscode(this.brokerSystemPassword)
                    .setRelayHost(this.brokerHost)
                    .setRelayPort(parsedPort)
                    .setUserDestinationBroadcast(Constants.USER_DESTINATION)
                    .setUserRegistryBroadcast(Constants.USER_REGISTRY);

        } else {
            registry.enableSimpleBroker(Constants.TOPIC_EP, Constants.AMQ_QUEUES, Constants.EXCHANGE_EP);
        }

        // According to this: https://github.com/spring-projects/spring-framework/issues/16228
        // it is perfectly fine to add Constants.USER_PREFIX here
        registry.setApplicationDestinationPrefixes(Constants.APP_PREFIX, Constants.USER_PREFIX);
        registry.setUserDestinationPrefix(Constants.USER_PREFIX);
    }
}
