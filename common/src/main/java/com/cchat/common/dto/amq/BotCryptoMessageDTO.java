package com.cchat.common.dto.amq;

import com.cchat.common.dto.websocket.CryptoMessageDTO;
import lombok.Data;

@Data
public class BotCryptoMessageDTO {

    private String bot;
    private CryptoMessageDTO message;

}
