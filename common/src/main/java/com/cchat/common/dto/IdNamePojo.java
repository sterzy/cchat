package com.cchat.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Generic Object to send id/name pairs.
 */
@Data
@AllArgsConstructor
public class IdNamePojo {
    private Integer objectId;
    private String name;
}
