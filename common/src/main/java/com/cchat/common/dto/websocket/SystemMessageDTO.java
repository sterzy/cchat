package com.cchat.common.dto.websocket;

import lombok.Data;

@Data
public class SystemMessageDTO extends BaseMessageDTO {

    private String messageId;

    {{
        super.setType(MessagingType.MSG_SYSTEM);
    }}

}
