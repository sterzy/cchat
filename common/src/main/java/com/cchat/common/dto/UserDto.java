package com.cchat.common.dto;

import lombok.Data;

import java.util.List;

/**
 * A DTO to store information about a user.
 *
 * It is primarily used for JWT tokens.
 */
@Data
public class UserDto {
    private String id;
    private String username;
    private List<String> groups;
}
