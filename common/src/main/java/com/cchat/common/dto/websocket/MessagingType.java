package com.cchat.common.dto.websocket;

import com.fasterxml.jackson.annotation.*;

public enum MessagingType {

    MSG_CRYPTO("msg-cry"),
    MSG_REGISTER("msg-reg"),
    MSG_SYSTEM("msg-sys");

    private String value;

    MessagingType(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
