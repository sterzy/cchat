package com.cchat.common.dto.websocket;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
public class CryptoMessageDTO extends BaseMessageDTO {

    private String id;
    private String from;

    @NotNull(message = "please specify a valid crypto type")
    @Min(value = 1, message = "a valid crypto type should be between 1 and 3")
    @Max(value = 3, message = "a valid crypto type should be between 1 and 3")
    private Long cryptoType;

    {{
        super.setType(MessagingType.MSG_CRYPTO);
    }}

}
