package com.cchat.common.dto.websocket;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode
public class BaseMessageDTO {

    private MessagingType type;

    @NotNull(message = "please provide a timestamp")
    private Long timestamp;

    @NotBlank(message = "message text is malformed")
    private String payload;

}
