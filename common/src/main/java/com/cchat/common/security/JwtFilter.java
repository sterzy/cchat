package com.cchat.common.security;

import com.cchat.common.security.utils.CookieUtils;
import com.cchat.common.security.utils.JwtUtils;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.*;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

import static com.cchat.common.security.utils.CookieUtils.JWT_ACCESS;
import static com.cchat.common.security.utils.CookieUtils.JWT_REFRESH;

@Log4j2
public class JwtFilter extends OncePerRequestFilter {
    private String keycloakUrl;
    private String keycloakRealm;
    private String keycloakClientId;
    private JwtUtils jwtUtils;
    private CookieUtils cookieUtils;

    @Deprecated
    public JwtFilter(String keycloakUrl, String keycloakRealm, String keycloakClientId, JwtUtils jwtUtils, CookieUtils cookieUtils, String cookieDomain) {
        this(keycloakUrl, keycloakRealm, keycloakClientId, jwtUtils, cookieUtils);
    }

    public JwtFilter(String keycloakUrl, String keycloakRealm, String keycloakClientId, JwtUtils jwtUtils, CookieUtils cookieUtils) {
        this.keycloakUrl = keycloakUrl;
        this.keycloakRealm = keycloakRealm;
        this.keycloakClientId = keycloakClientId;
        this.jwtUtils = jwtUtils;
        this.cookieUtils = cookieUtils;
    }

    private AbstractAuthenticationToken handleAccessToken(HttpServletRequest request, HttpServletResponse response) {
        Cookie cookie = cookieUtils.getCookie(JWT_ACCESS, request);
        if (cookie == null || cookie.getValue() == null || cookie.getValue().isEmpty()) {
            // Kein (gültiges) JWT im Request gefunden
            return null;
        }

        try {
            SecurityUser user = jwtUtils.getUserFromToken(cookie.getValue());
            if (user != null) {
                // Gültiger Access-Token im Request vorhanden
                return new RememberMeAuthenticationToken(user.getId(), user, user.getAuthorities());
            }
        } catch (ExpiredJwtException e) {
            // Aktueller Access-Token abgelaufen -> über den Refresh Token neue Tokens anfordern
            return handleRefreshToken(request, response);
        }

        return null;
    }

    private AbstractAuthenticationToken handleRefreshToken(HttpServletRequest request, HttpServletResponse response) {
        Cookie cookie = cookieUtils.getCookie(JWT_REFRESH, request);
        if (cookie == null || cookie.getValue() == null || cookie.getValue().isEmpty()) {
            // Kein (gültiges) JWT im Request gefunden
            return null;
        }

        RestTemplate rt = new RestTemplate();

        // Headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        // Parameters
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("client_id", keycloakClientId);
        map.add("grant_type", "refresh_token");
        map.add("refresh_token", cookie.getValue());

        try {
            ResponseEntity<HashMap> responseEntity = rt.exchange(keycloakUrl + "/realms/" + keycloakRealm + "/protocol/openid-connect/token", HttpMethod.POST, new HttpEntity<>(map, headers), HashMap.class);
            if (!responseEntity.getStatusCode().isError()) {
                String accessTokenNew = (String) responseEntity.getBody().get("access_token");
                String refreshTokenNew = (String) responseEntity.getBody().get("refresh_token");
                cookieUtils.setCookie(JWT_ACCESS, accessTokenNew, response);
                cookieUtils.setCookie(JWT_REFRESH, refreshTokenNew, response);
                SecurityUser user = jwtUtils.getUserFromToken(accessTokenNew);
                if (user != null) {
                    return new RememberMeAuthenticationToken(user.getId(), user, user.getAuthorities());
                }
            }
        } catch (HttpClientErrorException e) {
            // Refresh Token wurde als invalide/veraltet abgelehnt
        }

        return null;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        try {
            AbstractAuthenticationToken authToken = handleAccessToken(request, response);
            if (authToken != null) {
                SecurityContextHolder.getContext().setAuthentication(authToken);
            }
        } catch (Exception e) {
            log.error(e);
        }

        chain.doFilter(request, response);
    }
}
