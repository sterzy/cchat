package com.cchat.common.security.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.stream.Stream;


public class CookieUtils {
    public static final String JWT_ACCESS = "jwt-access";
    public static final String JWT_REFRESH = "jwt-refresh";
    public static final int MAX_AGE_MINUTES = 1440;

    @Deprecated
    public void setCookie(String domain, String name, String value, HttpServletResponse response) {
        this.setCookie(name, value, response);
    }

    public void setCookie(String name, String value, HttpServletResponse response) {
        Cookie cookie = new Cookie(name, value);
        cookie.setMaxAge(MAX_AGE_MINUTES * 60000);
        cookie.setPath(getPath());
        response.addCookie(cookie);
    }

    public void removeCookie(String name, HttpServletResponse response) {
        Cookie cookie = new Cookie(name, null);
        cookie.setMaxAge(0);
        cookie.setPath(getPath());
        response.addCookie(cookie);
    }

    public Cookie getCookie(String name, HttpServletRequest request) {
        if (request.getCookies() != null) {
            return Stream.of(request.getCookies())
                    .filter(p -> p.getName().equals(name))
                    .findFirst()
                    .orElse(null);
        }

        return null;
    }

    private String getPath() {
        return "/";
    }
}
