package com.cchat.common.security.utils;

import com.cchat.common.security.SecurityUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class JwtUtils {

    private PublicKey rsaPublicKeycloak;
    private byte[] sharedSecret;
    private boolean enableSharedSecret;

    public JwtUtils(String sharedSecret, boolean enableSharedSecret) {
        this.sharedSecret = sharedSecret.getBytes();
        this.enableSharedSecret = enableSharedSecret;
    }

    public JwtUtils(String rsaPublicKeycloak) throws InvalidKeySpecException {
        this(getKey(rsaPublicKeycloak));
        this.enableSharedSecret = false;
    }

    public JwtUtils(PublicKey rsaPublicKeycloak) {
        this.rsaPublicKeycloak = rsaPublicKeycloak;
        this.enableSharedSecret = false;
    }


    /**
     * Extrahiert einen Benutzer aus dem erhaltenen JWT.
     *
     * @param jwt der Token
     * @return {@link SecurityUser}
     */
    public SecurityUser getUserFromToken(String jwt) {
        try {

            Claims claims;

            if (this.enableSharedSecret) {
                claims = Jwts.parser()
                    .setSigningKey(this.sharedSecret)
                    .parseClaimsJws(jwt)
                    .getBody();

            } else {
                claims = Jwts.parser().setSigningKey(this.rsaPublicKeycloak).parseClaimsJws(jwt).getBody();

            }

            return new SecurityUser(claims);

        } catch (SignatureException | MalformedJwtException e) {
            // Irgendetwas stimmt mit dem empfangenen Token nicht (gefälscht, ungültig, syntaktisch inkorrekt...)
            return null;
        }
    }

    private static PublicKey getKey(String key) throws InvalidKeySpecException {
        try {
            byte[] byteKey = Base64.getDecoder().decode(key);
            X509EncodedKeySpec X509publicKey = new X509EncodedKeySpec(byteKey);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            return kf.generatePublic(X509publicKey);
        } catch (NoSuchAlgorithmException e) {
            throw new InvalidKeySpecException("Failed to get RSA algorithm", e);
        }
    }


}
