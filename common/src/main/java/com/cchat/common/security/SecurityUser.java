package com.cchat.common.security;

import io.jsonwebtoken.Claims;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Spring Security representation of a User
 */
@Data
@AllArgsConstructor
public class SecurityUser implements UserDetails {
    private String id;
    private String username;
    private List<SimpleGrantedAuthority> authorities;

    public SecurityUser(Claims claims) {
        this.id = claims.getSubject();
        this.username = (String) claims.get("username");
        List<String> groups = (List<String>) claims.get("groups");

        if (groups != null) {
            this.authorities = groups.stream()
                .map(p -> new SimpleGrantedAuthority(p))
                .collect(Collectors.toList());
        }
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return "";
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
