package com.cchat.common.security;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@Configuration
@Order(2)
@Log4j2
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired(required = false)
    private JwtFilter jwtFilter;

    @Value("${security.enabled:true}")
    private boolean enabled;

    @Value("${security.permittedPaths:''}")
    private String permittedPaths;

    @Value("${local:false}")
    private boolean local;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.headers().frameOptions().sameOrigin();
        http.headers().cacheControl().disable();

        if (enabled) {

            log.info("ReST API endpoints are SECURED");
            ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry securityConfig = http.authorizeRequests();

            securityConfig.antMatchers(permittedPaths.split(",")).permitAll();
            securityConfig.antMatchers("/ws").authenticated();
            securityConfig.antMatchers("/api/**").authenticated();

            if (!local) {
                securityConfig.antMatchers("/swagger-ui.html").denyAll();
            }

            securityConfig.anyRequest().permitAll();
            http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);

        } else {
            log.info("ReST API endpoints are UNSECURED");
            http.authorizeRequests()
                    .anyRequest().permitAll();
        }
    }
}
