package com.cchat.common.security;

import com.cchat.common.security.utils.CookieUtils;
import com.cchat.common.security.utils.JwtUtils;
import lombok.extern.log4j.Log4j2;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.util.*;

import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;


@Configuration
@Log4j2
@ConditionalOnProperty(name = "security.enabled")
public class Config {
    @Value("${security.keycloak.url:}")
    private String keycloakUrl;

    @Value("${security.keycloak.realm:}")
    private String keycloakRealm;

    @Value("${security.keycloak.clientId:}")
    private String keycloakClientId;

    @Value("${server.name:}")
    private String serverName;

    @Value("${security.keycloak.sharedSecret:}")
    private String sharedSecret;

    @Value("${local:false}")
    private boolean local;

    private String keycloakPublicKey;

    @Bean
    public ResteasyClient resteasyClientBuilder() {
        return new ResteasyClientBuilder().connectionPoolSize(10).build();
    }

    @Bean
    public JwtFilter jwtFilter() {

        if (!StringUtils.isEmpty(this.sharedSecret) && this.local) {
            log.warn("using insecure shared secret for authenticating tokens!");
            return new JwtFilter(keycloakUrl, keycloakRealm, keycloakClientId, new JwtUtils(this.sharedSecret, this.local), cookieUtils());
        }

        if (keycloakPublicKey == null) {
            loadKeycloakPublicKey();
        }

        return new JwtFilter(keycloakUrl, keycloakRealm, keycloakClientId, jwtUtils(), cookieUtils());
    }

    @Bean
    public JwtUtils jwtUtils() {


        if (!StringUtils.isEmpty(this.sharedSecret) && this.local) {
            log.warn("using insecure shared secret for authenticating tokens!");
            return new JwtUtils(this.sharedSecret, this.local);
        }

        if (keycloakPublicKey == null) {
            loadKeycloakPublicKey();
        }

        try {
            return new JwtUtils(keycloakPublicKey);
        } catch (InvalidKeySpecException e) {
            throw new IllegalStateException("Failed to parse public key of keycloak: ", e);
        }
    }

    @Bean
    public CookieUtils cookieUtils() {
        return new CookieUtils();
    }

    private void loadKeycloakPublicKey() {
        boolean publicKeyLoaded = false;

        do {
            RestTemplate rt = new RestTemplate();
            try {
                keycloakPublicKey = (String) rt.getForObject(keycloakUrl + "/realms/" + keycloakRealm, HashMap.class).get("public_key");
                publicKeyLoaded = true;
            } catch (RestClientException e) {
                log.error("Keycloak is currently not reachable, retry in 5 seconds.");
                try {
                    TimeUnit.SECONDS.sleep(5);
                } catch (InterruptedException e1) {
                    throw e;
                }
            }
        } while (!publicKeyLoaded);
    }
}
