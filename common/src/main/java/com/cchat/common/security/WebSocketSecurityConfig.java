package com.cchat.common.security;

import lombok.extern.log4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.annotation.*;
import org.springframework.core.annotation.*;
import org.springframework.security.config.annotation.web.messaging.*;
import org.springframework.security.config.annotation.web.socket.*;
import org.springframework.boot.autoconfigure.condition.*;

@Configuration
@Log4j2
@ConditionalOnProperty(name = "security.ws.enabled")
public class WebSocketSecurityConfig extends AbstractSecurityWebSocketMessageBrokerConfigurer {

    @Value("${security.enabled:true}")
    private boolean enabled;

    @Value("${security.ws.destinations:}")
    private String destinations;

    @Value("${security.ws.subscriptions:}")
    private String subscriptions;

    @Override
    protected void configureInbound(MessageSecurityMetadataSourceRegistry messages) {

        if (enabled) {
            log.info("enabling security for web sockets");

            messages.nullDestMatcher().authenticated() // Require authentication for all messages without a destination
                .simpDestMatchers(this.destinations.split(",")).authenticated() // Only allow users to send messages
                .simpSubscribeDestMatchers(this.subscriptions.split(",")).authenticated() // Only allow users to receive errors and messages
                .anyMessage().denyAll(); // Reject all other messages
        }

    }

    @Override
    protected boolean sameOriginDisabled() {
        return true;
    }
}
