#!/bin/bash

openssl req -subj '/CN=localhost' -x509 -nodes -days 365 -newkey rsa:4096 -keyout ssl/ssl_certificate_key.key -out ssl/ssl_certificate.crt
