#!/usr/bin/env sh

# If the necessary secrets were mounted, overwrite certificates in container
if [ -f /run/secrets/g-ase-ng-cert ] && [ -f /run/secrets/g-ase-ng-key ]; then
    cat /run/secrets/g-ase-ng-cert > /etc/ssl/ssl_certificate.crt
    cat /run/secrets/g-ase-ng-key > /etc/ssl/ssl_certificate_key.key
fi

nginx -g "daemon off;"
