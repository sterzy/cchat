#!/usr/bin/env bash

# carefull the following line will remove *all* secrets!
# docker secret rm  docker secret ls -q`

# remove all secrets that will be re-created
docker secret rm g-ase-pg-user
docker secret rm g-ase-pg-pass
docker secret rm g-ase-mg-user
docker secret rm g-ase-mg-pass
docker secret rm g-ase-rq-user
docker secret rm g-ase-rq-pass
docker secret rm g-ase-kc-user
docker secret rm g-ase-kc-pass
docker secret rm g-ase-ng-cert
docker secret rm g-ase-ng-key

# create a self sign certificate and set the key and certificate as a secret
# in production this should be replaced with a valid certificate
openssl req -subj '/CN=localhost' -x509 -nodes -days 365 -newkey rsa:4096 -keyout cert-key.key -out cert.crt

cat cert-key.key | docker secret create g-ase-ng-key -
cat cert.crt | docker secret create g-ase-ng-cert -

# Postgres
printf "postgres-user" | docker secret create g-ase-pg-user -
cat /dev/urandom | tr -dc '0-9a-zA-Z' | head -c16 | docker secret create g-ase-pg-pass -

# MongoDB
printf "mongo-user" | docker secret create g-ase-mg-user -
cat /dev/urandom | tr -dc '0-9a-zA-Z' | head -c16 | docker secret create g-ase-mg-pass -

# RabbitMQ
printf "rabbit-user" | docker secret create g-ase-rq-user -
printf "test-pass" | docker secret create g-ase-rq-pass -

# Keycloak
printf "admin" | docker secret create g-ase-kc-user -
printf "admin" | docker secret create g-ase-kc-pass -

