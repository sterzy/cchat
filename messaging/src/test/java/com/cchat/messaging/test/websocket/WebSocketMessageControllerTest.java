package com.cchat.messaging.test.websocket;

import com.cchat.common.dto.amq.BotCryptoMessageDTO;
import com.cchat.common.util.Constants;
import com.cchat.messaging.controller.WebSocketEventListener;
import com.cchat.common.dto.websocket.*;
import com.cchat.messaging.domain.Message;
import com.cchat.messaging.repository.MessageRepository;
import com.cchat.messaging.service.CacheService;
import com.cchat.messaging.service.UserService;
import com.cchat.messaging.test.util.*;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

import org.junit.*;
import org.junit.runner.*;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.test.context.*;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.messaging.converter.*;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.stomp.*;
import org.springframework.test.context.junit4.*;
import org.springframework.web.socket.*;
import org.springframework.web.socket.client.standard.*;
import org.springframework.web.socket.messaging.*;

import static java.util.concurrent.TimeUnit.*;
import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration(exclude={MongoAutoConfiguration.class, MongoDataAutoConfiguration.class})
public class WebSocketMessageControllerTest {

    @Value("${local.server.port}")
    private int port;

    @Value("${security.keycloak.sharedSecret:''}")
    private String sharedSecret;

    @MockBean
    private MessageRepository messageRepository;

    @SpyBean
    private UserService userService;

    @SpyBean
    private CacheService cacheService;

    @SpyBean
    private WebSocketEventListener listener;

    @SpyBean
    private SimpMessagingTemplate messagingTemplate;

    private StompSession session;
    private String uId  = "test-user";
    private String uId2 = "test-user2";
    private String bot  = "bot";

    @Before
    public void setup() throws ExecutionException, InterruptedException, TimeoutException {
        this.session = this.spawnSession(this.uId);
    }

    @After
    public void teardown() {
        this.session.disconnect();
    }

    @Test
    public void subscribeWithoutCachedMessage_sendsNothing() throws InterruptedException {

        SynchronousQueue<BaseMessageDTO> response = new SynchronousQueue<>();
        CompletableFuture<String> error             = new CompletableFuture<>();

        this.session.subscribe(Constants.USER_PREFIX+Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response, error, BaseMessageDTO.class));
        this.session.subscribe(Constants.USER_PREFIX+Constants.ERROR_EP, new ChatStompMessageFrameHandler<>(response, error, BaseMessageDTO.class));

        verify(cacheService, timeout(500).atLeastOnce())
            .hasCachedMessages(this.uId);

        BaseMessageDTO messageDTO = response.poll(5, SECONDS);
        assertEquals("Subscribe without cached message returned something!", null, messageDTO);

    }

    @Test
    public void subscribeAndCachedMessages_sendsCachedMessages() throws InterruptedException {


        SynchronousQueue<CryptoMessageDTO> response = new SynchronousQueue<>();
        CompletableFuture<String> error           = new CompletableFuture<>();

        this.session.subscribe(Constants.USER_PREFIX+Constants.ERROR_EP, new ChatStompMessageFrameHandler<>(response, error, CryptoMessageDTO.class));

        List<Message> messages = new LinkedList<>();
        messages.add(new Message("id", MessagingType.MSG_CRYPTO, 5L, this.uId, this.uId2, 3L, "test-load"));

        given(this.messageRepository.existsMessageByTo(this.uId))
            .willReturn(true);

        given(this.messageRepository.findAllByToOrderByTimestampAsc(this.uId))
            .willReturn(messages);

        this.session.subscribe(Constants.USER_PREFIX+Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response, error, CryptoMessageDTO.class));

        verify(this.cacheService, timeout(500).atLeastOnce())
            .hasCachedMessages(this.uId);

        verify(this.cacheService, timeout(500).atLeastOnce())
            .sendCachedMessagesToUser(this.uId);

        CryptoMessageDTO responseMsg2 = response.poll(5, SECONDS);

        assertEquals("Registering Failed: ", MessagingType.MSG_CRYPTO, responseMsg2.getType());
        assertEquals("Registering Failed: ", "test-load", responseMsg2.getPayload());
        assertEquals("Registering Failed: ", 3L, (long)  responseMsg2.getCryptoType());
        assertEquals("Registering Failed: ", this.uId2, responseMsg2.getFrom());
        assertEquals("Registering Failed: ", "id", responseMsg2.getId());
        assertEquals("Registering Failed: ", 5L, (long) responseMsg2.getTimestamp());

    }

    @Test
    public void sendWithSubscribedAndValidMessage_succeeds() throws ExecutionException, InterruptedException, TimeoutException {

        SynchronousQueue<SystemMessageDTO> response = new SynchronousQueue<>();
        CompletableFuture<String> error             = new CompletableFuture<>();

        this.session.subscribe(Constants.USER_PREFIX+Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));

        verify(cacheService, timeout(1000).atLeastOnce())
            .hasCachedMessages(this.uId);

        StompSession session2 = this.spawnSession(this.uId2);

        SynchronousQueue<CryptoMessageDTO> response2 = new SynchronousQueue<>();
        CompletableFuture<String>  error2            = new CompletableFuture<>();

        session2.subscribe(Constants.USER_PREFIX + Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response2, error2, CryptoMessageDTO.class));

        verify(cacheService, timeout(1000).atLeastOnce())
            .hasCachedMessages(this.uId2);

        Long time = System.currentTimeMillis();

        CryptoMessageDTO request = new CryptoMessageDTO();
        request.setPayload("my secret message");
        request.setTimestamp(time);
        request.setFrom(this.uId);
        request.setCryptoType(3L);
        request.setId("500");

        given(this.userService.exists(this.uId2))
            .willReturn(true);

        given(this.userService.isConnected(this.uId2))
            .willReturn(true);

        this.session.send(Constants.APP_PREFIX + Constants.SEND_ROUTE + "/" + this.uId2, request);

        SystemMessageDTO receipt  = response.poll(5, SECONDS);
        CryptoMessageDTO message = response2.poll(5, SECONDS);

        assertEquals("Received message had a mismatching payload failed: ", "my secret message",  message.getPayload());
        assertEquals("Received message did not have to correct sender: ", this.uId,  message.getFrom());
        assertEquals("Received message did not have the correct type: ", MessagingType.MSG_CRYPTO,  message.getType());
        assertEquals("Received message did not have the correct timestamp: ", time, message.getTimestamp());
        assertEquals("Received message did not have the correct id: ", "", message.getId());

        assertEquals("Sending Receipt did not have the correct payload: ", "SENT",  receipt.getPayload());
        assertEquals("Sending Receipt did not have the correct type: ", MessagingType.MSG_SYSTEM,  receipt.getType());
        assertEquals("Sending Receipt did not have the correct id: ", "500",  receipt.getMessageId());

        session2.disconnect();

    }

    @Test
    public void sendToNonExistentUser_failsWithMessage() throws ExecutionException, InterruptedException, TimeoutException {

        SynchronousQueue<BaseMessageDTO> response = new SynchronousQueue<>();
        CompletableFuture<String> error           = new CompletableFuture<>();

        this.session.subscribe(Constants.USER_PREFIX+Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response, error, BaseMessageDTO.class));
        this.session.subscribe(Constants.USER_PREFIX+Constants.ERROR_EP, new ChatStompMessageFrameHandler<>(response, error, BaseMessageDTO.class));

        verify(cacheService, timeout(1000).atLeastOnce())
            .hasCachedMessages(this.uId);

        Long time = System.currentTimeMillis();

        CryptoMessageDTO request = new CryptoMessageDTO();
        request.setPayload("my secret message");
        request.setTimestamp(time);
        request.setFrom(this.uId);
        request.setCryptoType(3L);
        request.setId("500");

        given(this.userService.exists(this.uId2))
            .willReturn(false);

        this.session.send(Constants.APP_PREFIX + Constants.SEND_ROUTE + "/" + this.uId2, request);

        String errorMsg = error.get(5, SECONDS);

        assertEquals("Wrong error message: ", "user does not exist",  errorMsg);

    }

    @Test
    public void sendFromNonSubscribedUser_failsWithMessage() throws ExecutionException, InterruptedException, TimeoutException {

        SynchronousQueue<BaseMessageDTO> response = new SynchronousQueue<>();
        CompletableFuture<String> error           = new CompletableFuture<>();

        this.session.subscribe(Constants.USER_PREFIX+Constants.ERROR_EP, new ChatStompMessageFrameHandler<>(response, error, BaseMessageDTO.class));

        Long time = System.currentTimeMillis();

        CryptoMessageDTO request = new CryptoMessageDTO();
        request.setPayload("my secret message");
        request.setTimestamp(time);
        request.setFrom(this.uId);
        request.setCryptoType(3L);
        request.setId("500");

        this.session.send(Constants.APP_PREFIX + Constants.SEND_ROUTE + "/" + this.uId2, request);

        String errorMsg = error.get(5, SECONDS);

        assertEquals("Wrong error message: ", "please subscribe to "+ Constants.CHAT_EP +" first",  errorMsg);

    }

    @Test
    public void sendToDisconnectedUser_cachesMessage() throws ExecutionException, InterruptedException, TimeoutException {

        SynchronousQueue<SystemMessageDTO> response = new SynchronousQueue<>();
        CompletableFuture<String> error             = new CompletableFuture<>();

        this.session.subscribe(Constants.USER_PREFIX + Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));
        this.session.subscribe(Constants.USER_PREFIX + Constants.ERROR_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));

        verify(cacheService, timeout(1000).atLeastOnce())
            .hasCachedMessages(this.uId);

        StompSession session2 = this.spawnSession(this.uId2);

        SynchronousQueue<CryptoMessageDTO> response2 = new SynchronousQueue<>();
        CompletableFuture<String> error2             = new CompletableFuture<>();

        session2.subscribe(Constants.USER_PREFIX + Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response2, error2, CryptoMessageDTO.class));

        verify(cacheService, timeout(1000).atLeastOnce())
            .hasCachedMessages(this.uId2);

        Long time = System.currentTimeMillis();

        CryptoMessageDTO request = new CryptoMessageDTO();
        request.setPayload("my secret message");
        request.setTimestamp(time);
        request.setFrom(this.uId);
        request.setCryptoType(3L);
        request.setId("500");

        given(this.userService.exists(this.uId2))
            .willReturn(true);

        this.session.send(Constants.APP_PREFIX + Constants.SEND_ROUTE + "/" + this.uId2, request);

        SystemMessageDTO receipt = response.poll(5, SECONDS);
        CryptoMessageDTO message = response2.poll(5, SECONDS);

        assertEquals("Received message had a mismatching payload failed: ", "my secret message",  message.getPayload());
        assertEquals("Received message did not have to correct sender: ", this.uId,  message.getFrom());
        assertEquals("Received message did not have the correct type: ", MessagingType.MSG_CRYPTO,  message.getType());
        assertEquals("Received message did not have the correct timestamp: ", time, message.getTimestamp());
        assertEquals("Received message did not have the correct id: ", "", message.getId());

        assertEquals("Sending Receipt malformed: ", "SENT",  receipt.getPayload());
        assertEquals("Sending Receipt malformed: ", MessagingType.MSG_SYSTEM,  receipt.getType());

        session2.disconnect();

        verify(this.listener, timeout(1000).atLeastOnce())
            .handleWebSocketDisconnectListener(ArgumentMatchers.any(SessionDisconnectEvent.class));

        given(this.userService.exists(this.uId2))
            .willReturn(true);

        this.session.send(Constants.APP_PREFIX + Constants.SEND_ROUTE + "/" + this.uId2, request);

        Message message1 = new Message("", MessagingType.MSG_CRYPTO, time, this.uId2, this.uId, 3L, "my secret message");

        verify(this.messageRepository, timeout(1000).atLeastOnce())
            .save(message1);

        receipt  = response.poll(5, SECONDS);

        assertEquals("Wrong payload in message: ", "SENT", receipt.getPayload());

    }

    @Test
    public void sendWithEmptyMessage_failsWithMessage() throws ExecutionException, InterruptedException, TimeoutException {

        SynchronousQueue<SystemMessageDTO> response = new SynchronousQueue<>();
        CompletableFuture<String> error             = new CompletableFuture<>();

        this.session.subscribe(Constants.USER_PREFIX + Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));
        this.session.subscribe(Constants.USER_PREFIX + Constants.ERROR_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));

        verify(cacheService, timeout(1000).atLeastOnce())
            .hasCachedMessages(this.uId);

        StompSession session2 = this.spawnSession(this.uId2);

        SynchronousQueue<CryptoMessageDTO> response2 = new SynchronousQueue<>();
        CompletableFuture<String> error2             = new CompletableFuture<>();

        session2.subscribe(Constants.USER_PREFIX + Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response2, error2, CryptoMessageDTO.class));

        verify(cacheService, timeout(1000).atLeastOnce())
            .hasCachedMessages(this.uId2);

        Long time = System.currentTimeMillis();

        CryptoMessageDTO request = new CryptoMessageDTO();
        request.setPayload("");
        request.setTimestamp(time);
        request.setFrom(this.uId);
        request.setCryptoType(3L);
        request.setId("500");

        given(this.userService.exists(this.uId2))
            .willReturn(true);

        this.session.send(Constants.APP_PREFIX + Constants.SEND_ROUTE + "/" + this.uId2, request);

        String errorMsg = error.get(5, SECONDS);

        assertEquals("Wrong error message:", "message text is malformed", errorMsg);

        session2.disconnect();

    }

    @Test
    public void sendWithoutTimestamp_failsWithMessage() throws ExecutionException, InterruptedException, TimeoutException {

        SynchronousQueue<SystemMessageDTO> response = new SynchronousQueue<>();
        CompletableFuture<String> error             = new CompletableFuture<>();

        this.session.subscribe(Constants.USER_PREFIX + Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));
        this.session.subscribe(Constants.USER_PREFIX + Constants.ERROR_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));

        verify(cacheService, timeout(1000).atLeastOnce())
            .hasCachedMessages(this.uId);

        StompSession session2 = this.spawnSession(this.uId2);

        SynchronousQueue<CryptoMessageDTO> response2 = new SynchronousQueue<>();
        CompletableFuture<String> error2             = new CompletableFuture<>();

        session2.subscribe(Constants.USER_PREFIX + Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response2, error2, CryptoMessageDTO.class));

        verify(cacheService, timeout(1000).atLeastOnce())
            .hasCachedMessages(this.uId2);

        Long time = System.currentTimeMillis();

        CryptoMessageDTO request = new CryptoMessageDTO();
        request.setPayload("hi");
        request.setFrom(this.uId);
        request.setCryptoType(3L);
        request.setId("500");

        given(this.userService.exists(this.uId2))
            .willReturn(true);

        this.session.send(Constants.APP_PREFIX + Constants.SEND_ROUTE + "/" + this.uId2, request);

        String errorMsg = error.get(5, SECONDS);

        assertEquals("Wrong error message:", "please provide a timestamp", errorMsg);

        session2.disconnect();

    }

    @Test
    public void sendWithInvalidCryptoType_failsWithMessage() throws ExecutionException, InterruptedException, TimeoutException {

        SynchronousQueue<SystemMessageDTO> response = new SynchronousQueue<>();
        CompletableFuture<String> error             = new CompletableFuture<>();

        this.session.subscribe(Constants.USER_PREFIX + Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));
        this.session.subscribe(Constants.USER_PREFIX + Constants.ERROR_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));

        verify(cacheService, timeout(1000).atLeastOnce())
            .hasCachedMessages(this.uId);

        StompSession session2 = this.spawnSession(this.uId2);

        SynchronousQueue<CryptoMessageDTO> response2 = new SynchronousQueue<>();
        CompletableFuture<String> error2             = new CompletableFuture<>();

        session2.subscribe(Constants.USER_PREFIX + Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response2, error2, CryptoMessageDTO.class));

        verify(cacheService, timeout(1000).atLeastOnce())
            .hasCachedMessages(this.uId2);

        Long time = System.currentTimeMillis();

        CryptoMessageDTO request = new CryptoMessageDTO();
        request.setPayload("hi");
        request.setFrom(this.uId);
        request.setCryptoType(0L);
        request.setTimestamp(System.currentTimeMillis());
        request.setId("500");

        given(this.userService.exists(this.uId2))
            .willReturn(true);

        this.session.send(Constants.APP_PREFIX + Constants.SEND_ROUTE + "/" + this.uId2, request);

        String errorMsg = error.get(5, SECONDS);

        assertEquals("Wrong error message:", "a valid crypto type should be between 1 and 3", errorMsg);

        request.setCryptoType(4L);

        given(this.userService.exists(this.uId2))
            .willReturn(true);

        this.session.send(Constants.APP_PREFIX + Constants.SEND_ROUTE + "/" + this.uId2, request);

        errorMsg = error.get(5, SECONDS);

        assertEquals("Wrong error message:", "a valid crypto type should be between 1 and 3", errorMsg);

        session2.disconnect();

    }

    @Test
    public void sendWithNullMessage_failsWithMessage() throws ExecutionException, InterruptedException, TimeoutException {

        SynchronousQueue<SystemMessageDTO> response = new SynchronousQueue<>();
        CompletableFuture<String> error             = new CompletableFuture<>();

        this.session.subscribe(Constants.USER_PREFIX + Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));
        this.session.subscribe(Constants.USER_PREFIX + Constants.ERROR_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));

        verify(cacheService, timeout(1000).atLeastOnce())
            .hasCachedMessages(this.uId);

        StompSession session2 = this.spawnSession(this.uId2);

        SynchronousQueue<CryptoMessageDTO> response2 = new SynchronousQueue<>();
        CompletableFuture<String> error2             = new CompletableFuture<>();

        session2.subscribe(Constants.USER_PREFIX + Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response2, error2, CryptoMessageDTO.class));

        verify(cacheService, timeout(1000).atLeastOnce())
            .hasCachedMessages(this.uId2);

        Long time = System.currentTimeMillis();

        CryptoMessageDTO request = new CryptoMessageDTO();
        request.setPayload(null);
        request.setTimestamp(time);
        request.setFrom(this.uId);
        request.setCryptoType(3L);
        request.setId("500");

        given(this.userService.exists(this.uId2))
            .willReturn(true);

        this.session.send(Constants.APP_PREFIX + Constants.SEND_ROUTE + "/" + this.uId2, request);

        String errorMsg = error.get(5, SECONDS);

        assertEquals("Wrong error message:", "message text is malformed", errorMsg);

        session2.disconnect();

    }

    @Test
    public void sendBotWithValidMessage_succeeds() throws ExecutionException, InterruptedException, TimeoutException {

        SynchronousQueue<SystemMessageDTO> response = new SynchronousQueue<>();
        CompletableFuture<String> error             = new CompletableFuture<>();

        this.session.subscribe(Constants.USER_PREFIX+Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));

        verify(cacheService, timeout(1000).atLeastOnce())
            .hasCachedMessages(this.uId);

        Long time = System.currentTimeMillis();

        CryptoMessageDTO request = new CryptoMessageDTO();
        request.setPayload("my secret message");
        request.setTimestamp(time);
        request.setFrom(this.uId);
        request.setCryptoType(3L);
        request.setId("500");

        this.session.send(Constants.APP_PREFIX + Constants.BOT_SEND_ROUTE + "/" + this.bot, request);

        SystemMessageDTO receipt  = response.poll(5, SECONDS);

        request.setId("");
        BotCryptoMessageDTO dto = new BotCryptoMessageDTO();
        dto.setBot(this.bot);
        dto.setMessage(request);

        verify(this.messagingTemplate, timeout(500).atLeastOnce())
            .convertAndSend(Constants.AMQ_QUEUES + Constants.BOT_QUEUE, dto);

        assertEquals("Sending Receipt did not have the correct payload: ", "SENT",  receipt.getPayload());
        assertEquals("Sending Receipt did not have the correct type: ", MessagingType.MSG_SYSTEM,  receipt.getType());
        assertEquals("Sending Receipt did not have the correct id: ", "500",  receipt.getMessageId());

    }

    @Test
    public void sendBotWithNonSubscribedUser_failsWithMessage() throws ExecutionException, InterruptedException, TimeoutException {

        SynchronousQueue<SystemMessageDTO> response = new SynchronousQueue<>();
        CompletableFuture<String> error             = new CompletableFuture<>();

        this.session.subscribe(Constants.USER_PREFIX+Constants.ERROR_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));

        Long time = System.currentTimeMillis();

        CryptoMessageDTO request = new CryptoMessageDTO();
        request.setPayload("my secret message");
        request.setTimestamp(time);
        request.setFrom(this.uId);
        request.setCryptoType(3L);
        request.setId("500");

        this.session.send(Constants.APP_PREFIX + Constants.BOT_SEND_ROUTE + "/" + this.bot, request);

        String errorMsg = error.get(5, SECONDS);

        assertEquals("Wrong error message:", "please subscribe to "+ Constants.CHAT_EP +" first", errorMsg);

    }

    @Test
    public void sendBotWithNoTimestamp_failsWithMessage() throws ExecutionException, InterruptedException, TimeoutException {

        SynchronousQueue<SystemMessageDTO> response = new SynchronousQueue<>();
        CompletableFuture<String> error             = new CompletableFuture<>();

        this.session.subscribe(Constants.USER_PREFIX+Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));
        this.session.subscribe(Constants.USER_PREFIX+Constants.ERROR_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));

        verify(cacheService, timeout(1000).atLeastOnce())
            .hasCachedMessages(this.uId);

        CryptoMessageDTO request = new CryptoMessageDTO();
        request.setPayload("my secret message");
        request.setFrom(this.uId);
        request.setCryptoType(3L);
        request.setId("500");

        this.session.send(Constants.APP_PREFIX + Constants.BOT_SEND_ROUTE + "/" + this.bot, request);

        String errorMsg = error.get(5, SECONDS);

        assertEquals("Wrong error message:", "please provide a timestamp", errorMsg);

    }

    @Test
    public void sendBotWithInvalidCryptoType_failsWithMessage() throws ExecutionException, InterruptedException, TimeoutException {

        SynchronousQueue<SystemMessageDTO> response = new SynchronousQueue<>();
        CompletableFuture<String> error             = new CompletableFuture<>();

        this.session.subscribe(Constants.USER_PREFIX+Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));
        this.session.subscribe(Constants.USER_PREFIX+Constants.ERROR_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));

        CryptoMessageDTO request = new CryptoMessageDTO();
        request.setPayload("my secret message");
        request.setTimestamp(System.currentTimeMillis());
        request.setFrom(this.uId);
        request.setCryptoType(5L);
        request.setId("500");

        this.session.send(Constants.APP_PREFIX + Constants.BOT_SEND_ROUTE + "/" + this.bot, request);

        String errorMsg = error.get(5, SECONDS);

        assertEquals("Wrong error message:", "a valid crypto type should be between 1 and 3", errorMsg);

        request.setCryptoType(0L);

        this.session.send(Constants.APP_PREFIX + Constants.BOT_SEND_ROUTE + "/" + this.bot, request);

        errorMsg = error.get(5, SECONDS);

        assertEquals("Wrong error message:", "a valid crypto type should be between 1 and 3", errorMsg);

    }

    @Test
    public void sendBotWithNullPayload_failsWithMessage() throws ExecutionException, InterruptedException, TimeoutException {

        SynchronousQueue<SystemMessageDTO> response = new SynchronousQueue<>();
        CompletableFuture<String> error             = new CompletableFuture<>();

        this.session.subscribe(Constants.USER_PREFIX+Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));
        this.session.subscribe(Constants.USER_PREFIX+Constants.ERROR_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));

        verify(cacheService, timeout(1000).atLeastOnce())
            .hasCachedMessages(this.uId);

        CryptoMessageDTO request = new CryptoMessageDTO();
        request.setPayload(null);
        request.setTimestamp(System.currentTimeMillis());
        request.setFrom(this.uId);
        request.setCryptoType(1L);
        request.setId("500");

        this.session.send(Constants.APP_PREFIX + Constants.BOT_SEND_ROUTE + "/" + this.bot, request);

        String errorMsg = error.get(5, SECONDS);

        assertEquals("Wrong error message:", "message text is malformed", errorMsg);

    }

    @Test
    public void sendBotWithEmptyPayload_failsWithMessage() throws ExecutionException, InterruptedException, TimeoutException {

        SynchronousQueue<SystemMessageDTO> response = new SynchronousQueue<>();
        CompletableFuture<String> error             = new CompletableFuture<>();

        this.session.subscribe(Constants.USER_PREFIX+Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));
        this.session.subscribe(Constants.USER_PREFIX+Constants.ERROR_EP, new ChatStompMessageFrameHandler<>(response, error, SystemMessageDTO.class));

        verify(cacheService, timeout(1000).atLeastOnce())
            .hasCachedMessages(this.uId);

        CryptoMessageDTO request = new CryptoMessageDTO();
        request.setPayload("");
        request.setTimestamp(System.currentTimeMillis());
        request.setFrom(this.uId);
        request.setCryptoType(1L);
        request.setId("500");

        this.session.send(Constants.APP_PREFIX + Constants.BOT_SEND_ROUTE + "/" + this.bot, request);

        String errorMsg = error.get(5, SECONDS);

        assertEquals("Wrong error message:", "message text is malformed", errorMsg);

    }

    private StompSession spawnSession(String userId) throws ExecutionException, InterruptedException, TimeoutException {

        TokenGenerator generator = new TokenGenerator(this.sharedSecret);
        String url = "ws://localhost:" + port + Constants.WEB_SOCKET_EP;

        WebSocketHttpHeaders headers = new WebSocketHttpHeaders();
        WebSocketStompClient stompClient = new WebSocketStompClient(new StandardWebSocketClient());

        // Set auth cookie
        headers.add(WebSocketHttpHeaders.COOKIE, Constants.JWT_COOKIE+"="+generator.generate(userId)+";");
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        return stompClient.connect(url, headers, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);

    }

}
