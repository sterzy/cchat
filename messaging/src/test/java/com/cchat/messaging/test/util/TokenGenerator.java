package com.cchat.messaging.test.util;

import io.jsonwebtoken.security.*;
import io.jsonwebtoken.*;
import java.util.*;

public class TokenGenerator {

    private String secretKey;
    private Long expiry;

    public TokenGenerator(String secretKey) {
        this.secretKey = secretKey;
        this.expiry = 1800000L;
    }

    public void setExpiry(Long expiry) {
        this.expiry = expiry;
    }

    public String generate(String subject) {

        return Jwts.builder()
            .signWith(Keys.hmacShaKeyFor(secretKey.getBytes()), SignatureAlgorithm.HS256)
            .setHeaderParam("typ", "JWT")
            .setSubject(subject)
            .setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis() + this.expiry)) // Token is valid for 30 mins
            .claim("username", subject)
            .compact();

    }
}
