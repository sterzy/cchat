package com.cchat.messaging.test.security;

import com.cchat.common.util.Constants;
import com.cchat.messaging.repository.MessageRepository;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.messaging.simp.stomp.*;
import com.cchat.messaging.test.util.*;
import java.util.concurrent.*;
import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.test.context.*;
import org.springframework.messaging.converter.*;
import org.springframework.test.context.junit4.*;
import org.springframework.web.socket.*;
import org.springframework.web.socket.client.standard.*;
import org.springframework.web.socket.messaging.*;

import static java.util.concurrent.TimeUnit.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration(exclude={MongoAutoConfiguration.class, MongoDataAutoConfiguration.class})
public class JWTAuthenticationTest {

    @Value("${local.server.port}")
    private int port;

    @Value("${security.keycloak.sharedSecret:''}")
    private String sharedSecret;

    @MockBean
    private MessageRepository messageRepository;

    private TokenGenerator generator;
    private String url;
    private WebSocketStompClient stompClient;

    @Before
    public void setup() {

        this.url      = "ws://localhost:" + port + Constants.WEB_SOCKET_EP;
        this.stompClient = new WebSocketStompClient(new StandardWebSocketClient());
        this.stompClient.setMessageConverter(new MappingJackson2MessageConverter());

    }

    @Test
    public void connectWithValidToken_succeeds() throws ExecutionException, InterruptedException, TimeoutException {

        WebSocketHttpHeaders headers = new WebSocketHttpHeaders();

        this.generator = new TokenGenerator(this.sharedSecret);

        headers.add(WebSocketHttpHeaders.COOKIE, Constants.JWT_COOKIE+"="+this.generator.generate("test-id")+";");

        StompSession session = stompClient.connect(this.url, headers, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);

        session.disconnect();

    }

    @Test(expected = ExecutionException.class)
    public void connectWithoutHeader_fails() throws ExecutionException, InterruptedException, TimeoutException {

        StompSession session = stompClient.connect(this.url, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);

        session.disconnect();

    }

    @Test(expected = ExecutionException.class)
    public void connectWithEmptyToken_fails() throws ExecutionException, InterruptedException, TimeoutException {

        WebSocketHttpHeaders headers = new WebSocketHttpHeaders();

        headers.add(WebSocketHttpHeaders.COOKIE, Constants.JWT_COOKIE+"=;");

        StompSession session = stompClient.connect(this.url, headers, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);

        session.disconnect();

    }

    @Test(expected = ExecutionException.class)
    public void connectWithInvalidToken_fails() throws ExecutionException, InterruptedException, TimeoutException {

        WebSocketHttpHeaders headers = new WebSocketHttpHeaders();

        headers.add(WebSocketHttpHeaders.COOKIE, Constants.JWT_COOKIE+"=blub;");

        StompSession session = stompClient.connect(this.url, headers, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);

        session.disconnect();

    }

    @Test(expected = ExecutionException.class)
    public void connectWithInvalidSignature_fails() throws ExecutionException, InterruptedException, TimeoutException {

        WebSocketHttpHeaders headers = new WebSocketHttpHeaders();

        this.generator = new TokenGenerator(this.sharedSecret.toUpperCase());

        headers.add(WebSocketHttpHeaders.COOKIE, Constants.JWT_COOKIE+"="+this.generator.generate("test-id")+";");

        StompSession session = stompClient.connect(this.url, headers, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);

        session.disconnect();

    }

    @Test(expected = ExecutionException.class)
    public void connectWithoutUsername_fails() throws ExecutionException, InterruptedException, TimeoutException {

        WebSocketHttpHeaders headers = new WebSocketHttpHeaders();

        this.generator = new TokenGenerator(this.sharedSecret);

        headers.add(WebSocketHttpHeaders.COOKIE, Constants.JWT_COOKIE+"="+this.generator.generate(null)+";");

        StompSession session = stompClient.connect(this.url, headers, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);

        session.disconnect();

    }

    @Test(expected = ExecutionException.class)
    public void connectWithExpiredToken_fails() throws ExecutionException, InterruptedException, TimeoutException {

        WebSocketHttpHeaders headers = new WebSocketHttpHeaders();

        this.generator = new TokenGenerator(this.sharedSecret);
        this.generator.setExpiry(-1L);

        headers.add(WebSocketHttpHeaders.COOKIE, Constants.JWT_COOKIE+"="+this.generator.generate(null)+";");

        StompSession session = stompClient.connect(this.url, headers, new StompSessionHandlerAdapter() {
        }).get();

        session.disconnect();

    }

}
