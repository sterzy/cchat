package com.cchat.messaging.test.websocket;

import ch.qos.logback.classic.Level;
import ch.qos.logback.core.*;
import com.cchat.common.util.Constants;
import com.cchat.common.dto.websocket.SystemMessageDTO;
import com.cchat.messaging.repository.MessageRepository;
import com.cchat.messaging.test.util.*;
import java.util.concurrent.*;
import org.junit.*;
import org.junit.runner.*;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.test.context.*;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.messaging.converter.*;
import org.springframework.messaging.simp.stomp.*;
import org.springframework.test.context.junit4.*;
import org.springframework.web.socket.*;
import org.springframework.web.socket.client.standard.*;
import org.springframework.web.socket.messaging.*;

import static java.util.concurrent.TimeUnit.*;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration(exclude={MongoAutoConfiguration.class, MongoDataAutoConfiguration.class})
public class WebSocketEventListenerTest {

    @MockBean
    private MessageRepository messageRepository;

    @Value("${local.server.port}")
    private int port;

    @Value("${security.keycloak.sharedSecret:''}")
    private String sharedSecret;

    private String url;
    private WebSocketStompClient stompClient;
    private WebSocketHttpHeaders headers;
    private final Appender mockAppender = mock(Appender.class);

    @Before
    public void setup() {

        TokenGenerator generator = new TokenGenerator(this.sharedSecret);

        this.headers = new WebSocketHttpHeaders();
        // Set auth cookie
        this.headers.add(WebSocketHttpHeaders.COOKIE, Constants.JWT_COOKIE+"="+generator.generate("test-user")+";");

        this.url      = "ws://localhost:" + port + Constants.WEB_SOCKET_EP;
        this.stompClient = new WebSocketStompClient(new StandardWebSocketClient());
        this.stompClient.setMessageConverter(new MappingJackson2MessageConverter());

        when(this.mockAppender.getName()).thenReturn("MOCK");

        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
        root.addAppender(this.mockAppender);

    }

    @After
    public void teardown() {
        ch.qos.logback.classic.Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);
        root.detachAppender(this.mockAppender);
    }

    @Test
    public void connectEvent_logs() throws ExecutionException, InterruptedException, TimeoutException {

        StompSession session = stompClient.connect(this.url, this.headers, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);

        verify(this.mockAppender, timeout(500))
            .doAppend(argThat(new LogTestMatcher("a new connection was established, ", Level.INFO)));

    }

    @Test
    public void closeEventWithNonRegisteredUser_logsNotRegistered() throws ExecutionException, InterruptedException, TimeoutException {

        StompSession session = stompClient.connect(this.url, this.headers, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);

        session.disconnect();

        verify(this.mockAppender, timeout(500))
            .doAppend(argThat(new LogTestMatcher("a connection was closed, user not-registered", Level.INFO)));

    }

    @Test
    public void closeEventWithRegisteredUser_logsUsername() throws ExecutionException, InterruptedException, TimeoutException {

        StompSession session = stompClient.connect(this.url, this.headers, new StompSessionHandlerAdapter() {
        }).get(1, SECONDS);

        SynchronousQueue<SystemMessageDTO> response = new SynchronousQueue<>();
        session.subscribe(Constants.USER_PREFIX + Constants.CHAT_EP, new ChatStompMessageFrameHandler<>(response,null, SystemMessageDTO.class));

        verify(this.mockAppender, timeout(500).atLeastOnce())
            .doAppend(argThat(new LogTestMatcher("user test-user successfully subscribed", Level.INFO)));

        session.disconnect();

        verify(this.mockAppender, timeout(500))
            .doAppend(argThat(new LogTestMatcher("a connection was closed, user test-user", Level.INFO)));

    }

}
