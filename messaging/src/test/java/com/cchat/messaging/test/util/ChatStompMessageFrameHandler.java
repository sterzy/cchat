package com.cchat.messaging.test.util;

import java.lang.reflect.*;
import java.util.concurrent.*;
import com.cchat.common.dto.websocket.BaseMessageDTO;
import org.springframework.messaging.simp.stomp.*;

/**
 * Helper Class for handeling web socket tests. Allows for the reception of
 * MessageDTO object and Errors (errors are just a simple string parsed from
 * a custom "error" header field in the STOMP message).
 */
public class ChatStompMessageFrameHandler<T extends BaseMessageDTO> implements StompFrameHandler {


    private SynchronousQueue<T> response;
    private Class<T> type;
    private CompletableFuture<String> error;

    public ChatStompMessageFrameHandler() {}

    public ChatStompMessageFrameHandler(SynchronousQueue<T> response, CompletableFuture<String> error, Class<T> type) {
        this.response = response;
        this.error = error;
        this.type = type;
    }

    @Override
    public Type getPayloadType(StompHeaders stompHeaders) {
        return this.type;
    }

    @Override
    public void handleFrame(StompHeaders stompHeaders, Object o) {

        if (stompHeaders.containsKey("error")) {
            this.error.complete(stompHeaders.toSingleValueMap().get("error"));
        }

        try {
            this.response.put((T) o);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
