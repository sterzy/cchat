package com.cchat.messaging.test.util;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.*;
import org.mockito.ArgumentMatcher;

public class LogTestMatcher implements ArgumentMatcher {

    private String containing;
    private Level level;

    public LogTestMatcher(String containing, Level level) {
        this.containing = containing;
        this.level = level;
    }

    @Override
    public boolean matches(final Object argument) {
        LoggingEvent event = (LoggingEvent)argument;

        return  event.getFormattedMessage().contains(this.containing)
                && event.getLevel() == this.level;
    }

    @Override
    public String toString() {
        return  "[" + this.level + "] "
                + this.containing;
    }
}
