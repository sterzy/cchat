package com.cchat.messaging.config;

import com.cchat.common.config.ConfigFactory;
import lombok.extern.log4j.Log4j2;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import java.util.concurrent.Executor;

@Configuration
@EnableSwagger2
@EnableAsync
@Log4j2
class Config {

    /**
     * Dozer Mapper is used to map DTOs to JSON and vise versa.
     */
    @Bean
    public Mapper getDozerBeanMapper() {
        return new DozerBeanMapper();
    }

    /**
     * Swagger is a utility to automatically document API endpoints and call
     * them via a website.
     *
     * Visit /swagger-ui.html to view the site.
     */
    @Bean
    public Docket swagger() {
        return ConfigFactory.swagger();
    }

    /**
     * This let's us configure the Executor used for asynchronous methods.
     *
     * @return  The executor that will be used for asynchronous methods.
     */
    @Bean
    public Executor taskExecutor() {

        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(5);
        taskExecutor.setMaxPoolSize(16);
        taskExecutor.setQueueCapacity(500);
        taskExecutor.setThreadNamePrefix("CChat-Messaging-");
        taskExecutor.initialize();
        return taskExecutor;

    }

}
