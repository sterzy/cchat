package com.cchat.messaging.exception;

import com.cchat.common.dto.websocket.BaseMessageDTO;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class RequestNotAllowedException extends Exception {

    @Getter @Setter
    private BaseMessageDTO malformed;

    public RequestNotAllowedException(String message) {
        super(message);
    }

    public RequestNotAllowedException(String message, BaseMessageDTO malformed) {
        super(message);
        this.setMalformed(malformed);
    }

}
