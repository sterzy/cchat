package com.cchat.messaging.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.event.*;
import org.springframework.messaging.simp.stomp.*;
import org.springframework.stereotype.*;
import org.springframework.web.socket.messaging.*;

@Log4j2
@Component
public class WebSocketEventListener {

    /**
     * Handles new connection events
     *
     * @param      event  The event
     */
    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {
        log.info("a new connection was established, {}", event.getMessage());
    }

    /**
     * Handles socket.close() and other disconnection events.
     *
     * @param      event  The event
     */
    @EventListener
    public void handleWebSocketDisconnectListener(SessionDisconnectEvent event) {

        StompHeaderAccessor header = StompHeaderAccessor.wrap(event.getMessage());
        String username = (String) header.getSessionAttributes().get("username");

        if (username == null) {
            username = "not-registered";
        }

        log.info("a connection was closed, user {}", username);


    }
}
