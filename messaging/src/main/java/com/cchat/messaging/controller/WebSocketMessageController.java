package com.cchat.messaging.controller;

import com.cchat.common.dto.amq.BotCryptoMessageDTO;
import com.cchat.common.util.Constants;
import com.cchat.messaging.service.CacheService;
import com.cchat.common.dto.websocket.*;
import com.cchat.messaging.exception.*;
import com.cchat.messaging.service.UserService;
import java.security.Principal;
import org.springframework.beans.factory.annotation.*;
import org.springframework.messaging.handler.annotation.*;
import org.springframework.messaging.handler.annotation.support.MethodArgumentNotValidException;
import org.springframework.messaging.simp.*;
import org.springframework.messaging.simp.annotation.*;
import org.springframework.messaging.simp.stomp.*;
import org.springframework.stereotype.*;
import org.springframework.util.*;
import lombok.extern.log4j.*;

import javax.validation.Valid;


@Log4j2
@Controller
public class WebSocketMessageController {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private UserService userService;

    @Autowired
    private CacheService cacheService;

    /**
     * This mapping is triggered when a user subscribes to the chat endpoint.
     * We then check if the user was properly authorized and send all cached
     * messages to them if there are any.
     *
     * @param   header      The header accessor
     * @param   principal   The principal of the currently logged in user
     *
     * @throws  RequestNotAllowedException  This exception is thrown if the user is not allowed to send the request
     */
    @SubscribeMapping(Constants.CHAT_EP)
    public void chatSubscribe(SimpMessageHeaderAccessor header,
                                     Principal principal) throws RequestNotAllowedException {

        // check if the principal was properly set
        if (principal == null) {
            throw new RequestNotAllowedException("user not authorized");
        }

        log.debug("as user is trying to subscribe with principal {}", principal);

        header.getSessionAttributes().put("username", principal.getName());

        // Check whether we have messages for a user in our cache, if yes send
        // them to the user now
        if (this.cacheService.hasCachedMessages(principal.getName())) {
            this.cacheService.sendCachedMessagesToUser(principal.getName());
        }

        log.info("user {} successfully subscribed", principal.getName());

    }

    /**
     * Delivers new messages to a user that is currently online. If a user is
     * not online the message will be cached.
     *
     * @param   user        The currently logged in user
     * @param   request     The chat message
     * @param   header      The header accessor
     * @param   principal   The principal of the currently logged in user
     *
     * @return  A receipt message for the sender.
     *
     * @throws  RequestNotAllowedException  This exception is thrown if the user is not allowed to send the request
     */
    @MessageMapping(Constants.SEND_ROUTE + "/{user}")
    @SendToUser(destinations = Constants.CHAT_EP, broadcast = false)
    public SystemMessageDTO send(@DestinationVariable String user,
                                 @Valid @Payload CryptoMessageDTO request,
                                 SimpMessageHeaderAccessor header,
                                 Principal principal) throws RequestNotAllowedException {

        log.debug("a user is trying to send {} to user {}", request, user);

        // check if the principal was properly set
        if (principal == null) {
            throw new RequestNotAllowedException("user not authorized");
        }

        // Check if the user is already subscribed
        if (StringUtils.isEmpty(header.getSessionAttributes().get("username"))) {
            throw new RequestNotAllowedException("please subscribe to "+ Constants.CHAT_EP +" first", request);
        }

        // Check that the recipient of the message actually exists
        if (!this.userService.exists(user)) {
            throw new RequestNotAllowedException("user does not exist", request);
        }

        // Create a receipt message for the sender
        SystemMessageDTO receipt = new SystemMessageDTO();
        receipt.setPayload(Constants.MESSAGE_SENT);
        receipt.setMessageId(request.getId());
        receipt.setTimestamp(System.currentTimeMillis());

        // Clean up the message for the recipient
        request.setFrom(principal.getName());
        request.setId(""); // leaving this information in the message would be
                           // unnecessary information disclosure

        // Check if user is online, if not cache the message
        if (this.userService.isConnected(user)) {
            // Send message to user
            this.messagingTemplate.convertAndSendToUser(user, Constants.CHAT_EP, request);
        } else {
            // Cache message
            this.cacheService.cacheMessage(user, request);
        }

        log.info("user {} sent message to {}", principal.getName(), user);

        return receipt;

    }

    /**
     * Sends a message to the bot queue that will be processed by a bot service.
     *
     * @param   bot         The bot the sender wants to send a message to
     * @param   request     The request we will send to the bot
     * @param   header      The header of the message that will be send to a bot
     * @param   principal   The principal that represents the user that is sending the message
     *
     * @return  A receipt message for the sender acknowledging the message that will be send
     *
     * @throws  RequestNotAllowedException  This exception is thrown if the user is not allowed to send the request
     */
    @MessageMapping(Constants.BOT_SEND_ROUTE+"/{bot}")
    @SendToUser(destinations = Constants.CHAT_EP, broadcast = false)
    public SystemMessageDTO botSend(@DestinationVariable String bot,
                                    @Valid @Payload CryptoMessageDTO request,
                                    SimpMessageHeaderAccessor header,
                                    Principal principal) throws RequestNotAllowedException {

        log.debug("a user is trying to send {} to bot {}", request, bot);

        // check if the principal was properly set
        if (principal == null) {
            throw new RequestNotAllowedException("user not authorized");
        }

        // Check if a user is subscribed properly
        if (StringUtils.isEmpty(header.getSessionAttributes().get("username"))) {
            throw new RequestNotAllowedException("please subscribe to "+ Constants.CHAT_EP +" first", request);
        }

        // Create the receipt for the sender
        SystemMessageDTO receipt = new SystemMessageDTO();
        receipt.setPayload(Constants.MESSAGE_SENT);
        receipt.setMessageId(request.getId());
        receipt.setTimestamp(System.currentTimeMillis());

        // Create the message for the bot
        request.setFrom(principal.getName());
        request.setId(""); // leaving this information in the message would be
                           // unnecessary information disclosure
        BotCryptoMessageDTO dto = new BotCryptoMessageDTO();
        dto.setBot(bot);
        dto.setMessage(request);

        // Send the message to the bot queue
        this.messagingTemplate.convertAndSend(Constants.AMQ_QUEUES + Constants.BOT_QUEUE, dto);

        log.info("user {} send a message to bot {}", principal.getName(), bot);

        return receipt;

    }

    /**
     * A Message Exception Handler that catches RequestNotAllowedException
     * exceptions and processes them.
     *
     * @param   e           The exception that will be processed
     * @param   header      The header of the request that caused the exception
     * @param   principal   The principal of the user that caused the exception
     */
    @MessageExceptionHandler(RequestNotAllowedException.class)
    public void handleRequestNotAllowedException(RequestNotAllowedException e,
                                                 StompHeaderAccessor header,
                                                 Principal principal) {

        // check if the principal was properly set
        if (principal == null) {
            return;
        }

        log.warn("error encountered: {} - {} ", principal.getName(), e.getMessage());

        // Create the header that explains the issue
        StompHeaderAccessor newHeader = StompHeaderAccessor.create(StompCommand.MESSAGE);
        newHeader.setSessionId(header.getSessionId());
        newHeader.setHeader("error", e.getMessage());

        // Send the error message with the custom header
        this.messagingTemplate.convertAndSendToUser(
            principal.getName(),
            Constants.ERROR_EP,
            e.getMalformed(),
            newHeader.toMap()
        );

    }

    /**
     * This exception handler handles MethodArgumentNotValidException and sends
     * a message that indicates the validation issue to the client.
     *
     * @param   e           The exception that will be processed
     * @param   header      The header of the request that caused the exception
     * @param   principal   The principal of the user that caused the exception
     */
    @MessageExceptionHandler(MethodArgumentNotValidException.class)
    public void handleMethodArgumentNotValidExceptionException(MethodArgumentNotValidException e,
                                                               StompHeaderAccessor header,
                                                               Principal principal) {

        // check if the principal was properly set
        if (principal == null) {
            return;
        }

        log.warn("error encountered: {} - {} ", principal.getName(), e.getMessage());

        // Create the header that explains the issue
        StompHeaderAccessor newHeader = StompHeaderAccessor.create(StompCommand.MESSAGE);
        newHeader.setSessionId(header.getSessionId());
        newHeader.setHeader("error", e.getBindingResult().getFieldError().getDefaultMessage());

        // Send the error message with the custom header
        this.messagingTemplate.convertAndSendToUser(
            principal.getName(),
            Constants.ERROR_EP,
            e.getBindingResult().getTarget(),
            newHeader.toMap()
        );

    }

}
