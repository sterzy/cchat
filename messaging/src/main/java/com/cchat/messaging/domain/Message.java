package com.cchat.messaging.domain;

import com.cchat.common.dto.websocket.MessagingType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document
public class Message {

    @Id
    private String id;
    private MessagingType type;

    @Indexed
    private Long timestamp;

    @Indexed
    private String to;
    private String from;
    private Long cryptoType;
    private String payload;

}
