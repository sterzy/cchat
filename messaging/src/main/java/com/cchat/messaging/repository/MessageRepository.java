package com.cchat.messaging.repository;

import com.cchat.messaging.domain.Message;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MessageRepository extends MongoRepository<Message, String> {

    void deleteMessageById(String id);

    boolean existsMessageByTo(String to);

    List<Message> findAllByToOrderByTimestampAsc(String to);

}
