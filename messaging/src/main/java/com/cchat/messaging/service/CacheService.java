package com.cchat.messaging.service;

import com.cchat.common.dto.websocket.CryptoMessageDTO;
import com.cchat.messaging.domain.Message;
import com.cchat.messaging.repository.MessageRepository;
import com.cchat.common.util.Constants;
import com.cchat.messaging.util.MapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class CacheService {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private MapperUtil mapper;

    /**
     * Stores a given message for a particular user.
     *
     * @param to        The receiver of the given message that will be cached
     * @param dto       The message that should be send to the user
     */
    public void cacheMessage(String to, CryptoMessageDTO dto) {
        Message message = this.mapper.toDomain(dto, Message.class);
        message.setTo(to);
        this.messageRepository.save(message);
    }

    /**
     * Checks if a given user has cached messages associated with them.
     *
     * @param to        The username of the user
     * @return          true if cached messages for the user exists, false
     *                  otherwise
     */
    public boolean hasCachedMessages(String to) {
        return this.messageRepository.existsMessageByTo(to);
    }

    /**
     * This sends cached messages to a user asynchronously
     *
     * @param to        The username of the user to which we will send messages
     *                  that have been cached for them
     */
    @Async
    public void sendCachedMessagesToUser(String to) {

        for (Message m : messageRepository.findAllByToOrderByTimestampAsc(to)) {

            // As far as I can tell a runtime exception is thrown here if a
            // message was not delivered. This should mean it is safe to delete
            // the message like this.
            this.messagingTemplate.convertAndSendToUser(to, Constants.CHAT_EP, this.mapper.toDto(m, CryptoMessageDTO.class));
            this.messageRepository.deleteMessageById(m.getId());

        }

    }

}
