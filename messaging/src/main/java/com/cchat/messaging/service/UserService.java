package com.cchat.messaging.service;

import lombok.extern.log4j.Log4j2;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;

@Log4j2
@Service
public class UserService {

    @Value("${security.keycloak.url:}")
    private String keycloakUrl;

    @Value("${security.keycloak.realm:}")
    private String keycloakRealm;

    @Value("${security.keycloak.username:}")
    private String keycloakUsername;

    @Value("${security.keycloak.password:}")
    private String keycloakPassword;

    @Value("${security.keycloak.clientId:}")
    private String keycloakClientId;

    @Autowired(required = false)
    private ResteasyClient resteasyClient;

    @Autowired
    private SimpUserRegistry userRegistry;

    private RealmResource realmResourceAdmin;

    @PostConstruct
    private void init() {
        this.realmResourceAdmin = getRealmResourceForAdmin();
    }

    private RealmResource getRealmResourceForAdmin() {

        Keycloak k = KeycloakBuilder.builder()
            .serverUrl(this.keycloakUrl)
            .username(this.keycloakUsername)
            .password(this.keycloakPassword)
            .clientId(this.keycloakClientId)
            .realm(this.keycloakRealm)
            .resteasyClient(this.resteasyClient)
            .build();

        return k.realm(keycloakRealm);

    }

    /**
     * Checks whether a user is connected by their username.
     *
     * @param username  The username of the user
     * @return          true if the user is connected, false otherwise
     */
    public boolean isConnected(String username) {
        return !(this.userRegistry.getUser(username) == null);
    }

    /**
     * Checks whether a user exists in the system via keycloak.
     *
     * @param username  The username of the user
     * @return          true if a user with the given username exists, false
     *                  otherwise
     */
    public boolean exists(String username) {

        log.trace("checking if user {} exists", username);
        return !(this.realmResourceAdmin.users().get(username) == null);

    }

}
