package com.cchat.sticker.test;

import com.cchat.common.security.SecurityUser;
import com.cchat.sticker.domain.Sticker;
import com.cchat.sticker.domain.StickerPack;
import com.cchat.sticker.exception.NotFoundException;
import com.cchat.sticker.repository.StickerRepository;
import com.cchat.sticker.resource.StickersResource;
import com.cchat.sticker.resource.dto.AddStickerpackDto;
import com.cchat.sticker.resource.dto.StickerUploadDto;
import com.cchat.sticker.resource.dto.StickerpackDto;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(StickersResource.class)
@Import({com.cchat.sticker.resource.util.MapperUtil.class})
public class StickersResourceTest {

    private static final Logger logger = LoggerFactory.getLogger(StickersResourceTest.class);

    @Autowired
    private MockMvc mvc;

    @Autowired
    private StickersResource resource;

    @MockBean
    private StickerRepository stickerRepository;


    @Spy
    private StickerPack STICKER_PACK1 = new StickerPack(
        "sp1_id",
        "user1",
        "sp1",
        new ArrayList<Sticker>() {{
            add(new Sticker("sp1_s1_id", ":)", "AAAA"));
            add(new Sticker("sp1_s2_id", ":|", "BBBB"));
            add(new Sticker("sp1_s3_id", ":(", "<svg>fakesvg</svg>"));
        }}
    );
    private final StickerPack STICKER_PACK2 = new StickerPack(
        "sp2_id",
        "user1",
        "sp2",
        new ArrayList<Sticker>()
    );
    private final List<StickerPack> STICKER_PACK_LIST = new ArrayList<>() {{
        add(STICKER_PACK1);
        add(STICKER_PACK2);
    }};
    private static final SecurityUser USER1 = new SecurityUser("aa", "user1", new ArrayList<>());


    @Before
    public void setup() {
    }

    @Test(expected = NotFoundException.class)
    public void testStickerpackById_UnknownNameReturns404() throws Exception {
        resource.getStickerpackById(USER1, "unknown");
    }

    @Test
    public void testFindStickerpackById_successful() {
        given(stickerRepository.findById(any()))
            .willReturn(Optional.of(STICKER_PACK1));

        ResponseEntity<StickerpackDto> responseEntity = resource.getStickerpackById(USER1, "sp1_id");
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertNotNull(responseEntity.getBody());
        assertEquals(responseEntity.getBody().get_id(), "sp1_id");
    }

    @Test
    public void testGetStickerpacks_successful() throws Exception {

        given(stickerRepository.findAll())
            .willReturn(STICKER_PACK_LIST);

        ResponseEntity<List<StickerpackDto>> responseEntity = resource.getStickerpacks(USER1, true, null, null);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertNotNull(responseEntity.getBody());
        assertEquals(responseEntity.getBody().get(0).getName(), "sp1");
    }

    @Test
    public void testGetStickerpacksWithParams_successful() throws Exception {

        given(stickerRepository.findByCreator(any()))
            .willReturn(STICKER_PACK_LIST);

        ResponseEntity<List<StickerpackDto>> responseEntity = resource.getStickerpacks(USER1, false, "user1", null);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertNotNull(responseEntity.getBody());
        assertEquals(responseEntity.getBody().get(0).getName(), "sp1");
        assertNull(responseEntity.getBody().get(0).getStickers());

        verify(stickerRepository, times(1)).findByCreator(any());
    }

    @Test
    public void testAddStickerToStickerpack_successful() {
        given(stickerRepository.findById(any()))
            .willReturn(Optional.of(STICKER_PACK1));

        StickerUploadDto newSticker = new StickerUploadDto("adsf", "<svg>stuff</svg>");
        ResponseEntity<Void> responseEntity = resource.addStickerToStickerpack(USER1, "sp1_id", newSticker);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);

        verify(STICKER_PACK1, times(1)).setStickers(any());
    }

    @Test
    public void testRemoveStickerpack_successful() {
        given(stickerRepository.findById(any()))
            .willReturn(Optional.of(STICKER_PACK1));

        ResponseEntity<Void> responseEntity = resource.removeStickerpack(USER1, "sp1_id");
        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);

        verify(stickerRepository, times(1)).delete(any());
    }

    @Test
    public void testAddNewStickerpack_successful() {

        StickerUploadDto newStickerDto = new StickerUploadDto("adsf", "<xml?><svg some params>asdf</svg>");
        AddStickerpackDto newStickerpackDto = new AddStickerpackDto("newsp", Arrays.asList(newStickerDto));
        ResponseEntity<Void> responseEntity = resource.addNewStickerpack(USER1, newStickerpackDto);

        assertEquals(responseEntity.getStatusCode(), HttpStatus.CREATED);
    }

    @Test
    @Ignore
    public void testGetRawStickerByUrl_invalidSvgEmptyResponse() throws IOException {

        given(stickerRepository.findById(any()))
            .willReturn(Optional.of(STICKER_PACK1));

        ResponseEntity<String> responseEntity = resource.getRawStickerByUrl(USER1, "sp1_id", ":)");

        assertEquals(responseEntity.getStatusCode(), HttpStatus.NO_CONTENT);  // not a valid svg
    }

    @Test
    public void testGetRawStickerByUrl_successful() throws IOException {

        given(stickerRepository.findById(any()))
            .willReturn(Optional.of(STICKER_PACK1));

        ResponseEntity<String> responseEntity = resource.getRawStickerByUrl(USER1, "sp1_id", ":(");

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
        assertNotNull(responseEntity.getBody());
        assertEquals(responseEntity.getHeaders().getContentType().toString(), "image/svg+xml");
    }
}
