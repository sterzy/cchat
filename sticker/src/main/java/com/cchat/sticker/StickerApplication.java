package com.cchat.sticker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.jdbc.DataSourceHealthIndicatorAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(exclude = {DataSourceHealthIndicatorAutoConfiguration.class})
public class StickerApplication {
    public static void main(String[] args) {
        SpringApplication.run(StickerApplication.class, args);
    }
}
