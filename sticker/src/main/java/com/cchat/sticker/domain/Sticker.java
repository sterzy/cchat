package com.cchat.sticker.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Sticker {
    @Id
    private String _id;
    private String emoji_id;
    private String data;

    //maybe: enum data_type (SVG/PNG/...)
}
