package com.cchat.sticker.resource.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StickerpackDto {
    private String _id;
    private String creator;
    private String name;  // index?
    private List<StickerDto> stickers;
}
