package com.cchat.sticker.resource;

import com.cchat.sticker.resource.util.MapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * A Base resource to define all paths and autowire services.
 */
@CrossOrigin
public abstract class BaseResource {

    @Autowired
    protected MapperUtil mapper;
//
//    @Autowired
//    protected UserService userService;

    public static final String PREFIX = "/api";

    public static final String STICKERPACKS_COLL = PREFIX + "/stickerpacks";
    public static final String STICKERPACKS_BYID = PREFIX + "/stickerpacks/{id}";
    public static final String STICKERPACKS_BYID_STICKER = PREFIX + "/stickerpacks/{id}/{emojiId}";
}
