package com.cchat.sticker.resource.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StickerDto {
    @NotEmpty
    private String emoji_id;
    @NotEmpty
    private String data;
    private String data_url;
}
