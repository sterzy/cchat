package com.cchat.sticker.resource;

import com.cchat.common.security.SecurityUser;
import com.cchat.sticker.domain.Sticker;
import com.cchat.sticker.domain.StickerPack;
import com.cchat.sticker.exception.BadRequestException;
import com.cchat.sticker.exception.ForbiddenException;
import com.cchat.sticker.exception.NotFoundException;
import com.cchat.sticker.repository.StickerRepository;
import com.cchat.sticker.resource.dto.AddStickerpackDto;
import com.cchat.sticker.resource.dto.StickerDto;
import com.cchat.sticker.resource.dto.StickerUploadDto;
import com.cchat.sticker.resource.dto.StickerpackDto;
import lombok.extern.log4j.Log4j2;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

/**
 * description of endpoints
 * POST /api/stickerpacks (Upload new stickerpack including some stickers)
 * GET /api/stickerpacks (Get all Stickerpacks)
 *   - Query: expand=(boolean) to get stickers or not
 *   - Query: sticker (find stickerpacks by sticker)
 *   - Query: name (find stickerpack by sticker/name (requires sticker)
 * GET /api/stickerpacks/id (get single stickerpack)
 * POST /api/stickerpacks/id/stickers (add stickers, only creator)
 * DELETE /api/stickerpacks/id (delete a stickerpack, only creator)
 * GET /api/stickerpacks/id/emojiid (get single sticker in raw format (not json) with appropriate Content-Type
 */

@RestController
@Log4j2
public class StickersResource extends BaseResource {

    @Autowired
    private StickerRepository stickerRepository;


    @RequestMapping(value = STICKERPACKS_COLL, method = RequestMethod.POST)
    public ResponseEntity<Void> addNewStickerpack(@AuthenticationPrincipal SecurityUser user, @Valid @RequestBody AddStickerpackDto dto) {

        log.info("user '{}' is adding a stickerpack", user.getUsername());

        StickerPack newStickerPack = this.mapper.toDomain(dto, StickerPack.class);

        newStickerPack.setCreator(user.getUsername());
        newStickerPack.set_id(ObjectId.get().toHexString());

        for (Sticker sticker : newStickerPack.getStickers()) {
            if(!isValidSvg(sticker.getData())) {
                throw new BadRequestException("invalid svg uploaded");
            }
            sticker.set_id(ObjectId.get().toHexString());
        }

        stickerRepository.save(newStickerPack);

        log.info("new stickerpack '{}/{}' has been added", newStickerPack.getCreator(), newStickerPack.getName());

        try {
            return ResponseEntity
                    .created(new URI("/api/stickerpacks/" + newStickerPack.get_id()))
                    .build();
        } catch (URISyntaxException e) {
            return ResponseEntity.created(null).build();
        }
    }


    @RequestMapping(value = STICKERPACKS_COLL, method = RequestMethod.GET)
    public ResponseEntity<List<StickerpackDto>> getStickerpacks(
                @AuthenticationPrincipal SecurityUser user,
                @RequestParam(defaultValue = "true", name = "expand") Boolean expand,
                @RequestParam(required = false, name = "creator") String creator,
                @RequestParam(required = false, name="name") String name) {

        log.info("user '{}' is getting stickerpack collection", user.getUsername());

        List<StickerPack> stickerPacks;
        if (creator == null && name == null) {
            stickerPacks = stickerRepository.findAll();
        } else if (name == null) {
            stickerPacks = stickerRepository.findByCreator(creator);
        } else if (creator == null) {
            throw new BadRequestException("cannot search by name only");
        } else {
            stickerPacks = stickerRepository.findByCreatorAndName(creator, name);
        }

        List<StickerpackDto> stickerpackDtos = mapper.toDto(stickerPacks, StickerpackDto.class);

        if(!expand) {
            log.info("not expand - collapsing stickerpacks");
            for (StickerpackDto stickerpackDto : stickerpackDtos) {
                stickerpackDto.setStickers(null);
            }
        } else {
            for (StickerpackDto stickerpackDto : stickerpackDtos) {
                setStickerPackStickerURL(stickerpackDto);
            }
        }

        return ResponseEntity
                .ok(stickerpackDtos);
    }

    @RequestMapping(value = STICKERPACKS_BYID, method = RequestMethod.GET)
    public ResponseEntity<StickerpackDto> getStickerpackById(@AuthenticationPrincipal SecurityUser user, @PathVariable(name = "id") String stickerpackId) {

        log.info("user '{}' is getting stickerpack by id ({})", user.getUsername(), stickerpackId);

        StickerPack stickerPack = stickerRepository.findById(stickerpackId).orElseThrow(NotFoundException::new);

        StickerpackDto stickerpackDto = mapper.toDto(stickerPack, StickerpackDto.class);

        setStickerPackStickerURL(stickerpackDto);

        return ResponseEntity
            .ok(stickerpackDto);
    }

    @RequestMapping(value = STICKERPACKS_BYID, method = RequestMethod.POST)
    public ResponseEntity<Void> addStickerToStickerpack(
                @AuthenticationPrincipal SecurityUser user,
                @PathVariable(name = "id") String stickerpackId,
                @Valid @RequestBody StickerUploadDto stickerDto) {

        log.info("user '{}' is adding stickers to stickerpack ({})", user.getUsername(), stickerpackId);

        if(!isValidSvg(stickerDto.getData())) {
            log.warn("{}: invalid sticker data. returning error", stickerpackId);
            throw new BadRequestException("invalid sticker data");
        }

        Sticker newSticker = mapper.toDomain(stickerDto, Sticker.class);

        StickerPack stickerPack = stickerRepository.findById(stickerpackId).orElseThrow(NotFoundException::new);

        if (!Objects.equals(stickerPack.getCreator(), user.getUsername())) {
            throw new ForbiddenException("stickerpack doesn't belong to user");
        }

        List<Sticker> stickers = stickerPack.getStickers();
        stickers.add(newSticker);
        stickerPack.setStickers(stickers);

        return ResponseEntity
            .ok()
            .build();
    }

    @RequestMapping(value = STICKERPACKS_BYID, method = RequestMethod.DELETE)
    public ResponseEntity<Void> removeStickerpack(@AuthenticationPrincipal SecurityUser user, @PathVariable(name = "id") String stickerpackId) {

        log.info("user '{}' is removing stickerpack ({})", user.getUsername(), stickerpackId);

        StickerPack stickerPack = stickerRepository.findById(stickerpackId).orElseThrow(NotFoundException::new);// check if it exists

        if (!Objects.equals(stickerPack.getCreator(), user.getUsername())) {
            throw new ForbiddenException("stickerpack doesn't belong to user");
        }

        stickerRepository.delete(stickerPack);

        return ResponseEntity
            .ok()
            .build();
    }

    @RequestMapping(value = STICKERPACKS_BYID_STICKER, method = RequestMethod.GET)
    public ResponseEntity<String> getRawStickerByUrl(
                @AuthenticationPrincipal SecurityUser user,
                @PathVariable(name = "id") String stickerpackId,
                @PathVariable(name = "emojiId") String emojiId) throws IOException {

        log.info("user '{}' called getRawStickerByUrl", user.getUsername());

        StickerPack stickerPack = stickerRepository.findById(stickerpackId).orElseThrow(NotFoundException::new);

        Sticker sticker1 = stickerPack.getStickers()
            .stream()
            .filter(sticker -> sticker.getEmoji_id() != null && sticker.getEmoji_id().equals(emojiId))
            .findFirst()
            .orElseThrow(NotFoundException::new);

        String responseContent = sticker1.getData(); // always svg for now

        if(!isValidSvg(responseContent)) {
            log.warn("{}/{}: no displayable content found - ignoring", stickerpackId, emojiId);
        }

        return ResponseEntity
            .ok()
            .contentType(MediaType.valueOf("image/svg+xml"))
            .body(responseContent);

    }

    private static void setStickerPackStickerURL(StickerpackDto stickerpackDto) {
        String spid = stickerpackDto.get_id();
        for (StickerDto sticker : stickerpackDto.getStickers()) {
            sticker.setData_url("/api/stickerpacks/"+spid+"/"+sticker.getEmoji_id());
        }
    }

    private boolean isValidSvg(String stickerData) {
        //return stickerData.matches(".*<svg.*</svg>.*");
        return stickerData.contains("<svg");
    }
}
