package com.cchat.sticker.resource.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddStickerpackDto {
    private String name;
    private List<StickerUploadDto> stickers;
}
