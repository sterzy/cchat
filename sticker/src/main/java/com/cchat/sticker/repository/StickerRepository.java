package com.cchat.sticker.repository;

import com.cchat.sticker.domain.StickerPack;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface StickerRepository extends MongoRepository<StickerPack, String> {
    public List<StickerPack> findByCreator(String creator);

    public List<StickerPack> findByCreatorAndName(String creator, String name);
}
