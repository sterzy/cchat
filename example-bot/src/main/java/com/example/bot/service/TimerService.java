package com.example.bot.service;

import com.cchat.common.dto.websocket.CryptoMessageDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class TimerService {


    private static final Logger logger = LoggerFactory.getLogger(TimerService.class);

    private ScheduledExecutorService executor = Executors.newScheduledThreadPool(10);

    public void startTimerThread(int seconds, CryptoMessageDTO message, String token, String remoteAddress) {
        logger.info("Timer Thread will be started");
        this.executor.schedule(() -> {
            this.sendMessage(message, token, remoteAddress);
        }, seconds, TimeUnit.SECONDS);
    }

    private void sendMessage(CryptoMessageDTO message, String token, String remoteAddress) {
        RestTemplate request = new RestTemplate();
        ResponseEntity<Void> resp;

        // overwrite timestamp because the first one is wrong
        message.setTimestamp(new Date().getTime());

        logger.info("Message will be send back to \""+remoteAddress+"\"");

        try {
            resp = request.postForEntity(remoteAddress + "?token=" + token, message, Void.class);
        } catch (Exception e) {
            logger.error("Failed to send message:", e);
            return;
        }
        if (!resp.getStatusCode().is2xxSuccessful()) {
            logger.error("message could not be send back");
        }
    }
}
