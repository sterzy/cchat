package com.example.bot.service;

import com.cchat.common.dto.websocket.CryptoMessageDTO;
import com.example.bot.resource.dto.TimeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.whispersystems.libsignal.*;
import org.whispersystems.libsignal.protocol.CiphertextMessage;
import org.whispersystems.libsignal.protocol.PreKeySignalMessage;
import org.whispersystems.libsignal.protocol.SignalMessage;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CryptoService {


    private static final Logger logger = LoggerFactory.getLogger(CryptoService.class);

    /**
     * Parses the message in the correct json format and encrypt it afterwards.
     * @param sessionCipher     the sessionCipher with which we need to encrypt the message
     * @param message           the decrypted message we want to encrypt
     * @param botName           the name of our bot which will be used for the from field
     * @return                  a valid CryptoMessageDTO or null if the operation fails
     */
    public CryptoMessageDTO encryptMessage(SessionCipher sessionCipher, String message, String botName) {

        // message needs to be in json format
        String jsonMessage = Json.createObjectBuilder()
            .add("type", "pay-txt")
            .add("text", message)
            .build().toString();

        try {
            CryptoMessageDTO cryptoMessageDTO = new CryptoMessageDTO();
            // create encrypted message
            CiphertextMessage ciphertextMessage = sessionCipher.encrypt(jsonMessage.getBytes());

            // get type from message and set it
            cryptoMessageDTO.setCryptoType((long) ciphertextMessage.getType());
            cryptoMessageDTO.setPayload(Base64.getEncoder().encodeToString(ciphertextMessage.serialize()));
            // our bot name
            cryptoMessageDTO.setFrom(botName);
            cryptoMessageDTO.setTimestamp(new Date().getTime());

            return cryptoMessageDTO;

        } catch (Exception e) {
            logger.error("Could not encrypt message: ", e);
            return null;
        }
    }

    /**
     * Decrypt the given message.
     * @param sessionCipher     the sessioCipher with which we need to decrypt the message
     * @param encryptedMessage  the encrypted message
     * @param type              specifies with which type the message is encrypted
     * @return                  the decrypted and parsed message, or null if it fails.
     */
    public String decryptMessage(SessionCipher sessionCipher, String encryptedMessage, int type) {

        String decryptedMessage = "";
        try {
            switch (type) {
                case 1: {
                    // SignalMessage
                    SignalMessage signalMessage = new SignalMessage(Base64.getDecoder().decode(encryptedMessage));
                    decryptedMessage = new String(sessionCipher.decrypt(signalMessage), StandardCharsets.UTF_8);
                    break;
                }
                case 3: {
                    // PreKeySignalMessage
                    PreKeySignalMessage preKeySignalMessage = new PreKeySignalMessage(Base64.getDecoder().decode(encryptedMessage));
                    decryptedMessage = new String(sessionCipher.decrypt(preKeySignalMessage), StandardCharsets.UTF_8);
                    break;
                }
            }

        } catch (Exception e) {
            logger.error("Could not decrypt message: ", e);
            return null;
        }
        return decryptedMessage;
    }

    /**
     * Parses the given time message from json to a string
     * @param message   the message which should be parsed
     * @return          the parsed message or null if the format wasn't correct.
     */
    public TimeDTO parseTimeMessage(String message) {
        logger.info("Start parsing the message");
        // parse text first because it is in json format
        JsonReader jsonReader = Json.createReader(new StringReader(message));
        JsonObject object = jsonReader.readObject();
        jsonReader.close();

        Pattern pattern = Pattern.compile("^time:([0-9]+),message:(.+)");
        Matcher matcher = pattern.matcher(object.getString("text"));
        TimeDTO timeDTO = new TimeDTO();
        if (matcher.find()) {
            timeDTO.setTime(Integer.parseInt(matcher.group(1)));
            timeDTO.setMessage(matcher.group(2));

            logger.info("Time was: {} and message was: {}", timeDTO.getTime(), timeDTO.getMessage());
        } else {
            logger.error("Message wrong formatted\nMessage was: " + message + ")");
            return null;
        }
        return timeDTO;
    }

}
