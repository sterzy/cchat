package com.example.bot.resource.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InitialKeyBundle {
    private Integer registrationId;
    private String identityKey;
    private List<PreKey> preKeys;
    private SignedPreKey signedPreKey;
}
