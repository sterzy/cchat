package com.example.bot.resource;

import com.cchat.common.dto.websocket.CryptoMessageDTO;
import com.example.bot.exceptions.UnauthorizedException;
import com.example.bot.repository.SignalRepository;
import com.example.bot.repository.BotTokenRepository;
import com.example.bot.resource.dto.InitialKeyBundle;
import com.example.bot.resource.dto.SignedPreKey;
import com.example.bot.resource.dto.BotTokenDTO;
import com.example.bot.resource.dto.TimeDTO;
import com.example.bot.service.CryptoService;
import com.example.bot.service.TimerService;
import com.example.bot.util.BotPaths;
import lombok.extern.log4j.Log4j2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.whispersystems.libsignal.*;
import org.whispersystems.libsignal.state.PreKeyRecord;
import org.whispersystems.libsignal.state.SignedPreKeyRecord;
import org.whispersystems.libsignal.util.KeyHelper;

import javax.annotation.PostConstruct;
import java.util.Base64;
import java.util.List;

@RestController
@Log4j2
public class ExampleBotManagementResource extends BaseResource {

    private static final Logger logger = LoggerFactory.getLogger(ExampleBotManagementResource.class);

    @Value("${bot.name}")
    private String botName;
    @Value("${bot.owner}")
    private String botOwnerName;
    @Value("${server.name}")
    private String serverName;
    @Value("${server.port}")
    private String serverPort;
    @Value("${bot_service.location}")
    private String botServiceLocation;

    @Autowired
    private SignalRepository signalRepository;

    @Autowired
    private BotTokenRepository botTokenRepository;

    @Autowired
    TimerService timerService;

    @Autowired
    CryptoService cryptoService;

    @PostConstruct
    private void init() throws InvalidKeyException {
        if (!this.signalRepository.containsIdentityKeyPair()) {
            this.signalRepository.saveLocalRegistrationId(KeyHelper.generateRegistrationId(true));
            IdentityKeyPair identityKeyPair =  KeyHelper.generateIdentityKeyPair();
            this.signalRepository.saveIdentityKeyPair(identityKeyPair);

            List<PreKeyRecord> preKeys = KeyHelper.generatePreKeys(2,100);
            for (PreKeyRecord preKeyRecord : preKeys) {
                this.signalRepository.storePreKey(preKeyRecord.getId(), preKeyRecord);
            }

            // SignedPreKey will always have id 1
            this.signalRepository.storeSignedPreKey(1, KeyHelper.generateSignedPreKey(identityKeyPair, 1));

            logger.info("Completed the signal setup");
        }
    }

    public ExampleBotManagementResource() {
    }



    @RequestMapping(method = RequestMethod.GET, value = INITIAL)
    public ResponseEntity<InitialKeyBundle> initial(@RequestParam String userId, @RequestParam String userName, @RequestParam String token) {
        if (!this.botOwnerName.equals(userName)) {
            log.error("User {} tried to register this bot", userName);
            throw new UnauthorizedException();
        }

        // create initialKeyBundle
        InitialKeyBundle initialKeyBundle = new InitialKeyBundle();
        initialKeyBundle.setRegistrationId(this.signalRepository.getLocalRegistrationId());
        initialKeyBundle.setIdentityKey(this.signalRepository.getPublicIdentityKeyBase64());
        initialKeyBundle.setPreKeys(this.signalRepository.loadPublicPreKeysBase64());

        try {
            // create SignedPreKey
            SignedPreKeyRecord signedPreKeyRecord = this.signalRepository.loadSignedPreKey(1);
            SignedPreKey signedPreKey = new SignedPreKey();
            signedPreKey.setKeyId(signedPreKeyRecord.getId());
            signedPreKey.setPublicKey(Base64.getEncoder().encodeToString(signedPreKeyRecord.getKeyPair().getPublicKey().serialize()));
            signedPreKey.setSignature(Base64.getEncoder().encodeToString(signedPreKeyRecord.getSignature()));
            initialKeyBundle.setSignedPreKey(signedPreKey);
        } catch (InvalidKeyIdException e) {
            log.error("Failed to load signed pre key:", e);
            throw new IllegalStateException("Failed to load signed pre key");
        }

        // saves the token from our new user
        this.botTokenRepository.save(new BotTokenDTO("token",token));

        logger.info("Initial setup with user {} completed", userName);
        return ResponseEntity.ok(initialKeyBundle);
    }

    @RequestMapping(method = RequestMethod.GET, value = TICTACTOE)
    public ResponseEntity<String> playTicTacToe(@RequestParam String userID) {

        return null;
    }

    @RequestMapping(method = RequestMethod.POST, value = TESTMESSAGE)
    public ResponseEntity<CryptoMessageDTO> handleTestMessage(@RequestBody CryptoMessageDTO messageDTO) {

        SignalProtocolAddress signalProtocolAddress = new SignalProtocolAddress(messageDTO.getFrom(), 1);
        SessionCipher sessionCipher = new SessionCipher(this.signalRepository, signalProtocolAddress);
        String message = this.cryptoService.decryptMessage(sessionCipher, messageDTO.getPayload(), messageDTO.getCryptoType().intValue());
        if (message == null) {
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Message could not be decrypted");
        }

        CryptoMessageDTO cryptoMessageDTO = this.cryptoService.encryptMessage(sessionCipher, message, this.botName);
        if (cryptoMessageDTO == null) {
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Message could not be encrypted");
        }

        return ResponseEntity.ok(cryptoMessageDTO);


    }

    @RequestMapping(method = RequestMethod.POST, value = MESSAGE)
    public ResponseEntity<CryptoMessageDTO> handleMessage(@RequestBody CryptoMessageDTO messageDTO) {

        SignalProtocolAddress signalProtocolAddress = new SignalProtocolAddress(messageDTO.getFrom(), 1);
        SessionCipher sessionCipher = new SessionCipher(this.signalRepository, signalProtocolAddress);
        String message = this.cryptoService.decryptMessage(sessionCipher, messageDTO.getPayload(), messageDTO.getCryptoType().intValue());
        if (message == null) {
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Message could not be decrypted");
        }

        TimeDTO timeDTO = this.cryptoService.parseTimeMessage(message);
        if (timeDTO == null) {
            return ResponseEntity.ok(this.cryptoService.encryptMessage(sessionCipher, usage(), this.botName));
        }

        CryptoMessageDTO cryptoMessageDTO = this.cryptoService.encryptMessage(sessionCipher, timeDTO.getMessage(), this.botName);
        if (cryptoMessageDTO == null) {
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, "Message could not be encrypted");
        }

        // get token so the bot service know that we are we
        String token = this.botTokenRepository.findByBotId("token").getToken();
        this.timerService.startTimerThread(timeDTO.getTime(), cryptoMessageDTO, token, this.botServiceLocation+BotPaths.SEND_MESSAGE+messageDTO.getFrom());
        String returnToBotService = "I will send you the message \""+timeDTO.getMessage()+"\" in "+timeDTO.getTime()+" seconds";
        return ResponseEntity.ok(this.cryptoService.encryptMessage(sessionCipher, returnToBotService, this.botName));

    }

    public String usage() {
        return "Sorry the format was wrong, please use the following:  \n" +
            "\"time:[time in seconds],message:[message you want to receive]\"";
    }


}
