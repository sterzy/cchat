package com.example.bot.resource.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SessionRecordDTO {
    @Id
    private String id;
    @Indexed
    private SessionAddressDTO sessionAddress;
    private String sessionData;
}
