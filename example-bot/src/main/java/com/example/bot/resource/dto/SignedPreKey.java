package com.example.bot.resource.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Class that holds the Base64 encoded public key as well as its signature.
 *
 * NOTE: This object is not annotated with @Document as it is embedded within the owning object.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignedPreKey {
    private Integer keyId;
    private String publicKey;
    private String signature;
}
