package com.example.bot.resource.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.whispersystems.libsignal.IdentityKey;
import org.whispersystems.libsignal.ecc.ECPublicKey;
import org.whispersystems.libsignal.state.SignedPreKeyRecord;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InitialDTO {
    private Integer registrationId;
    private List<ECPublicKey> preKeys;
    private ECPublicKey signedPreKey;
    private IdentityKey identityKey;

}
