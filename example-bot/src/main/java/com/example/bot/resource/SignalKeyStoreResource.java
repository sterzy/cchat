package com.example.bot.resource;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.util.HashMap;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignalKeyStoreResource {
    @Id
    private String id;
    private Integer nextKeyId;
    private Integer registrationId;
    private String identityKeyPair;
    private HashMap<Integer, String> signedPreKeyRecord;
    private HashMap<Integer, String> preKeyRecord;
}
