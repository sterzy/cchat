package com.example.bot.resource;


import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * A Base resource to define all paths and autowire services.
 */
@CrossOrigin
public abstract class BaseResource {


    public static final String PREFIX = "/api";

    public static final String INITIAL = PREFIX + "/initial";
    public static final String TICTACTOE = PREFIX + "/ticttactoe";
    public static final String MESSAGE = PREFIX + "/message";
    public static final String TESTMESSAGE = PREFIX + "/testmessage";
}
