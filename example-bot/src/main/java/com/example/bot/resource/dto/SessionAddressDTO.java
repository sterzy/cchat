package com.example.bot.resource.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SessionAddressDTO {
    private String name;
    private int deviceId;

    @Override
    public String toString() {
        return name + deviceId;
    }
}
