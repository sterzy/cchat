package com.example.bot.repository;

import com.example.bot.resource.dto.BotTokenDTO;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BotTokenRepository extends PagingAndSortingRepository<BotTokenDTO, String> {
    BotTokenDTO findByBotId(String botId);
}
