package com.example.bot.repository;

import com.example.bot.resource.SignalKeyStoreResource;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SignalKeyStoreRepository extends PagingAndSortingRepository<SignalKeyStoreResource, String> {
    SignalKeyStoreResource findFirstById(String id);
    boolean existsById(String id);
}
