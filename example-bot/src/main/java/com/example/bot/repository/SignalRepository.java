package com.example.bot.repository;

import java.io.IOException;
import java.util.*;

import com.example.bot.resource.SignalKeyStoreResource;
import com.example.bot.resource.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.whispersystems.libsignal.*;
import org.whispersystems.libsignal.state.*;
import org.whispersystems.libsignal.util.KeyHelper;

@Service
public class SignalRepository implements SignalProtocolStore {

    @Autowired
    private SignalSessionRepository signalSessionRepository;
    @Autowired
    private SignalKeyStoreRepository signalKeyStoreRepository;

    private SignalKeyStoreResource getSignalKeyStoreResource() {

        String id = "123456789";
        if (this.signalKeyStoreRepository.existsById(id)) {
            SignalKeyStoreResource s = this.signalKeyStoreRepository.findFirstById(id);

            return s;
        } else {
            SignalKeyStoreResource signalKeyStoreResource = new SignalKeyStoreResource();
            signalKeyStoreResource.setId(id);
            signalKeyStoreResource.setPreKeyRecord(new HashMap<>());
            signalKeyStoreResource.setSignedPreKeyRecord(new HashMap<>());
            signalKeyStoreResource.setNextKeyId(1);
            signalKeyStoreResource.setRegistrationId(0);
            signalKeyStoreResource.setIdentityKeyPair("");
            this.signalKeyStoreRepository.save(signalKeyStoreResource);
            return signalKeyStoreResource;
        }
    }

    public SignalRepository() {
    }

    @Override
    public IdentityKeyPair getIdentityKeyPair() {
        SignalKeyStoreResource s = this.getSignalKeyStoreResource();
        try {
            return new IdentityKeyPair(Base64.getDecoder().decode(s.getIdentityKeyPair()));
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean containsIdentityKeyPair() {
        SignalKeyStoreResource s = this.getSignalKeyStoreResource();
        return !s.getPreKeyRecord().isEmpty();

    }

    public String getPublicIdentityKeyBase64() {
        IdentityKeyPair identityKeyPair = this.getIdentityKeyPair();
        if (identityKeyPair != null) {
            return Base64.getEncoder().encodeToString(identityKeyPair.getPublicKey().serialize());
        }
        return null;
    }


    public boolean saveIdentityKeyPair(IdentityKeyPair identityKeyPair) {
        SignalKeyStoreResource s = this.getSignalKeyStoreResource();
        s.setIdentityKeyPair(Base64.getEncoder().encodeToString(identityKeyPair.serialize()));
        this.signalKeyStoreRepository.save(s);

        return true;
    }

    @Override
    public int getLocalRegistrationId() {
        SignalKeyStoreResource s = this.getSignalKeyStoreResource();
        if (s.getRegistrationId() > 0) {
            return s.getRegistrationId();
        }
        return -1;
    }

    public void saveLocalRegistrationId(int registrationId) {
        SignalKeyStoreResource s = this.getSignalKeyStoreResource();
        s.setRegistrationId(registrationId);
        this.signalKeyStoreRepository.save(s);
    }

    @Override
    public boolean saveIdentity(SignalProtocolAddress signalProtocolAddress, IdentityKey identityKey) {
        return false;
    }

    @Override
    public boolean isTrustedIdentity(SignalProtocolAddress signalProtocolAddress, IdentityKey identityKey, Direction direction) {
        return true;
    }

    @Override
    public IdentityKey getIdentity(SignalProtocolAddress signalProtocolAddress) {
        return null;
    }

    @Override
    public PreKeyRecord loadPreKey(int i) throws InvalidKeyIdException {
        SignalKeyStoreResource s = this.getSignalKeyStoreResource();
        if (s.getPreKeyRecord().containsKey(i)) {
            try {

                return new PreKeyRecord(Base64.getDecoder().decode(s.getPreKeyRecord().get(i)));
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } else throw new InvalidKeyIdException("PreKeyId " + i + " is not valid");
    }


    public List<PreKeyRecord> loadPreKeys() {
        SignalKeyStoreResource s = this.getSignalKeyStoreResource();
        if (!s.getPreKeyRecord().isEmpty()) {
            try {
                ArrayList<PreKeyRecord> preKeyRecords = new ArrayList<>();
                for (String preKey : s.getPreKeyRecord().values()) {
                    preKeyRecords.add(new PreKeyRecord(Base64.getDecoder().decode(preKey)));
                }
                return preKeyRecords;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } else return null;
    }

    public List<PreKey> loadPublicPreKeysBase64() {
        List<PreKeyRecord> preKeyRecords = this.loadPreKeys();
        ArrayList<PreKey> publicPreKeyBase64 = new ArrayList<>();

        for (PreKeyRecord key : preKeyRecords) {
            PreKey preKey = new PreKey();
            preKey.setPublicKey(Base64.getEncoder().encodeToString(key.getKeyPair().getPublicKey().serialize()));
            preKey.setKeyId(key.getId());
            publicPreKeyBase64.add(preKey);

        }

        return publicPreKeyBase64;
    }


    @Override
    public void storePreKey(int i, PreKeyRecord preKeyRecord) {
        SignalKeyStoreResource s = this.getSignalKeyStoreResource();
        if (s.getNextKeyId() <= i) {
            s.getPreKeyRecord().put(i, Base64.getEncoder().encodeToString(preKeyRecord.serialize()));
            s.setNextKeyId(i+1);
        } else {
            s.getPreKeyRecord().put(s.getNextKeyId(), Base64.getEncoder().encodeToString(preKeyRecord.serialize()));
            s.setNextKeyId(s.getNextKeyId()+1);
        }
        this.signalKeyStoreRepository.save(s);
    }

    @Override
    public boolean containsPreKey(int i) {
        SignalKeyStoreResource s = this.getSignalKeyStoreResource();
        return s.getPreKeyRecord().containsKey(i);
    }

    @Override
    public void removePreKey(int i) {
        SignalKeyStoreResource s = this.getSignalKeyStoreResource();
        s.getPreKeyRecord().remove(i);
        this.signalKeyStoreRepository.save(s);
    }

    @Override
    public SessionRecord loadSession(SignalProtocolAddress signalProtocolAddress) {
        SessionAddressDTO sessionAddressDTO = new SessionAddressDTO();
        sessionAddressDTO.setName(signalProtocolAddress.getName());
        sessionAddressDTO.setDeviceId(signalProtocolAddress.getDeviceId());
        SessionRecordDTO sessionRecordDTO = this.signalSessionRepository.findBySessionAddress(sessionAddressDTO);

        if (sessionRecordDTO == null) {
            return new SessionRecord();
        }
        try {
            return new SessionRecord(Base64.getDecoder().decode(sessionRecordDTO.getSessionData()));
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public List<Integer> getSubDeviceSessions(String s) {
        return null;
    }

    @Override
    public void storeSession(SignalProtocolAddress signalProtocolAddress, SessionRecord sessionRecord) {
        SessionAddressDTO addressDTO = new SessionAddressDTO();
        addressDTO.setName(signalProtocolAddress.getName());
        addressDTO.setDeviceId(signalProtocolAddress.getDeviceId());
        String data = Base64.getEncoder().encodeToString(sessionRecord.serialize());
        SessionRecordDTO sessionRecordDTO = new SessionRecordDTO();
        sessionRecordDTO.setSessionAddress(addressDTO);
        sessionRecordDTO.setSessionData(data);

        // set id so we update if we already have saved a session for this user
        sessionRecordDTO.setId(addressDTO.toString());
        this.signalSessionRepository.save(sessionRecordDTO);
    }

    @Override
    public boolean containsSession(SignalProtocolAddress signalProtocolAddress) {
        SessionAddressDTO sessionAddressDTO = new SessionAddressDTO();
        sessionAddressDTO.setName(signalProtocolAddress.getName());
        sessionAddressDTO.setDeviceId(signalProtocolAddress.getDeviceId());
        if (this.signalSessionRepository.existsBySessionAddress(sessionAddressDTO)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void deleteSession(SignalProtocolAddress signalProtocolAddress) {
        SessionAddressDTO sessionAddressDTO = new SessionAddressDTO();
        sessionAddressDTO.setName(signalProtocolAddress.getName());
        sessionAddressDTO.setDeviceId(signalProtocolAddress.getDeviceId());
        this.signalSessionRepository.deleteBySessionAddress(sessionAddressDTO);
    }

    @Override
    public void deleteAllSessions(String s) {
        this.signalSessionRepository.deleteAll();
    }

    @Override
    public SignedPreKeyRecord loadSignedPreKey(int i) throws InvalidKeyIdException {
        SignalKeyStoreResource s = this.getSignalKeyStoreResource();
        if (!s.getSignedPreKeyRecord().isEmpty()) {
            try {
                HashMap<Integer, String> signedPreKeys = s.getSignedPreKeyRecord();
                return new SignedPreKeyRecord(Base64.getDecoder().decode(signedPreKeys.get(i)));
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } else throw new InvalidKeyIdException("SignedPreKeyId " + i + " is not valid");
    }

    @Override
    public List<SignedPreKeyRecord> loadSignedPreKeys() {
        SignalKeyStoreResource s = this.getSignalKeyStoreResource();
        if (!s.getSignedPreKeyRecord().isEmpty()) {
            try {
                ArrayList<SignedPreKeyRecord> signedPreKeyRecords = new ArrayList<>();
                for (String signedPreKey : s.getSignedPreKeyRecord().values()) {
                    signedPreKeyRecords.add(new SignedPreKeyRecord(Base64.getDecoder().decode(signedPreKey)));
                }
                return signedPreKeyRecords;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } else return null;
    }

    @Override
    public void storeSignedPreKey(int i, SignedPreKeyRecord signedPreKeyRecord) {
        SignalKeyStoreResource s = this.getSignalKeyStoreResource();
        s.getSignedPreKeyRecord().put(i, Base64.getEncoder().encodeToString(signedPreKeyRecord.serialize()));
        this.signalKeyStoreRepository.save(s);
    }

    @Override
    public boolean containsSignedPreKey(int i) {
        SignalKeyStoreResource s = this.getSignalKeyStoreResource();
        return s.getSignedPreKeyRecord().containsKey(i);
    }

    @Override
    public void removeSignedPreKey(int i) {
        SignalKeyStoreResource s = this.getSignalKeyStoreResource();
        s.getSignedPreKeyRecord().remove(i);
        this.signalKeyStoreRepository.save(s);
    }

}
