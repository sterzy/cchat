package com.example.bot.repository;

import com.example.bot.resource.dto.SessionAddressDTO;
import com.example.bot.resource.dto.SessionRecordDTO;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.whispersystems.libsignal.SignalProtocolAddress;
import org.whispersystems.libsignal.state.SessionRecord;

public interface SignalSessionRepository extends PagingAndSortingRepository<SessionRecordDTO, String> {
    SessionRecordDTO findBySessionAddress(SessionAddressDTO sessionAddress);
    boolean existsBySessionAddress(SessionAddressDTO sessionAddress);
    void deleteBySessionAddress(SessionAddressDTO sessionAddress);

}
