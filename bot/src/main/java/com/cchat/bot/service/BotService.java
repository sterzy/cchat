package com.cchat.bot.service;

import com.cchat.bot.domain.Bot;
import com.cchat.bot.domain.WhitelistedBots;
import com.cchat.bot.repository.BotRepository;
import com.cchat.bot.repository.WhitelistedBotsRepository;
import com.cchat.bot.resource.dto.PreKeyBundleDTO;
import com.cchat.common.exceptions.http.NotFoundException;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Service
@Log4j2
@Repository
public class BotService {

    @Autowired
    private BotRepository botRepository;

    @Autowired
    private WhitelistedBotsRepository whitelistedBotsRepository;

    @Transactional
    public Bot saveBot(Bot bot) {
        return this.botRepository.save(bot);
    }

    @Transactional(readOnly = true)
    public Optional<Bot> getBotByIdOrName(String botIdOrName) {
        Optional<Bot> bot = this.getBotById(botIdOrName);
        if (bot.isPresent()) {
            return bot;
        }

        return this.getBotByName(botIdOrName);
    }

    @Transactional(readOnly = true)
    public Optional<Bot> getBotById(String botId) {
        return this.botRepository.findById(botId);
    }

    @Transactional(readOnly = true)
    public List<Bot> getBotsByNameLike(String botName) {
        return this.botRepository.findAllAvailableByNameLike(botName);
    }

    @Transactional(readOnly = true)
    public Optional<Bot> getBotByName(String botName) {
        return this.botRepository.findByName(botName);
    }

    @Transactional(readOnly = true)
    public Optional<Bot> getAvailableBotByName(String botName) {
        return this.botRepository.findAvailableBotByName(botName);
    }

    @Transactional(readOnly = true)
    public Optional<Bot> getBotByToken(String token) {
        return this.botRepository.findBotByToken(token);
    }

    @Transactional
    public void deleteBotById(String botId) {
        this.botRepository.deleteById(botId);
    }

    @Transactional(readOnly = true)
    public List<Bot> getBotsForUser(String uuid) {
        return this.botRepository.findAllByRegisteredByUUID(uuid);
    }

    @Transactional(readOnly = true)
    public boolean botWithNameExists(String name) {
        return this.botRepository.findByName(name).isPresent();
    }

    /**
     * This function will return a PreKeyBundle of a bot. I the bot could not be found, or the bot has not PreKeys
     * left, a {@link NotFoundException} will be thrown.
     * <p>
     * In case a bot is found, the PreKeBundle will be removed from the list of PreKeys, meaning that every time this
     * method is called will return a unique PreKeyBundle for the bot.
     *
     * @param botId The ID of the bot to ge the PreKeyBundle for.
     * @return A PreKeyBundle from the bot.
     */
    @Transactional
    public PreKeyBundleDTO getPreKeyForBot(String botId) {
        Bot bot = this.botRepository.findByIdOrNameAndPopPreKey(botId).orElseThrow(() -> new NotFoundException("No bot found with id=" + botId));

        if (bot.getPreKeys().size() == 0) {
            throw new NotFoundException("The bot has no PreKeys left");
        }

        PreKeyBundleDTO preKeyBundle = new PreKeyBundleDTO();
        preKeyBundle.setIdentityKey(bot.getIdentityKey());
        preKeyBundle.setPreKey(bot.getPreKeys().get(0));
        preKeyBundle.setSignedPreKey(bot.getSignedPreKey());
        preKeyBundle.setRegistrationId(bot.getRegistrationId());

        return preKeyBundle;
    }

    /**
     * This method will add a bot to a user's whitelist.
     *
     * @param userName The id of the user to add the bot to the whitelist for.
     * @param bot      The bot to add to the whitelist.
     */
    @Transactional
    public void addBotToWhitelist(String userName, Bot bot) {
        WhitelistedBots whitelist = this.whitelistedBotsRepository.findById(userName)
            .orElse(new WhitelistedBots(userName, new HashMap<>()));

        whitelist.getAddedBots().put(bot.getId(), true);

        this.whitelistedBotsRepository.save(whitelist);
    }

    /**
     * Checks if the bot is whitelisted for a user.
     *
     * @param userName The user to check for.
     * @param bot    The bot to search in the whitelist.
     * @return True if the user added the bot to his/her whitelist, false otherwise.
     */
    @Transactional(readOnly = true)
    public boolean isBotWhitelisted(String userName, Bot bot) {
        return this.whitelistedBotsRepository.userHasBotWhitelisted(userName, bot.getId());
    }
}
