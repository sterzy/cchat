package com.cchat.bot.repository;

import com.cchat.bot.domain.WhitelistedBots;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface WhitelistedBotsRepository extends PagingAndSortingRepository<WhitelistedBots, String>, ComplexWhitelistedBotsRepository {

}
