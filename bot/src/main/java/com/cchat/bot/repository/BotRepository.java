package com.cchat.bot.repository;

import com.cchat.bot.domain.Bot;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

public interface BotRepository extends PagingAndSortingRepository<Bot, String>, ComplexBotRepository {

    @Query("{id: ?0, isActive: true, idPublic: true}")
    Optional<Bot> findAvailableBotById(String botId);


    default List<Bot> findAllAvailableByNameLike(String name) {
        return this.findAllAvailableByNameWithEscapedRegex(".*" + Pattern.quote(name) + ".*");
    }

    @Query("{name: {$regex: ?0}, isActive: true, isPublic: true}")
    List<Bot> findAllAvailableByNameWithEscapedRegex(String name);

    @Query("{name: ?0, isActive: true, isPublic: true}")
    Optional<Bot> findAvailableBotByName(String name);

    List<Bot> findAllByRegisteredByUUID(String uuid);

    Optional<Bot> findByName(String name);

    Optional<Bot> findBotByToken(String token);
}
