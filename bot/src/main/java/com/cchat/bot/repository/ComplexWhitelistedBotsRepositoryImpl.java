package com.cchat.bot.repository;

import com.cchat.bot.domain.WhitelistedBots;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

public class ComplexWhitelistedBotsRepositoryImpl implements ComplexWhitelistedBotsRepository {

    @Autowired
    private MongoTemplate template;

    @Override
    public boolean userHasBotWhitelisted(String userName, String botId) {

        Query query = new Query();
        query.addCriteria(Criteria.where("userName").is(userName));
        query.addCriteria(Criteria.where("addedBots." + botId).exists(true));

        return this.template.findOne(query, WhitelistedBots.class) != null;
    }
}
