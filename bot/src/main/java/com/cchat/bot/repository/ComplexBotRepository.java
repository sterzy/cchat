package com.cchat.bot.repository;

import com.cchat.bot.domain.Bot;

import java.util.Optional;

/**
 * This interface is for Queries that are too complex to be implemented via Spring Data alone.
 */
public interface ComplexBotRepository {

    /**
     * This method will find a bot first by ID, then by name if no bot is found, remove one PreKey from the bot and
     * returns the old bot. This is an atomic operation.
     *
     * @param botIdOrName The ID or name of the bot.
     * @return The Bot as it were before the modification, or empty if the bot was not found.
     */
    Optional<Bot> findByIdOrNameAndPopPreKey(String botIdOrName);
}
