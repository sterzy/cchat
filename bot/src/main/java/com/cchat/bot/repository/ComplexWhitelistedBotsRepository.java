package com.cchat.bot.repository;

public interface ComplexWhitelistedBotsRepository {

    boolean userHasBotWhitelisted(String userName, String botId);
}
