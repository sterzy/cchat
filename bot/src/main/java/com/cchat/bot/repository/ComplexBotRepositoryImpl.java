package com.cchat.bot.repository;

import com.cchat.bot.domain.Bot;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.Optional;

public class ComplexBotRepositoryImpl implements ComplexBotRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * This method will find a bot first by ID, then by name if no bot is found, remove one PreKey from the bot and
     * returns the old bot. This is an atomic operation.
     *
     * Only active bots are found by this method.
     *
     * @param botIdOrName The ID or name of the bot.
     * @return The Bot as it were before the modification, or empty if the bot was not found.
     */
    public Optional<Bot> findByIdOrNameAndPopPreKey(String botIdOrName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(botIdOrName));
        query.addCriteria(Criteria.where("isActive").is(true));
        Update update = new Update();
        update.pop("preKeys", Update.Position.FIRST);

        Bot bot = this.mongoTemplate.findAndModify(query, update, Bot.class);

        if (bot != null) {
            // Found bot by ID, return it.
            return Optional.of(bot);
        }

        query = new Query();
        query.addCriteria(Criteria.where("name").is(botIdOrName));
        query.addCriteria(Criteria.where("isActive").is(true));

        return Optional.ofNullable(this.mongoTemplate.findAndModify(query, update, Bot.class));
    }
}
