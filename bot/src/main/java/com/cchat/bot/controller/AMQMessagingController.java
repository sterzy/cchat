package com.cchat.bot.controller;

import com.cchat.bot.domain.Bot;
import com.cchat.bot.service.BotService;
import com.cchat.bot.util.BotFacade;
import com.cchat.common.dto.amq.BotCryptoMessageDTO;
import com.cchat.common.dto.websocket.CryptoMessageDTO;
import com.cchat.common.exceptions.http.BadGatewayException;
import com.cchat.common.util.Constants;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

@Log4j2
@Component
public class AMQMessagingController {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private BotService botService;

    @Autowired
    private RestTemplate restTemplate;

    /**
     * Docu for the annotation: https://docs.spring.io/spring-amqp/api/org/springframework/amqp/rabbit/annotation/RabbitListener.html
     * queue = which queue we want to listen to
     * autoStartup = should we start immediately
     *
     * @param content The message sent to a bot.
     */
    @RabbitListener(queues = Constants.BOT_QUEUE, autoStartup = "true")
    public void processMessage(BotCryptoMessageDTO content) {
        Optional<Bot> maybeBot = this.botService.getBotByIdOrName(content.getBot());

        // Check if the bot exits
        if (maybeBot.isEmpty()) {
            this.messagingTemplate.convertAndSendToUser(
                content.getMessage().getFrom(),
                Constants.ERROR_EP,
                "No bot found for '" + content.getBot() + "'"
            );
            return;
        }

        Bot bot = maybeBot.get();

        // Check if the bot is active
        if (!bot.getIsActive()) {
            this.messagingTemplate.convertAndSendToUser(
                content.getMessage().getFrom(),
                Constants.ERROR_EP,
                "Bot '" + content.getBot() + "' is not active"
            );
            return;
        }

        // Send the message to the bot
        BotFacade facade = new BotFacade(bot, restTemplate);
        CryptoMessageDTO response;
        try {
            response = facade.sendMessage(content.getMessage());
        } catch (BadGatewayException e) {
            this.messagingTemplate.convertAndSendToUser(
                content.getMessage().getFrom(),
                Constants.ERROR_EP,
                "Sorry, this bots seems to be offline right now"
            );
            return;
        }

        // Check if the response is non-empty
        if (response != null) {
            this.messagingTemplate.convertAndSendToUser(content.getMessage().getFrom(), Constants.CHAT_EP, response);
        }
    }

}
