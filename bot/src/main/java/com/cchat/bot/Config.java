package com.cchat.bot;

import com.cchat.common.config.ConfigFactory;
import lombok.extern.log4j.Log4j2;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestTemplate;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableAsync
@EnableMongoRepositories
@EnableRabbit
class Config {
    /**
     * Dozer Mapper is used to map DTOs to JSON and vise versa.
     */
    @Bean
    public Mapper getDozerBeanMapper() {
        return new DozerBeanMapper();
    }

    /**
     * Swagger is a utility to automatically document API endpoints and call them via a website.
     *
     * Visit /swagger-ui.html to view the site.
     */
    @Bean
    public Docket swagger() {
        return ConfigFactory.swagger();
    }

    @Bean
    public RestTemplate singleTonRestTemplate() {
        return new RestTemplate();
    }

    @Bean
    public Queue botIncoming() {

        // This declares a queue with:
        // first-param: a name
        // seceond-param: should the queue survive a server restart?
        // third-param: should the queue also deliver messages to other connections than the one that declared it?
        // fourth-param: should the queue be deleted if no one is listening on it anymore?
        return new Queue("bot-incoming", true, false, true);

    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
