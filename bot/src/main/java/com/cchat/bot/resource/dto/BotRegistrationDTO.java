package com.cchat.bot.resource.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BotRegistrationDTO {
    @NotBlank(message = "The name of the bot must not be null")
    @NotNull(message = "The name of the bot must be set")
    private String name;
    @Mapping("webHookRoot")
    @NotBlank(message = "The name of the bot must not be null")
    @NotNull(message = "The name of the bot must be set")
    private String webHookRootURL;
    private String description;
    @NotNull(message = "Please specify if this bot is public or not")
    private Boolean isPublic;
}
