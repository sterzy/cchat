package com.cchat.bot.resource.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BotSearchResponseDTO {
    private String id;
    private String name;
    private String description;
}
