package com.cchat.bot.resource;

import com.cchat.bot.domain.Bot;
import com.cchat.bot.resource.dto.*;
import com.cchat.bot.util.BotFacade;
import com.cchat.bot.util.TokenUtil;
import com.cchat.common.dto.websocket.CryptoMessageDTO;
import com.cchat.common.exceptions.http.*;
import com.cchat.common.security.SecurityUser;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.validation.ConstraintViolation;
import javax.validation.Valid;
import javax.validation.Validator;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@Log4j2
public class BotManagementResource extends BaseResource {

    @Autowired
    private Validator validator;

    @Autowired
    private RestTemplate restTemplate;

    /**
     * This endpoint registers a new bot.
     * <p>
     * The bot is connected to the currently online user.
     *
     * @param user The currently logged in user.
     * @param reg  The Registration request
     * @return The registered bot.
     */
    @RequestMapping(method = RequestMethod.POST, value = BOTS)
    public ResponseEntity<MyBotResponseDTO> registerBot(@AuthenticationPrincipal SecurityUser user, @RequestBody @Valid BotRegistrationDTO reg) {

        if (this.botService.botWithNameExists(reg.getName())) {
            throw new ConflictException("A bot with name '" + reg.getName() + "' already exists");
        }

        try {
            new URL(reg.getWebHookRootURL());
        } catch (MalformedURLException e) {
            throw new BadRequestException("Given URL '" + reg.getWebHookRootURL() + "' is not a URL");
        }

        if (!reg.getWebHookRootURL().endsWith("/")) {
            reg.setWebHookRootURL(reg.getWebHookRootURL() + "/");
        }

        // Set all fields of the new bot
        Bot newBot = this.mapper.toDomain(reg, Bot.class);
        newBot.setRegisteredBy(user.getUsername());
        newBot.setRegisteredByUUID(user.getId());
//        newBot.setWebHookRoot(reg.getWebHookRootURL());
        newBot.setIsActive(false);
//        newBot.setIsPublic(reg.getIsPublic());
        newBot.setToken(TokenUtil.generateToken());

        // Create a new facade with the bot
        BotFacade facade = new BotFacade(newBot, restTemplate);

        // Get the InitialKeyBundle from the bot
        InitialKeyBundle keyBundle = facade.getInitialKeyBundle();

        // Validate the returned initial key bundle via the javax.validation
        Set<ConstraintViolation<InitialKeyBundle>> violations = this.validator.validate(keyBundle);

        if (!violations.isEmpty()) {
            String errorMsg = violations
                .stream()
                .map(violation -> violation.getPropertyPath() +  violation.getMessage())
                .collect(Collectors.joining("\n"));
            throw new BadRequestException("Failed to validate key bundle returned by bot: " + errorMsg);
        }

        // All valid, set signal keys to store
        newBot.setIdentityKey(keyBundle.getIdentityKey());
        newBot.setRegistrationId(keyBundle.getRegistrationId());
        newBot.setPreKeys(keyBundle.getPreKeys());
        newBot.setSignedPreKey(keyBundle.getSignedPreKey());

        // Save the new bot
        Bot savedBot = this.botService.saveBot(newBot);

        return ResponseEntity.ok(this.mapper.toDto(savedBot, MyBotResponseDTO.class));
    }

    /**
     * Deletes a bot.
     * <p>
     * The bot is only deleted if the bot was registered by the user.
     *
     * @param user  The currently logged in user.
     * @param botId The ID of the bot to delete.
     * @return 200 if the bot was deleted. 404 if the bot was not found. 403 if the bot was not registered by this user.
     */
    @RequestMapping(method = RequestMethod.DELETE, value = BOT_BY_ID)
    public ResponseEntity<Void> deleteBot(@AuthenticationPrincipal SecurityUser user, @PathVariable String botId) {

        Bot bot = this.botService.getBotByIdOrName(botId).orElseThrow(() -> new NotFoundException("No bot found with id=" + botId));

        if (!bot.getRegisteredByUUID().equals(user.getId())) {
            throw new ForbiddenException("This user did not register the bot with id=" + botId);
        }

        this.botService.deleteBotById(bot.getId());

        return ResponseEntity.ok().build();
    }


    /**
     * Marks a bot as disabled.
     *
     * @param user  The currently logged in user.
     * @param botId The ID of the bot to disable.
     * @return 200 if the bot was successfully disabled. 404 if no bot was found. 403 if the user did not register the bot.
     */
    @RequestMapping(method = RequestMethod.POST, value = BOT_DISABLE_BY_ID)
    public ResponseEntity<MyBotResponseDTO> disable(@AuthenticationPrincipal SecurityUser user, @PathVariable String botId) {
        Bot bot = this.botService.getBotByIdOrName(botId).orElseThrow(() -> new NotFoundException("No bot found with id=" + botId));

        if (!bot.getRegisteredByUUID().equals(user.getId())) {
            throw new ForbiddenException("This user did not register the bot with id=" + botId);
        }

        if (!bot.getIsActive()) {
            return ResponseEntity.ok(
                this.mapper.toDto(bot, MyBotResponseDTO.class)
            );
        }

        bot.setIsActive(false);

        return ResponseEntity.ok(
            this.mapper.toDto(this.botService.saveBot(bot), MyBotResponseDTO.class)
        );
    }

    /**
     * Marks a bot as active.
     *
     * @param user  The currently logged in user.
     * @param botId The ID of the bot to activate.
     * @return 200 if the bot was successfully activated. 404 if no bot was found. 403 if the user did not register the bot.
     */
    @RequestMapping(method = RequestMethod.POST, value = BOT_ACTIVATE_BY_ID)
    public ResponseEntity<MyBotResponseDTO> activate(@AuthenticationPrincipal SecurityUser user, @PathVariable String botId) {
        Bot bot = this.botService.getBotByIdOrName(botId).orElseThrow(() -> new NotFoundException("No bot found with id=" + botId));

        if (!bot.getRegisteredByUUID().equals(user.getId())) {
            throw new ForbiddenException("This user did not register the bot with id=" + botId);
        }

        if (bot.getIsActive()) {
            return ResponseEntity.ok(
                this.mapper.toDto(bot, MyBotResponseDTO.class)
            );
        }

        bot.setIsActive(true);

        return ResponseEntity.ok(
            this.mapper.toDto(this.botService.saveBot(bot), MyBotResponseDTO.class)
        );
    }

    /**
     * Marks a bot as private.
     *
     * @param user  The currently logged in user.
     * @param botId The ID of the bot to privatise.
     * @return 200 if the bot was successfully disabled. 404 if no bot was found. 403 if the user did not register the bot.
     */
    @RequestMapping(method = RequestMethod.POST, value = BOT_PRIVATISE_BY_ID)
    public ResponseEntity<MyBotResponseDTO> privatise(@AuthenticationPrincipal SecurityUser user, @PathVariable String botId) {
        Bot bot = this.botService.getBotByIdOrName(botId).orElseThrow(() -> new NotFoundException("No bot found with id=" + botId));

        if (!bot.getRegisteredByUUID().equals(user.getId())) {
            throw new ForbiddenException("This user did not register the bot with id=" + botId);
        }

        if (!bot.getIsPublic()) {
            return ResponseEntity.ok(
                this.mapper.toDto(bot, MyBotResponseDTO.class)
            );
        }

        bot.setIsPublic(false);

        return ResponseEntity.ok(
            this.mapper.toDto(this.botService.saveBot(bot), MyBotResponseDTO.class)
        );
    }

    /**
     * Marks a bot as public.
     *
     * @param user  The currently logged in user.
     * @param botId The ID of the bot to publicise.
     * @return 200 if the bot was successfully activated. 404 if no bot was found. 403 if the user did not register the bot.
     */
    @RequestMapping(method = RequestMethod.POST, value = BOT_PUBLICISE_BY_ID)
    public ResponseEntity<MyBotResponseDTO> publicise(@AuthenticationPrincipal SecurityUser user, @PathVariable String botId) {
        Bot bot = this.botService.getBotByIdOrName(botId).orElseThrow(() -> new NotFoundException("No bot found with id=" + botId));

        if (!bot.getRegisteredByUUID().equals(user.getId())) {
            throw new ForbiddenException("This user did not register the bot with id=" + botId);
        }

        if (bot.getIsPublic()) {
            return ResponseEntity.ok(
                this.mapper.toDto(bot, MyBotResponseDTO.class)
            );
        }

        bot.setIsPublic(true);

        return ResponseEntity.ok(
            this.mapper.toDto(this.botService.saveBot(bot), MyBotResponseDTO.class)
        );
    }

    /**
     * Returns a list of bots that are registered by this user.
     *
     * @param user The currently logged in user.
     * @return A list of bots that where registered by this user.
     */
    @RequestMapping(method = RequestMethod.GET, value = BOTS_BY_ME)
    public ResponseEntity<List<MyBotResponseDTO>> getMyBots(@AuthenticationPrincipal SecurityUser user) {
        return ResponseEntity.ok(
            mapper.toDto(this.botService.getBotsForUser(user.getId()), MyBotResponseDTO.class)
        );
    }

    /**
     * Endpoint to get a single bot.
     * <p>
     * If the Bot is registered by the use, the response will contain additional information (see
     * {@link MyBotResponseDTO}).
     *
     * @param user  The user who sent the request.
     * @param botId The ID of the bot to get.
     * @return The found bot or a 404 response if the bot was not found.
     */
    @RequestMapping(method = RequestMethod.GET, value = BOT_BY_ID)
    public ResponseEntity<BotDetailsDTO> getBotById(@AuthenticationPrincipal SecurityUser user, @PathVariable String botId) {

        // Find all bots (even disabled/private ones)
        Bot bot = this.botService.getBotByIdOrName(botId).orElseThrow(() -> new NotFoundException("No bot found with id=" + botId));

        // Convert the bot to a DTO
        // This also checks if the user has access to the bot and if the user owns the bot
        BotDetailsDTO botDto = this.convertBotToDTO(bot, user);

        return ResponseEntity.ok(botDto);
    }

    /**
     * Adds a bot for a user. This also means, that this bot is whitelisted for this user.
     *
     * @param user  The user who sent the request.
     * @param botId The ID of the bot to add.
     * @return The found bot or a 404 response if the bot was not found.
     */
    @RequestMapping(method = RequestMethod.GET, value = BOT_ADD_BOT)
    public ResponseEntity<BotDetailsDTO> addBot(@AuthenticationPrincipal SecurityUser user, @PathVariable String botId) {

        // Find all bots (even disabled/private ones)
        Bot bot = this.botService.getBotByIdOrName(botId).orElseThrow(() -> new NotFoundException("No bot found with id=" + botId));

        // Convert the bot to a DTO
        // This also checks if the user has access to the bot and if the user owns the bot
        BotDetailsDTO botDto = this.convertBotToDTO(bot, user);

        // Add the bot to the whitelist of the user
        this.botService.addBotToWhitelist(user.getUsername(), bot);

        return ResponseEntity.ok(botDto);
    }

    /**
     * Test if the bot has already been added.
     *
     * This endpoint is meant for testing only.
     *
     * @param user  The user who sent the request.
     * @param botId The bot to check if it has been added
     * @return True if the bot has been added.
     */
    @RequestMapping(method = RequestMethod.GET, value = BOT_IS_ADDED)
    public ResponseEntity<Boolean> isBotAdded(@AuthenticationPrincipal SecurityUser user, @PathVariable String botId) {
        Bot bot = this.botService
            .getBotByIdOrName(botId)
            .orElseThrow(() -> new NotFoundException("No bot found with id=" + botId));
        return ResponseEntity.ok(this.botService.isBotWhitelisted(user.getUsername(), bot));
    }

    /**
     * Find bots which have a name similar to the given one.
     *
     * Only available bots are returned by this endpoint - even if the owner of the bot searches by name.
     *
     * @param query The name of the bots to find.
     * @return A list of bots that match the name.
     */
    @RequestMapping(method = RequestMethod.GET, value = BOT_SEARCH)
    public ResponseEntity<List<BotSearchResponseDTO>> findBotsByName(
        @RequestParam("q") String query,
        @RequestParam(value = "exact", defaultValue = "false") boolean exact
    ) {

        if (exact) {
            Bot bot = this.botService.getAvailableBotByName(query).orElseThrow(() -> new NotFoundException("No bot found with name=" + query));
            return ResponseEntity.ok(
                this.mapper.toDto(Collections.singletonList(bot), BotSearchResponseDTO.class)
            );
        }
        return ResponseEntity.ok(
            this.mapper.toDto(this.botService.getBotsByNameLike(query), BotSearchResponseDTO.class)
        );
    }

    /**
     * Returns a PreKeyBundle for a bot.
     *
     * @param botId The ID of the bot to get the PreKeyBundle from
     * @return A PreKeyBundle on success. 404 if the bot was not found or the bot has no PreKeys.
     */
    @RequestMapping(method = RequestMethod.GET, value = BOT_PRE_KEY_BUNDLE)
    public ResponseEntity<PreKeyBundleDTO> getPreKeyBundle(@PathVariable String botId) {
        return ResponseEntity.ok(this.botService.getPreKeyForBot(botId));
    }

    /**
     * This endpoint will send a test message to the bot.
     * <p>
     * The user can specify what the message sent to the bot is.
     * <p>
     * This endpoint can only by called for a bot by the user who created this bot.
     *
     * @param user    The user who sent the request.
     * @param botId   The ID of the bot to send the message to.
     * @param message The message to send to the bot.
     * @return On success the message returned by the bot.
     */
    @RequestMapping(method = RequestMethod.POST, value = BOT_TEST_MESSAGE)
    public ResponseEntity<CryptoMessageDTO> sendTestMessage(
        @AuthenticationPrincipal SecurityUser user,
        @PathVariable String botId,
        @RequestBody CryptoMessageDTO message
    ) {
        Bot bot = this.botService.getBotByIdOrName(botId).orElseThrow(() -> new NotFoundException("No bot found with id=" + botId));

        if (!user.getId().equals(bot.getRegisteredByUUID())) {
            throw new UnauthorizedException("This bot was not created by you");
        }

        message.setFrom(user.getUsername());

        return ResponseEntity.ok(new BotFacade(bot, restTemplate).sendTestMessage(message));
    }

    /**
     * This helper function will convert the bot to a BotDetailsDTO.
     *
     * If the given user registered the bot, then the returned value will contain more infos (see {@link MyBotResponseDTO}).
     * Else if the bot is private or disabled, then a NotFoundException will be thrown.
     * Else the bot will be converted to a {@link BotDetailsDTO}.
     *
     * @param bot     The bot to convert.
     * @param forUser The user for whom the conversation should be done.
     * @return The converted bot.
     */
    private BotDetailsDTO convertBotToDTO(Bot bot, SecurityUser forUser) {
        if (bot.getRegisteredByUUID().equals(forUser.getId())) {
            return this.mapper.toDto(bot, MyBotResponseDTO.class);
        } else if (!bot.getIsActive()) {
            throw new NotFoundException("No bot found with id=" + bot.getId());
        } else {
            return this.mapper.toDto(bot, BotDetailsDTO.class);
        }
    }
}
