package com.cchat.bot.resource.dto;

import com.cchat.bot.domain.PreKey;
import com.cchat.bot.domain.SignedPreKey;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PreKeyBundleDTO {
    private Integer registrationId;
    private String identityKey;
    private PreKey preKey;
    private SignedPreKey signedPreKey;

}
