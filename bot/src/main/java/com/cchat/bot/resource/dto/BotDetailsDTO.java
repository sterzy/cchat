package com.cchat.bot.resource.dto;

import lombok.*;
import org.dozer.Mapping;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class BotDetailsDTO {
    private String id;
    private String name;
    private String description;
    private String registeredBy;
    private String registeredByUUID;
}
