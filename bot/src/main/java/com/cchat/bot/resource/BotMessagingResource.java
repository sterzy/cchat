package com.cchat.bot.resource;

import com.cchat.bot.domain.Bot;
import com.cchat.common.dto.websocket.CryptoMessageDTO;
import com.cchat.common.exceptions.http.BadRequestException;
import com.cchat.common.exceptions.http.UnauthorizedException;
import com.cchat.common.util.Constants;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * This class is for messaging from/to bots via an REST endpoint.
 */
@RestController
@Log4j2
public class BotMessagingResource extends BaseResource {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    /**
     * This endpoint will send a message to a user.
     *
     *
     * @param userId  The ID of the user to send the message to.
     * @param message The message to send.
     * @param token   The token of the bot.
     * @return 200 on success. 403 if the token is invalid. 400 if the message is invalid.
     */
    @RequestMapping(method = RequestMethod.POST, value = SEND_WITH_USER_ID)
    public ResponseEntity<Void> sendMessageToUser(
        @PathVariable("userId") String userId,
        @RequestBody CryptoMessageDTO message,
        @RequestParam("token") String token
    ) {

        Optional<Bot> maybeBot = this.botService.getBotByToken(token);

        if (maybeBot.isEmpty()) {
            throw new UnauthorizedException("Given token is invalid");
        }

        Bot bot = maybeBot.get();

        if (!bot.getName().equals(message.getFrom()) && !bot.getId().equals(message.getFrom())) {
            throw new BadRequestException("The from field was not set correctly");
        }

        if (!this.botService.isBotWhitelisted(userId, bot)) {
            throw new UnauthorizedException("The user did not whitelist this bot");
        }

        this.messagingTemplate.convertAndSendToUser(userId, Constants.CHAT_EP, message);

        return ResponseEntity.ok().build();
    }

}
