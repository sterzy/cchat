package com.cchat.bot.resource;

import com.cchat.bot.resource.util.MapperUtil;
import com.cchat.bot.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * A Base resource to define all paths and autowire services.
 */
@CrossOrigin
public abstract class BaseResource {

    @Autowired
    protected MapperUtil mapper;

    @Autowired
    protected BotService botService;

    public static final String PREFIX = "/api";

    public static final String BOTS = PREFIX + "/bots";
    public static final String BOTS_BY_ME = BOTS + "/mine";
    public static final String BOT_BY_ID = BOTS + "/{botId}";
    public static final String BOT_DISABLE_BY_ID = BOT_BY_ID + "/disable";
    public static final String BOT_ACTIVATE_BY_ID = BOT_BY_ID + "/activate";
    public static final String BOT_PRIVATISE_BY_ID = BOT_BY_ID + "/privatise";
    public static final String BOT_PUBLICISE_BY_ID = BOT_BY_ID + "/publicise";
    public static final String BOT_PRE_KEY_BUNDLE = BOT_BY_ID + "/bundle";
    public static final String BOT_TEST_MESSAGE = BOT_BY_ID + "/test";
    public static final String BOT_ADD_BOT = BOT_BY_ID + "/add";
    public static final String BOT_IS_ADDED = BOT_BY_ID + "/isadded";
    public static final String BOT_SEARCH = BOTS + "/search";

    public static final String SEND = PREFIX + "/send";
    public static final String SEND_WITH_USER_ID = SEND + "/{userId}";

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
        MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
