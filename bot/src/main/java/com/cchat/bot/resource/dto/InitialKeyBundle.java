package com.cchat.bot.resource.dto;

import com.cchat.bot.domain.PreKey;
import com.cchat.bot.domain.SignedPreKey;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InitialKeyBundle {
    @NotNull
    private Integer registrationId;
    @NotNull
    private String identityKey;
    @NotNull
    @NotEmpty
    private List<@Valid PreKey> preKeys;
    @NotNull
    @Valid
    private SignedPreKey signedPreKey;
}
