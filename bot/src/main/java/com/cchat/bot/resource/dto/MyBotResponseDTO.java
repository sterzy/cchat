package com.cchat.bot.resource.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ToString
public class MyBotResponseDTO extends BotDetailsDTO {
    private Boolean isActive;
    private Boolean isPublic;
}
