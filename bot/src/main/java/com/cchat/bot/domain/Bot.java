package com.cchat.bot.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * This class holds information about a bot so that a user can initiate a session with a bot.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document
public class Bot {

    @Id
    private String id;
    @Indexed(background = true, unique = true)
    private String name;
    private String description;

    // The name of the user who registered this bot
    private String registeredBy;
    // UUID of the user who registered this bot
    private String registeredByUUID;
    // This field indicates if a bot is active or not
    private Boolean isActive;
    // This field indicates if a bot is public or not
    private Boolean isPublic;

    private String webHookRoot;

    // The following fields are required by the signal library

    // The registration ID of the bot
    private Integer registrationId;
    // The identity key is the public key of the bot
    private String identityKey;
    // PreKeys are used to initiate a session by another user
    private List<PreKey> preKeys;
    // The signed PreKey is used to verify the bot before initiating a session by the other user
    private SignedPreKey signedPreKey;

    // Used to authenticate a bot
    @Indexed(background = true, unique = true)
    private String token;
}
