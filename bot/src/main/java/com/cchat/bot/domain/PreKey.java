package com.cchat.bot.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;

/**
 * Class that holds the Base64 encoded public key.
 *
 * NOTE: This object is not annotated with @Document as it is embedded within the owning object.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class PreKey {
    @NotNull
    private Integer keyId;
    @NotNull
    private String publicKey;
}
