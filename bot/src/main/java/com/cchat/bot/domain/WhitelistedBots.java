package com.cchat.bot.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;

@Document
@Data
@AllArgsConstructor
@EqualsAndHashCode
public class WhitelistedBots {

    /**
     * The id of the user whose whitelist this is.
     */
    @Id
    private String userName;

    /**
     * This map holds all bot ids which are whitelisted by this user.
     */
    private HashMap<String, Boolean> addedBots;
}
