package com.cchat.bot.domain;

import lombok.*;

import javax.validation.constraints.NotNull;

/**
 * Class that holds the Base64 encoded public key as well as its signature.
 *
 * NOTE: This object is not annotated with @Document as it is embedded within the owning object.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class SignedPreKey {
    @NotNull
    private Integer keyId;
    @NotNull
    private String publicKey;
    @NotNull
    private String signature;
}
