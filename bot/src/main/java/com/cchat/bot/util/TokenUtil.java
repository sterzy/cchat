package com.cchat.bot.util;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class TokenUtil {

    public static final int TOKEN_LENGTH = 32;

    private static final String ALPHABET = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ$-_.!*'()";

    public static String generateToken() {
        StringBuilder token = new StringBuilder();

        try {
            SecureRandom rand = SecureRandom.getInstanceStrong();
            byte[] bytes = new byte[TOKEN_LENGTH];
            rand.nextBytes(bytes);

            for (byte b : bytes) {
                token.append(ALPHABET.charAt(Math.floorMod(b, ALPHABET.length())));
            }

            return token.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Failed to get secure random number generator");
        }
    }

}
