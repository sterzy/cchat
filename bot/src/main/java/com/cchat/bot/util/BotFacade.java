package com.cchat.bot.util;

import com.cchat.bot.domain.Bot;
import com.cchat.bot.resource.dto.InitialKeyBundle;
import com.cchat.common.dto.websocket.CryptoMessageDTO;
import com.cchat.common.exceptions.http.BadGatewayException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.constraints.NotNull;

@Log4j2
public class BotFacade {

    private RestTemplate restTemplate;
    private Bot bot;

    public BotFacade(@NotNull Bot bot, @NotNull RestTemplate restTemplate) {
        this.bot = bot;
        this.restTemplate = restTemplate;
    }

    /**
     * Sends a message to the bot.
     *
     * @param message The message to send
     * @return The response from the bot.
     */
    public CryptoMessageDTO sendMessage(CryptoMessageDTO message) {
        return this.sendMessageTo(message, BotPaths.MESSAGE);
    }

    /**
     * Sends a test message to the bot.
     *
     * @param message The message to send.
     * @return The response from the bot.
     */
    public CryptoMessageDTO sendTestMessage(CryptoMessageDTO message) {
        return this.sendMessageTo(message, BotPaths.TEST_MESSAGE);
    }

    /**
     * This helper function will call the bots /intial endpoint to get its InitialKeyBundle.
     * <p>
     * If this method fails, a BadGateway exception is thrown.
     *
     * @return The InitialKeyBundle returned by the bot.
     */
    public InitialKeyBundle getInitialKeyBundle() {

        ResponseEntity<InitialKeyBundle> resp;
        try {
            resp = restTemplate.getForEntity(
                UriComponentsBuilder.fromHttpUrl(this.bot.getWebHookRoot() + BotPaths.INTIAL_KEYS)
                    .queryParam("userId", this.bot.getRegisteredByUUID())
                    .queryParam("userName", this.bot.getRegisteredBy())
                    .queryParam("token", this.bot.getToken())
                    .toUriString(),
                InitialKeyBundle.class
            );
        } catch (RestClientException e) {
            log.warn("Error while getting initial keys of bot {}:", this.bot.getName(), e);
            throw new BadGatewayException("Bot at " + this.bot.getName() + " did not respond with 200 status code");
        }

        if (!resp.getStatusCode().is2xxSuccessful()) {
            log.warn("Bot {} did not return with 200 status code when getting the initial key bundle: {}", this.bot.getName(), resp.getStatusCode());
            throw new BadGatewayException("Bot at " + this.bot.getWebHookRoot() + " did not respond with 200 status code");
        }

        return resp.getBody();
    }

    /**
     * This method will execute the send request to the bot.
     *
     * @param message The message to send.
     * @param route   The route to send the message to.
     * @return The response from the bot.
     */
    private CryptoMessageDTO sendMessageTo(CryptoMessageDTO message, String route) {

        ResponseEntity<CryptoMessageDTO> resp;
        try {
            resp = restTemplate.postForEntity(this.bot.getWebHookRoot() + route, message, CryptoMessageDTO.class);
        } catch (RestClientException e) {
            log.warn("Error while forwarding message to bot {}: {}", this.bot.getName(), e.getMessage());
            throw new BadGatewayException("Bot at " + this.bot.getWebHookRoot() + " did not respond with 200 status code");
        }

        if (!resp.getStatusCode().is2xxSuccessful()) {
            log.warn("Bot {} did not return with 200 status code when forwarding message: {}", this.bot.getName(), resp.getStatusCode());
            throw new BadGatewayException("Bot at " + this.bot.getName() + " did not respond with 200 status code");
        }

        CryptoMessageDTO response = resp.getBody();

        if (response == null) {
            return null;
        }

        response.setFrom(this.bot.getName());

        return response;
    }
}
