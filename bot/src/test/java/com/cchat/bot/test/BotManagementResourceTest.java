package com.cchat.bot.test;

import com.cchat.bot.BotApplication;
import com.cchat.bot.domain.Bot;
import com.cchat.bot.domain.PreKey;
import com.cchat.bot.domain.SignedPreKey;
import com.cchat.bot.domain.WhitelistedBots;
import com.cchat.bot.resource.dto.*;
import com.cchat.bot.test.util.TestConfig;
import com.cchat.bot.util.BotPaths;
import com.cchat.common.dto.websocket.CryptoMessageDTO;
import com.cchat.common.exceptions.http.*;
import com.cchat.common.security.SecurityUser;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.text.MatchesPattern.matchesPattern;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
    TestConfig.class,
    BotApplication.class,
})
@Import({
    com.cchat.common.config.WebSocketConfig.class,
})
@Slf4j
public class BotManagementResourceTest extends BaseTest {

    @Test
    public void registerBotAddsBotsToDatabase() throws Exception {
        BotRegistrationDTO reg = this.getValidRegistration();

        mockServer.expect(requestTo(matchesPattern(botRegWebHookRootURL + BotPaths.INTIAL_KEYS + ".*")))
            .andExpect(method(HttpMethod.GET))
            .andExpect(queryParam("userId", user1UUID))
            .andExpect(queryParam("userName", user1Name))
            .andExpect(queryParam("token", notNullValue(String.class)))
            .andRespond(withStatus(HttpStatus.OK)
            .contentType(MediaType.APPLICATION_JSON_UTF8)
            .body(mapper.writeValueAsString(botRegInitialKeyBundle))
            );

        this.botManagementResource.registerBot(this.user1, reg);

        verify(this.botRepository, times(1)).save(any());
        verify(this.botRepository, times(1)).findByName(botRegName);

        verifyNoMoreInteractions(this.botRepository);

        Optional<Bot> fromDb = this.botRepository.findByName(botRegName);

        assertThat(fromDb).isPresent();

        Bot newBot = fromDb.get();

        assertThat(newBot.getDescription()).isEqualTo(botRegDescription);
        assertThat(newBot.getIsPublic()).isEqualTo(botRegIsPublic);
        assertThat(newBot.getWebHookRoot()).isEqualTo(botRegWebHookRootURL);
        assertThat(newBot.getPreKeys()).isEqualTo(botRegPreKeys);
        assertThat(newBot.getSignedPreKey()).isEqualTo(botRegSignedPreKey);

        // Newly registered bots are not marked as active
        assertThat(newBot.getIsActive()).isEqualTo(false);
        // Check if user1 registered this bot
        assertThat(newBot.getRegisteredByUUID()).isEqualTo(user1UUID);
        assertThat(newBot.getRegisteredBy()).isEqualTo(user1Name);

    }

    @Test(expected = ConflictException.class)
    public void registerBotWithSameNameThrowsException() {
        BotRegistrationDTO reg = this.getValidRegistration();

        // Set the name to a bot in the database
        reg.setName(bot1Name);

        try {
            this.botManagementResource.registerBot(this.user1, reg);
        } finally {
            this.verifyBotRepoFindByIdOrFindByNameCalled();

            verifyNoMoreInteractions(this.botRepository);
        }
    }

    @Test(expected = BadRequestException.class)
    public void registerBotWithInvalidURLThrowsException() {
        BotRegistrationDTO reg = this.getValidRegistration();

        reg.setWebHookRootURL("this is not a url");

        try {
            this.botManagementResource.registerBot(this.user1, reg);
        } finally {
            this.verifyBotRepoFindByIdOrFindByNameCalled();

            verifyNoMoreInteractions(this.botRepository);
        }
    }

    @Test(expected = BadRequestException.class)
    public void regsiterBotWithThrowsExcpetionWhenBotReturnsInvalidPreKeys() throws Exception {

        List<PreKey> invalidPreKeys = new LinkedList<>(botRegPreKeys);
        // Add a PreKey with null as public key
        invalidPreKeys.add(new PreKey(2, null));

        InitialKeyBundle bundle = new InitialKeyBundle(
            botRegRegistrationId,
            botRegIdentityKey,
            invalidPreKeys,
            botRegSignedPreKey
        );

        mockServer.expect(requestTo(matchesPattern(botRegWebHookRootURL + BotPaths.INTIAL_KEYS + ".*")))
            .andExpect(method(HttpMethod.GET))
            .andExpect(queryParam("userId", user1UUID))
            .andExpect(queryParam("userName", user1Name))
            .andExpect(queryParam("token", notNullValue(String.class)))
            .andRespond(withStatus(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(mapper.writeValueAsString(bundle))
            );

        try {
            this.botManagementResource.registerBot(this.user1, this.getValidRegistration());
        } finally {
            verifyBotRepoFindByIdOrFindByNameCalled();
            verifyNoMoreInteractions(this.botRepository);
        }
    }

    @Test(expected = BadRequestException.class)
    public void registerBotWithThrowsExcpetionWhenBotReturnsAnInvalidSignedPreKey() throws Exception {
        InitialKeyBundle bundle = new InitialKeyBundle(
            botRegRegistrationId,
            botRegIdentityKey,
            botRegPreKeys,
            new SignedPreKey(1, null, null)
        );

        mockServer.expect(requestTo(matchesPattern(botRegWebHookRootURL + BotPaths.INTIAL_KEYS + ".*")))
            .andExpect(method(HttpMethod.GET))
            .andExpect(queryParam("userId", user1UUID))
            .andExpect(queryParam("userName", user1Name))
            .andExpect(queryParam("token", notNullValue(String.class)))
            .andRespond(withStatus(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(mapper.writeValueAsString(bundle))
            );

        try {
            this.botManagementResource.registerBot(this.user1, this.getValidRegistration());
        } finally {
            verifyBotRepoFindByIdOrFindByNameCalled();
            verifyNoMoreInteractions(this.botRepository);
        }
    }

    @Test
    public void registerBotAppendsSlashIfNotPresent() throws Exception {
        BotRegistrationDTO reg = this.getValidRegistration();
        String url = "http://localhost:10808/api";
        reg.setWebHookRootURL(url);

        mockServer.expect(requestTo(matchesPattern(botRegWebHookRootURL + BotPaths.INTIAL_KEYS + ".*")))
            .andExpect(method(HttpMethod.GET))
            .andExpect(queryParam("userId", user1UUID))
            .andExpect(queryParam("userName", user1Name))
            .andExpect(queryParam("token", notNullValue(String.class)))
            .andRespond(withStatus(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(mapper.writeValueAsString(botRegInitialKeyBundle))
            );

        this.botManagementResource.registerBot(this.user1, reg);

        verify(this.botRepository, times(1)).save(any());
        verify(this.botRepository, times(1)).findByName(botRegName);

        verifyNoMoreInteractions(this.botRepository);

        Optional<Bot> fromDb = this.botRepository.findByName(botRegName);

        assertThat(fromDb).isPresent();

        Bot newBot = fromDb.get();

        assertThat(newBot.getWebHookRoot()).isEqualTo(url + "/");
    }

    @Test
    public void deleteBotRemovesBotFromDatabase() {
        this.botManagementResource.deleteBot(user1, bot1Id);

        verifyBotRepoFindByIdOrFindByNameCalled();
        verify(this.botRepository, times(1)).deleteById(bot1Id);

        verifyNoMoreInteractions(this.botRepository);

        assertThat(this.botRepository.findById(bot1Id)).isEmpty();
    }

    @Test(expected = NotFoundException.class)
    public void deleteBotWithInvalidIdThrowsException() {
        try {
            this.botManagementResource.deleteBot(user1, "this-is-not-a-valid-id");
        } finally {
            verifyBotRepoFindByIdOrFindByNameCalled();

            verifyNoMoreInteractions(this.botRepository);
        }
    }

    @Test(expected = ForbiddenException.class)
    public void deleteBotFromNonOwningUserThrowsException() {
        try {
            this.botManagementResource.deleteBot(new SecurityUser("dummy-user", "dummy-user", new LinkedList<>()), bot1Id);
        } finally {
            verifyBotRepoFindByIdOrFindByNameCalled();

            verifyNoMoreInteractions(this.botRepository);
        }
    }

    @Test
    public void callingDisableBotSetsFlagInDatabaseToFalse() {
        this.bot1.setIsActive(true);
        this.botRepository.save(this.bot1);

        Bot botFromDB = this.botRepository.findById(this.bot1.getId()).get();
        assertThat(botFromDB.getIsActive()).isTrue();

        ResponseEntity<MyBotResponseDTO> response = this.botManagementResource.disable(this.user1, this.bot1.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        botFromDB = this.botRepository.findById(this.bot1.getId()).get();
        assertThat(botFromDB.getIsActive()).isFalse();
    }

    @Test(expected = NotFoundException.class)
    public void callingDisableBotWithUnknownIDThrowsException() {
        String invalidID = "this-is-not-a-valid-id";
        try {
            this.botManagementResource.disable(this.user1, invalidID);
        } finally {
            verify(this.botRepository, times(1)).findByName(invalidID);
            verify(this.botRepository, times(1)).findById(invalidID);

            verifyNoMoreInteractions(this.botRepository);
        }
    }

    @Test(expected = ForbiddenException.class)
    public void callingDisableBotAsWrongUserThrowsException() {
        this.bot1.setIsActive(true);
        this.botRepository.save(this.bot1);

        Bot botFromDB = this.botRepository.findById(this.bot1.getId()).get();
        assertThat(botFromDB.getIsActive()).isTrue();

        Mockito.clearInvocations(this.botRepository);

        try {
            this.botManagementResource.disable(new SecurityUser("dummy-user", "dummy-user", new LinkedList<>()), this.bot1.getId());
        } finally {
            botFromDB = this.botRepository.findById(this.bot1.getId()).get();
            assertThat(botFromDB.getIsActive()).isTrue();

            this.verifyBotRepoFindByIdOrFindByNameCalled();

            verifyNoMoreInteractions(this.botRepository);
        }
    }

    @Test
    public void callingDisableBotOnAlreadyDisabledBotDoesNotInvokeRepository() {
        Bot botFromDB = this.botRepository.findById(this.bot1.getId()).get();
        assertThat(botFromDB.getIsActive()).isFalse();

        Mockito.clearInvocations(this.botRepository);

        ResponseEntity<MyBotResponseDTO> response = this.botManagementResource.disable(this.user1, this.bot1.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        botFromDB = this.botRepository.findById(this.bot1.getId()).get();
        assertThat(botFromDB.getIsActive()).isFalse();

        this.verifyBotRepoFindByIdOrFindByNameCalled();

        verifyNoMoreInteractions(this.botRepository);
    }

    @Test
    public void callingActivateBotSetsFlagInDatabaseToTrue() {
        Bot botFromDB = this.botRepository.findById(this.bot1.getId()).get();
        assertThat(botFromDB.getIsActive()).isFalse();

        ResponseEntity<MyBotResponseDTO> response = this.botManagementResource.activate(this.user1, this.bot1.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        botFromDB = this.botRepository.findById(this.bot1.getId()).get();
        assertThat(botFromDB.getIsActive()).isTrue();
    }

    @Test(expected = NotFoundException.class)
    public void callingActivateBotWithUnknownIDThrowsException() {
        String invalidID = "this-is-not-a-valid-id";
        try {
            this.botManagementResource.activate(this.user1, invalidID);
        } finally {
            verify(this.botRepository, times(1)).findByName(invalidID);
            verify(this.botRepository, times(1)).findById(invalidID);

            verifyNoMoreInteractions(this.botRepository);
        }
    }

    @Test(expected = ForbiddenException.class)
    public void callingActivateBotAsWrongUserThrowsException() {

        Bot botFromDB = this.botRepository.findById(this.bot1.getId()).get();
        assertThat(botFromDB.getIsActive()).isFalse();

        Mockito.clearInvocations(this.botRepository);

        try {
            this.botManagementResource.activate(new SecurityUser("dummy-user", "dummy-user", new LinkedList<>()), this.bot1.getId());
        } finally {
            botFromDB = this.botRepository.findById(this.bot1.getId()).get();
            assertThat(botFromDB.getIsActive()).isFalse();

            this.verifyBotRepoFindByIdOrFindByNameCalled();

            verifyNoMoreInteractions(this.botRepository);
        }
    }

    @Test
    public void callingActivateBotOnAlreadyActivatedBotDoesNotInvokeRepository() {
        this.bot1.setIsActive(true);
        this.botRepository.save(this.bot1);

        Bot botFromDB = this.botRepository.findById(this.bot1.getId()).get();
        assertThat(botFromDB.getIsActive()).isTrue();

        Mockito.clearInvocations(this.botRepository);

        ResponseEntity<MyBotResponseDTO> response = this.botManagementResource.activate(this.user1, this.bot1.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        this.verifyBotRepoFindByIdOrFindByNameCalled();

        verifyNoMoreInteractions(this.botRepository);
    }

    @Test
    public void callingPrivatiseBotSetsFlagInDatabaseToFalse() {
        this.bot1.setIsPublic(true);
        this.botRepository.save(this.bot1);

        Bot botFromDB = this.botRepository.findById(this.bot1.getId()).get();
        assertThat(botFromDB.getIsPublic()).isTrue();

        ResponseEntity<MyBotResponseDTO> response = this.botManagementResource.privatise(this.user1, this.bot1.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        botFromDB = this.botRepository.findById(this.bot1.getId()).get();
        assertThat(botFromDB.getIsActive()).isFalse();
    }

    @Test(expected = ForbiddenException.class)
    public void callingPrivatiseBotAsWrongUserThrowsException() {
        this.bot1.setIsPublic(true);
        this.botRepository.save(this.bot1);

        Bot botFromDB = this.botRepository.findById(this.bot1.getId()).get();
        assertThat(botFromDB.getIsPublic()).isTrue();

        Mockito.clearInvocations(this.botRepository);

        try {
            this.botManagementResource.privatise(new SecurityUser("dummy-user", "dummy-user", new LinkedList<>()), this.bot1.getId());
        } finally {
            botFromDB = this.botRepository.findById(this.bot1.getId()).get();
            assertThat(botFromDB.getIsPublic()).isTrue();

            this.verifyBotRepoFindByIdOrFindByNameCalled();

            verifyNoMoreInteractions(this.botRepository);
        }
    }

    @Test(expected = NotFoundException.class)
    public void callingPrivatiseBotWithUnknownIDThrowsException() {
        String invalidID = "this-is-not-a-valid-id";
        try {
            this.botManagementResource.privatise(this.user1, invalidID);
        } finally {
            verify(this.botRepository, times(1)).findByName(invalidID);
            verify(this.botRepository, times(1)).findById(invalidID);

            verifyNoMoreInteractions(this.botRepository);
        }
    }

    @Test
    public void callingPrivatiseBotOnAlreadyPrivatisedBotDoesNotInvokeRepository() {
        Bot botFromDB = this.botRepository.findById(this.bot1.getId()).get();
        assertThat(botFromDB.getIsPublic()).isFalse();

        Mockito.clearInvocations(this.botRepository);

        ResponseEntity<MyBotResponseDTO> response = this.botManagementResource.privatise(this.user1, this.bot1.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        botFromDB = this.botRepository.findById(this.bot1.getId()).get();
        assertThat(botFromDB.getIsPublic()).isFalse();

        this.verifyBotRepoFindByIdOrFindByNameCalled();

        verifyNoMoreInteractions(this.botRepository);
    }

    @Test
    public void callingPubliciseBotSetsFlagInDatabaseToTrue() {
        Bot botFromDB = this.botRepository.findById(this.bot1.getId()).get();
        assertThat(botFromDB.getIsPublic()).isFalse();

        ResponseEntity<MyBotResponseDTO> response = this.botManagementResource.publicise(this.user1, this.bot1.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        botFromDB = this.botRepository.findById(this.bot1.getId()).get();
        assertThat(botFromDB.getIsPublic()).isTrue();
    }

    @Test(expected = NotFoundException.class)
    public void callingPubliciseBotWithUnknownIDThrowsException() {
        String invalidID = "this-is-not-a-valid-id";
        try {
            this.botManagementResource.publicise(this.user1, invalidID);
        } finally {
            verify(this.botRepository, times(1)).findByName(invalidID);
            verify(this.botRepository, times(1)).findById(invalidID);

            verifyNoMoreInteractions(this.botRepository);
        }
    }

    @Test(expected = ForbiddenException.class)
    public void callingPubliciseBotAsWrongUserThrowsException() {

        Bot botFromDB = this.botRepository.findById(this.bot1.getId()).get();
        assertThat(botFromDB.getIsPublic()).isFalse();

        Mockito.clearInvocations(this.botRepository);

        try {
            this.botManagementResource.publicise(new SecurityUser("dummy-user", "dummy-user", new LinkedList<>()), this.bot1.getId());
        } finally {
            botFromDB = this.botRepository.findById(this.bot1.getId()).get();
            assertThat(botFromDB.getIsPublic()).isFalse();

            this.verifyBotRepoFindByIdOrFindByNameCalled();

            verifyNoMoreInteractions(this.botRepository);
        }
    }

    @Test
    public void callingPubliciseBotOnAlreadyPublicisedBotDoesNotInvokeRepository() {
        this.bot1.setIsPublic(true);
        this.botRepository.save(this.bot1);

        Bot botFromDB = this.botRepository.findById(this.bot1.getId()).get();
        assertThat(botFromDB.getIsPublic()).isTrue();

        Mockito.clearInvocations(this.botRepository);

        ResponseEntity<MyBotResponseDTO> response = this.botManagementResource.publicise(this.user1, this.bot1.getId());

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        this.verifyBotRepoFindByIdOrFindByNameCalled();

        verifyNoMoreInteractions(this.botRepository);
    }

    @Test
    public void getMyBotsReturnsListOfBotsRegisteredByUser() {
        List<MyBotResponseDTO> bots = this.botManagementResource.getMyBots(this.user1).getBody();

        assertThat(bots).isEqualTo(List.of(
            this.mapperUtil.toDto(bot1, MyBotResponseDTO.class),
            this.mapperUtil.toDto(bot2, MyBotResponseDTO.class)
        ));

        verify(this.botRepository, times(1)).findAllByRegisteredByUUID(user1UUID);

        verifyNoMoreInteractions(this.botRepository);
    }

    @Test
    public void getMyBotsReturnsEmptyListForUserWithoutBots() {
        List<MyBotResponseDTO> bots = this.botManagementResource
            .getMyBots(new SecurityUser("dummy-user", "dummy-user", new LinkedList<>()))
            .getBody();

        assertThat(bots).isEmpty();

        verify(this.botRepository, times(1)).findAllByRegisteredByUUID("dummy-user");

        verifyNoMoreInteractions(this.botRepository);
    }

    @Test
    public void getBotByIdReturnsFullBotDetailsForOwnBot() {

        // Disable bot (it should still be accessible by user)
        this.bot1.setIsActive(false);
        this.bot1.setIsPublic(false);
        this.botRepository.save(this.bot1);

        Mockito.reset(this.botRepository);

        BotDetailsDTO bot = this.botManagementResource.getBotById(this.user1, bot1Id).getBody();

        assertThat(bot).isNotNull();
        assertThat(bot).isExactlyInstanceOf(MyBotResponseDTO.class);

        verifyBotRepoFindByIdOrFindByNameCalled();
        verifyNoMoreInteractions(this.botRepository);
    }

    @Test
    public void getBotByIdReturnsStrippedDownDetailsForForeignBot() {

        this.bot1.setIsActive(true);
        // Bot is not public, should still be accessible by get by ID
        this.bot1.setIsPublic(false);
        this.botRepository.save(this.bot1);

        Mockito.reset(this.botRepository);

        BotDetailsDTO bot = this.botManagementResource
            .getBotById(new SecurityUser("dummy-user", "dummy-user", new LinkedList<>()), bot1Id)
            .getBody();

        assertThat(bot).isNotNull();
        assertThat(bot).isExactlyInstanceOf(BotDetailsDTO.class);

        verifyBotRepoFindByIdOrFindByNameCalled();
        verifyNoMoreInteractions(this.botRepository);
    }

    @Test(expected = NotFoundException.class)
    public void getBotByIdThrowsExceptionForDisabledBot() {

        this.bot1.setIsActive(false);
        this.bot1.setIsPublic(true);
        this.botRepository.save(this.bot1);

        Mockito.reset(this.botRepository);

        try {
            this.botManagementResource
                .getBotById(new SecurityUser("dummy-user", "dummy-user", new LinkedList<>()), bot1Id)
                .getBody();
        } finally {
            verifyBotRepoFindByIdOrFindByNameCalled();
            verifyNoMoreInteractions(this.botRepository);
        }
    }

    @Test(expected = NotFoundException.class)
    public void getBotByIdForUnknownBotThrowsExcption() {
        this.botManagementResource.getBotById(this.user1, "this-id-is-not-valid");
    }

    @Test
    public void addBotAddsBotToWhitelist() {

        this.botManagementResource.addBot(this.user1, bot1Id);

        verifyBotRepoFindByIdOrFindByNameCalled();

        verifyBotRepoFindByIdOrFindByNameCalled();

        verify(this.whitelistedBotsRepository, times(1)).save(new WhitelistedBots(
            user1Name,
            new HashMap<>() {{
                put(bot2Id, Boolean.TRUE);
                put(bot1Id, Boolean.TRUE);
            }}
        ));

        Mockito.reset(this.whitelistedBotsRepository, this.botRepository);

        Boolean isAdded = this.botManagementResource.isBotAdded(this.user1, bot1Id).getBody();

        assertThat(isAdded).isTrue();

        verifyBotRepoFindByIdOrFindByNameCalled();

        verify(this.whitelistedBotsRepository, times(1)).userHasBotWhitelisted(user1Name, bot1Id);
    }

    @Test(expected = NotFoundException.class)
    public void addBotForUnknownBotThrowsException() {
        this.botManagementResource.addBot(this.user1, "this-id-does-not-exist");
    }

    @Test(expected = NotFoundException.class)
    public void isBotAddedWithUnknownBotThrowsException() {
        this.botManagementResource.isBotAdded(this.user1, "this-id-does-not-exist");
    }

    @Test
    public void name() {
    }

    @Test
    public void findBotsByNameReturnsListOfBotsWithSimilarName() {

        bot1.setIsActive(true);
        bot1.setIsPublic(true);

        this.botRepository.save(bot1);

        List<BotSearchResponseDTO> bots = this.botManagementResource
            .findBotsByName(bot1Name.substring(2, bot1Name.length() - 2), false)
            .getBody();

        assertThat(bots).isNotEmpty();

        assertThat(bots).contains(mapperUtil.toDto(bot1, BotSearchResponseDTO.class));
    }

    @Test
    public void findBotsByNameReturnsEmptyListForUnknownString() {

        bot1.setIsActive(true);
        bot1.setIsPublic(true);

        this.botRepository.save(bot1);

        List<BotSearchResponseDTO> bots = this.botManagementResource
            .findBotsByName(bot1Name + "*", false)
            .getBody();

        assertThat(bots).isEmpty();
    }

    @Test
    public void findBotsWithExactFlagFindsSingleResult() {
        bot1.setIsActive(true);
        bot1.setIsPublic(true);
        this.botRepository.save(bot1);

        List<BotSearchResponseDTO> bots = this.botManagementResource.findBotsByName(bot1Name, true).getBody();

        assertThat(bots).hasSize(1);

        assertThat(bots).isEqualTo(List.of(mapperUtil.toDto(bot1, BotSearchResponseDTO.class)));
    }

    @Test(expected = NotFoundException.class)
    public void findBotsWithExactFlagThrowsExceptionIfNoMatchIsFound() {
        this.botManagementResource.findBotsByName("this-is-not-a-valid-bot-name", true);
    }

    @Test
    public void getPreKeyBundleGetsAndRemovesAPreKeyFromTheDatabase() {
        bot1.setIsActive(true);
        this.botRepository.save(bot1);

        PreKeyBundleDTO bundle = this.botManagementResource.getPreKeyBundle(bot1Id).getBody();

        verify(this.botRepository, times(1)).findByIdOrNameAndPopPreKey(bot1Id);

        assertThat(bundle).isNotNull();

        Bot bot = this.botRepository.findById(bot1Id).get();

        assertThat(bot.getPreKeys()).hasSize(1);
        assertThat(bot.getPreKeys()).doesNotContain(bundle.getPreKey());
    }

    @Test
    public void sendTestMessageSendsMessageToBot() throws Exception {
        CryptoMessageDTO testMessage = new CryptoMessageDTO();
        testMessage.setId("testMessage");
        testMessage.setCryptoType(1L);
        testMessage.setPayload("This is an encrypted message");
        testMessage.setFrom(user1Name);
        testMessage.setTimestamp(2019L);

        CryptoMessageDTO response = new CryptoMessageDTO();
        response.setId("responseMessage");
        response.setCryptoType(1L);
        response.setPayload("This is an encrypted response");
        response.setFrom("this-is-not-the-bot-who-is-responding");
        response.setTimestamp(2020L);

        this.setupMockServerForTestMessage(testMessage, response);

        CryptoMessageDTO actual = this.botManagementResource.sendTestMessage(this.user1, bot1Name, testMessage).getBody();

        this.mockServer.verify();

        response.setFrom(bot1Name);

        assertThat(actual).isEqualTo(response);

        verifyBotRepoFindByIdOrFindByNameCalled();
    }

    @Test(expected = NotFoundException.class)
    public void sendTestMessageForUnknownBotThrowsException() {
        CryptoMessageDTO message = spy(new CryptoMessageDTO());

        try {
            this.botManagementResource.sendTestMessage(this.user1, "this-is-not-a-bot-id", message);
        } finally {
            // Verify that the message was not used at all
            verifyNoMoreInteractions(message);

            // Verify that findById or findByName was called
            verifyBotRepoFindByIdOrFindByNameCalled();
        }
    }

    @Test
    public void sendTestMessageReturnsNullFromBot() throws Exception {
        CryptoMessageDTO testMessage = new CryptoMessageDTO();
        testMessage.setId("testMessage");
        testMessage.setCryptoType(1L);
        testMessage.setPayload("This is an encrypted message");
        testMessage.setFrom(user1Name);
        testMessage.setTimestamp(2019L);

        this.setupMockServerForTestMessage(testMessage, null);

        CryptoMessageDTO actual = this.botManagementResource.sendTestMessage(this.user1, bot1Name, testMessage).getBody();

        this.mockServer.verify();

        assertThat(actual).isNull();
    }

    @Test(expected = UnauthorizedException.class)
    public void sendTestMessageThrowsExceptionForUserWhoDidNotRegisterBot() {

        CryptoMessageDTO testMessage = new CryptoMessageDTO();
        testMessage.setId("testMessage");
        testMessage.setCryptoType(1L);
        testMessage.setPayload("This is an encrypted message");
        testMessage.setFrom(user1Name);
        testMessage.setTimestamp(2019L);

        testMessage = spy(testMessage);

        try {
            this.botManagementResource.sendTestMessage(new SecurityUser("dummy-user", "dummy-user", new LinkedList<>()), bot1Name, testMessage);
        } finally {
            verifyNoMoreInteractions(testMessage);
            this.mockServer.verify();
        }
    }
}
