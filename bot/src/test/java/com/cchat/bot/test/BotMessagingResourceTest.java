package com.cchat.bot.test;

import com.cchat.bot.BotApplication;
import com.cchat.bot.test.util.TestConfig;
import com.cchat.common.dto.websocket.CryptoMessageDTO;
import com.cchat.common.exceptions.http.BadRequestException;
import com.cchat.common.exceptions.http.UnauthorizedException;
import com.cchat.common.util.Constants;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
    TestConfig.class,
    BotApplication.class,
})
@Import({
    com.cchat.common.config.WebSocketConfig.class,
})
@Slf4j
public class BotMessagingResourceTest extends BaseTest {

    @Test(expected = UnauthorizedException.class)
    public void sendMessageToUserThrowsExceptionWHenUserDidNotWhitelistBot() {

        CryptoMessageDTO message = new CryptoMessageDTO();
        message.setFrom(bot1Name);

        message = spy(message);


        try {
            this.botMessagingResource.sendMessageToUser(user1Name, message, bot1Token);
        } finally {
            // getFrom might get called by the resource endpoint, bot no other method on the message
            verify(message, atLeast(0)).getFrom();
            verifyNoMoreInteractions(message, this.messagingTemplate);

            verify(this.botRepository, times(1)).findBotByToken(bot1Token);
        }
    }

    @Test(expected = BadRequestException.class)
    public void sendMessageToUserThrowsExceptionIfFromFieldIsNotSetCorrectly() {
        CryptoMessageDTO message = new CryptoMessageDTO();
        message.setFrom("this-is-not-a-valid-from-field");

        message = spy(message);


        try {
            this.botMessagingResource.sendMessageToUser(user1Name, message, bot1Token);
        } finally {
            // getFrom might get called by the resource endpoint, bot no other method on the message
            verify(message, atLeast(1)).getFrom();
            verifyNoMoreInteractions(message, this.messagingTemplate);

            verify(this.botRepository, times(1)).findBotByToken(bot1Token);
        }
    }

    @Test
    public void sendMessageToUserCallsConvertAndSendToUser() {
        CryptoMessageDTO message = new CryptoMessageDTO();
        message.setFrom(bot2Id);
        message.setCryptoType(2L);
        message.setId("message-1");
        message.setPayload("this is an encrypted message");
        message.setTimestamp(2019L);

        message = spy(message);

        ResponseEntity<Void> response = this.botMessagingResource.sendMessageToUser(user1Name, message, bot2Token);

        assertThat(response.getStatusCode().is2xxSuccessful()).isTrue();

        verify(this.messagingTemplate, times(1)).convertAndSendToUser(user1Name, Constants.CHAT_EP, message);
    }

    @Test(expected = UnauthorizedException.class)
    public void sendMessageToUserThrowsExceptionWithInvalidToken() {
        CryptoMessageDTO message = new CryptoMessageDTO();

        message = spy(message);

        try {
            this.botMessagingResource.sendMessageToUser(user1Name, message, "this-is-not-a-valid-token");
        } finally {
            verifyNoMoreInteractions(message, this.messagingTemplate);
        }
    }
}
