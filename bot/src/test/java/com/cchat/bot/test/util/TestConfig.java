package com.cchat.bot.test.util;

import com.cchat.bot.repository.BotRepository;
import com.cchat.bot.repository.WhitelistedBotsRepository;
import org.mockito.AdditionalAnswers;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.client.RestTemplate;

@Configuration
public class TestConfig {

    /**
     * This configuration will return a mock of the BotRepository instead of the real (final) class.
     *
     * @param botRepository The actual repository.
     * @return A mocked repository, which passes everything to the real repository (if no {@code when} clause or similar
     * was specified).
     */
    @Primary
    @Bean
    public BotRepository spyBotRepository(BotRepository botRepository) {
        return Mockito.mock(BotRepository.class, AdditionalAnswers.delegatesTo(botRepository));
    }

    /**
     * This configuration will return a mock of the WhitelistedBotsRepository instead of the real (final) class.
     *
     * @param whitelistedBotsRepository The actual repository.
     * @return A mocked repository, which passes everything to the real repository (if no {@code when} clause or similar
     * was specified).
     */
    @Primary
    @Bean
    public WhitelistedBotsRepository spyWhitelistedBotsRepository(WhitelistedBotsRepository whitelistedBotsRepository) {
        return Mockito.mock(WhitelistedBotsRepository.class, AdditionalAnswers.delegatesTo(whitelistedBotsRepository));
    }

    /**
     * This configuration will return a mock of the WhitelistedBotsRepository instead of the real (final) class.
     *
     * @param simpMessagingTemplate The actual messaging template.
     * @return A mocked repository, which passes everything to the real repository (if no {@code when} clause or similar
     * was specified).
     */
    @Primary
    @Bean
    public SimpMessagingTemplate mockSimpleMessagingTemplate(SimpMessagingTemplate simpMessagingTemplate) {
        return Mockito.mock(SimpMessagingTemplate.class, AdditionalAnswers.delegatesTo(simpMessagingTemplate));
    }

    /**
     * This bean is required to intercept HTTP calls to our bot.
     *
     * @return The RestTemplate to use.
     */
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
