package com.cchat.bot.test;

import com.cchat.bot.domain.Bot;
import com.cchat.bot.domain.PreKey;
import com.cchat.bot.domain.SignedPreKey;
import com.cchat.bot.domain.WhitelistedBots;
import com.cchat.bot.repository.BotRepository;
import com.cchat.bot.repository.WhitelistedBotsRepository;
import com.cchat.bot.resource.BotManagementResource;
import com.cchat.bot.resource.BotMessagingResource;
import com.cchat.bot.resource.dto.BotRegistrationDTO;
import com.cchat.bot.resource.dto.InitialKeyBundle;
import com.cchat.bot.resource.util.MapperUtil;
import com.cchat.bot.util.BotPaths;
import com.cchat.common.dto.websocket.CryptoMessageDTO;
import com.cchat.common.security.SecurityUser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.text.MatchesPattern.matchesPattern;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@RunWith(SpringRunner.class)
public abstract class BaseTest {

    protected static final String user1Name = "user1";
    protected static final String user1UUID = "user1-uuid";

    protected static final String bot1Id = "1";
    protected static final String bot1Name = "first-bot";
    protected static final String bot1Description = "this is the first bot";
    protected static final String bot1RegisteredBy = user1Name;
    protected static final String bot1RegisteredByUUID = user1UUID;
    protected static final Boolean bot1IsActive = false;
    protected static final Boolean bot1IsPublic = false;
    protected static final String bot1WebHookRoot = "http://localhost:10099/api/";
    protected static final Integer bot1RegistrationId = 1;
    protected static final String bot1IdentityKey = "bot1-identity-key";
    protected static final List<PreKey> bot1PreKeys = List.of(
        new PreKey(1, "bot1-pre-key-1-publicKey"),
        new PreKey(2, "bot1-pre-key-2-publicKey")
    );
    protected static final SignedPreKey bot1SignedPreKey = new SignedPreKey(
        3,
        "bot1-signed-pre-key-public-key",
        "bot1-signed-pre-key-signature"
    );
    protected static final String bot1Token = "bot1-token";

    protected static final String bot2Id = "2";
    protected static final String bot2Name = "second-bot";
    protected static final String bot2Description = "this is the first bot";
    protected static final String bot2RegisteredBy = user1Name;
    protected static final String bot2RegisteredByUUID = user1UUID;
    protected static final Boolean bot2IsActive = false;
    protected static final Boolean bot2IsPublic = false;
    protected static final String bot2WebHookRoot = "http://localhost:20099/api/";
    protected static final Integer bot2RegistrationId = 2;
    protected static final String bot2IdentityKey = "bot2-identity-key";
    protected static final List<PreKey> bot2PreKeys = List.of(
        new PreKey(2, "bot2-pre-key-2-publicKey"),
        new PreKey(2, "bot2-pre-key-2-publicKey")
    );
    protected static final SignedPreKey bot2SignedPreKey = new SignedPreKey(
        3,
        "bot2-signed-pre-key-public-key",
        "bot2-signed-pre-key-signature"
    );
    protected static final String bot2Token = "bot2-token";

    protected static final String botRegName = "new-bot";
    protected static final String botRegWebHookRootURL = "http://localhost:10808/api/";
    protected static final String botRegDescription = "a new bot";
    protected static final Boolean botRegIsPublic = Boolean.TRUE;

    protected static final Integer botRegRegistrationId = 123;
    protected static final String botRegIdentityKey = "reg-identity-key";
    protected static final List<PreKey> botRegPreKeys = List.of(
        new PreKey(1, "reg-pre-key-1-publicKey"),
        new PreKey(2, "reg-pre-key-2-publicKey")
    );
    protected static final SignedPreKey botRegSignedPreKey = new SignedPreKey(
        3,
        "reg-signed-pre-key-public-key",
        "reg-signed-pre-key-signature"
    );
    protected static final InitialKeyBundle botRegInitialKeyBundle = new InitialKeyBundle(
        botRegRegistrationId,
        botRegIdentityKey,
        botRegPreKeys,
        botRegSignedPreKey
    );

    @Autowired
    protected RestTemplate restTemplate;

    protected MockRestServiceServer mockServer;
    protected ObjectMapper mapper = new ObjectMapper();

    @Autowired
    protected MapperUtil mapperUtil;

    @Autowired
    protected BotRepository botRepository;

    @Autowired
    protected WhitelistedBotsRepository whitelistedBotsRepository;

    @Autowired
    protected BotManagementResource botManagementResource;

    @Autowired
    protected BotMessagingResource botMessagingResource;

    @Autowired
    protected SimpMessagingTemplate messagingTemplate;
    protected ArgumentCaptor<String> convertAndSendToUserUserIdCaptor = ArgumentCaptor.forClass(String.class);
    protected ArgumentCaptor<String> convertAndSendToUserTopicCaptor = ArgumentCaptor.forClass(String.class);
    protected ArgumentCaptor<Object> convertAndSendToUserMessageCaptor = ArgumentCaptor.forClass(Object.class);

    @Autowired
    protected MongoTemplate mongoTemplate;

    protected Bot bot1 = this.getTestBot1();
    protected Bot bot2 = this.getTestBot2();
    protected SecurityUser user1 = this.getTestUser();

    @Before
    public void setup() throws Exception {
        assertThat(this.botRepository.findById(this.bot1.getId())).isEmpty();
        this.botRepository.save(this.bot1);
        this.botRepository.save(this.bot2);
        this.whitelistedBotsRepository.save(new WhitelistedBots(user1Name, new HashMap<>() {{
            put(bot2Id, Boolean.TRUE);
        }}));

        mockServer = MockRestServiceServer.createServer(restTemplate);

        Mockito.reset(this.botRepository, this.whitelistedBotsRepository);

        doNothing().when(this.messagingTemplate).convertAndSendToUser(
            this.convertAndSendToUserUserIdCaptor.capture(),
            this.convertAndSendToUserTopicCaptor.capture(),
            this.convertAndSendToUserMessageCaptor.capture()
        );
    }

    @After
    public void tearDown() throws Exception {
        this.mongoTemplate.getDb().drop();
    }

    protected Bot getTestBot1() {
        return new Bot(
            bot1Id,
            bot1Name,
            bot1Description,
            bot1RegisteredBy,
            bot1RegisteredByUUID,
            bot1IsActive,
            bot1IsPublic,
            bot1WebHookRoot,
            bot1RegistrationId,
            bot1IdentityKey,
            bot1PreKeys,
            bot1SignedPreKey,
            bot1Token
        );
    }

    protected Bot getTestBot2() {
        return new Bot(
            bot2Id,
            bot2Name,
            bot2Description,
            bot2RegisteredBy,
            bot2RegisteredByUUID,
            bot2IsActive,
            bot2IsPublic,
            bot2WebHookRoot,
            bot2RegistrationId,
            bot2IdentityKey,
            bot2PreKeys,
            bot2SignedPreKey,
            bot2Token
        );
    }

    protected SecurityUser getTestUser() {
        return new SecurityUser(
            user1UUID,
            user1Name,
            new LinkedList<>()
        );
    }

    protected BotRegistrationDTO getValidRegistration() {
        return new BotRegistrationDTO(
            botRegName,
            botRegWebHookRootURL,
            botRegDescription,
            botRegIsPublic
        );
    }

    protected void verifyBotRepoFindByIdOrFindByNameCalled() {
        // Get a capture for all argument of findById
        ArgumentCaptor<String> findByIdCapture = ArgumentCaptor.forClass(String.class);
        Mockito.verify(this.botRepository, atLeast(0)).findById(findByIdCapture.capture());

        // Get a capture for all argument of findByName
        ArgumentCaptor<String> findByNameCapture = ArgumentCaptor.forClass(String.class);
        Mockito.verify(this.botRepository, atLeast(0)).findByName(findByNameCapture.capture());

        // Verify that either findById or findByName (or both) are called
        assertThat(!findByIdCapture.getAllValues().isEmpty() || !findByNameCapture.getAllValues().isEmpty()).isTrue();
    }

    protected void setupMockServerForTestMessage(CryptoMessageDTO expectedMessage, CryptoMessageDTO response) throws Exception {
        mockServer.expect(requestTo(matchesPattern(bot1WebHookRoot + BotPaths.TEST_MESSAGE + ".*")))
            .andExpect(method(HttpMethod.POST))
            .andExpect(content().json(mapper.writeValueAsString(expectedMessage)))
            .andRespond(withStatus(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(mapper.writeValueAsString(response))
            );
    }
}
