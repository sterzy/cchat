package com.cchat.user.service;

import com.cchat.common.security.SecurityUser;
import com.cchat.user.domain.User;
import com.cchat.user.exception.ServiceException;
import com.cchat.user.exception.UnauthorizedException;
import com.cchat.user.repository.UserRepository;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.bson.Document;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.token.TokenManager;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;

@Service
@Log4j2
public class UserService {

    @Value("${security.keycloak.url:}")
    private String keycloakUrl;

    @Value("${security.keycloak.realm:}")
    private String keycloakRealm;

    @Value("${security.keycloak.username:}")
    private String keycloakUsername;

    @Value("${security.keycloak.password:}")
    private String keycloakPassword;

    @Value("${security.keycloak.clientId:}")
    private String keycloakClientId;

    @Autowired(required = false)
    private ResteasyClient resteasyClient;

    @Autowired
    private UserRepository userRepository;

    @Value("${server.name:}")
    private String serverName;

    private RealmResource realmResourceAdmin;

    @PostConstruct
    private void init() {
        realmResourceAdmin = getRealmResourceForAdmin();
    }

    @Transactional
    public void unregister(SecurityUser user) {
        realmResourceAdmin.users().delete(user.getId());
        // Need specific delete method because we dont have the mongo_object_id (mongo_object_id != uuid)
        this.userRepository.deleteUserByUsernameAndUuid(user.getUsername(), user.getId());
        log.info("User '" + user.getUsername() + "' successfully unregistered");
    }

    private RealmResource getRealmResourceForAdmin() {
        Keycloak k = KeycloakBuilder.builder()
                .serverUrl(keycloakUrl)
                .username(keycloakUsername)
                .password(keycloakPassword)
                .clientId(keycloakClientId)
                .realm(keycloakRealm)
                .resteasyClient(resteasyClient)
                .build();

        return k.realm(keycloakRealm);
    }

    private TokenManager getTokenManager(String username, String password) {
        Keycloak k = KeycloakBuilder.builder()
                .serverUrl(keycloakUrl)
                .username(username)
                .password(password)
                .realm(keycloakRealm)
                .clientId(keycloakClientId)
                .resteasyClient(resteasyClient)
                .build();

        return k.tokenManager();
    }

    public AccessTokenResponse login(String userName, String password) {
        try {
            AccessTokenResponse accessToken = getTokenManager(userName, password).getAccessToken();
            log.info("User '" + userName + "' successfully logged in");
            return accessToken;
        } catch (NotAuthorizedException e) {
            log.info("User '" + userName + "' tried to login with invalid credentials");
            throw new UnauthorizedException("Username or password invalid");
        }
    }

    @Transactional
    public void register(String username, String password) {
        try {
            CredentialRepresentation passwordCred = new CredentialRepresentation();
            passwordCred.setTemporary(false);
            passwordCred.setType(CredentialRepresentation.PASSWORD);
            passwordCred.setValue(password);

            UserRepresentation user = new UserRepresentation();
            user.setEnabled(true);
            user.setUsername(username);
            user.setCredentials(Arrays.asList(passwordCred));

            Response response = realmResourceAdmin.users().create(user);
            if (response.getStatus() == 409) {
                throw new ServiceException("Username already in use");
            }
            if (response.getStatus() != 201) {
                throw new ServiceException("Failed to register new user");
            }

        } catch (NotAuthorizedException e) {
            throw new UnauthorizedException();
        }

        // Get the UUID of the newly registered user
        List<UserRepresentation> users = realmResourceAdmin.users().search(username);
        createOrUpdateUser(users.get(0).getId(), users.get(0).getUsername());

        log.info("User '" + username + "' successfully registered");

    }

    @Transactional
    public void createOrUpdateUser(String uuid, String name) {
        User user = userRepository.findByUuid(uuid).orElse(new User(uuid, name));
        userRepository.save(user);
    }

    @Data
    private static class LogoutUser {
        private String realm;
        private String user;
    }
}
