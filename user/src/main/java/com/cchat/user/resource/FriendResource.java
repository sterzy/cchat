package com.cchat.user.resource;

import com.cchat.user.domain.User;
import com.cchat.user.repository.UserRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@Log4j2
public class FriendResource extends BaseResource  {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = FINDFRIEND, method = RequestMethod.POST)
    public ResponseEntity<List<User>> findFriend(@Valid @RequestBody String userName) { //@AuthenticationPrincipal SecurityUser user,
        List<User> list = this.userRepository.findByUsernameIsLike(userName).orElse(null);
        return ResponseEntity.ok(list);
    }

    @RequestMapping(value = FINDALL, method = RequestMethod.POST)
    public ResponseEntity<List<User>> findAll() {
        List<User> list = this.userRepository.findAll();
        return ResponseEntity.ok(list);
    }

}
