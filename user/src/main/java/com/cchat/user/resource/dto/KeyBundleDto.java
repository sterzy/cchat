package com.cchat.user.resource.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KeyBundleDto {

    @NotNull
    private Integer registrationId;

    @NotNull
    private String identityKey;

    @NotNull
    private SignedKeyDto signedPreKey;

    private PreKeyDto preKey;

}
