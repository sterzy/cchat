package com.cchat.user.resource;

import com.cchat.user.resource.util.MapperUtil;
import com.cchat.user.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 * A Base resource to define all paths and autowire services.
 */
@CrossOrigin
public abstract class BaseResource {

    @Autowired
    protected MapperUtil mapper;

    @Autowired
    protected UserService userService;

    public static final String PREFIX = "/api";
    public static final String LOGIN = PREFIX + "/login";
    public static final String LOGOUT = PREFIX + "/logout";
    public static final String REGISTER = PREFIX + "/register";
    public static final String UNREGISTER = PREFIX + "/unregister";
    public static final String KEYSUNREGISTER = PREFIX + "/keys/unregister";
    public static final String WHOAMI = PREFIX + "/whoami";

    public static final String INITIAL = PREFIX + "/keys/initial";
    public static final String SIGNEDKEY = PREFIX + "/keys/signedkey";
    public static final String PREKEYS = PREFIX + "/keys/prekeys";
    public static final String KEYBUNDLE = PREFIX + "/keys/keybundle/{userName}";
    public static final String KEYCOUNT = PREFIX + "/keys/count/{userName}";

    public static final String FINDFRIEND = PREFIX + "/friends/find";
    public static final String FINDALL = PREFIX + "/friends/findAll";
}
