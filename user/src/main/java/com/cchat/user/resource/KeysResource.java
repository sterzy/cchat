package com.cchat.user.resource;

import com.cchat.common.security.SecurityUser;
import com.cchat.user.domain.PreKey;
import com.cchat.user.domain.SignedPreKey;
import com.cchat.user.domain.UserBaseKeys;
import com.cchat.user.repository.PreKeyRepository;
import com.cchat.user.repository.UserBaseKeysRepository;
import com.cchat.user.resource.dto.*;
import lombok.extern.log4j.Log4j2;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@Log4j2
public class KeysResource extends BaseResource {

    @Autowired
    private UserBaseKeysRepository userBaseKeysRepository;

    @Autowired
    private PreKeyRepository preKeyRepository;


    @RequestMapping(value = INITIAL, method = RequestMethod.POST)
    public ResponseEntity<Void> addInitialKeys(@AuthenticationPrincipal SecurityUser user, @Valid @RequestBody InitialKeysDto keys) {

        log.info("user '{}' is initializing their key bundle ", user.getUsername());

        UserBaseKeys newUserBaseKeys = new UserBaseKeys(
            user.getId(), user.getUsername(), keys.getRegistrationId(),
            keys.getIdentityKey(), new SignedPreKey(
            keys.getSignedPreKey().getKeyId(),
            keys.getSignedPreKey().getPublicKey(),
            keys.getSignedPreKey().getSignature()
        )
        );

        preKeyRepository.deleteAllByUuid(user.getId());
        userBaseKeysRepository.deleteAllByUuid(user.getId());
        userBaseKeysRepository.save(newUserBaseKeys);

        for (PreKeyDto preKey : keys.getPreKeys()) {
            preKeyRepository.save(new PreKey(ObjectId.get().toHexString(),
                user.getId(), preKey.getKeyId(),
                preKey.getPublicKey()));
        }
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = PREKEYS, method = RequestMethod.POST)
    public ResponseEntity<Void> addPreKeys(@AuthenticationPrincipal SecurityUser user, @Valid @RequestBody PreKeysDto preKeysDto) {

        log.info("user '{}' is adding preKeys ", user.getUsername());

        Optional<UserBaseKeys> userBaseKeysOptional = userBaseKeysRepository.findById(user.getId());
        if (userBaseKeysOptional.isEmpty()) {
            log.error("Initial Keys have to be added before adding additional preKeys");
            throw new ResponseStatusException(
                HttpStatus.EXPECTATION_FAILED, "Initial Keys have to be added before adding additional preKeys");
        }

        for (PreKeyDto preKey : preKeysDto.getPreKeys()) {
            preKeyRepository.save(new PreKey(ObjectId.get().toHexString(),
                user.getId(), preKey.getKeyId(),
                preKey.getPublicKey()));
        }
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = SIGNEDKEY, method = RequestMethod.PUT)
    public ResponseEntity<Void> updateSignedKey(@AuthenticationPrincipal SecurityUser user, @Valid @RequestBody SignedKeyDto signedKeyDto) {

        log.info("user '{}' is updating their signed key", user.getUsername());

        try {

            UserBaseKeys userBaseKeys = userBaseKeysRepository.findById(user.getId()).orElseThrow();
            SignedPreKey signedPreKey = new SignedPreKey(signedKeyDto.getKeyId(),
                signedKeyDto.getPublicKey(), signedKeyDto.getSignature());
            userBaseKeys.setSignedPreKey(signedPreKey);

            userBaseKeysRepository.save(userBaseKeys);

        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "UserBaseKeys with this ID not found - cannot be updated", e);
        }
        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = KEYCOUNT, method = RequestMethod.GET)
    public ResponseEntity<PreKeyCountDto> getPreKeyCount(@PathVariable String userName) {

        try {
            UserBaseKeys userBaseKeys = userBaseKeysRepository.findByUsername(userName).orElseThrow();
            Long numberOfPreKeys = preKeyRepository.countByUuid(userBaseKeys.getUuid());

            return ResponseEntity.ok(
                new PreKeyCountDto(numberOfPreKeys));
        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "UserBaseKeys with this userName not found", e);
        }
    }

    @RequestMapping(value = KEYBUNDLE, method = RequestMethod.GET)
    public ResponseEntity<KeyBundleDto> getKeyBundle(@PathVariable String userName) {
        try {
            UserBaseKeys userBaseKeys = userBaseKeysRepository.findByUsername(userName).orElseThrow();
            PreKey preKey = preKeyRepository.findFirstByUuid(userBaseKeys.getUuid());

            PreKeyDto lastPreKey = null;
            if (preKey != null) {
                lastPreKey = new PreKeyDto(preKey.getKeyId(),
                    preKey.getPublicKey());
                preKeyRepository.delete(preKey);
            }

            SignedKeyDto sigDto = new SignedKeyDto(
                userBaseKeys.getSignedPreKey().getKeyId(),
                userBaseKeys.getSignedPreKey().getPublicKey(),
                userBaseKeys.getSignedPreKey().getSignature());
            KeyBundleDto dto = new KeyBundleDto(
                userBaseKeys.getRegistrationId(),
                userBaseKeys.getIdentityKey(),
                sigDto, lastPreKey);
            return ResponseEntity.ok(dto);

        } catch (NoSuchElementException e) {
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "UserBaseKeys with this userName not found", e);
        }
    }

    @RequestMapping(value = KEYSUNREGISTER, method = RequestMethod.GET)
    public ResponseEntity<Void> unregister(@AuthenticationPrincipal SecurityUser user) {
        //Propagate exceptions to frontend
        this.userBaseKeysRepository.deleteAllByUuid(user.getId());
        this.preKeyRepository.deleteAllByUuid(user.getId());

        return ResponseEntity.ok().build();
    }
}
