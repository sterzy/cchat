package com.cchat.user.resource.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InitialKeysDto {
    @NotNull
    private Integer registrationId;
    @NotEmpty
    private String identityKey;
    @NotNull
    private SignedKeyDto signedPreKey;
    @NotNull
    private List<PreKeyDto> preKeys;
}
