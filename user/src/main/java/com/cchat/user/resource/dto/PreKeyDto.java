package com.cchat.user.resource.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PreKeyDto {
    @NotNull
    private Integer keyId;
    @NotNull
    private String publicKey;
}
