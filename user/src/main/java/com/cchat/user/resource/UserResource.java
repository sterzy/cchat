package com.cchat.user.resource;

import com.cchat.common.dto.UserDto;
import com.cchat.common.security.SecurityUser;
import com.cchat.common.security.utils.CookieUtils;
import com.cchat.common.security.utils.JwtUtils;
import com.cchat.common.util.UserUtil;
import com.cchat.user.resource.dto.LoginDto;
import com.cchat.user.resource.dto.RegisterDto;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.representations.AccessTokenResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

import static com.cchat.common.security.utils.CookieUtils.JWT_ACCESS;
import static com.cchat.common.security.utils.CookieUtils.JWT_REFRESH;

@RestController
@Slf4j
public class UserResource extends BaseResource {

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private CookieUtils cookieUtils;

    @Value("${server.name:}")
    private String serverName;

    @Value("${local}")
    private boolean local;

    /**
     * Versuch sich über das UIM am System anzumelden. Im Erfolgsfall wird ein Cookie mit einem JWT returniert,
     * sowie {@link UserDto} mit Informationen über den nun angemeldeten User.
     *
     * @param loginDto {@link LoginDto} mit E-Mail und Passwort
     * @param response {@link HttpServletResponse}
     * @return {@link ResponseEntity}
     */
    @RequestMapping(value = LOGIN, method = RequestMethod.POST)
    public ResponseEntity<UserDto> login(@RequestBody LoginDto loginDto, HttpServletResponse response) {
        AccessTokenResponse jwtResponse = userService.login(loginDto.getUsername(), loginDto.getPassword());
        SecurityUser user = jwtUtils.getUserFromToken(jwtResponse.getToken());

        cookieUtils.setCookie(JWT_ACCESS, jwtResponse.getToken(), response);
        cookieUtils.setCookie(JWT_REFRESH, jwtResponse.getRefreshToken(), response);

        return ResponseEntity.ok(mapper.toUserDto(user));
    }

    /**
     * Logout des Benutzers vom System, und invalidiere seine JWT Cookies.
     *
     * @param response  {@link HttpServletResponse}
     * @return {@link ResponseEntity}
     */
    @RequestMapping(value = LOGOUT, method = RequestMethod.GET)
    public ResponseEntity<Void> logout(HttpServletResponse response) {
        cookieUtils.removeCookie(JWT_ACCESS, response);
        cookieUtils.removeCookie(JWT_REFRESH, response);

        return ResponseEntity.ok().build();
    }

    /**
     * Liefert Informationen über den aktuell angemeldeten User.
     *
     * @param principal {@link Principal}
     * @return {@link ResponseEntity}
     */
    @RequestMapping(value = WHOAMI, method = RequestMethod.GET)
    public ResponseEntity<UserDto> whoAmI(Principal principal) {
        return ResponseEntity.ok(mapper.toUserDto(UserUtil.getUser(principal)));
    }

    /**
     * Register a new user at the UIM.
     *
     * @param registerDto {@link RegisterDto}
     * @return {@link ResponseEntity}
     */
    @RequestMapping(value = REGISTER, method = RequestMethod.POST)
    public ResponseEntity<UserDto> register(@RequestBody RegisterDto registerDto, HttpServletResponse response) {
        userService.register(registerDto.getUsername(), registerDto.getPassword());
        return this.login(new LoginDto(registerDto.getUsername(), registerDto.getPassword()), response);
    }

    /**
     * Removes a user from the platform.
     *
     * This is currently only done via Keycloak.
     *
     * @param principal The logged in user.
     * @return {@link ResponseEntity#ok()}
     */
    @RequestMapping(value = UNREGISTER, method = RequestMethod.GET)
    public ResponseEntity<Void> unregister(Principal principal, HttpServletResponse request) {
        this.userService.unregister(UserUtil.getUser(principal));
        return this.logout(request);
    }
}
