package com.cchat.user.repository;

import com.cchat.user.domain.PreKey;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PreKeyRepository extends MongoRepository<PreKey, String> {

    List<PreKey> findAllByUuid(String uuid);

    PreKey findFirstByUuid(String uuid);

    Long countByUuid(String uuid);

    void deleteAllByUuid(String uuid);

}
