package com.cchat.user.repository;

import com.cchat.user.domain.UserBaseKeys;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserBaseKeysRepository extends MongoRepository<UserBaseKeys, String> {

    Optional<UserBaseKeys> findByUsername(String username);

    void deleteAllByUuid(String uuid);

}
