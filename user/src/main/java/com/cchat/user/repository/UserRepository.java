package com.cchat.user.repository;

import com.cchat.user.domain.User;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends PagingAndSortingRepository<User, Integer> {
    Optional<User> findByUuid(String uuid);
    List<User> findAll();
    Optional<List<User>> findByUsernameIsLike(String name);
    Long deleteUserByUsernameAndUuid(String username, String uuid);
}
