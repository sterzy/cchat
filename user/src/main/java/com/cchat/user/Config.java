package com.cchat.user;

import com.cchat.common.config.ConfigFactory;
import lombok.extern.log4j.Log4j2;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableAsync
@Log4j2
@EnableMongoRepositories
class Config {
    /**
     * Dozer Mapper is used to map DTOs to JSON and vise versa.
     */
    @Bean
    public Mapper getDozerBeanMapper() {
        return new DozerBeanMapper();
    }

    /**
     * Swagger is a utility to automatically document API endpoints and call them via a website.
     *
     * Visit /swagger-ui.html to view the site.
     */
    @Bean
    public Docket swagger() {
        return ConfigFactory.swagger();
    }
}
