package com.cchat.user.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document
public class UserBaseKeys {
    @Id
    private String uuid;
    @Indexed(unique = true)
    private String username;
    private Integer registrationId;
    private String identityKey;
    private SignedPreKey signedPreKey;
}
