package com.cchat.user.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignedPreKey {
    private Integer keyId;
    private String publicKey;
    private String signature;
}
