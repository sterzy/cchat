package com.cchat.user.test;

import com.cchat.user.domain.User;
import com.cchat.user.repository.UserRepository;
import com.cchat.user.resource.FriendResource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(FriendResource.class)
@Import({com.cchat.user.resource.util.MapperUtil.class,
    com.cchat.user.service.UserService.class,
    com.cchat.common.security.utils.CookieUtils.class,
    com.cchat.user.repository.UserRepository.class})
public class FriendResourceTest {

    @Autowired
    private FriendResource friendResource;

    @MockBean
    private UserRepository userRepository;

    private static final User test_user = new User("01", "test-user");

    @Before
    public void setup() {
        // tmp test data
        userRepository.deleteAll();
        userRepository.save(test_user);
    }

    @Test
    public void findAllWithRegisteredUser_returnsListContainingUser() {

        List<User> results = new ArrayList<>();
        results.add(test_user);

        given(this.userRepository.findAll()).willReturn(results);

        ResponseEntity<List<User>> re = this.friendResource.findAll();

        verify(this.userRepository, atLeastOnce()).findAll();

        assertFalse("Response should not be empty", re.getBody().isEmpty());
        assertTrue("Response should contain User test-user", re.getBody().contains(test_user));
        assertEquals("Response Code did not match", HttpStatus.OK, re.getStatusCode());
    }

    @Test
    public void findFriendWithNameLikeRegisteredUser_returnsListContainingUser() {
        List<User> results = new ArrayList<>();
        results.add(test_user);

        given(this.userRepository.findByUsernameIsLike("test")).willReturn(Optional.of(results));

        ResponseEntity<List<User>> re = this.friendResource.findFriend("test");

        verify(this.userRepository, atLeastOnce()).findByUsernameIsLike("test");

        assertFalse("Response should not be empty", re.getBody().isEmpty());
        assertTrue("Response should contain User test-user", re.getBody().contains(test_user));
        assertEquals("Response Code did not match", HttpStatus.OK, re.getStatusCode());
    }


}
