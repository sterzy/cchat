package com.cchat.user.test;

import com.cchat.common.dto.UserDto;
import com.cchat.common.security.SecurityUser;
import com.cchat.common.security.utils.JwtUtils;
import com.cchat.user.resource.UserResource;
import com.cchat.user.resource.dto.LoginDto;
import com.cchat.user.resource.dto.RegisterDto;
import com.cchat.user.service.UserService;
import com.cchat.user.test.util.TestHttpServletResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.keycloak.representations.AccessTokenResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.servlet.http.Cookie;
import java.security.Principal;
import java.security.Security;
import java.util.*;

import static com.cchat.common.security.utils.CookieUtils.JWT_ACCESS;
import static com.cchat.common.security.utils.CookieUtils.JWT_REFRESH;
import static org.mockito.BDDMockito.given;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(UserResource.class)
@Import({com.cchat.user.resource.util.MapperUtil.class,
         com.cchat.user.service.UserService.class,
         com.cchat.common.security.utils.CookieUtils.class})
public class UserResourceTest {

    @MockBean
    private JwtUtils utils;

    @MockBean
    private UserService userService;

    @Autowired
    private UserResource resource;

    @Test
    public void testLoginWithValidCredentials_setsCookies() {

        AccessTokenResponse token = new AccessTokenResponse();
        token.setToken("token");
        token.setRefreshToken("token-refresh");
        Map<String, String> cookies = new HashMap<>();

        TestHttpServletResponse response = new TestHttpServletResponse();

        given(userService.login("test-user", "test-password"))
            .willReturn(token);

        ResponseEntity re = this.resource.login(new LoginDto("test-user", "test-password"), response);

        assertEquals("Token cookie did not match", token.getToken(), response.getCookie(JWT_ACCESS));
        assertEquals("Refresh Token cookie did not match", token.getRefreshToken(), response.getCookie(JWT_REFRESH));
        assertEquals("Response Code did not match", HttpStatus.OK, re.getStatusCode());

    }

    @Test
    public void testGetWhoAmI_returnsUser() throws Exception {

        SecurityUser user = new SecurityUser("id","username",null);
        Principal principal = new RememberMeAuthenticationToken("key", user, null);

        ResponseEntity re = this.resource.whoAmI(principal);

        assertEquals("Response status code did not match", HttpStatus.OK, re.getStatusCode());

        UserDto dto = (UserDto) re.getBody();
        assertEquals("Username did not match", user.getUsername(), dto.getUsername());
        assertEquals("Id did not match", user.getId(), dto.getId());

    }

    @Test
    public void testRegisterWithValidCredentials_setsCookies() {

        AccessTokenResponse token = new AccessTokenResponse();
        token.setToken("token");
        token.setRefreshToken("token-refresh");
        Map<String, String> cookies = new HashMap<>();

        TestHttpServletResponse response = new TestHttpServletResponse();

        given(userService.login("test-user", "test-password"))
            .willReturn(token);

        ResponseEntity re = this.resource.register(new RegisterDto("test-user", "test-password"), response);

        verify(userService, atLeastOnce()).register("test-user","test-password");

        assertEquals("Token cookie did not match", token.getToken(), response.getCookie(JWT_ACCESS));
        assertEquals("Refresh Token cookie did not match", token.getRefreshToken(), response.getCookie(JWT_REFRESH));
        assertEquals("Response Code did not match", HttpStatus.OK, re.getStatusCode());

    }

    @Test
    public void testLogout_removesCookies() {

        TestHttpServletResponse response = new TestHttpServletResponse();

        response.addCookie(new Cookie(JWT_ACCESS, "token"));
        response.addCookie(new Cookie(JWT_REFRESH, "token-refresh"));

        assertEquals("Token cookie did not match", "token", response.getCookie(JWT_ACCESS));
        assertEquals("Refresh Token cookie did not match", "token-refresh", response.getCookie(JWT_REFRESH));

        ResponseEntity re = this.resource.logout(response);

        assertEquals("Response code did not match", HttpStatus.OK, re.getStatusCode());
        assertEquals("Token cookie did not match", null , response.getCookie(JWT_ACCESS));
        assertEquals("Refresh Token cookie did not match", null, response.getCookie(JWT_REFRESH));

    }

    @Test
    public void testUnegisterWithValidCredentials_removesUser() {

        TestHttpServletResponse response = new TestHttpServletResponse();

        response.addCookie(new Cookie(JWT_ACCESS, "token"));
        response.addCookie(new Cookie(JWT_REFRESH, "token-refresh"));

        assertEquals("Token cookie did not match", "token", response.getCookie(JWT_ACCESS));
        assertEquals("Refresh Token cookie did not match", "token-refresh", response.getCookie(JWT_REFRESH));

        SecurityUser user = new SecurityUser("id","username",null);
        Principal principal = new RememberMeAuthenticationToken("key", user, null);

        ResponseEntity re = this.resource.unregister(principal, response);

        verify(userService, atLeastOnce()).unregister(user);

        assertEquals("Token cookie did not match", null, response.getCookie(JWT_ACCESS));
        assertEquals("Refresh Token cookie did not match", null, response.getCookie(JWT_REFRESH));
        assertEquals("Response Code did not match", HttpStatus.OK, re.getStatusCode());

    }
}
