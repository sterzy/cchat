package com.cchat.user.test;

import com.cchat.common.security.SecurityUser;
import com.cchat.user.domain.PreKey;
import com.cchat.user.domain.SignedPreKey;
import com.cchat.user.domain.UserBaseKeys;
import com.cchat.user.repository.PreKeyRepository;
import com.cchat.user.repository.UserBaseKeysRepository;
import com.cchat.user.repository.UserRepository;
import com.cchat.user.resource.KeysResource;
import com.cchat.user.resource.dto.InitialKeysDto;
import com.cchat.user.resource.dto.PreKeyDto;
import com.cchat.user.resource.dto.PreKeysDto;
import com.cchat.user.resource.dto.SignedKeyDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.server.ResponseStatusException;

import java.util.LinkedList;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(KeysResource.class)
@Import({com.cchat.user.resource.util.MapperUtil.class, com.cchat.user.service.UserService.class})
public class KeysResourceTest {

    private static final Logger logger = LoggerFactory.getLogger(com.cchat.user.test.KeysResourceTest.class);

    @Autowired
    private MockMvc mvc;

//    @Autowired
//    private MongoTemplate mongoTemplate;

    // I give up, keycloak is too hard...
    @Autowired
    private KeysResource resource;

    @MockBean
    private PreKeyRepository preKeyRepository;

    @MockBean
    UserBaseKeysRepository userBaseKeysRepository;

    @MockBean
    UserRepository userRepository;


    private static final UserBaseKeys USERA_BK = new UserBaseKeys(
            "AAAA", "userAName", 1, "userAId", new SignedPreKey(
                    1, "sKeyPub", "sKeySig")
    );
    private static final PreKey USERA_PRE1 = new PreKey("1", "AAAA", 1, "userAPre1");
    private static final PreKey USERA_PRE2 = new PreKey("2", "AAAA", 2, "userAPre2");
    private static final PreKey USERA_PRE3 = new PreKey("3", "AAAA", 3, "userAPre3");


    @Before
    public void setup() {

        // tmp test data
        userBaseKeysRepository.deleteAll();
        preKeyRepository.deleteAll();
        userBaseKeysRepository.save(USERA_BK);

        preKeyRepository.save(USERA_PRE1);
        preKeyRepository.save(USERA_PRE2);
        preKeyRepository.save(USERA_PRE3);
    }

    @Test
    @WithMockUser
    public void testGetPreKeyCount_UnknownNameReturns404() throws Exception {

        this.mvc.perform(get("/api/keys/count/unknownName"))
                .andExpect(status().is4xxClientError())
                .andExpect(status().isNotFound());

    }

    @Test
    @WithMockUser
    public void testGetPreKeyCount_successful() throws Exception {

        given(this.userBaseKeysRepository.findByUsername("userAName"))
                .willReturn(Optional.of(USERA_BK));
        given(this.preKeyRepository.countByUuid("AAAA"))
                .willReturn((long) 3);

        this.mvc.perform(get("/api/keys/count/userAName"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.count", is(3)));
    }

    @Test
    @WithMockUser
    public void testGetKeyBundle_successful() throws Exception {

        given(this.userBaseKeysRepository.findByUsername("userAName"))
                .willReturn(Optional.of(USERA_BK));
        given(this.preKeyRepository.findFirstByUuid("AAAA"))
                .willReturn(USERA_PRE1);

        this.mvc.perform(get("/api/keys/keybundle/userAName"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.identityKey", is("userAId")))
                .andExpect(jsonPath("$.preKey.publicKey", is("userAPre1")));
    }

    @Test
    @WithMockUser
    public void testGetKeyBundleWithNoPrekey_successfulWithoutPrekey() throws Exception {

        given(this.userBaseKeysRepository.findByUsername("userAName"))
            .willReturn(Optional.of(USERA_BK));
        given(this.preKeyRepository.findFirstByUuid("AAAA"))
            .willReturn(null);

        this.mvc.perform(get("/api/keys/keybundle/userAName"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.identityKey", is("userAId")))
            .andExpect(jsonPath("$.preKey.publicKey").doesNotExist());
    }

    @Test
    @WithMockUser
    public void testAddInitialKeys_invalidMethodReturnsError() throws Exception {

        this.mvc.perform(get("/api/keys/initial"))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    @WithMockUser
    public void testAddInitialKeys_missingFieldReturnsValidationError() throws Exception {

        this.mvc.perform(post("/api/keys/initial")
                .content("{\"identityKey\":\"i\",\"signedPreKey\":\"s\",\"preKeys\":[\"x\"]}")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @WithMockUser
    public void testAddInitialKeys_successful() throws Exception {

        PreKeyDto prekey = new PreKeyDto(9, "x");
        LinkedList<PreKeyDto> help = new LinkedList<>();
        help.add(prekey);
        SignedKeyDto signedkey = new SignedKeyDto(5, "s", "s");
        InitialKeysDto dto = new InitialKeysDto(1, "i", signedkey, help);
        SecurityUser securityUser = new SecurityUser("a", "userA", null);

        ResponseEntity<Void> response = this.resource.addInitialKeys(securityUser, dto);
        assertEquals("Status code does not match", HttpStatus.OK, response.getStatusCode());

    }

    @Test
    @WithMockUser
    public void testUpdateSignedKey_invalidMethodReturnsError() throws Exception {

        this.mvc.perform(get("/api/keys/signedkey"))
                .andExpect(status().isMethodNotAllowed());
        this.mvc.perform(post("/api/keys/signedkey"))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "userAName")
    public void testUpdateSignedKey_successful() throws Exception {

        given(this.userBaseKeysRepository.findById("AAAA"))
                .willReturn(Optional.of(USERA_BK));


        SignedKeyDto dto = new SignedKeyDto(5, "s2", "s2");
        SecurityUser securityUser = new SecurityUser("AAAA", "userAName", null);

        ResponseEntity<Void> response = this.resource.updateSignedKey(securityUser, dto);
        assertEquals("Status code does not match", HttpStatus.OK, response.getStatusCode());

    }

    @Test
    @WithMockUser
    public void testAddPreKeys_successful() throws Exception {

        given(this.userBaseKeysRepository.findById("AAAA"))
                .willReturn(Optional.of(USERA_BK));

        PreKeyDto prekey = new PreKeyDto(7, "p1");
        LinkedList<PreKeyDto> help = new LinkedList<>();
        help.add(prekey);
        PreKeysDto dto = new PreKeysDto(help);
        SecurityUser securityUser = new SecurityUser("AAAA", "userA", null);

        ResponseEntity<Void> response = this.resource.addPreKeys(securityUser, dto);
        assertEquals("Status code does not match", HttpStatus.OK, response.getStatusCode());

    }

    @Test(expected = ResponseStatusException.class)
    public void testAddPreKeysWithEmpty_fails() throws Exception {

        given(this.userBaseKeysRepository.findById("AAAA"))
            .willReturn(Optional.empty());

        PreKeyDto prekey = new PreKeyDto(7, "p1");
        LinkedList<PreKeyDto> help = new LinkedList<>();
        help.add(prekey);
        PreKeysDto dto = new PreKeysDto(help);
        SecurityUser securityUser = new SecurityUser("AAAA", "userA", null);

        ResponseEntity<Void> response = this.resource.addPreKeys(securityUser, dto);
        assertEquals("Status code does not match", HttpStatus.OK, response.getStatusCode());

    }

    @Test
    public void testUnregister_successful() {
        SecurityUser securityUser = new SecurityUser("AAAA", "userA", null);

        this.resource.unregister(securityUser);
        verify(this.preKeyRepository,times(1)).deleteAllByUuid(securityUser.getId());
        verify(this.userBaseKeysRepository,times(1)).deleteAllByUuid(securityUser.getId());
    }
}
