import {envbase} from './environment.base';

export const environment = {
    ...envbase,
    brokerURL: 'wss://' + location.host + '/ws',
    production: true
};
