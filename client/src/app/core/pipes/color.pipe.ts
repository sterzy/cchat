import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'color'
})
export class ColorPipe implements PipeTransform {

    transform(username: string): PromiseLike<string> {

        if (!username) {
            return undefined;
        }

        const hash = crypto.subtle.digest('SHA-256', new Uint16Array(new TextEncoder().encode(username)));

        return hash.then(sha => {

            const code = new Uint8Array(sha);

            return `rgb(${code[0]},${code[1]},${code[2]})`;
        });
    }

}
