import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'pubKey'
})
export class PubKeyPipe implements PipeTransform {

    transform(value: ArrayBuffer): string {

        if (!value) {
            return '[no key]';
        }

        const buff = new Uint8Array(value);

        return 'Version: '
            + buff[0].toString(16).padStart(2, '0') + '\n'
            + buff.slice(1).reduce((hexString, num, i) => {
                hexString += num.toString(16).padStart(2, '0');

                if (i % 8 === 7) {
                    return hexString + '\n';
                }

                return hexString + ' ';
            }, '');
    }

}
