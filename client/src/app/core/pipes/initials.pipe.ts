import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'initials'
})
export class InitialsPipe implements PipeTransform {

    transform(username: string): string {

        if (!username) {
            return '';
        }

        const parts = username.split(/[\s._]+/);

        let initials = parts[0].charAt(0).toLocaleUpperCase();
        if (parts.length > 1) {
            initials += parts[1].charAt(0).toLocaleUpperCase();
        }

        return initials;
    }

}
