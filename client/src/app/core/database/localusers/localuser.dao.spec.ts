import { TestBed } from '@angular/core/testing';

import { LocaluserDao } from './localuser.dao';
import {CoreModule} from '../../core.module';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {MatSnackBarModule} from '@angular/material';

describe('LocaluserDao', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [CoreModule, HttpClientModule, RouterModule.forRoot([]), MatSnackBarModule]
  }));

  it('should be created', () => {
    const service: LocaluserDao = TestBed.get(LocaluserDao);
    expect(service).toBeTruthy();
  });
});
