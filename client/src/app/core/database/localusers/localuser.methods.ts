import {
    LocalUserCollection,
    LocalUserCollectionMethods,
    LocalUserDocMethods, LocalUserDocType,
    LocalUserDocument
} from './localuser.types';

export const localUserDocMethods: LocalUserDocMethods = {
    toDto(this: LocalUserDocument): LocalUserDocType {
        return {
            id: this.id,
            name: this.name,
            registrationId: this.registrationId,
            keysCount: this.keysCount,
            preKeyIds: this.preKeyIds,
            signedPreKeyIds: this.signedPreKeyIds,
            preKeys: this.preKeys,
            signedPreKeys: this.signedPreKeys,
            identityKeys: this.identityKeys,
        };
    },
};

export const localUserCollectionmethods: LocalUserCollectionMethods = {
    getById(this: LocalUserCollection, id: string) {
        return this.findOne({ id: {$eq: id}}).$;
    },
};
