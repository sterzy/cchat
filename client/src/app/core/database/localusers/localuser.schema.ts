import {RxJsonSchema} from 'rxdb';

export const localUserSchema: RxJsonSchema = {
    title: 'local user schema',
    description: 'Schema for local users',
    version: 0,
    type: 'object',
    properties: {
        id: {
            type: 'string',
            primary: true,
        },
        name: {
            type: 'string',
        },
        registrationId: {
            type: 'number',
        },
        keysCount: {
            type: 'number',
        },
        preKeyIds: {
            type: 'array',
            items: {
                type: 'number',
            },
            uniqueItems: true,
        },
        preKeys: {
            type: 'object',
            default: {},
        },
        signedPreKeyIds: {
            type: 'array',
            items: {
                type: 'number',
            },
            uniqueItems: true,
        },
        signedPreKeys: {
            type: 'object',
            default: {},
        },
        identityKeys: {
            type: 'object',
            properties: {
                privKey: {
                    type: 'string'
                },
                pubKey: {
                    type: 'string'
                }
            }
        },
    },
    required: ['id', 'keysCount'],
    attachments: {
        // TODO: Must be encrypted later on, because they contain the private keys
        encrypted: false,
    },
};
