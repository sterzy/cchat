import {RxCollection, RxDocument} from 'rxdb';
import {DatabaseMethods} from '../database.types';
import {Observable} from 'rxjs';
import {EncodedKeyPair} from '../../signal/models/keys.model';

export interface LocalUserDocType {
    id: string;
    name: string;
    registrationId: number;
    keysCount: number;
    preKeyIds: number[];
    preKeys: {[keyId: number]: EncodedKeyPair};
    signedPreKeyIds: number[];
    signedPreKeys: {[keyId: number]: EncodedKeyPair};
    identityKeys: EncodedKeyPair;
}

export interface LocalUserDocMethods extends DatabaseMethods<LocalUserDocument>{
    toDto: (this: LocalUserDocument) => LocalUserDocType;
}

export type LocalUserDocument = RxDocument<LocalUserDocType, LocalUserDocMethods>;

export interface LocalUserCollectionMethods extends DatabaseMethods<LocalUserCollection> {
    getById: (this: LocalUserCollection, id: string) => Observable<LocalUserDocument>;
}

export type LocalUserCollection = RxCollection<LocalUserDocType, LocalUserDocMethods, LocalUserCollectionMethods>;

