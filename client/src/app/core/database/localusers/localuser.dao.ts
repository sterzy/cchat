import {Injectable} from '@angular/core';
import {DatabaseDao} from '../database.dao';
import {Observable, of} from 'rxjs';
import {LocalUserCollection, LocalUserDocType, LocalUserDocument} from './localuser.types';
import {distinctUntilChanged, first, map, shareReplay, switchMap, tap} from 'rxjs/operators';
import {localUserSchema} from './localuser.schema';
import {localUserCollectionmethods, localUserDocMethods} from './localuser.methods';
import {UserManagementService} from '../../../landingpage/services/user-management.service';
import {ConfigService} from '../../services/config/config.service';
import {EncodedKeyPair} from '../../signal/models/keys.model';
import {SimpleUser} from "../../models/user.models";

@Injectable({
    providedIn: 'root'
})
export class LocaluserDao {

    private readonly localUserColection$: Observable<LocalUserCollection> = this.database.getDatabase().pipe(
        //
        // Local user collection creation
        //
        switchMap(db => {
            return db.collection({
                name: 'localusers',
                schema: localUserSchema,
                methods: localUserDocMethods,
                statics: localUserCollectionmethods,
            }) as Promise<LocalUserCollection>;
        }),

        //
        // Local user collection configuration
        //
        tap(coll => {
            // Nothing to do right now
        }),

        // shareReplay will cache the last returned value and not trigger another collection creation
        shareReplay(1),
    );

    private currentUser$: Observable<LocalUserDocument> = this.config.getUser().pipe(
        // Here we use a switchMap instead of combineLatest or withLatestFrom because we need to wait until the userId
        // is initially set (after the whoAmI request). After then, we need to trigger an update every time either
        // the userId or the collection changes, so a switchMap is the best fit.
        switchMap(user => {
            return this.localUserColection$.pipe(
                map(coll => ([coll, user] as [LocalUserCollection, SimpleUser])),
            );
        }),
        switchMap(([coll, user]) => {
            if (!user) {
                return of(null);
            }
            return coll.findOne({id: {$eq: user.id}}).$.pipe(
                switchMap(localUser => {
                    if (localUser) {
                        return of(localUser);
                    }
                    return coll.insert({
                        id: user.id,
                        keysCount: 0,
                        name: user.username,
                        registrationId: undefined,
                        preKeyIds: [],
                        preKeys: {},
                        signedPreKeyIds: [],
                        signedPreKeys: {},
                        identityKeys: undefined,
                        //deviceUUID: "hi",
                    });
                })
            );
        }),
        shareReplay(1),
    );

    constructor(
        private database: DatabaseDao,
        private userService: UserManagementService,
        private config: ConfigService,
    ) {
    }

    getLocaluser(): Observable<LocalUserDocument> {
        return this.currentUser$;
    }

    update(updateFn: (user: LocalUserDocType) => LocalUserDocType): Observable<LocalUserDocument> {
        return this.currentUser$.pipe(
            first(),
            switchMap(user => {
                return user.atomicUpdate(updateFn);
            }),
        );
    }

    getKeyId(): Observable<number> {
        return this.update((oldUser) => {
            oldUser.keysCount++;
            return oldUser;
        }).pipe(
            map(user => user.keysCount),
        );
    }

    storePreKey(keyId: number, keyPair: EncodedKeyPair): Observable<LocalUserDocument> {
        return this.update(oldUser => {
            oldUser.preKeyIds.push(keyId);
            oldUser.preKeys[keyId] = keyPair;
            return oldUser;
        });
    }

    getPreKey(keyId: number): Observable<EncodedKeyPair> {
        return this.getLocaluser().pipe(
            map(user => user.preKeys[keyId]),
            distinctUntilChanged(),
        );
    }

    removePreKey(keyId: number): Observable<LocalUserDocument> {
        return this.update(oldUser => {
            const index = oldUser.preKeyIds.indexOf(keyId);
            if (index > -1) {
                oldUser.preKeyIds.splice(index, 1);
            }
            delete oldUser.preKeys[keyId];
            return oldUser;
        });
    }

    storeSignedPreKey(keyId: number, keyPair: EncodedKeyPair): Observable<LocalUserDocument> {
        return this.update(oldUser => {
            oldUser.signedPreKeyIds.push(keyId);
            oldUser.signedPreKeys[keyId] = keyPair;
            return oldUser;
        });
    }

    getSignedPreKey(keyId: number): Observable<EncodedKeyPair> {
        return this.getLocaluser().pipe(
            map(user => user.signedPreKeys[keyId]),
            distinctUntilChanged(),
        );
    }

    removeSignedPreKey(keyId: number): Observable<LocalUserDocument> {
        return this.update(oldUser => {
            const index = oldUser.signedPreKeyIds.indexOf(keyId);
            if (index > -1) {
                oldUser.signedPreKeyIds.splice(index, 1);
            }
            delete oldUser.signedPreKeys[keyId];
            return oldUser;
        });
    }

    getIdentityKeyPair(): Observable<EncodedKeyPair> {
        return this.getLocaluser().pipe(
            map(user => user.identityKeys),
            distinctUntilChanged(),
        );
    }

    storeIdentityKeyPar(keyPair: EncodedKeyPair): Observable<LocalUserDocument> {
        return this.update(oldUser => {
            oldUser.identityKeys = keyPair;
            return oldUser;
        });
    }
}
