import {DatabaseMethods} from '../database.types';
import {RxCollection, RxDocument} from 'rxdb';
import {Observable} from 'rxjs';

export interface ContactDocType {
    id: string;
    uuid: string;
    name: string;
    lastOnline: number;
}

export interface ContactDocMethods extends DatabaseMethods<ContactDocument> {
    toString: (this: ContactDocument) => string;
    toDto: (this: ContactDocument) => ContactDocType;
}

export type ContactDocument = RxDocument<ContactDocType, ContactDocMethods>;

export interface ContactCollectionMethods extends DatabaseMethods<ContactCollection> {
    getContactById: (this: ContactCollection, id: string) => Observable<ContactDocument>;
}

export type ContactCollection = RxCollection<ContactDocType, ContactDocMethods, ContactCollectionMethods>;
