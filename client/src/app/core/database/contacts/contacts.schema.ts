import {RxJsonSchema} from 'rxdb';

export const contacsSchema: RxJsonSchema = {
    title: 'contacts schema',
    description: 'Schema for contacts a user has',
    version: 0,
    // keyCompression: true,
    type: 'object',
    properties: {
        id: {
            // The username of the contact
            type: 'string',
            primary: true,
        },
        uuid: {
            // The uuid of this user
            type: 'string',
            final: true,
        },
        name: {
            // The name the contact wants to display, cannot be changed by the local user.
            type: 'string',
        },
        lastOnline: {
            type: 'number',
            index: true,
        }
    },
    attachments: {
        // Contacts' attachments are there public keys, no need to encrypt them
        encrypted: false,
    },
    required: ['id', 'name', 'lastOnline'],
};
