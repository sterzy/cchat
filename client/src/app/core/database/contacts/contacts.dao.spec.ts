import { TestBed } from '@angular/core/testing';

import { ContactsDao } from './contacts.dao';

describe('ContactsDao', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContactsDao = TestBed.get(ContactsDao);
    expect(service).toBeTruthy();
  });
});
