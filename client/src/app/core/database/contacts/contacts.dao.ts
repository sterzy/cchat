import {Injectable} from '@angular/core';
import {DatabaseDao} from '../database.dao';
import {Observable} from 'rxjs';
import {ContactCollection} from './contacts.types';
import {shareReplay, switchMap} from 'rxjs/operators';
import {contacsSchema} from './contacts.schema';
import {contactCollectionMethods, contactDocMethos} from './contacts.methods';
import {tap} from 'rxjs/internal/operators/tap';

@Injectable({
    providedIn: 'root'
})
export class ContactsDao {

    private readonly contactCollection$: Observable<ContactCollection> = this.database.getDatabase().pipe(
        //
        // Contact collection creation
        //
        switchMap(db => {
            return db.collection({
                name: 'contacts',
                schema: contacsSchema,
                methods: contactDocMethos,
                statics: contactCollectionMethods,
            }) as Promise<ContactCollection>;
        }),

        //
        // Contact collection configuration
        //
        tap(coll => {
            // nothing to do right now
        }),

        // shareReplay will cache the last returned value and not trigger another collection creation
        shareReplay(1),
    );

    constructor(
        private database: DatabaseDao,
    ) {
    }
}
