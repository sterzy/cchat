import {ContactCollection, ContactCollectionMethods, ContactDocMethods, ContactDocument} from './contacts.types';

export const contactDocMethos: ContactDocMethods = {
    toDto(this: ContactDocument) {
        return {
            id: this.id,
            lastOnline: this.lastOnline,
            name: this.name,
            uuid: this.uuid,
        };
    },
    toString(this: ContactDocument) {
        return JSON.stringify(this.toDo());
    }
};

export const contactCollectionMethods: ContactCollectionMethods = {
    getContactById(this: ContactCollection, id: string) {
        return this.findOne({id: { $eq: id }}).$;
    },
};
