import {fakeAsync, TestBed, tick} from '@angular/core/testing';

import {ChatDao} from './chat.dao';
import {ChatType} from './chat.types';
import {concat} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs/internal/observable/of';
import {first} from 'rxjs/internal/operators/first';
import {DatabaseDao} from '../database.dao';
import {ConfigService} from '../../services/config/config.service';
import {SimpleUser} from '../../models/user.models';

describe('ChatDao', () => {

    let database: DatabaseDao;
    let service: ChatDao;
    let config: ConfigService;
    const testUser: SimpleUser = {
        id: 'test-id',
        username: 'test-username',
    };

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.get(ChatDao);
        database = TestBed.get(DatabaseDao);
        config = TestBed.get(ConfigService);
        config.setUser(testUser);

        jasmine.DEFAULT_TIMEOUT_INTERVAL = 5000;
    });

    afterEach(done => {
        database.clearDatabase().subscribe({
            next: () => done(),
            error: err => done.fail(err),
        });
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should throw error on inserting chat with existing name', fakeAsync(done => {

        concat(
            service.saveChat({
                id: 'asdf',
                name: 'A',
                users: ['abc'],
                type: ChatType.DirectMessage,
                lastMessage: 0,
                started: 0,
                lastOnline: Date.now(),
                blocked: false,
                verifiedKey: undefined,
                hidden: false,
            }).pipe(
                first(),
            ),
            service.saveChat({
                id: '132',
                name: 'A',
                users: ['abc'],
                type: ChatType.DirectMessage,
                lastMessage: 0,
                started: 0,
                lastOnline: Date.now(),
                blocked: false,
                verifiedKey: undefined,
                hidden: false,
            }).pipe(
                tap(() => done.fail()),
                catchError(err => {
                    expect(err).toBeTruthy();
                    return of(null);
                }),
                first(),
            ),
        ).subscribe({
            error: err => done.fail(err),
            complete: () => done(),
        });

        tick(200);
    }));
});
