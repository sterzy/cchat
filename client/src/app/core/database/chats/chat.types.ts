import {RxCollection, RxDocument} from 'rxdb';
import {DatabaseMethods} from '../database.types';
import {Observable} from 'rxjs';

// ChatType to use in the ChatDocType
export enum ChatType {
    DirectMessage = 0,
    GroupChat,
    Bot,
}

// This is an interface of the stored data in the database. It should be an implementation of the chatSchema in
// chat.schema.ts
export interface ChatDocType {
    id: string;
    type: ChatType;
    users: string[];
    lastMessage: number;
    started: number;
    name: string;
    lastOnline: number;
    blocked: boolean;
    verifiedKey: string;
    hidden: boolean;
}

// Interface that defines which methods are provided by the ChatDocument.
export interface ChatDocMethods extends DatabaseMethods<ChatDocument> {
    toString: (this: ChatDocument) => string;
    toDto: (this: ChatDocument) => ChatDocType;
}

// A ChatDocument is the document as stored in the ChatCollection.
// It provides some methods like update() or find().
// See https://rxdb.info/rx-document.html for more information.
export type ChatDocument = RxDocument<ChatDocType, ChatDocMethods>;

// Interface to define the methods available on the ChatCollection.
export interface ChatCollectionMethods extends DatabaseMethods<ChatCollection> {
    getChats: (this: ChatCollection) => Observable<ChatDocument[]>;
    getChatById: (this: ChatCollection, id: string) => Observable<ChatDocument>;
    getChatByName: (this: ChatCollection, name: string) => Observable<ChatDocument>;
    getChatsWithBot: (this: ChatCollection) => Observable<ChatDocument[]>;
}

// A ChatCollection is the collection of ChatDocument stored in the database.
// It can provide methods to interact with the collection (see ChatDocMethods).
// See https://rxdb.info/rx-collection.html for more information.
export type ChatCollection = RxCollection<ChatDocType, ChatDocMethods, ChatCollectionMethods>;
