import {RxJsonSchema} from 'rxdb';

/**
 * The chatSchema defines how the data will be saved into the databse.
 *
 * In general, this schema is just a JSON Schema and most of the rules apply to RxJsonSchema as well.
 *
 * However, some special cases like "oneOf" to create a union type in the database are not supported.
 *
 * WARNING: If ANY changes to the schema are done, rxdb will not allow the creation of the collection! In order
 * to still use the collection one has to either
 *   - clear the database (go to Apllication (Chrome)/Storage (Firefox) and remove all entries in the IndexDB
 *   - increase the version umber (only whole numbers are supported) and implement a migration
 * For further information please visit:
 * https://rxdb.info/data-migration.html
 */
export const chatSchema: RxJsonSchema = {
    title: 'chats',
    description: 'Schema for chats',
    version: 0,
    // keyCompression: true,
    type: 'object',
    properties: {
        id: {
            type: 'string',
            primary: true,
        },
        type: {
            type: 'number',
            default: 0,
            final: true,
        },
        users: {
            type: 'array',
            uniqueItems: true,
            items: {
                type: 'string',
            },
            final: true,
        },
        lastMessage: {
            type: 'number',
            index: true,
        },
        started: {
            type: 'number',
            final: true,
        },
        name: {
            type: 'string',
            index: true,
        },
        lastOnline: {
            type: 'number',
            index: true,
        },
        blocked: {
            type: 'boolean',
            default: false,
        },
        verifiedKey: {
            type: 'string',
        },
        hidden: {
            type: 'boolean',
            default: false,
        }
    },
    attachments: {
        encrypted: false,
    },
    required: ['id', 'users', 'lastMessage', 'started'],
};
