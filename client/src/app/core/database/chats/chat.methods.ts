import {ChatCollection, ChatCollectionMethods, ChatDocMethods, ChatDocType, ChatDocument, ChatType} from './chat.types';

/**
 * In this file all methods that should be present on ChatDocuments and the ChatCollection are defined.
 */

/**
 * Those are the methods on the single ChatDocuments.
 *
 * Currently only a toString is implemented.
 */
export const chatDocMethods: ChatDocMethods = {
    toString(this: ChatDocument) {
        return JSON.stringify(this.toDto());
    },

    toDto(this: ChatDocument): ChatDocType {
        return {
            id: this.id,
            started: this.started,
            lastMessage: this.lastMessage,
            type: this.type,
            users: this.users,
            name: this.name,
            lastOnline: this.lastOnline,
            blocked: this.blocked,
            verifiedKey: this.verifiedKey,
            hidden: this.hidden,
        };
    },
};

/**
 * Here the methods on ChatCollections are defined.
 *
 * It provides simple to use methods to get all chats and get a chat by name.
 */
export const chatCollectionMethods: ChatCollectionMethods = {

    getChats(this: ChatCollection) {
        return this.find({hidden: {$eq: false}}).sort({lastMessage: 'desc'}).$;
    },

    getChatByName(this: ChatCollection, name: string) {
        return this.findOne({name: {$eq: name}}).$;
    },

    getChatById(this: ChatCollection, id: string) {
        return this.findOne({id: {$eq: id}}).$;
    },

    getChatsWithBot(this: ChatCollection) {
        return this.find({type: {$eq: ChatType.Bot}}).$;
    },
};
