import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ChatCollection, ChatDocType, ChatDocument} from './chat.types';
import {map, mapTo, shareReplay, switchMap, tap} from 'rxjs/operators';
import {uuidv4} from '../../util/uuid.util';
import {first} from 'rxjs/internal/operators/first';
import {of} from 'rxjs/internal/observable/of';
import {DatabaseDao} from '../database.dao';
import {chatSchema} from './chat.schema';
import {chatCollectionMethods, chatDocMethods} from './chat.methods';
import {arrayBufEqual, getAttachment} from '../../util/array-buffer.util';
import {RxAttachment} from 'rxdb';

const IDENTITY_KEY = 'identityKey';

/**
 * Service to interact with Chat documents in the database.
 */
@Injectable({
    providedIn: 'root'
})
export class ChatDao {

    readonly chatCollection$: Observable<ChatCollection> = this.database.getDatabase().pipe(
        //
        // Chat Collection creation
        //
        switchMap(db => {
            return db.collection({
                name: 'chats',
                schema: chatSchema,
                methods: chatDocMethods,
                statics: chatCollectionMethods,
            }) as Promise<ChatCollection>;
        }),

        //
        // Chat Collection configuration
        //
        tap(coll => {

            // When a new chat is inserted, or an existing is updated, we have to check, if the same name exists in
            // the database already.

            const chatNameUniqueConstraint = chat => {
                if (chat.id == null) {
                    chat.id = uuidv4();
                    return Promise.resolve();
                }
                if (!chat.name) { // chat.name is either null/undefined, or empty ('')
                    return Promise.resolve();
                }
                return new Promise<void>((resolve, reject) => {
                    coll.find({id: {$ne: chat.id}, name: {$eq: chat.name}})
                        .exec()
                        .then(res => {
                            if (res != null && res.length > 0) {
                                reject(new Error('Chat found with same name as given chat:\n' +
                                    'Wanted to safe: ' + JSON.stringify(chat) + '\n' +
                                    'Found: ' + JSON.stringify(res)
                                ));
                            }
                            resolve();
                        })
                        .catch(err => {
                            reject(err);
                        });
                });
            };

            coll.preInsert(chatNameUniqueConstraint, false);
            coll.preSave(chatNameUniqueConstraint, false);
        }),
        shareReplay(1),
    );

    /**
     * This Observable is initialized globally, so that not every call to getChats creates a new Observable.
     */
    private readonly chats$: Observable<ChatDocument[]> = this.chatCollection$.pipe(
        switchMap(coll => coll.getChats()),
        shareReplay(1),
    );

    private readonly chatsWithBot$: Observable<ChatDocument[]> = this.chatCollection$.pipe(
        switchMap(coll => coll.getChatsWithBot()),
        shareReplay(1),
    );

    constructor(
        private database: DatabaseDao,
    ) {
    }

    /**
     * Returns all chats from the database.
     */
    getChats(): Observable<ChatDocument[]> {
        return this.chats$;
    }

    /**
     * Returns all chats with a bot.
     */
    getChatsWithBot(): Observable<ChatDocument[]> {
        return this.chatsWithBot$;
    }

    /**
     * Saves a chat to the database.
     *
     * @param chat The chat to save.
     */
    saveChat(chat: ChatDocType): Observable<ChatDocument> {
        return this.chatCollection$.pipe(
            switchMap(coll => {
                return coll.atomicUpsert(chat);
            }),
            shareReplay(1),
        );
    }

    /**
     * Find a chat by its id.
     *
     * If none is found, null or undefined is returned.
     *
     * @param id The id of the chat to find.
     */
    getChatById(id: string): Observable<ChatDocument> {
        return this.chatCollection$.pipe(
            switchMap(coll => {
                return coll.getChatById(id);
            }),
            shareReplay(1),
        );
    }

    /**
     * Find a chat by name in the database.
     *
     * If none is found, null or undefined is returned.
     *
     * @param name The name to search the database for.
     */
    getChatByName(name: string): Observable<ChatDocument> {
        return this.chatCollection$.pipe(
            switchMap(coll => {
                return coll.getChatByName(name);
            }),
            shareReplay(1),
        );
    }

    /**
     * Updates the lastOnline Field.
     *
     * @param chatId  chat id which should be updated
     * @param time    the new lastOnline
     */
    setLastOnline(chatId: string, time: number) {
        return this.chatCollection$.pipe(
            switchMap(coll => {
                return coll.getChatById(chatId);
            }),
            first(),
            switchMap(chat => {
                if (chat == null) {
                    return of(null);
                }
                return chat.atomicUpdate((oldChat) => {
                    oldChat.lastOnline = time;
                    return oldChat;
                });
            }),
            shareReplay(1),
        );
    }

    /**
     * Updates the blocked Field.
     *
     * @param chatId     chat id which should be updated
     * @param blocked    the new blocked value
     */
    setBlocked(chatId: string, blocked: boolean) {
        return this.chatCollection$.pipe(
            switchMap(coll => {
                return coll.getChatById(chatId);
            }),
            first(),
            switchMap(chat => {
                if (chat == null) {
                    return of(null);
                }
                return chat.atomicUpdate((oldChat) => {
                    oldChat.blocked = blocked;
                    return oldChat;
                });
            }),
            shareReplay(1),
        );
    }

    /**
     * Updates the hidden Field.
     *
     * @param chatId     chat id which should be updated
     * @param hidden    the new blocked value
     */
    setHidden(chatId: string, hidden: boolean) {
        return this.chatCollection$.pipe(
            switchMap(coll => {
                return coll.getChatById(chatId);
            }),
            first(),
            switchMap(chat => {
                if (chat == null) {
                    return of(null);
                }
                return chat.atomicUpdate((oldChat) => {
                    oldChat.hidden = hidden;
                    return oldChat;
                });
            }),
            shareReplay(1),
        );
    }

    /**
     * Updates the verifiedKey Field.
     *
     * @param chatId       chat id which should be updated
     * @param publicKey    the public-key which was verified
     */
    setVerified(chatId: string, publicKey: string) {
        return this.chatCollection$.pipe(
            switchMap(coll => {
                return coll.getChatById(chatId);
            }),
            first(),
            switchMap(chat => {
                if (chat == null) {
                    return of(null);
                }
                return chat.atomicUpdate((oldChat) => {
                    oldChat.verifiedKey = publicKey;
                    return oldChat;
                });
            }),
            shareReplay(1),
        );
    }

    /**
     * Marks chat as deleted in db.
     *
     * @param chat The chat to be hid.
     */
    hideChat(chat: ChatDocType): Observable<boolean> {
        return this.getChatById(chat.id).pipe(
            first(),
            switchMap((chatDoc) => {
            return chatDoc.remove().then( () => {
                   return true;
                },
                () => {
                    return false;
                }
            );
            })
        );
    }

    isTrustedIdentity(chatId: string, identityKey: ArrayBuffer): Observable<boolean> {
        return this.loadIdentityKey(chatId).pipe(
            map(trusted => {
                if (!trusted) {
                    return true;
                }
                return arrayBufEqual(identityKey, trusted);
            })
        );
    }

    saveIdentity(chatId: string, identityKey: ArrayBuffer): Observable<boolean> {
        return this.getChatById(chatId).pipe(
            first(),
            switchMap(chat => {
                // NOTE: casting to unknown and then to RxAttachment is just a hotfix, because the lib has the wrong
                // types
                return getAttachment(chat, IDENTITY_KEY).pipe(
                    map(att => ([chat, att] as [ChatDocument, ArrayBuffer]))
                );
            }),
            switchMap(([user, existing]) => {
                return user.putAttachment({
                    id: IDENTITY_KEY,
                    data: identityKey,
                    type: 'application/octet-stream',
                }).then(() => {
                    return existing != null && !arrayBufEqual(identityKey, existing);
                });
            }),
        );
    }

    loadIdentityKey(chatId: string): Observable<ArrayBuffer> {
        return this.getChatById(chatId).pipe(
            first(),
            switchMap(chat => getAttachment(chat, IDENTITY_KEY)),
        );
    }

    loadSession(chatId: string, identifier: string) {
        return this.getChatById(chatId).pipe(
            first(),
            switchMap(chat => {
                // NOTE: casting to unknown and then to RxAttachment is just a hotfix, because the lib has the wrong
                // types
                const att = (chat.getAttachment(`session.${identifier}`) as unknown as RxAttachment<{}, {}>);
                if (!att) {
                    return of(undefined);
                }
                return att.getData().then(blob => new Promise(res => {
                    // NOTE: normally, you should be able to use att.getTextData(), but because `Buffer` does not exist
                    // in browser, we have to convert it ourselves (copied from RxDB attachment.js)
                    const reader = new FileReader();
                    reader.addEventListener('loadend', e => {
                        const text = (e.target as any).result;
                        res(text);
                    });
                    reader.readAsText(blob);
                }));
            }),
        );
    }

    storeSession(chatId: string, identifier: string, record: string) {
        return this.getChatById(chatId).pipe(
            first(),
            switchMap(chat => {
                return chat.putAttachment({
                    id: `session.${identifier}`,
                    data: record,
                    type: 'application/json',
                });
            }),
            mapTo(undefined),
        );
    }

    removeSession(chatId: string, identifier: string) {
        return this.getChatById(chatId).pipe(
            first(),
            switchMap(chat => {
                return (chat.getAttachment(`session.${identifier}`) as unknown as RxAttachment<{}, {}>).remove();
            }),
            mapTo(undefined),
        );
    }

    removeAllSessions(chatId: string, identifier: string) {
        const deleteKey = `session.${identifier}`;

        return this.getChatById(chatId).pipe(
            first(),
            switchMap(chat => chat.allAttachments() ),
            switchMap(attachments => {
                const promises: Promise<void>[] = [];

                for (const attachment of attachments) {
                    if (attachment.id.startsWith(deleteKey)) {
                        promises.push(attachment.remove());
                    }
                }

                if (promises.length === 0) {
                    return of(undefined);
                }

                return Promise.all(promises);
            }),
            mapTo(undefined),
        );

    }
}
