import {MessageCollection} from './messages/message.types';
import {RxDatabase} from 'rxdb';
import {ChatCollection} from './chats/chat.types';
import {ContactCollection} from './contacts/contacts.types';
import {LocalUserCollection} from './localusers/localuser.types';

export interface DatabaseMethods<T> {
    [key: string]: (this: T, ...args: any) => any;
}

export interface DatabaseCollections {
    messages: MessageCollection;
    chats: ChatCollection;
    contacts: ContactCollection;
    localUsers: LocalUserCollection;
}

export type Database = RxDatabase<DatabaseCollections>;
