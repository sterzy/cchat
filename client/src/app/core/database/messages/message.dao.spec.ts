import {fakeAsync, TestBed, tick} from '@angular/core/testing';

import {MessageDao} from './message.dao';
import {uuidv4} from '../../util/uuid.util';
import {MessageMediaType, MessageStatus} from './message.types';
import {tap} from 'rxjs/operators';
import {first} from 'rxjs/internal/operators/first';
import {concat} from 'rxjs';
import {DatabaseDao} from '../database.dao';
import {ConfigService} from '../../services/config/config.service';
import {SimpleUser} from '../../models/user.models';

describe('MessageDao', () => {

    let database: DatabaseDao;
    let service: MessageDao;
    let config: ConfigService;
    const testUser: SimpleUser = {
        id: 'test-id',
        username: 'test-user',
    };

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.get(MessageDao);
        database = TestBed.get(DatabaseDao);
        config = TestBed.get(ConfigService);
        config.setUser(testUser);
    });

    afterEach(done => {
        database.clearDatabase().subscribe({
            next: () => done(),
            error: err => done.fail(err),
        });
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should save messages to the db', fakeAsync(done => {

        const chatId = 'a';
        const id = uuidv4();

        concat(
            service.addMessage({
                id,
                chat: chatId,
                from: '',
                message: 'Test message',
                read: false,
                status: MessageStatus.Sent,
                timeStamp: 0,
                mediaType: MessageMediaType.Text,
            }),
            service.getMessages(
                chatId
            ).pipe(
                first(),
                tap({
                    next: msgs => {
                        expect(msgs).toBeTruthy();
                        expect(msgs.length).toBe(1);
                        expect(msgs[0]).toBeTruthy();
                        expect(msgs[0].id).toBe(id);
                    },
                }),
            ),
        ).subscribe({
            error: err => done.fail(err),
            complete: () => {
                done();
            },
        });

        tick(200);
    }));
});
