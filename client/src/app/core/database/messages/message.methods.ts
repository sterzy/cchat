import {MessageCollection, MessageCollectionMethods, MessageDocMethods, MessageDocument} from './message.types';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

export const messageDocMethods: MessageDocMethods = {
    toString(this: MessageDocument) {
        return this.toJSON().toString();
    },

    toDto(this: MessageDocument) {
        return {
            id: this.id,
            chat: this.chat,
            timeStamp: this.timeStamp,
            from: this.from,
            message: this.message,
            read: this.read,
            status: this.status,
            mediaType: this.mediaType,
        };
    },
};

export const messageCollectionMethods: MessageCollectionMethods = {
    getMessages(this: MessageCollection, chatId: string): Observable<MessageDocument[]> {
        return this.find({chat: {$eq: chatId}}).sort({timeStamp: -1}).$;
    },

    getMostRecentMessage(this: MessageCollection, chatId: string): Observable<MessageDocument> {
        return this.findOne({chat: {$eq: chatId}})
            .sort({timeStamp: -1})
            .$;
    },

    getUnreadMessages(this: MessageCollection, chatId?: string) {
        if (chatId == null) {
            return this.find()
                .sort({timeStamp: -1})
                .$;
        }

        return this.find({chat: {$eq: chatId}})
            .sort({timeStamp: -1})
            .$;
    },

    getUnreadCount(this: MessageCollection, chatId?: string) {

        const boundFunction: (chatId?: string) => Observable<MessageDocument[]> = messageCollectionMethods
            .getUnreadMessages
            .bind(this);

        return boundFunction(chatId).pipe(
            map(messages => messages.length),
        );
    },

    searchForMessages(this: MessageCollection, chatId: string, like: string) {
        return this.find({chat: {$eq: chatId}}).where('message').regex(new RegExp(like)).sort({timeStamp: -1}).$;
    },
};
