import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {MessageCollection, MessageDocType} from './message.types';
import {shareReplay, switchMap, tap} from 'rxjs/operators';
import {DatabaseDao} from '../database.dao';
import {messageSchema} from './message.schema';
import {messageCollectionMethods, messageDocMethods} from './message.methods';

/**
 * This service provides means to interact with the database.
 *
 * Methods here are "unsafe", meaning that they can fail and pass the database errors to the upper layer.
 */
@Injectable({
    providedIn: 'root'
})
export class MessageDao {

    /**
     * This is an Observable providing an instance of the Message collection. It will only be initialized once the
     * first service subscribes to it. After that the same instance is reused.
     */
    readonly messageCollection$: Observable<MessageCollection> = this.database.getDatabase().pipe(
        //
        // Message collection creation
        //
        switchMap(db => {
            return db.collection({
                name: 'messages',
                schema: messageSchema,
                methods: messageDocMethods,
                statics: messageCollectionMethods,
            }) as Promise<MessageCollection>;
        }),

        //
        // Message collection configuration
        //
        tap(coll => {
            // Nothing to do right now
        }),
        // shareReplay will cache the last returned value and not trigger another collection creation
        shareReplay(1),
    );

    constructor(
        private database: DatabaseDao,
    ) {
    }

    /**
     * Returns all messages for a chat.
     *
     * @param chatId The ID of the chat to get the messages for.
     */
    getMessages(chatId: string) {
        return this.messageCollection$.pipe(
            switchMap(coll => {
                return coll.getMessages(chatId);
            }),
            shareReplay(1),
        );
    }

    /**
     * Returns the most recent message of at given chat.
     *
     * @param chatId The chat to get the most recent message for.
     */
    getMostRecentMessage(chatId: string) {
        return this.messageCollection$.pipe(
            switchMap(coll => {
                return coll.getMostRecentMessage(chatId);
            }),
            shareReplay(1),
        );
    }

    /**
     * Adds a message to the database.
     *
     * @param msg The message to save.
     */
    addMessage(msg: MessageDocType) {
        return this.messageCollection$.pipe(
            switchMap(coll => {
                return coll.insert(msg);
            }),
            shareReplay(1),
        );
    }

    /**
     * Searches for messages like a given string in a given conversation.
     * @param chatId    ID of the conversation
     * @param like      search-term
     */
    searchForMessages(chatId: string, like: string) {
        return this.messageCollection$.pipe(
            switchMap(coll => {
                return coll.searchForMessages(chatId, like);
            }),
            shareReplay(1),
        );
    }
}

function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
        /* tslint:disable-next-line */
        const r = Math.random() * 16 | 0;
        /* tslint:disable-next-line */
        const v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
