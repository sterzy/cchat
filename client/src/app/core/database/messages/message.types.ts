import {RxCollection, RxDocument} from 'rxdb';
import {DatabaseMethods} from '../database.types';
import {Observable} from 'rxjs';

export enum MessageStatus {
    Sending = 0,
    Sent,
    Read,
    Received,
}

export enum MessageMediaType {
    Text = 0,
    Sticker,  // on type sticker the message key contains the url
}

export interface MessageDocType {
    id: string;
    from: string; // null means, that the message was sent to the other side
    message: string;
    timeStamp: number;
    read: boolean;
    chat: string;
    status: MessageStatus;
    mediaType: MessageMediaType;
}

export interface MessageDocMethods extends DatabaseMethods<MessageDocument> {
    toString: (this: MessageDocument) => string;
    toDto: (this: MessageDocument) => MessageDocType;
}

export type MessageDocument = RxDocument<MessageDocType, MessageDocMethods>;

export interface MessageCollectionMethods extends DatabaseMethods<MessageCollection> {
    getUnreadMessages: (this: MessageCollection, chatId?: string) => Observable<MessageDocument[]>;
    getUnreadCount: (this: MessageCollection, chatId?: string) => Observable<number>;
    getMessages: (this: MessageCollection, chatId: string) => Observable<MessageDocument[]>;
    getMostRecentMessage: (this: MessageCollection, chatId: string) => Observable<MessageDocument>;
    searchForMessages: (this: MessageCollection, chatId: string, like: string) => Observable<MessageDocument[]>;
}

export type MessageCollection = RxCollection<MessageDocType, MessageDocMethods, MessageCollectionMethods>;
