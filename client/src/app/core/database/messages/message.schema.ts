import {RxJsonSchema} from 'rxdb';

export const messageSchema: RxJsonSchema = {
    title: 'message schema',
    description: 'Schema for text based messages sent and received by clients',
    version: 0,
    // keyCompression: true,
    type: 'object',
    properties: {
        id: {
            type: 'string',
            final: true,
            primary: true,
        },
        from: {
            type: 'string',
            final: true,
        },
        message: {
            type: 'string',
        },
        timeStamp: {
            type: 'number',
            final: true,
            index: true,
        },
        read: {
            type: 'boolean',
            default: false,
        },
        chat: {
            type: 'string',
            ref: 'chats',
            index: true,
            final: true,
        },
        status: {
            type: 'number',
        },
        mediaType: {
            type: 'number',
            default: 0,  // text
        }
    },
    required: ['id', 'message', 'timeStamp', 'chat', 'status'],
};
