import {Injectable, isDevMode} from '@angular/core';
import {ConfigService} from '../services/config/config.service';
import {Observable} from 'rxjs';
import RxDB from 'rxdb';
import {filter, shareReplay, skipWhile, switchMap} from 'rxjs/operators';
import {tap} from 'rxjs/internal/operators/tap';
import {Database, DatabaseCollections} from './database.types';

import * as PouchDBAdapterIdb from 'pouchdb-adapter-idb';

RxDB.plugin(PouchDBAdapterIdb);

@Injectable({
    providedIn: 'root',
})
export class DatabaseDao {

    private database: Database;

    private database$: Observable<Database> = this.config.getUser().pipe(
        // Skip while the user is not logged in
        skipWhile(user => !user),
        tap(user => {
            // Check if user logged out or similar
            if (!user && this.database) {
                // User is logged out
                if (isDevMode()) {
                    console.log('Destroying old database');
                }
                this.database.destroy();
            }
        }),
        filter(userId => !!userId),
        switchMap(user => {
            if (isDevMode()) {
                console.log('Creating database for user', user);
            }
            return RxDB.create<DatabaseCollections>({
                name: 'cchat/core/' + user.id,
                adapter: 'idb',
            });
        }),
        tap(db => this.database = db),
        shareReplay(1),
    );

    constructor(
        private config: ConfigService,
    ) {
    }

    getDatabase() {
        return this.database$;
    }

    clearDatabase(): Observable<void> {
        return this.database$.pipe(
            switchMap(db => db.remove()),
        );
    }
}
