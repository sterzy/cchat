import {BotCollection, BotCollectionMethods, BotDocMethods, BotDocument} from './bot.types';
import {Observable} from 'rxjs';

export const botDocMethods: BotDocMethods = {
    toDto(this: BotDocument) {
        return {
            id: this.id,
            name: this.name,
            ownerId: this.ownerId,
            ownerName: this.ownerName,
            isActive: this.isActive,
            description: this.description,
            isPublic: this.isPublic,
        };
    },
};

export const botCollectionMethods: BotCollectionMethods = {
    getAllBots(this: BotCollection): Observable<BotDocument[]> {
        return this.find().$;
    },

    getBotById(this: BotCollection, id: string): Observable<BotDocument> {
        return this.findOne({id}).$;
    },

    getBotsByIds(this: BotCollection, ids: string[]): Observable<BotDocument[]> {
        return this.find({id: { $in: ids }}).$;
    },

    getBotByName(this: BotCollection, name: string): Observable<BotDocument> {
        return this.findOne({name}).$;
    },

    getBotsByNames(this: BotCollection, names: string[]): Observable<BotDocument[]> {
        return this.find({name: { $in: names }}).$;
    },

    getBotsByOwnerName(this: BotCollection, ownerName: string): Observable<BotDocument[]> {
        return this.find({ownerName}).$;
    },

    getBotsNotMatchingOwnerName(this: BotCollection, ownerName: string): Observable<BotDocument[]> {
        return this.find({ownerName: { $ne: ownerName}}).$;
    },
};
