import {RxJsonSchema} from 'rxdb';

export const botSchema: RxJsonSchema = {
    title: 'bots',
    description: 'Schema for bots',
    version: 0,
    // keyCompression: true,
    type: 'object',
    properties: {
        id: {
            type: 'string',
            primary: true,
        },
        name: {
            type: 'string',
            final: true,
            index: true,
        },
        description: {
            type: 'string',
        },
        ownerName: {
            type: 'string',
        },
        ownerId: {
            type: 'string',
        },
        isActive: {
            type: 'boolean',
            default: true,
        },
        isPublic: {
            type: 'boolean',
            default: true,
        },
    },
    attachments: {
        encrypted: false,
    },
    required: ['id', 'name', 'ownerName', 'ownerId'],
};
