import {Injectable} from '@angular/core';
import {DatabaseDao} from '../database.dao';
import {Observable} from 'rxjs';
import {BotCollection, BotDocType, BotDocument} from './bot.types';
import {shareReplay, switchMap, tap} from 'rxjs/operators';
import {botSchema} from './bot.schema';
import {botCollectionMethods, botDocMethods} from './bot.methods';
import {PartialBy} from '../../util/partialby.type';

@Injectable({
    providedIn: 'root'
})
export class BotDao {

    private botCollection: Observable<BotCollection> = this.databaseDao.getDatabase().pipe(
        //
        // Bot Collection creation
        //
        switchMap(db => {
            return db.collection({
                name: 'bots',
                schema: botSchema,
                methods: botDocMethods,
                statics: botCollectionMethods,
            }) as Promise<BotCollection>;
        }),

        //
        // Bot Collection configuration
        //
        tap(coll => {
            // nothing to to right now
        }),
        shareReplay(1),
    );

    constructor(
        private databaseDao: DatabaseDao,
    ) {
    }

    getBots(): Observable<BotDocument[]> {
        return this.botCollection.pipe(
            switchMap(coll => coll.getAllBots()),
        );
    }

    getBotById(id: string): Observable<BotDocument> {
        return this.botCollection.pipe(
            switchMap(coll => coll.getBotById(id)),
        );
    }

    getBotByName(name: string): Observable<BotDocument> {
        return this.botCollection.pipe(
            switchMap(coll => coll.getBotByName(name)),
        );
    }

    getBotsByName(names: string[]) {
        return this.botCollection.pipe(
            switchMap(coll => coll.getBotsByNames(names)),
        );
    }

    getBotsByOwnerName(owner: string): Observable<BotDocument[]> {
        return this.botCollection.pipe(
            switchMap(coll => coll.getBotsByOwnerName(owner)),
        );
    }

    getBotsWithOtherOwnerName(owner: string): Observable<BotDocument[]> {
        return this.botCollection.pipe(
            switchMap(coll => coll.getBotsNotMatchingOwnerName(owner)),
        );
    }

    saveBot(bot: PartialBy<BotDocType, 'isPublic' | 'isActive'>): Observable<BotDocument> {
        return this.botCollection.pipe(
            switchMap(coll => coll.upsert(bot)),
        );
    }
}
