import { TestBed } from '@angular/core/testing';

import { BotDao } from './bot.dao';

describe('BotDao', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BotDao = TestBed.get(BotDao);
    expect(service).toBeTruthy();
  });
});
