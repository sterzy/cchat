import {DatabaseMethods} from '../database.types';
import {RxCollection, RxDocument} from 'rxdb';
import {Observable} from 'rxjs';

export interface BotDocType {
    id: string;
    name: string;
    description: string;
    ownerId: string;
    ownerName: string;
    isActive: boolean;
    isPublic: boolean;
}

export interface BotDocMethods extends DatabaseMethods<BotDocument> {
    toDto: (this: BotDocument) => BotDocType;
}

export type BotDocument = RxDocument<BotDocType, BotDocMethods>;

export interface BotCollectionMethods extends DatabaseMethods<BotCollection> {
    getAllBots(this: BotCollection): Observable<BotDocument[]>;
    getBotById(this: BotCollection, id: string): Observable<BotDocument>;
    getBotsByIds(this: BotCollection, ids: string[]): Observable<BotDocument[]>;
    getBotByName(this: BotCollection, name: string): Observable<BotDocument>;
    getBotsByNames(this: BotCollection, names: string[]): Observable<BotDocument[]>;
    getBotsByOwnerName(this: BotCollection, ownerName: string): Observable<BotDocument[]>;
    getBotsNotMatchingOwnerName(this: BotCollection, ownerName: string): Observable<BotDocument[]>;
}

export type BotCollection = RxCollection<BotDocType, BotDocMethods, BotCollectionMethods>;
