export interface KeyPair {
    pubKey: ArrayBuffer;
    privKey: ArrayBuffer;
}

export interface LocalPreKey {
    keyId: number;
    keyPair: KeyPair;
}

export interface LocalSignedPreKey extends LocalPreKey {
    signature: ArrayBuffer;
}

export interface PublicPreKey {
    keyId: number;
    publicKey: ArrayBuffer;
}

export interface PublicSignedPreKey {
    keyId: number;
    publicKey: ArrayBuffer;
    signature: ArrayBuffer;
}

export interface PreKeyBundle {
    registrationId: number;
    identityKey: ArrayBuffer;
    signedPreKey: PublicSignedPreKey;
    preKey: PublicPreKey;
}

// This type will convert an object to one which has no ArrayBuffers. This is a deep conversion, so all sub-objects
// have to be encoded to string too.
export type Encode<T> = T extends ArrayBuffer ? string : // T is an ArrayBuffer, has to be converted to string
    T extends object ? ({
        [P in keyof T]: T[P] extends object ? Encode<T[P]> : // Type of field P is an object, so it has to be deep-converted
            T[P]; // Type is neither ArrayBuffer nor object, so use the type of field P
    }) : T;

export type EncodedKeyPair = Encode<KeyPair>;
