export interface SignalProtocolAddress {
    getName(): string;

    getDeviceId(): string;

    equal(other: any): boolean;
}
