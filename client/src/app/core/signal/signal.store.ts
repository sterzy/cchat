import {Injectable} from '@angular/core';
import {ChatDao} from '../database/chats/chat.dao';
import {LocaluserDao} from '../database/localusers/localuser.dao';
import {forkJoin, Observable, throwError} from 'rxjs';
import {LocalUserDocType, LocalUserDocument} from '../database/localusers/localuser.types';
import {KeyPair} from './models/keys.model';
import {catchError, first, map, mapTo, switchMap} from 'rxjs/operators';
import {RxAttachment} from 'rxdb';
import {of} from 'rxjs/internal/observable/of';
import {SignalProtocolAddress} from './models/signal.model';
import {arrayBufferToBase64, base64ToArrayBuffer} from '../util/array-buffer.util';

declare var libsignal: any;
declare var dcodeIO: any;

@Injectable({
    providedIn: 'root'
})
export class SignalStore {

    readonly Direction = {
        SENDING: 1,
        RECEIVING: 2,
    };

    private localUser$: Observable<LocalUserDocument> = this.localuserDao.getLocaluser();

    constructor(
        private localuserDao: LocaluserDao,
        private chatDao: ChatDao,
    ) {
    }

    /**
     * Method to save the identity KeyPair of the current user.
     * @param keyPair       the KeyPair to be saved
     */
    saveIdentityKeyPair(keyPair: KeyPair): Promise<void> {
        return this.localuserDao.storeIdentityKeyPar({
                privKey: arrayBufferToBase64(keyPair.privKey),
                pubKey: arrayBufferToBase64(keyPair.pubKey),
        }).pipe(mapTo(undefined)).toPromise();
    }

    /**
     * Method to get the identity KeyPair of the current user.
     */
    getIdentityKeyPair(): Promise<KeyPair> {
        return this.localuserDao.getIdentityKeyPair().pipe(
                first(),
                map((keyPair): KeyPair => {
                    if (!keyPair) {
                        return undefined;
                    }

                    return {
                        pubKey: base64ToArrayBuffer(keyPair.pubKey),
                        privKey: base64ToArrayBuffer(keyPair.privKey),
                    };
                }),
        ).toPromise();
    }

    /**
     * Method to get the local registration-id of the user.
     */
    getLocalRegistrationId(): Promise<number> {
        return this.localUser$.pipe(
            first(),
            map(user => user.registrationId),
        ).toPromise();
    }

    saveLocalRegistrationId(registrationId: number): Promise<void> {
        return this.localUser$.pipe(
            first(),
            switchMap(user => {
                return this.localuserDao.update(oldUser => {
                    oldUser.registrationId = registrationId;
                    return oldUser;
                });
            }),
            mapTo(undefined),
        ).toPromise();
    }

    /**
     * Checks whether a given identifier and identity-key match.
     * @param identifier        the identifier to be checked
     * @param identityKey       the corresponding identity-key
     * @param direction         sending or receiving
     */
    isTrustedIdentity(identifier: string, identityKey: ArrayBuffer, direction: number): Promise<boolean> {
        if (identifier == null) {
            throw new Error('tried to check identity key for undefined/null key');
        }
        if (!(identityKey instanceof ArrayBuffer)) {
            throw new Error('Expected identityKey to be an ArrayBuffer');
        }

        return this.chatDao.isTrustedIdentity(identifier, identityKey).toPromise();
    }

    /**
     * Loads an identity-key for a corresponding identifier.
     * @param identifier        the identifier to get the key for
     */
    loadIdentityKey(identifier: string): Promise<ArrayBuffer> {
        if (identifier == null) {
            throw new Error('Tried to get identity key for undefined/null key');
        }

        return this.chatDao.loadIdentityKey(identifier).toPromise();
    }

    /**
     * Method to save an identity and corresponding identity-KeyPair
     * @param identifier        to be saved
     * @param identityKey       to be saved
     */
    saveIdentity(identifier: string, identityKey: ArrayBuffer | string): Promise<boolean> {
        if (identifier == null) {
            throw new Error('Tried to put identity key for undefined/null key');
        }

        if (typeof identityKey === 'string') {
            identityKey = new dcodeIO.ByteBuffer.wrap(identityKey, 'binary').toArrayBuffer() as ArrayBuffer;
        }

        if (!(identityKey instanceof ArrayBuffer)) {
            throw new Error('Expected identityKey to be an ArrayBuffer');
        }

        const address: SignalProtocolAddress = new libsignal.SignalProtocolAddress.fromString(identifier);

        return this.chatDao.saveIdentity(address.getName(), identityKey).toPromise();

    }

    /* Returns a prekeypair object or undefined */
    loadPreKey(keyId: number): Promise<KeyPair> {
        return this.localuserDao.getPreKey(keyId).pipe(
            first(),
            map((keyPair): KeyPair => {
                if (!keyPair) {
                    return undefined;
                }

                return {
                    pubKey: base64ToArrayBuffer(keyPair.pubKey),
                    privKey: base64ToArrayBuffer(keyPair.privKey),
                };
            }),
        ).toPromise();
    }

    /**
     * Method to store a PreKey for the current user.
     * @param keyId         id of the Key
     * @param keyPair       Pre-KeyPair for the user
     */
    storePreKey(keyId: number, keyPair: KeyPair): Promise<void> {
        return this.localuserDao.storePreKey(keyId, {
            privKey: arrayBufferToBase64(keyPair.privKey),
            pubKey: arrayBufferToBase64(keyPair.pubKey),
        }).pipe(
            first(),
            mapTo(undefined),
        ).toPromise();
    }

    /**
     * Method to remove a PreKey from the store.
     * @param keyId         id of the key to be removed
     */
    removePreKey(keyId: number): Promise<void> {
        return this.localuserDao.removePreKey(keyId).pipe(
            first(),
            mapTo(undefined),
        ).toPromise();
    }

    /* Returns a signed keypair object or undefined */
    loadSignedPreKey(keyId: number): Promise<KeyPair> {
        return this.localuserDao.getSignedPreKey(keyId).pipe(
            first(),
            map((keyPair): KeyPair => {
                if (!keyPair) {
                    return undefined;
                }

                return {
                    pubKey: base64ToArrayBuffer(keyPair.pubKey),
                    privKey: base64ToArrayBuffer(keyPair.privKey),
                };
            }),
        ).toPromise();
    }


    /**
     * Method to store a signed pre-key.
     * @param keyId         id of the signed pre-key
     * @param keyPair       the pre-KeyPair to be stored
     */
    storeSignedPreKey(keyId: number, keyPair: KeyPair): Promise<void> {
        return this.localuserDao.storeSignedPreKey(keyId, {
            pubKey: arrayBufferToBase64(keyPair.pubKey),
            privKey: arrayBufferToBase64(keyPair.privKey),
        }).pipe(
            first(),
            mapTo(undefined),
        ).toPromise();
    }

    /**
     * Method to remove a signed pre-key.
     * @param keyId         id of the signed pre-key
     */
    removeSignedPreKey(keyId: number): Promise<void> {
        return this.localuserDao.removeSignedPreKey(keyId).pipe(
            first(),
            mapTo(undefined),
        ).toPromise();
    }

    /**
     * Method to load a session.
     * @param identifier    of the conversation partner
     */
    loadSession(identifier: string): Promise<string> {
        const address: SignalProtocolAddress = new libsignal.SignalProtocolAddress.fromString(identifier);
        return this.chatDao.loadSession(address.getName(), identifier).toPromise();
    }

    /**
     * Method to store a session.
     * @param identifier        of the conversation partner
     * @param record            session returned by signal library
     */
    storeSession(identifier: string, record: string): Promise<void> {
        const address: SignalProtocolAddress = new libsignal.SignalProtocolAddress.fromString(identifier);
        return this.chatDao.storeSession(address.getName(), identifier, record).toPromise();
    }

    /**
     * Method to remove a session from the store.
     * @param identifier        of the conversation partner
     */
    removeSession(identifier: string): Promise<void> {
        const address: SignalProtocolAddress = new libsignal.SignalProtocolAddress.fromString(identifier);
        return this.chatDao.removeSession(address.getName(), identifier).toPromise();
    }

    /**
     * Method to remove all sessions.
     * @param identifier        of the conversation partner
     */
    removeAllSessions(identifier: string): Promise<void> {
        const address: SignalProtocolAddress = new libsignal.SignalProtocolAddress.fromString(identifier);
        return this.chatDao.removeAllSessions(address.getName(), identifier).toPromise();
    }

    /**
     * Gets a new unique KeyId for the current user.
     */
    getNewKeyId(): Observable<number> {
        return this.localuserDao.getKeyId();
    }

    /**
     * Removes a keypair with given names from the current user.
     *
     * @param privKeyName The name of the private key.
     * @param pubKeyName The name of the public key.
     */
    private removeKeyPair(privKeyName: string, pubKeyName: string): Observable<void> {
        return this.localUser$.pipe(
            first(), // Only take one user, otherwise we might load the keys from the wrong user (race condition)
            switchMap(user => {
                return forkJoin(
                    // NOTE: casting to unknown and then to RxAttachment is just a hotfix, because the lib has the wrong
                    // types
                    (user.getAttachment(privKeyName) as unknown as RxAttachment<{}, {}>).remove(),
                    (user.getAttachment(pubKeyName) as unknown as RxAttachment<{}, {}>).remove(),
                );
            }),
            mapTo(undefined),
        );
    }
}
