import {async, TestBed} from '@angular/core/testing';
import {DatabaseDao} from '../database/database.dao';
import {ConfigService} from '../services/config/config.service';
import {SignalStore} from '../signal/signal.store';
import {RouterModule} from '@angular/router';
import {CryptoService, Initial, KeyBundle} from '../../client/services/crypto/crypto.service';
import {interval, Subject, throwError} from 'rxjs';
import {filter, first, switchMap, takeUntil} from 'rxjs/operators';
import {ChatDao} from '../database/chats/chat.dao';
import {ChatDocType, ChatType} from '../database/chats/chat.types';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {MatSnackBarModule} from '@angular/material';

describe('SignalLibrary', () => {

    let db: DatabaseDao;
    let config: ConfigService;
    let signalStore: SignalStore;
    let cryptoService: CryptoService;
    let chatDao: ChatDao;
    const originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
    const testUserId = 'test';
    const testUser = {
        id: testUserId,
        username: testUserId,
    };

    const killSwitch = new Subject();
    let httpMock: HttpTestingController;

    const userChat: ChatDocType = {
        id: testUserId,
        name: testUserId,
        lastOnline: Date.now(),
        users: [testUserId],
        type: ChatType.DirectMessage,
        started: Date.now(),
        lastMessage: Date.now(),
        blocked: false,
        verifiedKey: undefined,
        hidden: false,
    };

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule, RouterModule.forRoot([]), MatSnackBarModule],
            providers: [],
        });
    }));

    beforeEach(() => {
        httpMock = TestBed.get(HttpTestingController);
        db = TestBed.get(DatabaseDao);
        config = TestBed.get(ConfigService);
        config.setUser(testUser);
        signalStore = TestBed.get(SignalStore);
        cryptoService = TestBed.get(CryptoService);
        chatDao = TestBed.get(ChatDao);

        jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000;
    });

    beforeEach(done => {
        chatDao.saveChat(userChat).subscribe(() => done(), err => done.fail(err));
    });

    afterEach(done => {
        db.clearDatabase().subscribe(() => done(), err => done.fail(err));
    });

    afterEach(done => {
        killSwitch.pipe(first()).subscribe(() => done());
        killSwitch.next();
    });

    it('should generate initial keys', done => {
        getIdentityKeyPair().subscribe(
            () => done(),
            err => done.fail(err),
        );
    });

    it('should generate preKey with id 1', done => {
        getPreKey().subscribe(
            () => done(),
            err => done.fail(err),
        );
    });


    it('should generate signed preKey for id 6', done => {
        getSignedPreKey().subscribe(
            () => done(),
            err => done.fail(err),
        );
    });

    it('should generate a session', done => {
        let httpRequest = httpMock.expectOne('/user/api/keys/initial');
        expect(httpRequest.request.method).toBe('POST');
        const data: Initial = httpRequest.request.body;

        const mock: KeyBundle = {
            identityKey: data.identityKey,
            preKey: data.preKeys[0],
            registrationId: data.registrationId,
            signedPreKey: data.signedPreKey,
        };

        cryptoService.build_session(testUserId).subscribe(() => {
                done();
            },
            err => {
                done.fail(err);
            }
        );

        setTimeout(() => {
            httpRequest = httpMock.expectOne('/user/api/keys/keybundle/' + testUserId);
            expect(httpRequest.request.method).toBe('GET');
            httpRequest.flush(mock);
            httpMock.verify();
        }, 200);
    });

    it('should decrypt a message encrypted by ourselves', done => {

        let httpRequest = httpMock.expectOne('/user/api/keys/initial');
        expect(httpRequest.request.method).toBe('POST');
        const data: Initial = httpRequest.request.body;

        console.warn(data);

        const mock: KeyBundle = {
            identityKey: data.identityKey,
            preKey: data.preKeys[0],
            registrationId: data.registrationId,
            signedPreKey: data.signedPreKey,
        };

        const testMsg = 'test message';

        cryptoService.build_session(testUserId).pipe(
            switchMap(
                () => cryptoService.encrypt(testMsg, userChat),
            ),
            switchMap(
                ciphertext => {
                    if (ciphertext.body === testMsg) {
                        return throwError('message was not encrypted');
                    }
                    return cryptoService.decrypt(ciphertext, userChat).then(plaintext => {
                            if (plaintext === testMsg) {
                                done();
                                return;
                            }
                            done.fail('plaintext does not match pt: ' + plaintext + '; msg: ' + testMsg);
                        }
                    );
                }
            )
        ).subscribe(done, done.fail);

        setTimeout(() => {
            httpRequest = httpMock.expectOne('/user/api/keys/keybundle/' + testUserId);
            expect(httpRequest.request.method).toBe('GET');
            httpRequest.flush(mock);
            httpMock.verify();
        }, 200);
    });

    it('encryption without key fails', done => {
        cryptoService.encrypt('hi', userChat).then(
            ciphertext => done.fail('encryption without keys should fail'),
            () => done());
    });

    /**
     * Helper functions
     */

    const getIdentityKeyPair = () => interval(1000).pipe(
        takeUntil(killSwitch),
        switchMap(() => signalStore.getIdentityKeyPair()),
        filter(keys => !!keys),
        first(),
    );

    const getPreKey = () => interval(1000).pipe(
        takeUntil(killSwitch),
        switchMap(() => signalStore.loadPreKey(1)),
        filter(keys => !!keys),
        first(),
    );

    const getSignedPreKey = () => interval(1000).pipe(
        takeUntil(killSwitch),
        switchMap(() => signalStore.loadSignedPreKey(6)),
        filter(keys => !!keys),
        first(),
    );
});
