import {RxAttachment, RxDocument} from 'rxdb';
import {from, Observable, of, Subject} from 'rxjs';
import {switchMap} from 'rxjs/operators';

export function arrayBufEqual(buf1: ArrayBuffer, buf2: ArrayBuffer): boolean {
    if (buf1 === buf2) {
        return true;
    }

    if (buf1.byteLength !== buf2.byteLength) {
        return false;
    }

    let equal = true;

    const dv1 = new Int8Array(buf1);
    const dv2 = new Int8Array(buf2);

    for (let i = 0; i !== buf1.byteLength; i++) {
        equal = equal && (dv1[i] === dv2[i]);
    }

    return equal;

}

/**
 * Get an attachment from a document in ArrayBuffer form.
 *
 * @param doc The doc to get the attachment from. Can be any RxDocument.
 * @param key The name of the attachment to retrieve.
 */
export function getAttachment<T = {}, M = {}>(doc: RxDocument<T, M>, key: string): Observable<ArrayBuffer> {
    // NOTE: This workaround is needed because getAttachment does not return a promise, but the attachment itself
    if (!doc) {
        return of(undefined);
    }
    return of(doc.getAttachment(key) as unknown as RxAttachment<T, M>).pipe(
        switchMap(att => {
            if (att == null) {
                // No attachment for the given name, return undefined
                return of(undefined);
            }
            // map the attachment to its data
            return from(att.getData()).pipe(
                switchMap(blob => {
                    // map the blob to an ArrayBuffer
                    return blobToArrayBuffer(blob);
                }),
            );
        }),
    );
}

/**
 * Conversion method to get an ArrayBuffer from a Blob.
 *
 * @param blob The blob to convert.
 */
function blobToArrayBuffer(blob: Blob): Observable<ArrayBuffer> {
    if (blob == null) {
        return of(undefined);
    }
    const sub = new Subject<ArrayBuffer>();
    const fileReader = new FileReader();
    fileReader.onload = () => {
        sub.next(fileReader.result as ArrayBuffer);
        sub.complete();
    };
    fileReader.onerror = reason => {
        sub.error(reason);
    };
    fileReader.readAsArrayBuffer(blob);
    return sub;
}

/**
 * Converts a string encoded in base64 to an ArrayBuffer.
 *
 * @author Magdalena
 * @param base64 The base64 string to decode.
 */
export function base64ToArrayBuffer(base64: string): ArrayBuffer {
    const binaryString = window.atob(base64);
    const len = binaryString.length;
    const bytes = new Uint8Array(len);
    for (let i = 0; i < len; i++) {
        bytes[i] = binaryString.charCodeAt(i);
    }
    return bytes.buffer;
}

/**
 * Converts an ArrayBuffer to a base64 string.
 *
 * @author Magdalena
 * @param buff The buffer to encode.
 */
export function arrayBufferToBase64(buff: ArrayBuffer): string {
    let binary = '';
    const bytes = new Uint8Array(buff);
    const len = bytes.byteLength;
    for (let i = 0; i < len; i++) {
        binary += String.fromCharCode(bytes[i]);
    }
    return window.btoa(binary);
}
