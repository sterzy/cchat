/**
 * Omit will exlude some fields from a given Type.
 *
 * Example:
 *
 * Omit<{id: string, name: string}, 'id'> => {name: string}
 */
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;

/**
 * PartialBy will make some fields optional.
 *
 * Example:
 *
 * PartialBy<{id: string, name: string}, 'id'>  =>  {id?: string, name: string}
 */
export type PartialBy<T, K extends keyof T> = Omit<T, K> & Partial<Pick<T, K>>;
