/**
 * This User interface is used in the JWT token that is set as a cookie.
 */
export interface SimpleUser {
    id: string; // the uuid of the user
    username: string;
}
