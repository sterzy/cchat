import {Component, OnInit} from '@angular/core';
import {fromEvent} from 'rxjs';
import {filter, first, switchMap} from 'rxjs/operators';
import {throttleTime} from 'rxjs/operators';
import {ChatService} from '../../../client/services/chat/chat.service';
import {CommunicationService} from '../../../client/services/communication/communication.service';

@Component({
    selector: 'cchat-root',
    templateUrl: './root.component.html',
    styleUrls: ['./root.component.scss']
})
export class RootComponent implements OnInit {

    constructor(
        private chatService: ChatService,
        private communicationService: CommunicationService
    ) {
    }

    ngOnInit() {
        fromEvent(document, 'mousemove')
            .pipe(
                throttleTime(10000),
                switchMap(() => this.chatService.getSelectedChat().pipe(
                    filter(chat => !!chat),
                    first(),
                )),
            )
            .subscribe(chat => {
                this.communicationService.sendLastOnline(chat);
            });
    }

}
