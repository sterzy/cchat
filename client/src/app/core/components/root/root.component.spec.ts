import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RootComponent} from './root.component';
import {CoreModule} from '../../core.module';
import {RouterModule} from '@angular/router';
import {TEST_RX_STOMP_PROVIDERS} from '../../../util/test/providers';
import {HttpClientModule} from '@angular/common/http';
import {MatSnackBarModule} from '@angular/material';

describe('RootComponent', () => {
    let component: RootComponent;
    let fixture: ComponentFixture<RootComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [CoreModule, RouterModule.forRoot([]), HttpClientModule, MatSnackBarModule],
            providers: TEST_RX_STOMP_PROVIDERS,
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RootComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
