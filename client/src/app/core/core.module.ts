import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RootComponent} from './components/root/root.component';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {PubKeyPipe} from './pipes/pub-key.pipe';
import {InitialsPipe} from './pipes/initials.pipe';
import {ColorPipe} from './pipes/color.pipe';

export const IMPORTS = [
    CommonModule,
    RouterModule,
    FormsModule,
];

export const DECLARATIONS = [
    RootComponent,
    PubKeyPipe,
    InitialsPipe,
    ColorPipe,
];

@NgModule({
    declarations: DECLARATIONS,
    imports: IMPORTS,
    exports: DECLARATIONS,
    /*providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: CryptoMockInterceptor,
            multi: true,
        }
    ]*/
})
export class CoreModule {
}
