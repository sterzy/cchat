import {Injectable, isDevMode} from '@angular/core';
import {fromEvent, Observable} from 'rxjs';
import {map, shareReplay, startWith, tap} from 'rxjs/operators';

const PREFIX = '--bp-';
const SIZES = ['xs', 'sm', 'md', 'lg', 'xl'];

export interface Matchers {
    xs: {
        eq: Observable<boolean>;
        gt: Observable<boolean>;
    };
    sm: {
        lt: Observable<boolean>;
        eq: Observable<boolean>;
        gt: Observable<boolean>;
    };
    md: {
        lt: Observable<boolean>;
        eq: Observable<boolean>;
        gt: Observable<boolean>;
    };
    lg: {
        lt: Observable<boolean>;
        eq: Observable<boolean>;
        gt: Observable<boolean>;
    };
    xl: {
        lt: Observable<boolean>;
        eq: Observable<boolean>;
    };
}

@Injectable({
    providedIn: 'root'
})
export class ResponsiveService {

    matchers: Matchers = {} as Matchers;

    constructor() {
        const bodyStyles = window.getComputedStyle(document.body);

        const sizes = SIZES.map(sizeName => ({
            sizeName,
            size: bodyStyles.getPropertyValue(PREFIX + sizeName),
        }));

        if (isDevMode()) {
            // Check if all sizes are found
            if (isDevMode()) {
                console.log('Found sizes in body styles:', sizes);
            }
            sizes.forEach(size => {
                if (!size.size) {
                    throw Error(`Size ${size.sizeName} was not set in body styles. Expected property '${PREFIX + size.sizeName}'`);
                }
            });
        }

        // Create for every size the appropriate matchers
        for (let i = 0; i < sizes.length; i++) {
            // First, get the sizeName and actual size
            const sizePair = sizes[i];
            const size = sizePair.size;
            const sizeName = sizePair.sizeName;

            // Define all three variables now (even if they stay undefined (see later)
            let lt: Observable<boolean>;
            let eq: Observable<boolean>;
            let gt: Observable<boolean>;

            if (i !== 0) {
                // This is not the first size, so we want a "less than" matcher
                lt = this.getMatcher(`(max-width: ${this.getUpperBound(size)})`, 'lt ' + sizeName);
            }

            if (i !== sizes.length - 1) {
                // This size is not the last one, so we want a "greater than" matcher and the "equal" matcher has an
                // upper bound
                const nextSizePair = sizes[i + 1];
                eq = this.getMatcher(`(min-width: ${size}) and (max-width: ${this.getUpperBound(nextSizePair.size)})`, sizeName);

                gt = this.getMatcher(`(min-width: ${size})`, 'gt ' + sizeName);
            } else {
                // This is the last size in the array, so we dont need a "greater than" matcher and no upper bound for
                // the "equals" matcher
                eq = this.getMatcher(`(min-width: ${size})`, sizeName);
            }

            // Here we set all matcher-Observables to the matchers objects
            // If any of them are not set (undefined), they will not be set to the object, so we can have simpler
            // logic that way.
            this.matchers[sizeName] = {
                lt,
                eq,
                gt,
            };
        }

        if (isDevMode()) {
            console.log('Matchers init done:', this.matchers);
        }
    }

    /**
     * This function will use the query to return a Observable that tells if the query is currently matched or not.
     *
     * @param query The query to match.
     * @param debugName The name to log when dev mode is active.
     */
    private getMatcher(query: string, debugName?: string): Observable<boolean> {
        const matcher = matchMedia(query);
        if (isDevMode()) {
            return fromEvent<MediaQueryListEvent>(matcher, 'change').pipe(
                map(event => event.matches),
                startWith(matcher.matches),
                tap(matches => console.warn(`Window matches ${debugName}:`, matches)),
                shareReplay(1),
            );
        }
        return fromEvent<MediaQueryListEvent>(matcher, 'change').pipe(
            map(event => event.matches),
            startWith(matcher.matches),
            shareReplay(1),
        );
    }

    /**
     * Helper function to calculate the actual size that can be used as an upper bound for the next value.
     *
     * The reason this method is needed is because we need a range like [md, lg) and not [md, lg]. In the second case
     * there might be issues that multiple profiles are true simultaneously. In the example above, if the viewport
     * matches exactly the size lg, both lg.eq and md.eq would be true.
     *
     * @param size The value of the next level that should not be included.
     */
    private getUpperBound(size: string): string {
        const sizeInt = parseInt(size, 10);
        if (isNaN(sizeInt)) {
            // Cant parse as number, just return size
            if (isDevMode()) {
                console.error('Got breakpoint size, which is not a number: ', size);
            }
            return size;
        } else {
            // Now we need to extract the unit, which is appended to the breakpoint-string
            let unit = '';
            if (sizeInt.toString(10) !== size) { // This is checks if there is a unit attached at all
                unit = size.match(/[0-9]+(.+)/)[1];
            }
            return (sizeInt - .02) + unit;
        }
    }
}
