import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, ReplaySubject} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {UserManagementService} from '../../../landingpage/services/user-management.service';
import {SimpleUser} from "../../models/user.models";

@Injectable({
    providedIn: 'root'
})
export class ConfigService {

    private userId$ = new ReplaySubject<SimpleUser>(1);

    constructor(
    ) {
    }

    setUser(userId: SimpleUser) {
        this.userId$.next(userId);
    }

    getUser(): Observable<SimpleUser> {
        return this.userId$;
    }
}
