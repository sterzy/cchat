import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Initial, KeyBundle} from '../../client/services/crypto/crypto.service';

@Injectable()
export class CryptoMockInterceptor implements HttpInterceptor {

    private intercepted: Initial = JSON.parse( "{\"identityKey\":\"BQTQXLOAwIo9oY4hktok7fo7ojYRSAXyJ1gXnt1BGHYZ\",\"preKeys\":[{\"publicKey\":\"BRQsnqeWItrT6VJSNZDY1BXVV44MygKcLT4nVWN7O/Ag\",\"keyId\":154},{\"publicKey\":\"BT44PvCHY55/dvff0Sk0PXIth8JdqggSetW/AJniFuZi\",\"keyId\":155},{\"publicKey\":\"Bcm8kCx8ViflZVdWHtoJmVZ7jK+X94GwGSmEnZpMlvIl\",\"keyId\":156},{\"publicKey\":\"BWHCb73md7RsPm2u+Zgrc4ZJ4bvMxvP4LaAPaS3IfWRW\",\"keyId\":157},{\"publicKey\":\"BTo+8ZmXmve6GjSP5RowtacwiymIDUPXNYpYSXilX3B2\",\"keyId\":158},{\"publicKey\":\"BbM9ZiLXF8tbHMaQXtiGBfl0+iBy+26x/VaQauJfpQFQ\",\"keyId\":159},{\"publicKey\":\"Bdotj8H/HI/PXcLiNyPFC9P8n1gyvAXk9FBqNWJUZFpi\",\"keyId\":160},{\"publicKey\":\"Bci5F1xK67D4czlNhXErv4U12iyguc3qXG9gKN8zjpQp\",\"keyId\":161},{\"publicKey\":\"Bd1t66UUlRPkdo21Ews1Bm7lGbZoEuceL8/UXiwjJcVg\",\"keyId\":162},{\"publicKey\":\"BW3Y3AV1on2pihcyddlQ4Nu3mnfpIw0J0uAzrHcCFGh7\",\"keyId\":163},{\"publicKey\":\"BcKTap6aR6dngORj3bEpWJn4f+I4OKUadwuA0NSuVgk9\",\"keyId\":164},{\"publicKey\":\"Bb/A7TLre+B6G9kR32xiq5ULtIsHvkXKA4h9sHYxuKtm\",\"keyId\":165},{\"publicKey\":\"BQ/ugrp28hoad8+1/cjnNhRB4zMSUAKTGHDaomoCIR9k\",\"keyId\":166},{\"publicKey\":\"BV/GY7GdrvOgSbfpzJtT/gaMBiedtA8R0LaSQNHpKhlx\",\"keyId\":167},{\"publicKey\":\"BTe5GMkecmePaxDzzxrsAccaqm1ZLmsKdz+spJLDXF5J\",\"keyId\":168},{\"publicKey\":\"Bcj26s7IN602Pb4rkA65InL6Peg8wki/PDA6K9mbJDk4\",\"keyId\":169},{\"publicKey\":\"Bcu5pdZrIuvXljxAx2lHY5GoZgj3YNZqf+gOiWxSxU5K\",\"keyId\":170},{\"publicKey\":\"BbCzluDODaR4fv/8fQ7mPNc3NpMU4Q0sH/5kT0q9FgAb\",\"keyId\":171},{\"publicKey\":\"Bc2Gex6xAH5E5E2oYCiNlJvklCuOuYoXBZ/KqjT1+Ppf\",\"keyId\":172},{\"publicKey\":\"BUwQy/p72RrnuZEuvWt557adWzgkFoBpvLozSjASXTFT\",\"keyId\":173},{\"publicKey\":\"BXIU18nj7PvSeAS+eeCRzBNscwgv/8XxU8DFI0AhOmNf\",\"keyId\":174},{\"publicKey\":\"BWbu0jT+7WK4lZuRfeVt33LQ1txD4yLrh1SwoDvX9DQQ\",\"keyId\":175},{\"publicKey\":\"BX7VrhEPIOIn4K9XfGSNZsBN1jOuxmC6L7M/n5jYtHtx\",\"keyId\":176},{\"publicKey\":\"BcpOs8ty0hjjQ1D66FfOU3fvHUPl8JYusptkraVGSZsZ\",\"keyId\":177},{\"publicKey\":\"BfNJkVN+AN9Qf4Px5BRJ8RYf1ksRPHwxiOeMQL1zNjUa\",\"keyId\":178},{\"publicKey\":\"BdnISgtPX8ekZkSqq8CPgIhlMcmCnK8sjnnUCbQrI8t1\",\"keyId\":179},{\"publicKey\":\"BfUJQp70osfwXfj9Zqdiq6vPPMsOZpR+RJdLhGzbhAQH\",\"keyId\":180},{\"publicKey\":\"BXMKSQFFvEv3RlogjO4twNVFqPYdlyHlE5arsQD7XBoU\",\"keyId\":181},{\"publicKey\":\"Bcsq8GcjYD83pJE6Ag5ARfMn37ogK9IaHO7Y7LqKof58\",\"keyId\":182},{\"publicKey\":\"BaPoEw0kNtq9M1khs8vzGnIDA/IDI5E3qxcNEX5hryx8\",\"keyId\":183},{\"publicKey\":\"BUV5X0culwOKs3XteVKaiVX9NbPSCxYk+L4aplo8+JBq\",\"keyId\":184},{\"publicKey\":\"BcjRJ41ZGs5cJ8wmAilMwtTiBoTM9bIIyekdo0+lIsQQ\",\"keyId\":185},{\"publicKey\":\"BZhJNuArhG1h18ywrnXmcZTnRW2vvL8R7b3stCEv+gcp\",\"keyId\":186},{\"publicKey\":\"BfrpFjgxaeXFlVG2cJUZWpjv02ruMbWUK1N3t5Lb5Y8R\",\"keyId\":187},{\"publicKey\":\"BRBhOhz3sw77jPidLmF6QAIG96FlE1WAcHuNChIZ3okH\",\"keyId\":188},{\"publicKey\":\"BZZbgD/hvxH8IEA/ePEls8aJtid2tE1ylRDWpz9MlXAs\",\"keyId\":189},{\"publicKey\":\"BdaAYtl0xI3CS7S/bz8sn0FTpyZRAA0ZrpEn2ERErBt0\",\"keyId\":190},{\"publicKey\":\"BcnyeoQI8E0zmy7ElZZLFuFVguDUDX+MOboPxTzYss4c\",\"keyId\":191},{\"publicKey\":\"BRD8rQ3iDlrogpqc7AY/O4MktG2n3iY8pLE9geWssadh\",\"keyId\":192},{\"publicKey\":\"Bdz/jDXoKYyzRLpjQpCpr6CTtCMdjDW4M/YHuYXdEWMF\",\"keyId\":193},{\"publicKey\":\"BWn4wwsdhupq9ZrCQbb4SMc/CiTdARdepdxBhrfmN9Qc\",\"keyId\":194},{\"publicKey\":\"BSf/OW6hrpoLc4trbb4Mnx2XD1iSWMXjYkNzl5PchVEE\",\"keyId\":195},{\"publicKey\":\"BRcuW02j1W6wac0SABqbeGi/gUjFd23pJX17E+R5cxF+\",\"keyId\":196},{\"publicKey\":\"BdB0jblsU9tzmm2xMCr7nL6Kml8J91D2E6Z9d1aRmjBj\",\"keyId\":197},{\"publicKey\":\"BamSgcAQiNTeKnMnY+351BbAsUgTi+7cEIUJbnpmkpYr\",\"keyId\":198},{\"publicKey\":\"BXnfWNfWqSuOf6DPaK6mfGiKUnsa23QSC99NYKLhAalP\",\"keyId\":199},{\"publicKey\":\"BUTqOmPAY/yDLbWxO2eFx8sSZw/lm6qbkmEU7YFxW3Fu\",\"keyId\":200},{\"publicKey\":\"Ba2JFcW19cMAZxbck+pSYA+PeYdrFGtUqJBANSg3xgs3\",\"keyId\":201},{\"publicKey\":\"BdDlKKznauYtLVZUARwzF8NTs/a9jZuHakWZE2fNlQ9y\",\"keyId\":202},{\"publicKey\":\"BRMxDeNtyv3OWKPt1bAgOn1VfXzDp1SgkFBuy727mFZ8\",\"keyId\":203}],\"registrationId\":5637,\"signedPreKey\":{\"keyId\":204,\"publicKey\":\"BWFWDQ4G977FBcd/GbuGsvUw5DNZB/U2RRWilihfMuka\",\"signature\":\"ksfWDCfHEtNqqQL3Ol9kFYdztzku6YZExXOVML3AuBoFn/BA308qSISF3Mka1SKdhynqy+oYYndbPTQABAg7gw==\"}}");

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        if (req.url.endsWith('user/api/keys/initial')) {

            console.warn('intercepted call', req.body);

            this.intercepted = req.body;

            return of(new HttpResponse({status: 200}));
        } else if (req.url.endsWith('user/api/keys/keybundle/test')) {
            const response = new HttpResponse<KeyBundle>({
                status: 200,
                body: {
                    registrationId: this.intercepted.registrationId,
                    identityKey: this.intercepted.identityKey,
                    signedPreKey: this.intercepted.signedPreKey,
                    preKey: this.intercepted.preKeys[0],
                }
            });

            return of(response);
        }

        return next.handle(req);
    }
}
