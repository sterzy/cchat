import {inject, TestBed} from '@angular/core/testing';

import {IsNotRegisteredGuard} from './is-not-registered.guard';
import {RouterModule} from '@angular/router';

describe('IsNotRegisteredGuard', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [RouterModule.forRoot([])],
            providers: [IsNotRegisteredGuard]
        });
    });

    it('should be created', inject([IsNotRegisteredGuard], (guard: IsNotRegisteredGuard) => {
        expect(guard).toBeTruthy();
    }));
});
