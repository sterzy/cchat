import {inject, TestBed} from '@angular/core/testing';

import {IsRegisteredGuard} from './is-registered.guard';
import {RouterModule} from '@angular/router';

describe('IsRegisteredGuard', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [RouterModule.forRoot([])],
            providers: [IsRegisteredGuard],
        });
    });

    it('should be created', inject([IsRegisteredGuard], (guard: IsRegisteredGuard) => {
        expect(guard).toBeTruthy();
    }));
});
