import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {ConfigService} from '../services/config/config.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class IsNotRegisteredGuard implements CanActivate {

    constructor(
        private router: Router,
        private config: ConfigService,
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UrlTree | true> {
        return this.config.getUser().pipe(
            map(user => {
                if (!user) {
                    return true;
                }
                return this.router.createUrlTree(['/']);
            })
        );
    }
}
