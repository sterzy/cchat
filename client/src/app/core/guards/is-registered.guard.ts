import {Injectable} from '@angular/core';
import {CanActivate, Router, UrlTree} from '@angular/router';
import {ConfigService} from '../services/config/config.service';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class IsRegisteredGuard implements CanActivate {

    constructor(
        private router: Router,
        private config: ConfigService,
    ) {
    }

    canActivate(): Observable<UrlTree | true> {
        return this.config.getUser().pipe(
            map(user => {
                if (!user) {
                    return this.router.createUrlTree(['/login']);
                }
                return true;
            })
        );
    }
}
