import {InjectableRxStompConfig, RxStompService, rxStompServiceFactory} from '@stomp/ng2-stompjs';
import {clientStompTestConfig} from './client-stop-test.comfig';

export const TEST_RX_STOMP_PROVIDERS = [
    {
        provide: InjectableRxStompConfig,
        useValue: clientStompTestConfig,
    },
    {
        provide: RxStompService,
        useFactory: rxStompServiceFactory,
        deps: [InjectableRxStompConfig]
    }
];
