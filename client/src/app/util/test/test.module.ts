import {
    ANALYZE_FOR_ENTRY_COMPONENTS,
    Component,
    InjectionToken,
    ModuleWithProviders,
    NgModule,
    Type
} from '@angular/core';
import {MatDialogModule, MatInputModule} from '@angular/material';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {OverlayContainer} from '@angular/cdk/overlay';

// Taken from http://angular-tips.com/blog/2018/02/testing-angular-material-dialog-templates/

export const OVERLAY_ELEMENT = new InjectionToken<HTMLElement>('test_overlay_component');

// Noop component is only a workaround to trigger change detection
@Component({
    template: ''
})
export class NoopComponent {
}

let overlayContainerElement = document.createElement('div');

@NgModule({
    imports: [MatDialogModule, NoopAnimationsModule, MatInputModule, FormsModule, ReactiveFormsModule],
    exports: [NoopComponent],
    declarations: [NoopComponent],
    providers: [
        {
            provide: OverlayContainer,
            useFactory: () => {
                return {getContainerElement: () => overlayContainerElement};
            }
        },
        {
            provide: OVERLAY_ELEMENT,
            useFactory: () => {
                return overlayContainerElement;
            },
        }
    ]
})
export class DialogTestModule {
    static forTest(entryComponents: Type<any> | Type<any>[]): ModuleWithProviders {
        overlayContainerElement = document.createElement('div')
        return {
            ngModule: DialogTestModule,
            providers: [
                {provide: ANALYZE_FOR_ENTRY_COMPONENTS, useValue: entryComponents, multi: true},
            ]
        };
    }
}
