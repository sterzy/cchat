import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ChatBoxComponent} from './components/chat-box/chat-box.component';
import {ChatListComponent} from './components/chat-list/chat-list.component';
import {ROUTES} from './routes';
import {ChatRootComponent} from './components/chat-root/chat-root.component';
import {BotDashboardComponent} from './components/bot-dashboard/bot-dashboard.component';

const routes: Routes = [
    {
        path: '',
        component: ChatRootComponent,
        children: [
            {path: '', outlet: 'aside', component: ChatListComponent},
            {
                path: ROUTES.CHAT + '/:chatId', component: ChatBoxComponent,
            },
            {
                path: ROUTES.BOTS, component: BotDashboardComponent,
            },
        ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
