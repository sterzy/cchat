import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {StickerListComponent} from './sticker-list.component';
import {MAT_DIALOG_DATA, MatDialog, MatDialogModule, MatIconModule, MatSnackBarModule} from '@angular/material';
import {Component, NgModule} from '@angular/core';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {OverlayContainer} from '@angular/cdk/overlay';
import {RxStompService} from '@stomp/ng2-stompjs';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';

describe('StickerListComponent', () => {
    let component: StickerListComponent;
    let fixture: ComponentFixture<NoopComponent>;

    let overlayContainerElement: HTMLElement;
    let dialog: MatDialog;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [DialogTestModule],
            providers: [
                {
                    provide: OverlayContainer, useFactory: () => {
                        overlayContainerElement = document.createElement('div');
                        return {getContainerElement: () => overlayContainerElement};
                    }
                }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NoopComponent);
        dialog = TestBed.get(MatDialog);
        component = dialog.open(StickerListComponent).componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});

// Taken from http://angular-tips.com/blog/2018/02/testing-angular-material-dialog-templates/

// Noop component is only a workaround to trigger change detection
@Component({
    template: ''
})
class NoopComponent {
}

@NgModule({
    imports: [MatDialogModule, NoopAnimationsModule, MatIconModule, MatDialogModule, RouterModule.forRoot([]),
    HttpClientModule, MatSnackBarModule],
    exports: [StickerListComponent, NoopComponent],
    declarations: [StickerListComponent, NoopComponent],
    providers: [RxStompService, {
        provide: MAT_DIALOG_DATA,
        useValue: {
            stickerPacks: null,
            chat: null,
        }
    }],
    entryComponents: [
        StickerListComponent
    ],
})
class DialogTestModule {
}
