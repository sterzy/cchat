import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {StickerAddStickerpackComponent} from './sticker-add-stickerpack.component';
import {FormsModule} from '@angular/forms';
import {MatDialog, MatDialogModule, MatFormFieldModule, MatInputModule} from '@angular/material';
import {FileDropModule} from 'ngx-file-drop';
import {Component, NgModule} from '@angular/core';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {OverlayContainer} from '@angular/cdk/overlay';
import {HttpClientModule} from '@angular/common/http';

describe('StickerAddStickerpackComponent', () => {
    let component: StickerAddStickerpackComponent;
    let fixture: ComponentFixture<NoopComponent>;


    let overlayContainerElement: HTMLElement;
    let dialog: MatDialog;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [DialogTestModule],
            providers: [
                {
                    provide: OverlayContainer, useFactory: () => {
                        overlayContainerElement = document.createElement('div');
                        return {getContainerElement: () => overlayContainerElement};
                    }
                }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NoopComponent);
        dialog = TestBed.get(MatDialog);
        component = dialog.open(StickerAddStickerpackComponent).componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});


// Taken from http://angular-tips.com/blog/2018/02/testing-angular-material-dialog-templates/

// Noop component is only a workaround to trigger change detection
@Component({
    template: ''
})
class NoopComponent {
}

@NgModule({
    imports: [MatDialogModule, NoopAnimationsModule, FormsModule, MatFormFieldModule, FileDropModule,
    HttpClientModule],
    exports: [StickerAddStickerpackComponent, NoopComponent],
    declarations: [StickerAddStickerpackComponent, NoopComponent],
    entryComponents: [
        StickerAddStickerpackComponent
    ],
})
class DialogTestModule {
}

