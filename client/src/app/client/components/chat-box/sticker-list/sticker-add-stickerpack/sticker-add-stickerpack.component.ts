import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {FileSystemFileEntry, UploadEvent} from 'ngx-file-drop';
import {StickerService} from '../../../../services/sticker/sticker.service';
import {StickerForUpload} from '../sticker-list-dialog-data';

@Component({
    selector: 'cchat-sticker-add-stickerpack',
    templateUrl: './sticker-add-stickerpack.component.html',
    styleUrls: ['./sticker-add-stickerpack.component.scss']
})
export class StickerAddStickerpackComponent implements OnInit {

    public files: StickerForUpload[] = [];
    public newStickerPackName: string;

    constructor(
        public dialogRef: MatDialogRef<StickerAddStickerpackComponent>,
        private stickerService: StickerService,
    ) {
    }

    ngOnInit() {
    }

    public dropped(event: UploadEvent) {
        for (const droppedFile of event.files) {
            if (droppedFile.fileEntry.isFile) {  // no directories
                let filename = droppedFile.fileEntry.name;
                filename = filename.replace(/\.svg/, '');
                let fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
                fileEntry.file(file => {
                    let fileReader = new FileReader();
                    fileReader.onload = ev => {
                        let newFile: StickerForUpload = {
                            emoji_id: filename,
                            data: fileReader.result.toString(),
                        }
                        this.files.push(newFile);
                    }
                    fileReader.readAsText(file);
                });


            }
        }
    }

    saveAndClose() {
        const successful = this.stickerService.addNewStickerpack(this.newStickerPackName, this.files);
        this.dialogRef.close(successful);  // return if saved
    }
}
