import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {StickerListDialogData} from './sticker-list-dialog-data';
import {CommunicationService} from '../../../services/communication/communication.service';
import {StickerAddStickerpackComponent} from './sticker-add-stickerpack/sticker-add-stickerpack.component';
import {Observable} from 'rxjs';
import {StickerService} from '../../../services/sticker/sticker.service';

@Component({
    selector: 'cchat-sticker-list',
    templateUrl: './sticker-list.component.html',
    styleUrls: ['./sticker-list.component.scss']
})
export class StickerListComponent implements OnInit {

    private addStickerpackDialogRef: MatDialogRef<StickerAddStickerpackComponent>;

    constructor(@Inject(MAT_DIALOG_DATA) public data: StickerListDialogData,
                public dialogRef: MatDialogRef<StickerListComponent>,
                public communicationService: CommunicationService,
                public stickerService: StickerService,
                private dialog: MatDialog) {
    }

    ngOnInit() {
  }

    sendSticker($event: MouseEvent) {
        const target = $event.target as HTMLImageElement;
        this.communicationService.sendSticker(target.src.toString(), this.data.chat);
        // this.dialogRef.close(); // TODO: probably close dialog after 1 sticker has been sent?!
    }

    openNewStickerpackDialog() {
         this.addStickerpackDialogRef = this.dialog.open(
             StickerAddStickerpackComponent, {
             });

         this.addStickerpackDialogRef.afterClosed().subscribe(
             (value: boolean | Observable<boolean>) => {
                 if (value && value instanceof Observable) {
                    value.subscribe((value1: boolean) => {
                        if (value1) { this.reload(); }
                    });
                 } else if (value) {
                     this.reload();
                 }
             }
         );
    }

    reload() {
        this.data.stickerPacks = this.stickerService.getAllStickers();
    }

    deleteStickerPack(stickerpackId: string) {
        // TODO: probably show confirmation dialog
        this.stickerService.deleteStickerpack(stickerpackId).subscribe(
            successful => {
                if (successful) { this.reload(); }
            }
        );
    }
}
