import {Observable} from 'rxjs';
import {ChatDocType} from '../../../../core/database/chats/chat.types';

export interface StickerListDialogData {
    stickerPacks: Observable<StickerPack[]>;
    chat: ChatDocType;
}

export interface StickerPack {
    _id: string;
    creator: string;
    name: string;
    stickers: StickerForDownload;
    isOwn?: boolean;
}

export interface StickerForUpload {
    emoji_id: string;
    data: string;
}

export interface StickerForDownload extends StickerForUpload {
    data_url: string;
}
