import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatMessageComponent} from './chat-message.component';
import {DECLARATIONS, IMPORTS} from '../../../../client.module';
import {MessageMediaType, MessageStatus} from '../../../../../core/database/messages/message.types';

describe('ChatMessageComponent', () => {
    let component: ChatMessageComponent;
    let fixture: ComponentFixture<ChatMessageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: IMPORTS,
            declarations: DECLARATIONS,
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ChatMessageComponent);
        component = fixture.componentInstance;
        component.message = {
            timeStamp: new Date().getTime(),
            chat: 'test',
            id: 'asdf-12345',
            from: 'other',
            message: 'This is a test message',
            read: false,
            status: MessageStatus.Received,
            mediaType: MessageMediaType.Text,
        };

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
