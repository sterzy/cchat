import {MessageDocType, MessageMediaType} from '../../../../../core/database/messages/message.types';
import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ElementRef,
    HostBinding,
    Input,
    OnInit
} from '@angular/core';
import {MessageService} from '../../../../services/message/message.service';

@Component({
    selector: 'cchat-chat-message',
    templateUrl: './chat-message.component.html',
    styleUrls: ['./chat-message.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChatMessageComponent implements OnInit {

    @Input()
    message: MessageDocType;

    MEDIA_TYPES: typeof MessageMediaType = MessageMediaType;

    @HostBinding('class.highlighted')
    highlighted = false;

    constructor(
        private messageService: MessageService,
        private elementRef: ElementRef,
        private cd: ChangeDetectorRef,
    ) {
    }

    ngOnInit() {
        this.messageService.registerMessage(this.message.chat, this.message.id, this);
    }

    scrollIntoView() {
        (this.elementRef.nativeElement as HTMLElement).scrollIntoView();
        this.highlighted = true;
        this.cd.markForCheck();
        setTimeout(() => {
            this.highlighted = false;
            this.cd.markForCheck();
        }, 800);
    }
}
