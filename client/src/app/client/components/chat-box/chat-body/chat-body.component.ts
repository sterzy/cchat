import {Component, Input, OnInit} from '@angular/core';
import {ChatDocType} from '../../../../core/database/chats/chat.types';
import {MessageDocType} from '../../../../core/database/messages/message.types';
import {MessageService} from '../../../services/message/message.service';
import {Observable} from 'rxjs';

@Component({
    selector: 'cchat-chat-body',
    templateUrl: './chat-body.component.html',
    styleUrls: ['./chat-body.component.scss']
})
export class ChatBodyComponent implements OnInit {

    messages$: Observable<MessageDocType[]>;

    currentChatId: string;

    @Input()
    set chat(chat: ChatDocType) {
        if (chat !== null && this.currentChatId !== chat.id) {
            this.currentChatId = chat.id;
            this.messages$ = this.messageService.getMessages(chat.id);
        }
    }

    constructor(private messageService: MessageService) {

    }

    ngOnInit() {
    }

    sameDay(da1: number, da2: number): boolean {
        if (da2 == null) {
            return false;
        }
        const d1 = new Date(da1);
        const d2 = new Date(da2);
        return d1.getFullYear() === d2.getFullYear() &&
            d1.getMonth() === d2.getMonth() &&
            d1.getDay() === d2.getDay();
    }

    trackByMsgId(index: number, item: MessageDocType) {
        return item.id;
    }

}
