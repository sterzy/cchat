import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatBodyComponent} from './chat-body.component';
import {DECLARATIONS, IMPORTS} from '../../../client.module';
import {TEST_RX_STOMP_PROVIDERS} from '../../../../util/test/providers';

describe('ChatBodyComponent', () => {
    let component: ChatBodyComponent;
    let fixture: ComponentFixture<ChatBodyComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: IMPORTS,
            declarations: DECLARATIONS,
            providers: [
                TEST_RX_STOMP_PROVIDERS,
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ChatBodyComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
