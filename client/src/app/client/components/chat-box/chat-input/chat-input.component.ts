import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {ChatDocType} from '../../../../core/database/chats/chat.types';
import {CommunicationService} from '../../../services/communication/communication.service';
import {StickerService} from '../../../services/sticker/sticker.service';
import {MatDialog} from '@angular/material';
import {StickerListComponent} from '../sticker-list/sticker-list.component';
import {EmojiService} from '@ctrl/ngx-emoji-mart/ngx-emoji/';
import {StickerListDialogData} from "../sticker-list/sticker-list-dialog-data";

@Component({
    selector: 'cchat-chat-input',
    templateUrl: './chat-input.component.html',
    styleUrls: ['./chat-input.component.scss']
})
export class ChatInputComponent implements OnInit {

    @Input()
    public initialVal = '';
    @Input()
    public chat: ChatDocType;

    @ViewChild('inputfield')
    private elem: ElementRef;


    private COLONS_REGEX = /(?:\:([^\:\s]+)\:)(?:\:skin-tone-(\d)\:)?/g;


    constructor(private communicationService: CommunicationService,
                private stickerService: StickerService,
                private dialog: MatDialog,
                private emoji: EmojiService) {
    }

    ngOnInit() {
    }

    sendMessage() {
        if (this.initialVal !== '' && this.initialVal !== null) {
            this.communicationService.sendMessage(this.initialVal, this.chat);
            this.initialVal = '';
        }
    }

    openStickerDialog() {
        const allStickers = this.stickerService.getAllStickers();
        this.dialog.open(StickerListComponent, {
            // width: '250rem',
            data: {
                stickerPacks: allStickers,
                chat: this.chat,
            } as StickerListDialogData
        });
    }

    onKeyDown(event: KeyboardEvent) {
        switch (event.key) {
            case 'Enter': {
                event.preventDefault();
                this.sendMessage();
            }

        }
    }

    addEmoji(event) {
        this.initialVal  = `${this.initialVal}${event.emoji.native}`;
    }

    setCursor() {
        // move cursor to last position
        const selection = window.getSelection();
        selection.collapse(this.elem.nativeElement.childNodes[0], this.initialVal.length);
        this.elem.nativeElement.focus();
    }

    shortcode2Emoji(event: string) {
        // console.warn('value is: ' + event);
        this.initialVal = event.replace(this.COLONS_REGEX, match => {
            const matchingData = this.emoji.getData(match);

            if (matchingData != null) {
                // console.warn(matchingData);
                return matchingData.native;
            }

            return match;
        });
        if (event.length !== this.initialVal.length) {
            setTimeout(() => this.setCursor(), 0);
        }
    }
}
