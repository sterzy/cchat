import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatInputComponent} from './chat-input.component';
import {DECLARATIONS, IMPORTS} from '../../../client.module';
import {TEST_RX_STOMP_PROVIDERS} from '../../../../util/test/providers';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';

describe('ChatInputComponent', () => {
    let component: ChatInputComponent;
    let fixture: ComponentFixture<ChatInputComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [RouterModule.forRoot([]), IMPORTS, HttpClientModule],
            declarations: DECLARATIONS,
            providers: [
                TEST_RX_STOMP_PROVIDERS,
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ChatInputComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
