import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ChatService} from '../../services/chat/chat.service';
import {takeUntil} from 'rxjs/operators';
import {ReplaySubject} from 'rxjs';
import {MatDialog} from '@angular/material';
import {ChatSettingsDialogComponent} from '../chat-settings-dialog/chat-settings-dialog.component';
import {ChatRootComponent} from '../chat-root/chat-root.component';

@Component({
    selector: 'cchat-chat-box',
    templateUrl: './chat-box.component.html',
    styleUrls: ['./chat-box.component.scss']
})
export class ChatBoxComponent implements OnInit, OnDestroy {

    selectedChat$ = this.chatService.getSelectedChat();

    private killSwitch$ = new ReplaySubject<void>(1);

    constructor(
        private route: ActivatedRoute,
        private chatService: ChatService,
        private dialog: MatDialog,
        private rootComp: ChatRootComponent,
    ) {
    }

    ngOnInit() {
        this.route.paramMap.pipe(
            takeUntil(this.killSwitch$),
        ).subscribe(
            params => {
                this.chatService.selectChatById(params.get('chatId'));
                this.rootComp.focusAside(false);
            },
        );
    }

    ngOnDestroy(): void {
        this.killSwitch$.next();
        this.chatService.selectChatById(null);
    }

    openSettings() {
        this.dialog.open(ChatSettingsDialogComponent);
    }

    openAside() {
        this.rootComp.focusAside();
    }

}
