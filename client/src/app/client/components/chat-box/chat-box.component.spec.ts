import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatBoxComponent} from './chat-box.component';
import {DECLARATIONS, IMPORTS} from '../../client.module';
import {RouterModule} from '@angular/router';
import {TEST_RX_STOMP_PROVIDERS} from '../../../util/test/providers';
import {HttpClientModule} from '@angular/common/http';
import {ChatRootComponent} from '../chat-root/chat-root.component';

describe('ChatBoxComponent', () => {
    let component: ChatBoxComponent;
    let fixture: ComponentFixture<ChatBoxComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [RouterModule.forRoot([]), IMPORTS, HttpClientModule],
            declarations: DECLARATIONS,
            providers: [
                TEST_RX_STOMP_PROVIDERS,
                ChatRootComponent,
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ChatBoxComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
