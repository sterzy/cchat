import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatHeaderComponent} from './chat-header.component';
import {DECLARATIONS, IMPORTS} from '../../../client.module';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';

describe('ChatHeaderComponent', () => {
    let component: ChatHeaderComponent;
    let fixture: ComponentFixture<ChatHeaderComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [IMPORTS, RouterModule.forRoot([]), HttpClientModule],
            declarations: DECLARATIONS,
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ChatHeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
