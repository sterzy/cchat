import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ChatDocType} from '../../../../core/database/chats/chat.types';
import {ResponsiveService} from '../../../../core/services/responsive/responsive.service';
import {MatDialog, MatSnackBar} from '@angular/material';
import {ChatDeleteDialogComponent} from './chat-delete-dialog/chat-delete-dialog.component';
import {Router} from '@angular/router';
import {ChatService} from '../../../services/chat/chat.service';
import {MessageService} from '../../../services/message/message.service';
import {MessageDocType} from '../../../../core/database/messages/message.types';
import {Subscription} from 'rxjs';

@Component({
    selector: 'cchat-chat-header',
    templateUrl: './chat-header.component.html',
    styleUrls: ['./chat-header.component.scss']
})
export class ChatHeaderComponent implements OnInit {

    searchbar = false;

    ltMD = this.responsive.matchers.md.lt;

    @Output()
    searchTerm = '';

    @Input()
    set chat(chat: ChatDocType) {
        this.pchat = chat;
        if (this.chat.id !== this.chatId) {
            this.closeSearch();
        }
        this.chatId = chat.id;
    }

    get chat() {
        return this.pchat;
    }

    @Output()
    headerClicked = new EventEmitter<void>();

    @Output()
    backButtonClicked = new EventEmitter<void>();

    foundResults: MessageDocType[] = [];
    index = 0;

    searchActive = false;

    prevTerm = '';

    searchSubscription: Subscription;

    private pchat: ChatDocType;
    private chatId: string;

    constructor(
        private responsive: ResponsiveService,
        private dialog: MatDialog,
        private router: Router,
        private snackBar: MatSnackBar,
        private chatService: ChatService,
        private messageService: MessageService
    ) {
    }

    ngOnInit() {
    }

    hideChatClicked() {
        const dialogRef = this.dialog.open(ChatDeleteDialogComponent, {
            width: '300px',
            data: {
                name: this.chat.id
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                console.warn('deleting chat ' + this.chat.id);
                this.chatService.updateHidden(this.chat.id, true).subscribe((success) => {
                    if (success) {
                        this.snackBar.open('Chat with ' + this.chat.id + '  is now hidden.', 'OK',
                            {duration: 2000});
                        this.router.navigateByUrl('/');
                    } else {
                        this.snackBar.open('An Error occurred, try again later!', 'OK',
                            {duration: 2000});
                    }
                });
            }
        });

    }

    blockUser() {
        this.chatService.updateBlocked(this.chat.id, !this.chat.blocked).subscribe(chat => {
                this.chat = chat;
                if (this.chat.blocked === true) {
                    this.snackBar.open('Successfully blocked User ' + this.chat.id, 'OK',
                        {duration: 2000});
                } else {
                    this.snackBar.open('Successfully unblocked User ' + this.chat.id, 'OK',
                        {duration: 2000});
                }
            },
            () => {
                this.snackBar.open('An Error occurred, try again later!', 'OK',
                    {duration: 2000});
            });
    }

    search() {
        if (this.searchTerm !== '') {

            if (this.prevTerm === this.searchTerm && this.foundResults.length !== 0) {
                this.arrow_up();
                return;
            }

            if (this.searchSubscription) {
                this.searchSubscription.unsubscribe();
                this.searchSubscription = undefined;
            }

            this.searchSubscription = this.messageService.searchForMessages(this.chat.id, this.searchTerm)
                .subscribe((msg: MessageDocType[]) => {
                    if (msg.length !== 0) {
                        this.messageService.setFocus(msg[0].id);
                        this.prevTerm = this.searchTerm;
                    }
                    this.searchActive = true;
                    this.foundResults = msg;
                    this.index = 0;

                });
        }
    }

    closeSearch() {
        this.foundResults = null;
        this.searchbar = false;
        this.searchTerm = '';
        this.searchActive = false;
        this.prevTerm = '';
        if (this.searchSubscription) {
            this.searchSubscription.unsubscribe();
            this.searchSubscription = undefined;
        }
    }

    arrow_up() {
        if (this.foundResults.length !== 0 && this.index < this.foundResults.length - 1) {
            this.index++;

        } else if (this.foundResults.length !== 0 && this.index === this.foundResults.length - 1) {
            this.index = 0;
        } else {
            return;
        }

        this.messageService.setFocus(this.foundResults[this.index].id);
    }

    arrow_down() {
        if (this.foundResults.length !== 0 && this.index > 0) {
            this.index--;

        } else if (this.foundResults.length !== 0 && this.index === 0) {
            this.index = this.foundResults.length - 1;
        } else {
            return;
        }

        this.messageService.setFocus(this.foundResults[this.index].id);
    }

}
