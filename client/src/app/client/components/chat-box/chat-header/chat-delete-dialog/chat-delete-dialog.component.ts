import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'cchat-chat-delete-dialog',
  templateUrl: './chat-delete-dialog.component.html',
  styleUrls: ['./chat-delete-dialog.component.scss']
})
export class ChatDeleteDialogComponent implements OnInit {

    constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }

    ngOnInit() {
    }
}
