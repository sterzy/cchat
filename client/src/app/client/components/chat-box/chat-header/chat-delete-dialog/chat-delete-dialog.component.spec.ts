import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatDeleteDialogComponent} from './chat-delete-dialog.component';
import {MAT_DIALOG_DATA, MatDialogModule} from '@angular/material';

describe('ChatDeleteDialogComponent', () => {
    let component: ChatDeleteDialogComponent;
    let fixture: ComponentFixture<ChatDeleteDialogComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ChatDeleteDialogComponent],
            imports: [MatDialogModule],
            providers: [{
                provide: MAT_DIALOG_DATA,
                useValue: {name: 'test'}
            }]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ChatDeleteDialogComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
