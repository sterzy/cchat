import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {SimpleErrorStateMatcher} from '../../../../util/form.utils';
import {MatDialogRef} from '@angular/material';
import {BotRegisterRequest} from '../../../models/bot.models';
import {BotService} from '../../../services/bot/bot.service';
import {HttpErrorResponse} from '@angular/common/http';
import {first} from 'rxjs/operators';
import {Subscription} from 'rxjs';
import {environment} from '../../../../../environments/environment';

@Component({
    selector: 'cchat-bot-dashboard-register-bot-dialog',
    templateUrl: './bot-dashboard-register-dialog.component.html',
    styleUrls: ['./bot-dashboard-register-dialog.component.scss']
})
export class BotDashboardRegisterDialogComponent implements OnInit {

    matcher = new SimpleErrorStateMatcher();

    regForm = this.fb.group({
        name: ['', [Validators.required]],
        description: [''],
        webHookProtocol: ['https://', [Validators.required]],
        webHook: ['', [Validators.required, Validators.pattern(environment.components.registerBot.regex)]],
        isPublic: [false],
    });

    regError: string;

    private onGoingRequest: Subscription;

    constructor(
        private fb: FormBuilder,
        private dialog: MatDialogRef<BotDashboardRegisterDialogComponent>,
        private botService: BotService,
    ) {
    }

    ngOnInit() {
    }

    register() {
        if (this.regForm.invalid || this.onGoingRequest) {
            return;
        }

        const regRequest: BotRegisterRequest = {
            name: this.name.value,
            webHookRootURL: this.webHookProtocol.value + this.webHook.value,
            description: this.description.value,
            isPublic: this.isPublic.value,
        };

        this.onGoingRequest = this.botService.registerBot(regRequest).pipe(
            first(),
        ).subscribe(
            () => {
                this.dialog.close();
                this.onGoingRequest = undefined;
            },
            (error: HttpErrorResponse) => {
                console.error(error.error.message);
                if (typeof error.error.message === 'string') {
                    this.regError = error.error.message;
                } else {
                    this.regError = `An unexpected error occurred:\n${error.message}`;
                }
                this.onGoingRequest = undefined;
            },
        );
    }

    /**
     * Getter for form fields
     */

    get name() {
        return this.regForm.get('name') as FormControl;
    }

    get description() {
        return this.regForm.get('description') as FormControl;
    }

    get webHookProtocol() {
        return this.regForm.get('webHookProtocol') as FormControl;
    }

    get webHook() {
        return this.regForm.get('webHook') as FormControl;
    }

    get isPublic() {
        return this.regForm.get('isPublic') as FormControl;
    }

}
