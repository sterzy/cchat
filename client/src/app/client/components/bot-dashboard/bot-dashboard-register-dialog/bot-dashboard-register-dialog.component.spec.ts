import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BotDashboardRegisterDialogComponent} from './bot-dashboard-register-dialog.component';
import {
    MatAutocompleteModule,
    MatCardModule, MatCheckboxModule, MatDialog, MatDialogModule, MatDialogRef, MatFormFieldModule,
    MatIconModule, MatInputModule, MatProgressSpinnerModule, MatSelectModule, MatSlideToggleModule,
    MatSnackBarModule,
    MatToolbarModule
} from '@angular/material';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {Component, NgModule} from '@angular/core';
import {OverlayContainer} from '@angular/cdk/overlay';

describe('BotDashboardRegisterBotDialogComponent', () => {
    let component: BotDashboardRegisterDialogComponent;

    let fixture: ComponentFixture<NoopComponent>;

    let overlayContainerElement: HTMLElement;
    let dialog: MatDialog;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [DialogTestModule],
            providers: [
                {
                    provide: OverlayContainer, useFactory: () => {
                        overlayContainerElement = document.createElement('div');
                        return {getContainerElement: () => overlayContainerElement};
                    }
                }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NoopComponent);
        dialog = TestBed.get(MatDialog);
        component = dialog.open(BotDashboardRegisterDialogComponent).componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});

// Taken from http://angular-tips.com/blog/2018/02/testing-angular-material-dialog-templates/

// Noop component is only a workaround to trigger change detection
@Component({
    template: ''
})
class NoopComponent {
}

@NgModule({
    imports: [MatSelectModule, MatCheckboxModule, MatCardModule, MatSnackBarModule, RouterModule.forRoot([]),
        MatToolbarModule, MatIconModule,
        MatAutocompleteModule, ReactiveFormsModule, MatFormFieldModule, MatDialogModule, HttpClientModule, MatInputModule,
        NoopAnimationsModule, MatSlideToggleModule, MatProgressSpinnerModule],
    exports: [BotDashboardRegisterDialogComponent, NoopComponent],
    declarations: [BotDashboardRegisterDialogComponent, NoopComponent],
    entryComponents: [
        BotDashboardRegisterDialogComponent
    ],
})
class DialogTestModule {
}
