import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BotDashboardManageBotDialogComponent} from './bot-dashboard-manage-bot-dialog.component';
import {
    MAT_DIALOG_DATA,
    MatAutocompleteModule,
    MatCardModule, MatDialogModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatProgressSpinnerModule, MatSlideToggleModule,
    MatSnackBarModule,
    MatToolbarModule
} from '@angular/material';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('BotDashboardManageBotDialogComponent', () => {
    let component: BotDashboardManageBotDialogComponent;
    let fixture: ComponentFixture<BotDashboardManageBotDialogComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [BotDashboardManageBotDialogComponent],
            imports: [MatCardModule, MatSnackBarModule, RouterModule.forRoot([]), MatToolbarModule, MatIconModule,
                MatAutocompleteModule, ReactiveFormsModule, MatFormFieldModule, MatDialogModule, HttpClientModule, MatInputModule,
                NoopAnimationsModule, MatSlideToggleModule, MatProgressSpinnerModule],
            providers: [
                {
                    provide: MAT_DIALOG_DATA,
                    useValue: {botId: 'id'},
                }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BotDashboardManageBotDialogComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
