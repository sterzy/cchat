import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {BotService} from '../../../services/bot/bot.service';
import {CryptoService} from '../../../services/crypto/crypto.service';
import {MAT_DIALOG_DATA} from '@angular/material';
import {from, of, ReplaySubject} from 'rxjs';
import {ChatService} from '../../../services/chat/chat.service';
import {catchError, first, shareReplay, switchMap, takeUntil, tap, withLatestFrom} from 'rxjs/operators';
import {MessagingCryptoMessage, MessagingTypes} from '../../../models/communication.models';
import {uuidv4} from '../../../../core/util/uuid.util';

export interface ManageBotData {
    botId: string;
}

@Component({
    selector: 'cchat-bot-dashboard-manage-bot-dialog',
    templateUrl: './bot-dashboard-manage-bot-dialog.component.html',
    styleUrls: ['./bot-dashboard-manage-bot-dialog.component.scss']
})
export class BotDashboardManageBotDialogComponent implements OnInit {

    botOptionsForm = this.fb.group({
        isActive: [false],
        isPublic: [false],
    });

    private botOption = {
        _super: this,
        get isActive() {
            return this._super.botOptionsForm.get('isActive') as FormControl;
        },
        get isPublic() {
            return this._super.botOptionsForm.get('isPublic') as FormControl;
        },
    };

    testMessageForm = this.fb.group({
        testMessage: ['', [Validators.required]],
    });

    bot$ = this.botService.getBotById(this.data.botId).pipe(
        tap(bot => {
            if (bot) {
                this.botOption.isActive.setValue(bot.isActive, {emitEvent: false});
                this.botOption.isPublic.setValue(bot.isPublic, {emitEvent: false});
            }
        }),
        shareReplay(1),
    );
    chat$ = this.bot$.pipe(
        switchMap(bot => {
            return this.chatService.getChatById(bot.name);
        }),
        shareReplay(1),
    );

    testMessageLoading = false;
    sentTestMessage: MessagingCryptoMessage;
    receivedTestMessage: MessagingCryptoMessage;
    decryptedTestMessage: string;
    testMessageError: string;

    private killSwitch$ = new ReplaySubject(1);

    constructor(
        private fb: FormBuilder,
        private botService: BotService,
        private cryptoService: CryptoService,
        @Inject(MAT_DIALOG_DATA) private data: ManageBotData,
        private chatService: ChatService,
    ) {
    }

    ngOnInit() {
        this.botOption.isActive.valueChanges.pipe(
            takeUntil(this.killSwitch$),
            switchMap(isActive => {
                return this.botService.setBotActive(this.data.botId, isActive).pipe(
                    catchError(err => {
                        console.error(err);
                        return of(undefined);
                    })
                );
            }),
        ).subscribe();
        this.botOption.isPublic.valueChanges.pipe(
            takeUntil(this.killSwitch$),
            switchMap(isPublic => {
                return this.botService.setBotPublic(this.data.botId, isPublic).pipe(
                    catchError(err => {
                        console.error(err);
                        return of(undefined);
                    })
                );
            }),
        ).subscribe();
    }

    ngOnDestory() {
        this.killSwitch$.next();
    }

    sendTestMessage() {

        if (this.testMessageForm.invalid) {
            return;
        }

        this.testMessageLoading = true;

        this.sentTestMessage = undefined;
        this.receivedTestMessage = undefined;
        this.testMessageError = undefined;
        this.decryptedTestMessage = undefined;

        const testMessage: string = this.testMessageForm.get('testMessage').value;

        this.chat$.pipe(
            first(),
            switchMap(chat => this.cryptoService.encrypt(testMessage, chat)),
            switchMap(cipherText => {
                const msg: MessagingCryptoMessage = {
                    id: uuidv4(),
                    payload: cipherText.body,
                    cryptoType: cipherText.type,
                    from: '',
                    timestamp: Date.now(),
                    type: MessagingTypes.MSG_CRYPTO,
                };

                this.sentTestMessage = msg;

                return this.botService.sendTestMessageToBot(this.data.botId, msg);
            }),
            withLatestFrom(this.chat$),
            switchMap(([response, chat]) => {
                this.receivedTestMessage = response || null;

                if (!response) {
                    return of(undefined);
                }
                return this.cryptoService.decrypt({
                    body: response.payload,
                    type: response.cryptoType,
                }, chat);
            })
        ).subscribe({
            next: (decryptedMessage) => {
                this.testMessageLoading = false;
                this.decryptedTestMessage = decryptedMessage;
            },
            error: err => {
                this.testMessageLoading = false;
                this.testMessageError = typeof err === 'string' ? err : JSON.stringify(err, null, 2);
                console.error(err);
            },
        });
    }

    addBotToChats() {
        this.botService.addBotToChats(this.data.botId).pipe(
            first(),
        ).subscribe();
    }

}
