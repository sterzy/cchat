import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {BotDashboardRegisterDialogComponent} from './bot-dashboard-register-dialog/bot-dashboard-register-dialog.component';
import {first} from 'rxjs/operators';
import {BotDashboardManageBotDialogComponent} from './bot-dashboard-manage-bot-dialog/bot-dashboard-manage-bot-dialog.component';
import {ChatRootComponent} from '../chat-root/chat-root.component';

@Component({
    selector: 'cchat-bot-dashboard',
    templateUrl: './bot-dashboard.component.html',
    styleUrls: ['./bot-dashboard.component.scss']
})
export class BotDashboardComponent implements OnInit {

    constructor(
        private dialog: MatDialog,
        private root: ChatRootComponent,
    ) {
    }

    ngOnInit() {
    }

    showAside() {
        this.root.focusAside();
    }

    openRegisterDialog() {
        const registerDialog = this.dialog.open(BotDashboardRegisterDialogComponent, {
            maxWidth: '30rem',
        });
        registerDialog.afterClosed().pipe(
            first(),
        ).subscribe();
    }

    openManageBotDialog(botId: string) {
        this.dialog.open(BotDashboardManageBotDialogComponent, {
            maxWidth: '30rem',
            data: {
                botId,
            },
        });
    }

}
