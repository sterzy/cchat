import {Component, OnInit} from '@angular/core';
import {BotService} from '../../../services/bot/bot.service';

@Component({
    selector: 'cchat-bot-dashboard-all-bots',
    templateUrl: './bot-dashboard-added-bots.component.html',
    styleUrls: ['./bot-dashboard-added-bots.component.scss']
})
export class BotDashboardAddedBotsComponent implements OnInit {

    bots$ = this.botService.getAddedBots();

    constructor(
        private botService: BotService,
    ) {
    }

    ngOnInit() {
    }

}
