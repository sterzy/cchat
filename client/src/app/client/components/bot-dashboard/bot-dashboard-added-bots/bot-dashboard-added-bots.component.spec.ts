import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BotDashboardAddedBotsComponent} from './bot-dashboard-added-bots.component';
import {
    MatAutocompleteModule,
    MatCardModule, MatDialogModule, MatFormFieldModule,
    MatIconModule, MatInputModule,
    MatSnackBarModule,
    MatToolbarModule
} from '@angular/material';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('BotDashboardAddedBotsComponent', () => {
    let component: BotDashboardAddedBotsComponent;
    let fixture: ComponentFixture<BotDashboardAddedBotsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [BotDashboardAddedBotsComponent],
            imports: [MatCardModule, MatSnackBarModule, RouterModule.forRoot([]), MatToolbarModule, MatIconModule,
                MatAutocompleteModule, ReactiveFormsModule, MatFormFieldModule, MatDialogModule, HttpClientModule, MatInputModule,
                NoopAnimationsModule],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BotDashboardAddedBotsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
