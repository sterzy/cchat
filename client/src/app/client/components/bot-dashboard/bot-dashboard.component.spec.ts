import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BotDashboardComponent} from './bot-dashboard.component';
import {
    MatAutocompleteModule,
    MatCardModule, MatDialogModule, MatFormFieldModule,
    MatIconModule, MatInputModule,
    MatSnackBarModule,
    MatToolbarModule
} from '@angular/material';
import {RouterModule} from '@angular/router';
import {BotDashboardHeaderComponent} from './bot-dashboard-header/bot-dashboard-header.component';
import {BotDashboardAddedBotsComponent} from './bot-dashboard-added-bots/bot-dashboard-added-bots.component';
import {BotDashboardMyBotsComponent} from './bot-dashboard-my-bots/bot-dashboard-my-bots.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ChatRootComponent} from '../chat-root/chat-root.component';
import {HttpClientModule} from '@angular/common/http';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('BotDashboardComponent', () => {
    let component: BotDashboardComponent;
    let fixture: ComponentFixture<BotDashboardComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [BotDashboardComponent, BotDashboardHeaderComponent, BotDashboardAddedBotsComponent, BotDashboardMyBotsComponent],
            imports: [MatCardModule, MatSnackBarModule, RouterModule.forRoot([]), MatToolbarModule, MatIconModule,
                MatAutocompleteModule, ReactiveFormsModule, MatFormFieldModule, MatDialogModule, HttpClientModule, MatInputModule,
                NoopAnimationsModule],
            providers: [ChatRootComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BotDashboardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
