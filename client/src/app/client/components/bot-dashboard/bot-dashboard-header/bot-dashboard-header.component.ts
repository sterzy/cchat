import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {BotSearchResponse} from '../../../models/bot.models';
import {FormControl, FormGroup} from '@angular/forms';
import {debounceTime, filter, switchMap} from 'rxjs/operators';
import {BotService} from '../../../services/bot/bot.service';
import {of} from 'rxjs';
import {ResponsiveService} from '../../../../core/services/responsive/responsive.service';

@Component({
    selector: 'cchat-bot-dashboard-header',
    templateUrl: './bot-dashboard-header.component.html',
    styleUrls: ['./bot-dashboard-header.component.scss']
})
export class BotDashboardHeaderComponent implements OnInit {

    botSearch = new FormControl('');

    botForm = new FormGroup({
        botSearch: this.botSearch,
    });

    bots$  = this.botSearch.valueChanges.pipe(
        debounceTime(200),
        switchMap((text: string) => {
            if (text.length < 3) {
                return of([]);
            }
            return this.botService.searchBotsByName(text);
        }),
    );

    @Output()
    backButtonClicked = new EventEmitter<void>();

    ltMD = this.responsive.matchers.md.lt;

    constructor(
        private botService: BotService,
        private responsive: ResponsiveService,
    ) {
    }

    ngOnInit() {
    }

    displayWithName = (item: {name: string}) => item.name;

    searchBot(bot: BotSearchResponse) {
        this.botService.addBotToChats(bot.id).subscribe();
    }

}
