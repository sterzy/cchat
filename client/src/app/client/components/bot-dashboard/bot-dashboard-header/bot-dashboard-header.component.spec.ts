import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BotDashboardHeaderComponent} from './bot-dashboard-header.component';
import {
    MatAutocompleteModule,
    MatCardModule, MatDialogModule, MatFormFieldModule,
    MatIconModule, MatInputModule,
    MatSnackBarModule,
    MatToolbarModule
} from '@angular/material';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('BotDashboardHeaderComponent', () => {
    let component: BotDashboardHeaderComponent;
    let fixture: ComponentFixture<BotDashboardHeaderComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [BotDashboardHeaderComponent],
            imports: [MatCardModule, MatSnackBarModule, RouterModule.forRoot([]), MatToolbarModule, MatIconModule,
                MatAutocompleteModule, ReactiveFormsModule, MatFormFieldModule, MatDialogModule, HttpClientModule, MatInputModule,
                NoopAnimationsModule],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BotDashboardHeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
