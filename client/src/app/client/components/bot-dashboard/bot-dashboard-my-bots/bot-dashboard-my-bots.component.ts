import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {BotService} from '../../../services/bot/bot.service';

@Component({
    selector: 'cchat-bot-dashboard-my-bots',
    templateUrl: './bot-dashboard-my-bots.component.html',
    styleUrls: ['./bot-dashboard-my-bots.component.scss']
})
export class BotDashboardMyBotsComponent implements OnInit {

    myBots$ = this.botService.getMyBots();

    @Output()
    addNewBot = new EventEmitter<void>();

    @Output()
    manageBot = new EventEmitter<string>();

    constructor(
        private botService: BotService,
    ) {
    }

    ngOnInit() {
    }

}
