import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {BotDashboardMyBotsComponent} from './bot-dashboard-my-bots.component';
import {
    MatAutocompleteModule,
    MatCardModule, MatDialogModule, MatFormFieldModule,
    MatIconModule, MatInputModule, MatProgressSpinnerModule, MatSlideToggleModule,
    MatSnackBarModule,
    MatToolbarModule
} from '@angular/material';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('BotDashboardMyBotsComponent', () => {
    let component: BotDashboardMyBotsComponent;
    let fixture: ComponentFixture<BotDashboardMyBotsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [BotDashboardMyBotsComponent],
            imports: [MatCardModule, MatSnackBarModule, RouterModule.forRoot([]), MatToolbarModule, MatIconModule,
                MatAutocompleteModule, ReactiveFormsModule, MatFormFieldModule, MatDialogModule, HttpClientModule, MatInputModule,
                NoopAnimationsModule, MatSlideToggleModule, MatProgressSpinnerModule],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(BotDashboardMyBotsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
