import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatRootComponent} from './chat-root.component';
import {RouterModule} from '@angular/router';
import {ClientModule} from '../../client.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';

describe('ChatRootComponent', () => {
    let component: ChatRootComponent;
    let fixture: ComponentFixture<ChatRootComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ClientModule, RouterModule.forRoot([]), NoopAnimationsModule, HttpClientModule],
            declarations: []
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ChatRootComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
