import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDrawerContainer} from '@angular/material';

@Component({
    selector: 'cchat-chat-root',
    templateUrl: './chat-root.component.html',
    styleUrls: ['./chat-root.component.scss'],
    providers: [ChatRootComponent],
})
export class ChatRootComponent implements OnInit {

    asideFocused = true;

    @ViewChild(MatDrawerContainer)
    private nav: MatDrawerContainer;


    private deactivateTimeout;

    constructor() {
    }

    ngOnInit() {
    }

    openNav() {
        this.nav.open();
    }

    focusAside(focus = true) {
        this.asideFocused = focus;
    }

    routerActivated() {
        if (this.deactivateTimeout) {
            clearTimeout(this.deactivateTimeout);
            this.deactivateTimeout = undefined;
        }
        this.focusAside(false);
    }

    routerDeactivated() {
        this.deactivateTimeout = setTimeout(() => {
            this.focusAside(true);
        }, 100);
    }

}
