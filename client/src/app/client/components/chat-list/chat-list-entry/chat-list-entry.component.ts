import {Component, HostBinding, Input, OnInit} from '@angular/core';
import {ChatDocType} from '../../../../core/database/chats/chat.types';
import {MessageDocType} from '../../../../core/database/messages/message.types';

@Component({
    selector: 'cchat-chat-list-entry',
    templateUrl: './chat-list-entry.component.html',
    styleUrls: ['./chat-list-entry.component.scss']
})
export class ChatListEntryComponent implements OnInit {

    @HostBinding('class.chat-list-entry')
    readonly chatListEntryClass = true;

    @Input()
    chat: ChatDocType;
    @Input()
    message: MessageDocType;

    constructor() {
    }

    ngOnInit() {
    }

}
