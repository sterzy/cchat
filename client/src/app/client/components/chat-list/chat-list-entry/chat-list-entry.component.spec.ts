import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatListEntryComponent} from './chat-list-entry.component';
import {ChatType} from '../../../../core/database/chats/chat.types';
import {MessageMediaType, MessageStatus} from '../../../../core/database/messages/message.types';
import {CoreModule} from '../../../../core/core.module';

describe('ChatListEntryComponent', () => {
    let component: ChatListEntryComponent;
    let fixture: ComponentFixture<ChatListEntryComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [CoreModule],
            declarations: [ChatListEntryComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ChatListEntryComponent);
        component = fixture.componentInstance;

        component.chat = {
            id: 'test',
            lastMessage: new Date().getTime(),
            started: new Date().getTime(),
            type: ChatType.DirectMessage,
            users: ['test'],
            name: '',
            lastOnline: new Date().getTime(),
            blocked: false,
            verifiedKey: undefined,
            hidden: false,
        };

        component.message = {
            id: '1234-abcd',
            status: MessageStatus.Received,
            from: 'test',
            chat: 'test',
            read: false,
            message: 'This is a test message',
            timeStamp: new Date().getTime(),
            mediaType: MessageMediaType.Text,
        };

        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
