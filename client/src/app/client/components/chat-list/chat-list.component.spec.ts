import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatListComponent} from './chat-list.component';
import {RouterModule} from '@angular/router';
import {ClientModule, DECLARATIONS, IMPORTS} from '../../client.module';
import {HttpClientModule} from '@angular/common/http';
import {ChatRootComponent} from '../chat-root/chat-root.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('ChatListComponent', () => {
    let component: ChatListComponent;
    let fixture: ComponentFixture<ChatListComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ClientModule, HttpClientModule, RouterModule.forRoot([]), NoopAnimationsModule],
            declarations: [],
            providers: [
                ChatRootComponent,
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ChatListComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
