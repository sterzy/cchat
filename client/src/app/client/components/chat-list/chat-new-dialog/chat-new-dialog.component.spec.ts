import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatNewDialogComponent} from './chat-new-dialog.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatDialog, MatDialogModule, MatInputModule} from '@angular/material';
import {Component, NgModule} from '@angular/core';
import {OverlayContainer} from '@angular/cdk/overlay';
import {FormsModule} from '@angular/forms';

describe('ChatNewDialogComponent', () => {
    let component: ChatNewDialogComponent;
    let fixture: ComponentFixture<NoopComponent>;

    let overlayContainerElement: HTMLElement;
    let dialog: MatDialog;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [DialogTestModule],
            declarations: [],
            providers: [
                {
                    provide: OverlayContainer, useFactory: () => {
                        overlayContainerElement = document.createElement('div');
                        return {getContainerElement: () => overlayContainerElement};
                    }
                }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NoopComponent);
        dialog = TestBed.get(MatDialog);
        component = dialog.open(ChatNewDialogComponent).componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});

// Taken from http://angular-tips.com/blog/2018/02/testing-angular-material-dialog-templates/

// Noop component is only a workaround to trigger change detection
@Component({
    template: ''
})
class NoopComponent {
}

@NgModule({
    imports: [MatDialogModule, NoopAnimationsModule, MatInputModule, FormsModule],
    exports: [ChatNewDialogComponent, NoopComponent],
    declarations: [ChatNewDialogComponent, NoopComponent],
    entryComponents: [
        ChatNewDialogComponent
    ],
})
class DialogTestModule {
}

