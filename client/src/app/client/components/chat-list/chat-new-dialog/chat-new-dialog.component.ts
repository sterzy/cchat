import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
    selector: 'cchat-chat-new-dialog',
    templateUrl: './chat-new-dialog.component.html',
    styleUrls: ['./chat-new-dialog.component.scss'],
})
export class ChatNewDialogComponent implements OnInit {

    constructor(
        public dialogRef: MatDialogRef<ChatNewDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
    }

    ngOnInit() {
    }

    onCancelClicked() {
        this.dialogRef.close();
    }

}
