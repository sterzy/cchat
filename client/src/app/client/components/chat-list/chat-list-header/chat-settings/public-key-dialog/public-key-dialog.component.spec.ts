import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PublicKeyDialogComponent} from './public-key-dialog.component';
import {ClientModule} from '../../../../../client.module';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';

describe('PublicKeyDialogComponent', () => {
    let component: PublicKeyDialogComponent;
    let fixture: ComponentFixture<PublicKeyDialogComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ClientModule, HttpClientModule, RouterModule.forRoot([])],
            declarations: []
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PublicKeyDialogComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
