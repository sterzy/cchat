import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {LocalUserDocument} from '../../../../../core/database/localusers/localuser.types';
import {LocaluserDao} from '../../../../../core/database/localusers/localuser.dao';
import {MatDialog} from '@angular/material';
import {PublicKeyDialogComponent} from './public-key-dialog/public-key-dialog.component';
import {ConfirmationDialogComponent} from './delete-database-dialog/confirmation-dialog.component';
import {DatabaseDao} from '../../../../../core/database/database.dao';
import {first} from 'rxjs/operators';
import {UserManagementService} from '../../../../../landingpage/services/user-management.service';
import {Router} from '@angular/router';
import {ConfigService} from '../../../../../core/services/config/config.service';
import {ChatRootComponent} from '../../../chat-root/chat-root.component';

@Component({
    selector: 'cchat-chat-settings',
    templateUrl: './chat-settings.component.html',
    styleUrls: ['./chat-settings.component.scss']
})
export class ChatSettingsComponent implements OnInit {

    @Input()
    localUser$: Observable<LocalUserDocument>;

    constructor(private localuserDao: LocaluserDao,
                private dialog: MatDialog,
                private databaseDao: DatabaseDao,
                private userService: UserManagementService,
                private router: Router,
                private configService: ConfigService,
                private root: ChatRootComponent,
    ) {
        this.localUser$ = this.localuserDao.getLocaluser();
    }

    ngOnInit() {
    }

    onPublicKeyClicked() {
        this.dialog.open(PublicKeyDialogComponent);
    }

    onClearDatabaseClicked() {
        const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
            data: {
                title: 'Do you really want to delete the local Database?',
                content: 'This action is not reversible!',
            }
        });

        dialogRef.afterClosed().pipe(first()).subscribe(result => {
            if (result) {
                this.databaseDao.clearDatabase().subscribe(() => window.location.reload());
            }
        });
    }

    onDeleteAccountClicked() {
        const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
            data: {
                title: 'Do you really want to delete your User-Account?',
                content: 'All data connected to your account will be deleted. ' +
                    'This action is not reversible!',
            }
        });
        dialogRef.afterClosed().pipe(first()).subscribe(result => {
            if (result) {
                this.userService.deleteAccountData().subscribe();
            }
        });
    }

    hideAside() {
        this.root.focusAside(false);
    }

}
