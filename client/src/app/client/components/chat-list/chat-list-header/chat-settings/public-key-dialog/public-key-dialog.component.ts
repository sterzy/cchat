import {Component, Input, OnInit} from '@angular/core';
import {SignalStore} from '../../../../../../core/signal/signal.store';

@Component({
    selector: 'cchat-public-key-dialog',
    templateUrl: './public-key-dialog.component.html',
    styleUrls: ['./public-key-dialog.component.scss']
})
export class PublicKeyDialogComponent implements OnInit {

    @Input()
    pubKey: ArrayBuffer;

    constructor(
        private store: SignalStore) {
        this.store.getIdentityKeyPair().then((keypair) => {
            this.pubKey = keypair.pubKey;
        });
    }

    ngOnInit() {
    }

}
