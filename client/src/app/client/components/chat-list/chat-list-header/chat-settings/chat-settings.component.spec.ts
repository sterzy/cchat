import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatSettingsComponent} from './chat-settings.component';
import {ClientModule} from '../../../../client.module';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {ChatRootComponent} from '../../../chat-root/chat-root.component';

describe('ChatSettingsComponent', () => {
    let component: ChatSettingsComponent;
    let fixture: ComponentFixture<ChatSettingsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ClientModule, HttpClientModule, RouterModule.forRoot([])],
            declarations: [],
            providers: [ChatRootComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ChatSettingsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
