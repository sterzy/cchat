import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatListHeaderComponent} from './chat-list-header.component';
import {DECLARATIONS, IMPORTS} from '../../../client.module';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';

describe('ChatListHeaderComponent', () => {
    let component: ChatListHeaderComponent;
    let fixture: ComponentFixture<ChatListHeaderComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [IMPORTS, HttpClientModule, RouterModule.forRoot([]), NoopAnimationsModule],
            declarations: DECLARATIONS,
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ChatListHeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
