import {Component, EventEmitter, Input, isDevMode, OnInit, Output} from '@angular/core';
import {Observable, of} from 'rxjs';
import {FormControl} from '@angular/forms';
import {SimpleUser} from '../../../../core/models/user.models';
import {FriendService} from '../../../services/friend/friend.service';
import {debounceTime, distinctUntilChanged, first, switchMap, tap} from 'rxjs/operators';
import {ChatService} from '../../../services/chat/chat.service';
import {MatDialog, MatSnackBar} from '@angular/material';
import {ChatNewDialogComponent} from '../chat-new-dialog/chat-new-dialog.component';

@Component({
    selector: 'cchat-chat-list-header',
    templateUrl: './chat-list-header.component.html',
    styleUrls: ['./chat-list-header.component.scss']
})
export class ChatListHeaderComponent implements OnInit {

    @Output()
    findUserForm: FormControl = new FormControl();

    @Output()
    newClicked = new EventEmitter<void>();

    @Output()
    settingsClicked = new EventEmitter<void>();

    @Input()
    like = '';

    @Output()
    results: Observable<SimpleUser[]>;

    @Output()
    name: string;

    constructor(
        private friendService: FriendService,
        private chatService: ChatService,
        private dialog: MatDialog,
        private snackBar: MatSnackBar,
    ) {

    }

    ngOnInit() {
        this.results = this.findUserForm.valueChanges.pipe(
            debounceTime(300),
            distinctUntilChanged(),
            switchMap(term => term.length < 2 ? of([])
                : this.friendService.findUser(term)
            ),
            tap(term => console.warn('new term:', term)),
        );
    }

    newChat() {
        if (this.name && this.name !== '') {
            console.log('username=' + this.name);

            this.openDialog();
        }
    }

    openDialog() {
        const dialogRef = this.dialog.open(ChatNewDialogComponent, {
            width: '250px',
            data: {
                name: this.name
            }
        });

        dialogRef.afterClosed().pipe(first()).subscribe(result => {
            if (result) {
                if (isDevMode()) {
                    console.warn('saving new chat ' + this.name);
                }
                this.chatService.newChat(this.name).subscribe({
                    error: err => {
                        console.error(err);
                        this.snackBar.open('Failed to add chat "' + this.name + '!"', 'OK', {
                            duration: 3000,
                        });
                    }
                });
            }
            this.name = '';
        });
    }

}
