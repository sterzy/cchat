import {Component, HostBinding, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {ChatDocType} from '../../../core/database/chats/chat.types';
import {map, takeUntil} from 'rxjs/operators';
import {ChatService} from '../../services/chat/chat.service';
import {MatDialog} from '@angular/material';
import {MessageDocType, MessageMediaType} from '../../../core/database/messages/message.types';
import {MessageService} from '../../services/message/message.service';
import {UserManagementService} from '../../../landingpage/services/user-management.service';
import {ChatRootComponent} from '../chat-root/chat-root.component';
import {EmojiService} from "@ctrl/ngx-emoji-mart/ngx-emoji";

@Component({
    selector: 'cchat-chat-list',
    templateUrl: './chat-list.component.html',
    styleUrls: ['./chat-list.component.scss']
})
export class ChatListComponent implements OnInit, OnDestroy {

    selectedChatId$ = this.chatService.getSelectedChat().pipe(
        map(chat => chat ? chat.id : null),
    );

    selectedChatId: string;

    @HostBinding('class.chat-list')
    readonly chatListClass = true;

    chats$: Observable<ChatDocType[]> = this.chatService.getChats();

    lastMessages: { [key: string]: Observable<MessageDocType>} = {};

    killSwitch$ = new Subject<void>();

    constructor(
        private chatService: ChatService,
        private dialog: MatDialog,
        private messageService: MessageService,
        private userService: UserManagementService,
        private chatRootComponent: ChatRootComponent,
        private emojiService: EmojiService,
    ) {
    }

    ngOnInit() {
        this.selectedChatId$.pipe(
            takeUntil(this.killSwitch$),
        ).subscribe(chatId => {
            this.selectedChatId = chatId;
        });
    }

    ngOnDestroy(): void {
        this.killSwitch$.next();
    }

    getLastMessage(chatId: string) {
        if (this.lastMessages[chatId]) {
            return this.lastMessages[chatId];
        }

        this.lastMessages[chatId] = this.messageService.getMostRecentMessage(chatId)
            .pipe(map(message => {
                if (message != null && message.mediaType === MessageMediaType.Sticker) {
                    const emojiId = message.message.replace(/.*\//, '')
                        .replace(/_\d+$/, '');
                    const emojiData = this.emojiService.getData(emojiId);
                    if (emojiData !== null) {
                        message.message = emojiData.native;
                    } else {
                        message.message = '<' + emojiId + '>';
                    }
                    message.mediaType = MessageMediaType.Text;
                }
                return message;
            }));
        return this.lastMessages[chatId];
    }

    openNav() {
        this.chatRootComponent.openNav();
    }

    selectChat(chatId: string) {
        this.chatService.selectChatById(chatId);
        this.chatRootComponent.focusAside(false);
    }

    trackByChatIdFn(index: number, chat: ChatDocType) {
        return chat.id;
    }

}
