import {Component, OnInit, ViewChild} from '@angular/core';
import {ChatService} from '../../services/chat/chat.service';
import {distinctUntilChanged, filter, first, map, switchMap} from 'rxjs/operators';
import {NgxZxingMulticodeComponent, ScanResult} from 'ngx-zxing-multicode';
import {PubKeyPipe} from '../../../core/pipes/pub-key.pipe';
import {MatSnackBar} from '@angular/material';
import {ChatType} from '../../../core/database/chats/chat.types';

@Component({
    selector: 'cchat-chat-settings-modal',
    templateUrl: './chat-settings-dialog.component.html',
    styleUrls: ['./chat-settings-dialog.component.scss']
})
export class ChatSettingsDialogComponent implements OnInit {

    @ViewChild('scanner')

    @ViewChild('myElem')
    set setScanner(scanner: NgxZxingMulticodeComponent) {
        if (scanner) {
            this.scanner = scanner;
            this.scanner.notFound.subscribe((devices: MediaDeviceInfo[]) => {
                console.error('An error has occurred.');
            });
        }
    }

    scanner: NgxZxingMulticodeComponent;

    chat$ = this.chatService.getSelectedChat();

    scan = false;

    pubkey: string;

    directMessage = ChatType.DirectMessage;

    chatPubKey$ = this.chat$.pipe(
        filter(chat => !!chat),
        map(chat => chat.id),
        distinctUntilChanged(),
        switchMap(chatId => {
            return this.chatService.getIdentity(chatId);
        }),
    );

    constructor(
        private chatService: ChatService,
        private snackBar: MatSnackBar,
    ) {
    }

    ngOnInit() {

        this.chatPubKey$.subscribe((key) => {
            this.pubkey = new PubKeyPipe().transform(key);
        });

    }

    scanSuccessHandler(resultString: ScanResult) {
        console.log('Result: ', resultString);
        if (resultString.code === this.pubkey) {
            console.warn('validated!');
            this.chat$.pipe(first()).subscribe( (chat) => {
                    this.chatService.updateVerified(chat.id, this.pubkey).pipe(first()).subscribe(() => console.warn('updated!'));
                }
            );
        } else {
            console.warn('keys not equal: ', this.pubkey);
            this.snackBar.open('Detected key does not match key of user!', 'OK',
                {duration: 3000});
        }
        this.scan = false;
    }

    onScanClicked() {
        this.scan = this.scan !== true;
    }

    scanErrorHandler($event) {
        console.log('Error: ', $event.toString());
        this.snackBar.open('An Error occurred, try again later!', 'OK',
            {duration: 3000});
        this.scan = false;
        this.scanner.stop();
    }


}
