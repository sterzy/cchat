import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ChatSettingsDialogComponent} from './chat-settings-dialog.component';
import {MatDialog} from '@angular/material';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {OverlayContainer} from '@angular/cdk/overlay';
import {ClientModule} from '../../client.module';
import {NoopComponent} from '../../../util/test/test.module';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';

describe('ChatSettingsDialogComponent', () => {
    let component: ChatSettingsDialogComponent;
    let fixture: ComponentFixture<NoopComponent>;

    let overlayContainerElement: HTMLElement;
    let dialog: MatDialog;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [ClientModule, NoopAnimationsModule, RouterModule.forRoot([]), HttpClientModule],
            declarations: [NoopComponent],
            providers: [
                {
                    provide: OverlayContainer, useFactory: () => {
                        overlayContainerElement = document.createElement('div');
                        return {getContainerElement: () => overlayContainerElement};
                    }
                }
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NoopComponent);
        dialog = TestBed.get(MatDialog);
        component = dialog.open(ChatSettingsDialogComponent).componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
