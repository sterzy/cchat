import {Injectable} from '@angular/core';
import {MessageDao} from '../../../core/database/messages/message.dao';
import {Observable, of} from 'rxjs';
import {MessageDocType, MessageMediaType, MessageStatus} from '../../../core/database/messages/message.types';
import {catchError, map} from 'rxjs/operators';
import {uuidv4} from '../../../core/util/uuid.util';
import {ChatMessageComponent} from '../../components/chat-box/chat-body/chat-message/chat-message.component';

@Injectable({
    providedIn: 'root'
})
export class MessageService {

    messageMap: Map<string, ChatMessageComponent>;
    currentChat: string;

    constructor(
        private messageDao: MessageDao,
    ) {
    }

    /**
     * Method to get all messages of the corresponding conversation.
     * @param chatId    of the conversation
     */
    getMessages(chatId: string): Observable<MessageDocType[]> {
        return this.messageDao.getMessages(chatId).pipe(
            map(msgs => msgs.map(msg => msg.toDto())),
            catchError(err => {
                console.error(err);
                return of(err);
            }),
        );
    }

    /**
     * Method to save a message for a chat.
     * @param text          of the message
     * @param chatId        of the chat
     * @param username      of the sender
     * @param timeStamp     when the message was sent
     */
    addMessage(text: string, chatId: string, username: string, timeStamp: number, mediaType: MessageMediaType = MessageMediaType.Text): Observable<MessageDocType> {
        const uuid = uuidv4();
        const message: MessageDocType = {
            id: uuid,
            from: username,
            message: text,
            timeStamp,
            read: false,
            chat: chatId,
            status: MessageStatus.Received,
            mediaType,
        };

        return this.messageDao.addMessage(message).pipe(
            map(msg => msg && msg.toDto()),
            catchError(err => {
                console.error(err);
                return of(err);
            }),
        );
    }

    /**
     * Method to get the most recent message of a conversation.
     * @param chatId    of the conversation.
     */
    getMostRecentMessage(chatId: string) {
        return this.messageDao.getMostRecentMessage(chatId).pipe(
            map(msg => msg && msg.toDto()),
        );
    }

    /**
     * Method to search for messages containing a given string in a given conversation.
     * @param chatId    of the conversation.
     * @param like      the string that should be searched for
     */
    searchForMessages(chatId: string, like: string): Observable<MessageDocType[]> {
        like = '.*' + like.replace(/[.*+?^${}()|[\]\\]/g, '\\$&') + '.*';
        return this.messageDao.searchForMessages(chatId, like).pipe(
            map(msgs => msgs.map(msg => msg.toDto())),
            catchError(err => {
                console.error(err);
                return of(err);
            }),
        );
    }

    /**
     * Method to save all MessageComponents for further processing.
     * @param chatId    of the conversation.
     * @param id        of the message.
     * @param message   the component corresponding to the id.
     */
    registerMessage(chatId: string, id: string, message: ChatMessageComponent) {
        if (this.currentChat !== chatId) {
            this.currentChat = chatId;
            this.messageMap = new Map<string, ChatMessageComponent>();
        }

        this.messageMap.set(id, message);
    }

    /**
     * Method to focus a specific message in the chat.
     * @param msgId     of the message that should be focused.
     */
    setFocus(msgId: string) {
        if (this.messageMap.has(msgId)) {
            this.messageMap.get(msgId).scrollIntoView();
        }
    }

}
