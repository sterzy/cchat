import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {StickerForUpload, StickerPack} from '../../components/chat-box/sticker-list/sticker-list-dialog-data';
import {map, switchAll} from 'rxjs/operators';
import {ConfigService} from '../../../core/services/config/config.service';
import {SimpleUser} from '../../../core/models/user.models';

@Injectable({
    providedIn: 'root'
})
export class StickerService {

    apiurl = '/sticker/api';

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type':  'application/json',
        })
    };

    private user$ = this.config.getUser();

    constructor(private http: HttpClient,
                private config: ConfigService) { }

    getAllStickers(): Observable<StickerPack[]> {
        const stuff = this.http.get<StickerPack[]>(this.apiurl + '/stickerpacks', this.httpOptions)
            .pipe(
                map((value: StickerPack[]) => {
                    const observable = this.user$.pipe(
                        map((user: SimpleUser) => {
                            for (const stickerPack of value) {
                                stickerPack.isOwn = stickerPack.creator === user.id;
                            }
                            return value;
                        }));
                    return observable;
                }),
                switchAll()
            );
        return stuff;
    }


    /**
     * adds a new stickerpack to the backend and returns whether the operation succeeded
     * @param name Name of the new Stickerpack
     * @param stickers List of sticker data (files?)
     * @return true if the operation was successful (client side or observable?)
     */
    addNewStickerpack(name: string, stickers: StickerForUpload[]): Observable<boolean> {
        const bodyData = {
            name,
            stickers
        };

        const postResponse = this.http.post(this.apiurl + '/stickerpacks', bodyData, this.httpOptions);

        return postResponse.pipe(
            map(() => true),
        );
    }

    deleteStickerpack(stickerpackId: string): Observable<boolean> {
        const httpResponse = this.http.delete(this.apiurl + '/stickerpacks/' + stickerpackId);

        return httpResponse.pipe(
            map<any, boolean>(() => true),
        );
    }
}
