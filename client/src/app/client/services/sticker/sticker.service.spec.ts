import {TestBed} from '@angular/core/testing';

import {StickerService} from './sticker.service';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';

describe('StickerService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [RouterModule.forRoot([]), HttpClientModule],
    }));

    it('should be created', () => {
        const service: StickerService = TestBed.get(StickerService);
        expect(service).toBeTruthy();
    });
});
