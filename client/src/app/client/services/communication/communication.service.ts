import {Injectable, isDevMode} from '@angular/core';
import {EMPTY, from, merge, Observable, throwError} from 'rxjs';
import {RxStompService} from '@stomp/ng2-stompjs';
import {MessageService} from '../message/message.service';
import {ChatService} from '../chat/chat.service';
import {ChatDocType, ChatType} from '../../../core/database/chats/chat.types';
import {catchError, filter, first, map, mapTo, mergeMap, skipWhile, switchMap, tap} from 'rxjs/operators';
import {ConfigService} from '../../../core/services/config/config.service';
import {
    MessagingCryptoMessage,
    MessagingOnlinePayload,
    MessagingPayload,
    MessagingPayloadTypes,
    MessagingReceivedMessage,
    MessagingTextPayload,
    MessagingStickerPayload,
    MessagingTypes,
} from '../../models/communication.models';
import {communcationStompConfig} from './communcation-stomp.config';
import {Ciphertext, CryptoService} from '../crypto/crypto.service';
import {MessageMediaType} from "../../../core/database/messages/message.types";
import {MatSnackBar} from '@angular/material';

@Injectable({
    providedIn: 'root'
})
export class CommunicationService {
    private userId$ = this.config.getUser();

    private registrations$ = this.userId$.pipe(
        skipWhile(userId => !userId),
        map(user => {

            this.rxStompService.deactivate();

            if (!user) {
                return null;
            }

            this.rxStompService.configure(communcationStompConfig);
            this.rxStompService.activate();

            return [
                this.rxStompService.watch('/user/exchange/amq.direct/error').pipe(
                    tap(msg => this.snackBar.open(msg.body, 'OK'))
                ),
                this.rxStompService.watch('/user/exchange/amq.direct/conversation').pipe(
                    map(msg => JSON.parse(msg.body) as MessagingReceivedMessage),
                    mergeMap((json): Observable<any> => {
                        return this.handleReceivedMessage(json);
                    })
                ),
            ];
        }),
        switchMap(obs => {
            if (!obs) {
                return EMPTY;
            }
            return merge(...obs);
        }),
        mapTo(undefined),
    );

    constructor(
        private rxStompService: RxStompService,
        private messageService: MessageService,
        private chatService: ChatService,
        private config: ConfigService,
        private cryptoService: CryptoService,
        private snackBar: MatSnackBar,
    ) {
        this.registrations$.subscribe();
    }

    /**
     * Sends a message to the given chat.
     *
     * The message is first persisted and then sent to the given chat.
     *
     * The message is also encrypted.
     *
     * @param text The plaintext message.
     * @param chat The chat to send the message to.
     */
    sendMessage(text: string, chat: ChatDocType) {
        this.userId$.pipe(
            filter(userId => !!userId),
            first(),
            switchMap(() => {
                // Here the username is '' so that we indicate that we sent it
                return this.messageService.addMessage(text, chat.id, '', Date.now());
            }),
        ).subscribe(msg => {

            const payload: MessagingTextPayload = {
                type: MessagingPayloadTypes.PAY_TEXT,
                text,
            };

            this.cryptoService.encrypt(JSON.stringify(payload), chat).then((cipher: Ciphertext) => {

                    const tmp: MessagingCryptoMessage = {
                        type: MessagingTypes.MSG_CRYPTO,
                        timestamp: Date.now(),
                        payload: cipher.body,
                        cryptoType: cipher.type,
                        id: msg.id,
                        from: undefined, // this will be overwritten anyway
                    };

                    if (isDevMode()) {
                        console.log('trying to send msg: ', cipher);
                    }

                    const destination = (chat.type === ChatType.Bot ? '/chat/bot/send/' : '/chat/send/') + chat.id;

                    this.rxStompService.publish({
                        destination,
                        body: JSON.stringify(tmp),
                    });
                },
                () => {
                    this.snackBar.open('Failed to encrypt message!', 'OK', {
                        duration: 3000,
                    });
                }
            );
        });
    }

    /**
     * Send the time when this user was last online to the open chat.
     *
     * @param chat The chat to notify about the current online status.
     */
    sendLastOnline(chat: ChatDocType) {

        const msg: MessagingOnlinePayload = {
            type: MessagingPayloadTypes.PAY_ONLINE,
            time: Date.now(),
        };

        this.cryptoService.encrypt(JSON.stringify(msg), chat).then((ciphertext: Ciphertext) => {
            const tmp: MessagingCryptoMessage = {

                type: MessagingTypes.MSG_CRYPTO,
                timestamp: Date.now(),
                payload: ciphertext.body,
                cryptoType: ciphertext.type,
                id: 'ignore-online-msg', // we don't really care about whether online messages are being delivered
                from: undefined,  // this will be overwritten anyway

            };

            this.rxStompService.publish({
                destination: '/chat/send/' + chat.id, body: JSON.stringify(tmp)
            });
        });
    }

    /**
     * Send a sticker (by URL) to the given chat.
     *
     * @param stickerUrl the sticker to be sent
     * @param chat The chat to send the message to
     */
    sendSticker(stickerUrl: string, chat: ChatDocType) {

        this.userId$.pipe(
            filter(userId => !!userId),
            first(),
            switchMap(() => {
                // Here the username is '' so that we indicate that we sent it
                return this.messageService.addMessage(stickerUrl, chat.id, '', Date.now(), MessageMediaType.Sticker);
            }),
        ).subscribe(msg => {

            const payload: MessagingStickerPayload = {
                type: MessagingPayloadTypes.PAY_STICKER,
                url: stickerUrl,
            };

            this.cryptoService.encrypt(JSON.stringify(payload), chat).then((cipher: Ciphertext) => {

                    const tmp: MessagingCryptoMessage = {
                        type: MessagingTypes.MSG_CRYPTO,
                        timestamp: Date.now(),
                        payload: cipher.body,
                        cryptoType: cipher.type,
                        id: msg.id,
                        from: undefined, // this will be overwritten anyway
                    };

                    console.log('trying to send msg (which is a sticker)');

                    const destination = (chat.type === ChatType.Bot ? '/chat/bot/send/' : '/chat/send/' ) + chat.id;

                    this.rxStompService.publish({
                        destination,
                        body: JSON.stringify(tmp),
                    });
                },
                () => {
                    console.error('Msg could not be encrypted', msg, chat.id);
                    this.snackBar.open('Failed to encrypt message');
                }
            );
        });

    }

    /**
     * This function will handle incoming messages.
     *
     * @param message The received message.
     */
    private handleReceivedMessage(message: MessagingReceivedMessage): Observable<any> {
        switch (message.type) {

            case MessagingTypes.MSG_CRYPTO: {
                if (isDevMode()) {
                    console.log('received message: ', message.payload);
                }
                return this.chatService.getChatByIdOrCreate(message.from).pipe(
                    first(),
                    mergeMap((chat) => {

                        if (chat.blocked) {
                            if (isDevMode()) {
                                console.warn('User is blocked: ', chat.id);
                            }
                            return EMPTY;
                        }

                        let decryption: Promise<string>;

                        const ciphertext: Ciphertext = {
                            body: message.payload,
                            type: message.cryptoType,
                        };

                        decryption = this.cryptoService.decrypt(ciphertext, chat);

                        return from(decryption.then(d => {
                            return d;
                        })).pipe(
                            mergeMap(decrypted => {
                                const payload: MessagingPayload = JSON.parse(decrypted);

                                switch (payload.type) {

                                    case MessagingPayloadTypes.PAY_ONLINE:
                                        return this.chatService.updateLastOnline(message.from, payload.time);

                                    case MessagingPayloadTypes.PAY_TEXT:
                                        return this.messageService.addMessage(
                                            payload.text,
                                            chat.id,
                                            message.from,
                                            message.timestamp,
                                        ).pipe(
                                            mapTo(chat),
                                        );

                                    case MessagingPayloadTypes.PAY_STICKER:
                                        console.warn('sticker received:', payload)
                                        return this.messageService.addMessage(
                                            payload.url,
                                            chat.id,
                                            message.from,
                                            message.timestamp,
                                            MessageMediaType.Sticker,
                                        ).pipe(
                                            mapTo(chat),
                                        );
                                }

                            }),
                            catchError(err => {
                                this.snackBar.open('Failed to decrypt message from ' + message.from, 'OK', {
                                    duration: 3000,
                                });
                                return throwError(err);
                            })
                        );
                    }),
                );
            }

            case MessagingTypes.MSG_SYSTEM: {
                // ignore those message for now
                return EMPTY;
            }

        }

        console.error('Received Message of unknown type:', message);
        return EMPTY;
    }


}
