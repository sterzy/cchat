import {TestBed} from '@angular/core/testing';

import {CommunicationService} from './communication.service';
import {ClientModule} from '../../client.module';
import {TEST_RX_STOMP_PROVIDERS} from '../../../util/test/providers';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';

describe('CommunicationService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [ClientModule, HttpClientModule, RouterModule.forRoot([])],
        providers: TEST_RX_STOMP_PROVIDERS,
    }));

    it('should be created', () => {
        const service: CommunicationService = TestBed.get(CommunicationService);
        expect(service).toBeTruthy();
    });
});
