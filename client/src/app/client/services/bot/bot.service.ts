import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {BotDetails, BotRegisterRequest, BotSearchResponse, MyBotReponse} from '../../models/bot.models';
import {BotDao} from '../../../core/database/bots/bot.dao';
import {catchError, first, map, mapTo, shareReplay, switchMap} from 'rxjs/operators';
import {EMPTY, merge, Observable, of, throwError} from 'rxjs';
import {BotDocType} from '../../../core/database/bots/bot.types';
import {ConfigService} from '../../../core/services/config/config.service';
import {ChatDocType, ChatType} from '../../../core/database/chats/chat.types';
import {ChatDao} from '../../../core/database/chats/chat.dao';
import {CryptoService} from '../crypto/crypto.service';
import {MessagingCryptoMessage} from '../../models/communication.models';
import {MatDialog, MatSnackBar} from '@angular/material';

@Injectable({
    providedIn: 'root'
})
export class BotService {

    private readonly apiBaseURL = 'bot/api/';

    constructor(
        private http: HttpClient,
        private botDao: BotDao,
        private chatDao: ChatDao,
        private config: ConfigService,
        private cryptoService: CryptoService,
        private snackBar: MatSnackBar,
    ) {
    }

    /**
     * Search for bots with similar names to the given name.
     *
     * @param name The name to search for.
     */
    searchBotsByName(name: string) {
        return this.http.get<BotSearchResponse[]>(this.apiBaseURL + 'bots/search', {
            params: new HttpParams().set('q', name),
        });
    }

    /**
     * Returns the information for a bot by id.
     *
     * @param id The id of the bot get information for.
     */
    getBotById(id: string) {
        return merge(
            this.http.get<MyBotReponse>(this.apiBaseURL + `bots/${id}`).pipe(
                switchMap(bot => this.saveBot(bot)),
                first(),
                switchMap(() => EMPTY),
                catchError(err => {
                    console.error(err);
                    return EMPTY;
                }),
                shareReplay(1),
            ),
            this.botDao.getBotById(id).pipe(
                map(bot => bot.toDto()),
            ),
        );
    }

    /**
     * Register a new bot.
     *
     * @param bot The details for a bot to register.
     */
    registerBot(bot: BotRegisterRequest) {
        return this.http.post<MyBotReponse>(this.apiBaseURL + 'bots', bot).pipe(
            switchMap(registeredBot => this.saveBot(registeredBot)),
        );
    }

    /**
     * Save bots to the local database.
     *
     * @param bot The details of a bot.
     */
    saveBot(bot: BotDetails) {
        return this.botDao.saveBot({
            id: bot.id,
            name: bot.name,
            description: bot.description,
            isActive: bot.isActive,
            ownerName: bot.registeredBy,
            ownerId: bot.registeredByUUID,
            isPublic: bot.isPublic,
        }).pipe(
            map(b => b.toDto()),
        );
    }

    /**
     * Saves bots to the local database.
     *
     * @param bots The details of bots to save.
     */
    saveBots(bots: BotDetails[]) {
        return merge(...bots.map(bot => this.saveBot(bot)));
    }

    /**
     * Get all bots currrently in the local database.
     */
    getAllBots(): Observable<BotDocType[]> {
        return this.botDao.getBots().pipe(
            map(bots => bots.map(bot => bot.toDto())),
        );
    }

    /**
     * Returns all bots which were registered by the local user.
     */
    getMyBots(): Observable<BotDocType[]> {

        return merge(
            // Update the data by getting the most recent data from the backend
            this.http.get<MyBotReponse[]>(this.apiBaseURL + 'bots/mine').pipe(
                switchMap(bots => this.saveBots(bots)),
                switchMap(() => EMPTY),
            ),

            // Get the data from the database
            this.config.getUser().pipe(
                switchMap(user => {
                    return this.botDao.getBotsByOwnerName(user.username);
                }),
                map(bots => bots.map(bot => bot.toDto())),
            ),
        ).pipe(
            shareReplay(1),
        );
    }

    /**
     * Returns all bots which were added by to the chat list.
     */
    getAddedBots(): Observable<BotDocType[]> {
        return this.chatDao.getChatsWithBot().pipe(
            switchMap(chats => this.botDao.getBotsByName(chats.map(chat => chat.id))),
        );
    }

    /**
     * Adds a bot to the chat list.
     *
     * This method will also whitelist the bot on the server, so that the bot is able to send messages to the current
     * user.
     *
     * @param id The id of the bot add to the chat list.
     */
    addBotToChats(id: string): Observable<ChatDocType> {
        return this.http.get<MyBotReponse>(this.apiBaseURL + `bots/${id}/add`).pipe(
            switchMap(bot => this.saveBot(bot)),
            first(),
            switchMap(bot => {
                return this.chatDao.getChatById(bot.name).pipe(
                    first(),
                    switchMap(chat => {
                        if (chat == null) {
                            return this.chatDao.saveChat({
                                id: bot.name,
                                lastMessage: Date.now(),
                                started: Date.now(),
                                type: ChatType.Bot,
                                users: [bot.name],
                                lastOnline: Date.now(),
                                name: bot.name,
                                blocked: false,
                                verifiedKey: undefined,
                                hidden: false,
                            });
                        }
                        return of(chat);
                    })
                );
            }),
            first(),
            switchMap(chat => {
                return this.cryptoService.build_session(chat.id).pipe(
                    mapTo(chat),
                );
            }),
            first(),
            catchError(error => {
                this.snackBar.open('Failed to add bot to chats');
                return of(undefined);
            }),
            shareReplay(1),
        );
    }

    /**
     * Sends a test message to a bot.
     *
     * @param id        The id of the bot to send a test message to.
     * @param message   The test message to send to the bot.
     */
    sendTestMessageToBot(id: string, message: MessagingCryptoMessage) {
        return this.http.post<MessagingCryptoMessage>(`${this.apiBaseURL}bots/${id}/test`, message).pipe(
            shareReplay(1),
        );
    }

    /**
     * Sets the 'active' property of a bot.
     *
     * Users cannot interact with inactive bots.
     *
     * @param botId     The id of the bot.
     * @param isActive  If the bot is active or not. Defaults to true.
     */
    setBotActive(botId: string, isActive = true): Observable<BotDocType> {
        const url = `${this.apiBaseURL}bots/${botId}/${isActive ? 'activate' : 'disable'}`;
        return this.http.post<BotDetails>(url, undefined).pipe(
            switchMap(bot => {
                return this.saveBot(bot);
            }),
            shareReplay(1),
        );
    }

    /**
     * Sets the 'public' attribute of a bot.
     *
     * Users cannot find a private bot, but can still send messages to it, if they have already added it.
     *
     * @param botId     The id of the bot.
     * @param isPublic  If the bot is public or not. Defaults to true.
     */
    setBotPublic(botId: string, isPublic = true): Observable<BotDocType> {
        const url = `${this.apiBaseURL}bots/${botId}/${isPublic ? 'publicise' : 'privatise'}`;
        return this.http.post<BotDetails>(url, undefined).pipe(
            switchMap(bot => {
                return this.saveBot(bot);
            }),
            shareReplay(1),
        );
    }
}
