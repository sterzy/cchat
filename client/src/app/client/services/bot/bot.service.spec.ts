import {TestBed} from '@angular/core/testing';

import {BotService} from './bot.service';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {MatSnackBarModule} from '@angular/material';

describe('BotService', () => {
    beforeEach(() => TestBed.configureTestingModule({
        imports: [HttpClientModule, RouterModule.forRoot([]), MatSnackBarModule]
    }));

    it('should be created', () => {
        const service: BotService = TestBed.get(BotService);
        expect(service).toBeTruthy();
    });
});
