import {Injectable} from '@angular/core';
import {ChatDao} from '../../../core/database/chats/chat.dao';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {ChatDocType, ChatType} from '../../../core/database/chats/chat.types';
import {catchError, distinctUntilChanged, first, map, mapTo, shareReplay, switchMap} from 'rxjs/operators';
import {PartialBy} from '../../../core/util/partialby.type';
import {uuidv4} from '../../../core/util/uuid.util';
import {Router} from '@angular/router';
import {ROUTES} from '../../routes';
import {CryptoService} from '../crypto/crypto.service';

/**
 * Service to manage chats.
 */
@Injectable({
    providedIn: 'root',
})
export class ChatService {

    private selectedChatId: string;
    private selectedChatId$: Subject<string> = new BehaviorSubject(null);

    private selectedChat$ = this.selectedChatId$.pipe(
        distinctUntilChanged(),
        switchMap(id => id ? this.getChatById(id) : of(null as ChatDocType)),
        shareReplay(1),
    );


    constructor(
        private chatDAO: ChatDao,
        private router: Router,
        private cryptoService: CryptoService,
    ) {
    }

    /**
     * Return all chats from the database.
     */
    getChats(): Observable<ChatDocType[]> {
        return this.chatDAO.getChats().pipe(
            map(chats => chats.map(chat => chat.toDto())),
        );
    }

    /**
     * Returns all chats with a bot.
     */
    getChatsWithBot(): Observable<ChatDocType[]> {
        return this.chatDAO.getChatsWithBot().pipe(
            map(chats => chats.map(chat => chat.toDto())),
        );
    }

    /**
     * Get a chat by its id.
     *
     * If no chat is found null or undefined is returned.
     *
     * @param id The id of the chat to search for.
     */
    getChatById(id: string): Observable<ChatDocType> {
        return this.chatDAO.getChatById(id).pipe(
            map(chat => chat && chat.toDto()),
            catchError(err => {
                console.error(err);
                return of(null);
            })
        );
    }

    /**
     * This method will either return a chat with a given ID or create a new chat with that id.
     *
     * @param id The ID of the chat.
     */
    getChatByIdOrCreate(id: string): Observable<ChatDocType> {
        return this.chatDAO.getChatById(id).pipe(
            first(),
            switchMap(chat => {
                if (chat) {
                    console.warn('chat with id already exists', id);
                    return of(chat as ChatDocType);
                }

                console.warn('No chat for id found', id);

                return this.newChat(id).pipe(
                    map(newChat => newChat as ChatDocType),
                );
            })
        );
    }

    /**
     * Get a chat by name.
     *
     * If no chat is found or multiple chats with the name are found, null will be returned.
     *
     * @param chatName The name to search for.
     */
    getChatByName(chatName: string): Observable<ChatDocType> {
        return this.chatDAO.getChatByName(chatName).pipe(
            map(chat => chat && chat.toDto()),
            catchError(err => {
                console.error(err);
                return of(null);
            }),
        );
    }

    /**
     * Returns the currently selected chat.
     *
     * Might be null or undefined if there currently is no selected chat or if there is no chat with the given name.
     */
    getSelectedChat() {
        return this.selectedChat$;
    }

    /**
     * Select a chat by its id.
     *
     * Also navigates to the selected chat.
     *
     * @param chatId The chat to select.
     */
    selectChatById(chatId: string) {
        this.selectedChatId = chatId;
        this.selectedChatId$.next(chatId);
        if (chatId != null) {
            this.router.navigate([ROUTES.CHAT, chatId]);
        }
    }

    /**
     * Creates a new chat with the given name.
     *
     * @param chatId The name of the chat to display.
     * @param type   The type of the new chat. Defaults to  {@link ChatType.DirectMessage}.
     */
    newChat(chatId: string, type = ChatType.DirectMessage): Observable<ChatDocType> {
        if (!chatId) {
            return of(null);
        }
        const now = new Date().getTime();
        return this.getChatById(chatId).pipe(
            first(),
            switchMap(foundChat => {
                if (foundChat != null) {
                    return this.updateHidden(chatId, false);
                } else {
                    return this.saveChat({
                        lastMessage: now,
                        id: chatId,
                        // Hard coded to DirectMessage for now
                        type,
                        started: now,
                        users: [],
                        lastOnline: now,
                        blocked: false,
                        verifiedKey: undefined,
                        hidden: false,
                    }).pipe(
                        switchMap(chat => {
                            return this.cryptoService.build_session(chat.id).pipe(
                                mapTo(chat),
                            );
                        })
                    );
                }
            })
        );
    }

    /**
     * Saves a chat to the database.
     *
     * If the chat has no ID set, a random UUIDv4 will be used.
     *
     * @param chat The Chat to save. ID is optional
     */
    saveChat(chat: PartialBy<ChatDocType, 'name'>): Observable<ChatDocType> {
        if (chat.id == null) {
            chat.id = uuidv4();
        }
        chat.name = chat.name || chat.id;
        chat.id = chat.id.toLowerCase();
        return this.chatDAO.saveChat(chat as ChatDocType).pipe(
            map(c => c && c.toDto()),
        );
    }

    /**
     * Update the LastOnline field
     *
     * @param chatName      The chat name which should be updated
     * @param lastOnline    The new LastOnline
     */
    updateLastOnline(chatName: string, lastOnline: number) {
        return this.chatDAO.setLastOnline(chatName, lastOnline).pipe(
            map(c => c && c.toDto()),
        );
    }

    /**
     * Update the blocked field
     *
     * @param chatName      The chat name which should be updated
     * @param blocked       The new blocked
     */
    updateBlocked(chatName: string, blocked: boolean) {
        return this.chatDAO.setBlocked(chatName, blocked).pipe(
            map(c => c && c.toDto()),
        );
    }

    /**
     * Update the hidden field
     *
     * @param chatName      The chat name which should be updated
     * @param hidden       The new blocked
     */
    updateHidden(chatName: string, hidden: boolean) {
        return this.chatDAO.setHidden(chatName, hidden).pipe(
            map(c => c && c.toDto()),
        );
    }

    /**
     * Update the verifiedKey field
     *
     * @param chatName      The chat name which should be updated
     * @param blocked       The new blocked
     */
    updateVerified(chatName: string, verifiedKey: string) {
        return this.chatDAO.setVerified(chatName, verifiedKey).pipe(
            map(c => c && c.toDto()),
        );
    }

    /**
     * Hides a specified chat.
     *
     * @param chatName     The chat name which should be hidden.
     */
    hideChat(chatName: string) {
        return this.getChatById(chatName).pipe(
            first(),
            switchMap((chat) => {
                return this.chatDAO.hideChat(chat);
            })
        );
    }

    /**
     * Gets the public key of a chat.
     *
     * @param chatId The chat to get the public key from.
     */
    getIdentity(chatId: string) {
        return this.chatDAO.loadIdentityKey(chatId);
    }
}
