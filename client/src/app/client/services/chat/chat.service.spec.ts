import {TestBed} from '@angular/core/testing';

import {ChatService} from './chat.service';
import {RouterModule} from '@angular/router';
import {ClientModule} from '../../client.module';
import {HttpClientModule} from '@angular/common/http';

describe('ChatService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [RouterModule.forRoot([]), ClientModule, HttpClientModule],
  }));

  it('should be created', () => {
    const service: ChatService = TestBed.get(ChatService);
    expect(service).toBeTruthy();
  });
});
