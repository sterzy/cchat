import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {SimpleUser} from '../../../core/models/user.models';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FriendService {

    apiurl = '/user/api/friends';

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
        })
    };

  constructor(private http: HttpClient) { }

    /**
     * Method to find a user with a username like the given name.
     * @param like  the term to search for
     */
  findUser(like: string): Observable<SimpleUser[]> {
      return this.http.post<SimpleUser[]>(this.apiurl + '/find', like, this.httpOptions);
  }

}
