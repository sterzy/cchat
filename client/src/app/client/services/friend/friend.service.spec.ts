import { TestBed } from '@angular/core/testing';

import { FriendService } from './friend.service';
import {HttpClientModule} from '@angular/common/http';

describe('FriendService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: FriendService = TestBed.get(FriendService);
    expect(service).toBeTruthy();
  });
});
