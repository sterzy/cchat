import { TestBed } from '@angular/core/testing';

import { CryptoService } from './crypto.service';
import {ClientModule} from '../../client.module';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';

describe('CryptoService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [ClientModule, HttpClientModule, RouterModule.forRoot([])]
  }));

  it('should be created', () => {
    const service: CryptoService = TestBed.get(CryptoService);
    expect(service).toBeTruthy();
  });
});
