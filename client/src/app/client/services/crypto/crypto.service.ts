import {Injectable} from '@angular/core';
import {SignalStore} from '../../../core/signal/signal.store';
import {forkJoin, from, Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {distinctUntilKeyChanged, filter, first, map, mapTo, switchMap} from 'rxjs/operators';
import {
    KeyPair,
    LocalPreKey,
    LocalSignedPreKey,
    PreKeyBundle,
    PublicPreKey,
    PublicSignedPreKey
} from '../../../core/signal/models/keys.model';
import {LocaluserDao} from '../../../core/database/localusers/localuser.dao';
import {ChatDocType, ChatType} from '../../../core/database/chats/chat.types';
import {ChatDao} from '../../../core/database/chats/chat.dao';
import {MatSnackBar} from '@angular/material';

declare var libsignal: any;
const KeyHelper = libsignal.KeyHelper;

@Injectable({
    providedIn: 'root'
})
export class CryptoService {


    private apiurl = '/user/api/keys';
    private botApiUrl = '/bot/api/';

    private httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
        })
    };

    constructor(
        private store: SignalStore,
        private http: HttpClient,
        private localuserDao: LocaluserDao,
        private chatDao: ChatDao,
        private snackBar: MatSnackBar,
    ) {
        this.localuserDao.getLocaluser().pipe(
            filter(user => !!user),
            distinctUntilKeyChanged('id'),
            switchMap(user => {
                // There is a new local user, check if there are already keys registered
                return this.store.getIdentityKeyPair();
            }),
        ).subscribe((keyPair) => {
            if (keyPair == null) {
                this.installation_time();
            }
        });
    }

    /**
     * Method to store Keys in local storage, sends keys to remote server.
     */
    private installation_time() {
        const registrationId = KeyHelper.generateRegistrationId();

        forkJoin(
            this.store.saveLocalRegistrationId(registrationId),
            forkJoin(this.generatePreKeys()),

            from(KeyHelper.generateIdentityKeyPair()).pipe(
                switchMap((identityKeyPair: KeyPair) => {
                    return from(this.store.saveIdentityKeyPair(identityKeyPair)).pipe(
                        mapTo(identityKeyPair),
                    );
                }),
            ),

            this.store.getNewKeyId(),
        ).pipe(
            switchMap(([id, preKeys, identityKeyPair, newKeyId]) => {
                return KeyHelper.generateSignedPreKey(identityKeyPair, newKeyId)
                    .then((signedPreKey: LocalSignedPreKey) => {
                        return this.store.storeSignedPreKey(signedPreKey.keyId, signedPreKey.keyPair)
                            .then(() => signedPreKey);
                    })
                    .then((signedPreKey: LocalSignedPreKey): Initial => {
                        return {
                            identityKey: this.byteArray_to_base64(identityKeyPair.pubKey),
                            preKeys,
                            registrationId,
                            signedPreKey: {
                                keyId: signedPreKey.keyId,
                                publicKey: this.byteArray_to_base64(signedPreKey.keyPair.pubKey),
                                signature: this.byteArray_to_base64(signedPreKey.signature),
                            },
                        };
                    });
            }),
            switchMap(json => {
                return this.http.post<Initial>(this.apiurl + '/initial', json, this.httpOptions);
            }),
        ).subscribe({
            error: err => {
                this.snackBar.open('Failed to initialize cryptographic keys!\nPlease clear the local data and reload the site.', 'OK', {
                    duration: Infinity,
                });
            }
        });
    }

    /**
     * Helper method to generate multiple preKeys
     */
    private generatePreKeys() {

        const promises: Observable<PreKey>[] = [];

        for (let i = 0; i < 5; i++) {
            promises.push(
                this.store.getNewKeyId().pipe(
                    switchMap(keyId => KeyHelper.generatePreKey(keyId).then((preKey: LocalPreKey) => {
                        return this.store.storePreKey(preKey.keyId, preKey.keyPair).then(
                            (): PreKey => ({
                                publicKey: this.byteArray_to_base64(preKey.keyPair.pubKey),
                                keyId: preKey.keyId
                            })
                        );
                    })),
                ),
            );
        }

        return promises;
    }

    /**
     * Method to create a session
     * @param userId to create a session with
     */
    build_session(userId: string): Observable<void> {
        // devideId hardcoded to 1 - support for only 1 device atm
        const address = new libsignal.SignalProtocolAddress(userId, 1);
        const sessionBuilder = new libsignal.SessionBuilder(this.store, address);

        let keybundle;
        return this.chatDao.getChatByName(userId).pipe(
            first(),
            map(chat => {
                switch (chat.type) {
                    case ChatType.DirectMessage:
                        return this.apiurl + '/keybundle/' + userId;
                    case ChatType.Bot:
                        return `${this.botApiUrl}bots/${userId}/bundle`;
                }
            }),
            switchMap(url => {
                console.warn("'Getting keys from:", url);
                return this.http.get<KeyBundle>(url, this.httpOptions).pipe(
                    switchMap((data: KeyBundle) => {

                        keybundle = {...data};

                        const preKeyBundle: PreKeyBundle = {
                            identityKey: this.base64_to_byteArray(data.identityKey),
                            preKey: {
                                keyId: data.preKey.keyId,
                                publicKey: this.base64_to_byteArray(data.preKey.publicKey),
                            },
                            registrationId: data.registrationId,
                            signedPreKey: {
                                signature: this.base64_to_byteArray(data.signedPreKey.signature),
                                keyId: data.signedPreKey.keyId,
                                publicKey: this.base64_to_byteArray(data.signedPreKey.publicKey),
                            },
                        };

                        return sessionBuilder.processPreKey(preKeyBundle) as Promise<void>;
                    }),
                );
            }),
        );
    }

    /**
     * Method to convert a base64 string to a ArrayBuffer
     * @param base64 string to be converted
     */
    private base64_to_byteArray(base64: string): ArrayBuffer {
        const binaryString = window.atob(base64);
        const len = binaryString.length;
        const bytes = new Uint8Array(len);
        for (let i = 0; i < len; i++) {
            bytes[i] = binaryString.charCodeAt(i);
        }
        return bytes.buffer;
    }

    /**
     * Method to convert an ArrayBuffer to a base64 string
     * @param byteArray to be converted
     */
    byteArray_to_base64(byteArray: ArrayBuffer): string {
        let binary = '';
        const bytes = new Uint8Array(byteArray);
        const len = bytes.byteLength;
        for (let i = 0; i < len; i++) {
            binary += String.fromCharCode(bytes[i]);
        }
        return window.btoa(binary);
    }

    /**
     * Method to encrypt a message
     * @param msg       to be encrypted
     * @param chat      the message should be sent to
     */
    encrypt(msg: string, chat: ChatDocType): Promise<Ciphertext> {
        const address = new libsignal.SignalProtocolAddress(chat.id, 1);
        const sessionCipher = new libsignal.SessionCipher(this.store, address);

        if (chat.type === ChatType.Bot) {
            // Messages to bots must be Base64 encoded
            return sessionCipher.encrypt(new TextEncoder().encode(msg), 'binary')
                .then((cipherText: Ciphertext) => {
                    return {
                        type: cipherText.type,
                        body: btoa(cipherText.body),
                    };
                });
        }
        return sessionCipher.encrypt(new TextEncoder().encode(msg), 'binary');

    }

    /**
     * Method to decrypt a message
     * @param msg       to be decrypted
     * @param chat      the message should be sent to
     */
    decrypt(msg: Ciphertext, chat: ChatDocType): Promise<string> {
        const address = new libsignal.SignalProtocolAddress(chat.id, 1);
        const sessionCipher = new libsignal.SessionCipher(this.store, address);

        switch (msg.type) {
            case 2:
            case 1: {
                if (chat.type === ChatType.Bot) {
                    // Messages from bots are Base64 encoded
                    return sessionCipher.decryptWhisperMessage(atob(msg.body), 'binary').then((plaintext) => {
                        return new TextDecoder().decode((plaintext));
                    });
                }
                return sessionCipher.decryptWhisperMessage(msg.body, 'binary').then((plaintext) => {
                    return new TextDecoder().decode((plaintext));
                });
            }
            case 3: {
                if (chat.type === ChatType.Bot) {
                    // Messages from bots are Base64 encoded
                    return sessionCipher.decryptPreKeyWhisperMessage(atob(msg.body), 'binary').then((plaintext) => {
                        return new TextDecoder().decode((plaintext));
                    });
                }
                return sessionCipher.decryptPreKeyWhisperMessage(msg.body, 'binary').then((plaintext) => {
                    return new TextDecoder().decode((plaintext));
                });
            }
            default: {
                throw new Error('Unknown Crypto Type: ' + msg.type);
            }
        }
    }


}

// This type will convert an object to one which has no ArrayBuffers. This is a deep conversion, so all sub-objects
// have to be encoded to string too.
type Encode<T> = T extends ArrayBuffer ? string : // T is an ArrayBuffer, has to be converted to string
    T extends object ? ({
        [P in keyof T]: T[P] extends object ? Encode<T[P]> : // Type of field P is an object, so it has to be deep-converted
            T[P]; // Type is neither ArrayBuffer nor object, so use the type of field P
    }) : T;

export interface Initial {
    registrationId: number;
    identityKey: string;
    signedPreKey: Encode<PublicSignedPreKey>;
    preKeys: Encode<PublicPreKey>[];
}

export type KeyBundle = Encode<PreKeyBundle>;

export type PreKey = Encode<PublicPreKey>;

export interface Ciphertext {
    type: number;
    body: string;
}
