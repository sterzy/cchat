import {CommonModule} from '@angular/common';

import {ClientRoutingModule} from './client-routing.module';
import {ChatBoxComponent} from './components/chat-box/chat-box.component';
import {ChatHeaderComponent} from './components/chat-box/chat-header/chat-header.component';
import {ChatInputComponent} from './components/chat-box/chat-input/chat-input.component';
import {ContenteditableModule} from '@ng-stack/contenteditable';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {ChatListComponent} from './components/chat-list/chat-list.component';
import {ChatListEntryComponent} from './components/chat-list/chat-list-entry/chat-list-entry.component';
import {
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDialogModule,
    MatDividerModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatToolbarModule,
    MatSnackBarModule,
    MatSlideToggleModule, MatProgressSpinnerModule,
} from '@angular/material';
import {ChatNewDialogComponent} from './components/chat-list/chat-new-dialog/chat-new-dialog.component';
import {ChatListHeaderComponent} from './components/chat-list/chat-list-header/chat-list-header.component';
import {ChatMessageComponent} from './components/chat-box/chat-body/chat-message/chat-message.component';
import {ChatBodyComponent} from './components/chat-box/chat-body/chat-body.component';
import {ChatSettingsDialogComponent} from './components/chat-settings-dialog/chat-settings-dialog.component';
import {ChatRootComponent} from './components/chat-root/chat-root.component';
import {StickerListComponent} from './components/chat-box/sticker-list/sticker-list.component';
import {ChatSettingsComponent} from './components/chat-list/chat-list-header/chat-settings/chat-settings.component';
import {CoreModule} from '../core/core.module';
import {PublicKeyDialogComponent} from './components/chat-list/chat-list-header/chat-settings/public-key-dialog/public-key-dialog.component';
import {ConfirmationDialogComponent} from './components/chat-list/chat-list-header/chat-settings/delete-database-dialog/confirmation-dialog.component';
import {PickerModule} from '@ctrl/ngx-emoji-mart';
import {MdePopoverModule} from '@material-extended/mde';
import {EmojiModule} from '@ctrl/ngx-emoji-mart/ngx-emoji';
import {BotDashboardComponent} from './components/bot-dashboard/bot-dashboard.component';
import {BotDashboardHeaderComponent} from './components/bot-dashboard/bot-dashboard-header/bot-dashboard-header.component';
import {BotDashboardMyBotsComponent} from './components/bot-dashboard/bot-dashboard-my-bots/bot-dashboard-my-bots.component';
import {BotDashboardAddedBotsComponent} from './components/bot-dashboard/bot-dashboard-added-bots/bot-dashboard-added-bots.component';
import {BotDashboardRegisterDialogComponent} from './components/bot-dashboard/bot-dashboard-register-dialog/bot-dashboard-register-dialog.component';
import {BotDashboardManageBotDialogComponent} from './components/bot-dashboard/bot-dashboard-manage-bot-dialog/bot-dashboard-manage-bot-dialog.component';
import {ChatDeleteDialogComponent} from './components/chat-box/chat-header/chat-delete-dialog/chat-delete-dialog.component';
import {QRCodeModule} from 'angularx-qrcode';
import {NgxZxingMulticodeModule} from 'ngx-zxing-multicode';
import {StickerAddStickerpackComponent} from './components/chat-box/sticker-list/sticker-add-stickerpack/sticker-add-stickerpack.component';
import {FileDropModule} from 'ngx-file-drop';

export const IMPORTS = [
    CoreModule,
    CommonModule,
    ClientRoutingModule,
    ContenteditableModule,
    FormsModule,
    FileDropModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatDividerModule,
    MatListModule,
    MatCardModule,
    MatRippleModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatCheckboxModule,
    PickerModule,
    MdePopoverModule,
    EmojiModule,
    ReactiveFormsModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    QRCodeModule,
    NgxZxingMulticodeModule,
    MatProgressSpinnerModule,
];

export const DECLARATIONS = [
    ChatBoxComponent,
    ChatHeaderComponent,
    ChatInputComponent,
    ChatListComponent,
    ChatListEntryComponent,
    ChatNewDialogComponent,
    ChatDeleteDialogComponent,
    ChatListHeaderComponent,
    ChatMessageComponent,
    ChatBodyComponent,
    ChatSettingsDialogComponent,
    ChatRootComponent,
    StickerListComponent,
    StickerAddStickerpackComponent,
    ChatSettingsComponent,
    PublicKeyDialogComponent,
    ConfirmationDialogComponent,
    BotDashboardComponent,
    BotDashboardHeaderComponent,
    BotDashboardMyBotsComponent,
    BotDashboardAddedBotsComponent,
    BotDashboardRegisterDialogComponent,
    BotDashboardManageBotDialogComponent,
];

@NgModule({
    declarations: DECLARATIONS,
    imports: [
        IMPORTS,
        MatProgressSpinnerModule,
    ],
    entryComponents: [
        ChatSettingsDialogComponent,
        ChatNewDialogComponent,
        ChatDeleteDialogComponent,
        StickerListComponent,
        StickerAddStickerpackComponent,
        PublicKeyDialogComponent,
        ConfirmationDialogComponent,
        BotDashboardRegisterDialogComponent,
        BotDashboardManageBotDialogComponent,
    ],
})
export class ClientModule {
}
