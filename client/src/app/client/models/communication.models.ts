export enum MessagingTypes {
    MSG_CRYPTO = 'msg-cry',
    MSG_REGISTER = 'msg-reg',
    MSG_SYSTEM = 'msg-sys',
}

export enum MessagingPayloadTypes {
    PAY_TEXT = 'pay-txt',
    PAY_ONLINE = 'pay-onl',
    PAY_STICKER = 'pay-sti',
}

export interface BaseMassaging {
    // Headers contain authentication data, they are not sent via the STOMP message payload

    readonly type: MessagingTypes;
    readonly timestamp: number;   // time of message sent
    readonly payload: string;     // encrypted or unencrypted

}

/**
 * This message has an encrypted payload and is used to exchange not only chat
 * messages, but also the online status of a given client.
 */
export interface MessagingCryptoMessage extends BaseMassaging {

    readonly id: string; // id of this message
    readonly type: MessagingTypes.MSG_CRYPTO;
    readonly from: string; // our id
    readonly cryptoType: number;

}

/**
 * This type is used by, for example, the messaging service to send things like
 * sent receipts etc.
 */
export interface MessagingSystemMessage extends BaseMassaging {

    readonly type: MessagingTypes.MSG_SYSTEM;
    readonly messageId: string; // id of the message this one is referencing

}

export interface BaseMessagingPayload {
    readonly type: MessagingPayloadTypes;
}

/**
 * This payload is used to send an online status update.
 */
export interface MessagingOnlinePayload extends BaseMessagingPayload {
    readonly type: MessagingPayloadTypes.PAY_ONLINE;
    readonly time: number;
}

/**
 * This payload is used to send a normal chat message.
 */
export interface MessagingTextPayload extends BaseMessagingPayload {
    readonly type: MessagingPayloadTypes.PAY_TEXT;
    readonly text: string;
}

/**
 * This payload is used to send a sticker.
 */
export interface MessagingStickerPayload extends BaseMessagingPayload {
    readonly type: MessagingPayloadTypes.PAY_STICKER;
    readonly url: string;
}

export type MessagingPayload =
    | MessagingOnlinePayload
    | MessagingTextPayload
    | MessagingStickerPayload
    ;

export type MessagingReceivedMessage =
    | MessagingCryptoMessage
    | MessagingSystemMessage
    ;
