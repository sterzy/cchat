
export interface BotSearchResponse {
    id: string;
    name: string;
    description: string;
}

export interface BotDetails extends BotSearchResponse {
    registeredBy: string;
    registeredByUUID: string;
    isActive?: boolean;
    isPublic?: boolean;
}

export interface MyBotReponse extends BotDetails {
    isActive: boolean;
    isPublic: boolean;
}

export interface BotRegisterRequest {
    name: string;
    webHookRootURL: string;
    description: string;
    isPublic: boolean;
}
