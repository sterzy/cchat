import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {CoreModule} from './core/core.module';
import {RootComponent} from './core/components/root/root.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RxStompService} from '@stomp/ng2-stompjs';
import {HttpClientModule} from '@angular/common/http';
import {UserManagementService} from './landingpage/services/user-management.service';
import {MatSnackBarModule} from '@angular/material';

@NgModule({
    imports: [
        BrowserModule,
        AppRoutingModule,
        CoreModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MatSnackBarModule,
    ],
    providers: [
        RxStompService,
    ],
    bootstrap: [RootComponent]
})
export class AppModule {

    constructor(
        userService: UserManagementService,
    ) {
        // Trigger initial whoAmI request
        userService.whoAmI().subscribe({
            error: () => {
                // WhoAmI failed, but nothing to do
            }
        });
    }
}
