import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserManagementService} from '../../services/user-management.service';

@Component({
    selector: 'cchat-multi-login-confirm',
    templateUrl: './multi-login-confirm.component.html',
    styleUrls: ['./multi-login-confirm.component.scss']
})
export class MultiLoginConfirmComponent implements OnInit {

    constructor(private router: Router, private userManagementService: UserManagementService) {
    }

    ngOnInit() {
    }

    procceed() {
        this.router.navigate(['/']);
    }

    decline() {
        this.userManagementService.logout().subscribe(() => this.router.navigate(['/']));
    }

}
