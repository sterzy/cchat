import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MultiLoginConfirmComponent} from './multi-login-confirm.component';
import {MatCardModule, MatSnackBarModule} from '@angular/material';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';

describe('MultiLoginConfirmComponent', () => {
    let component: MultiLoginConfirmComponent;
    let fixture: ComponentFixture<MultiLoginConfirmComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MultiLoginConfirmComponent],
            imports: [MatCardModule, RouterModule.forRoot([]), HttpClientModule, MatSnackBarModule]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MultiLoginConfirmComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
