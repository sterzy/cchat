import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {UserManagementService} from '../../services/user-management.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
    selector: 'cchat-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    static readonly defaultRegsiterErrorMessage = 'An unknown error occurred while registering, please try again later';

    registerForm: FormGroup;

    registerErrorMessage: string;

    loading = false;

    constructor(
        private formBuilder: FormBuilder,
        private userService: UserManagementService,
    ) {
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
                username: ['', Validators.required],
                password: ['', [Validators.required, Validators.minLength(6)]],
                confirmpassword: ['', Validators.required],
            }, {
                validator: this.MustMatch('password', 'confirmpassword'),
            }
        );
    }

    register() {

        this.registerErrorMessage = undefined;

        if (this.registerForm.invalid) {
            return;
        }

        const username = this.registerForm.get('username').value;
        const password = this.registerForm.get('password').value;

        this.loading = true;

        this.userService.register(username, password).subscribe({
            next: () => this.loading = false,
            error: (err: HttpErrorResponse) => {
                this.loading = false;

                if (err.error) {
                    this.registerErrorMessage = err.error.message || RegisterComponent.defaultRegsiterErrorMessage;
                } else {
                    this.registerErrorMessage = RegisterComponent.defaultRegsiterErrorMessage;
                }
            },
        });
    }


    // from: http://jasonwatmore.com/post/2018/11/07/angular-7-reactive-forms-validation-example
    MustMatch(controlName: string, matchingControlName: string): ValidatorFn {
        return (formGroup: FormGroup) => {
            const control = formGroup.get(controlName);
            const matchingControl = formGroup.get(matchingControlName);

            // set error on matchingControl if validation fails
            if (control.value !== matchingControl.value) {
                return {
                    mustMatch: true,
                };
            }

            return null;
        };
    }
}
