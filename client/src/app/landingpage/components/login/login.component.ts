import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserManagementService} from '../../services/user-management.service';
import {Router} from '@angular/router';
import {ConfigService} from '../../../core/services/config/config.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
    selector: 'cchat-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    static readonly defaultLoginErrorMessage = 'An unknown error occurred, please try again later';

    loginForm: FormGroup;

    loading = false;

    loginErrorMessage: string;

    constructor(
        private formBuilder: FormBuilder,
        private userService: UserManagementService,
    ) {
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required],
        });
    }

    login() {
        this.loginErrorMessage = undefined;

        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;

        const username = this.loginForm.get('username').value;
        const password = this.loginForm.get('password').value;

        this.userService.login(username, password).subscribe({
            next: () => this.loading = false,
            error: (err: HttpErrorResponse) => {
                this.loading = false;
                if (err.error) {
                    this.loginErrorMessage = err.error.message || LoginComponent.defaultLoginErrorMessage;
                } else {
                    this.loginErrorMessage = LoginComponent.defaultLoginErrorMessage;
                }
            }
        });

    }

}
