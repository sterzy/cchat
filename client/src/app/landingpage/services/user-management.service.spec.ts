import { TestBed } from '@angular/core/testing';

import { UserManagementService } from './user-management.service';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {MatSnackBarModule} from '@angular/material';

describe('UserManagementService', () => {
  beforeEach(() => TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterModule.forRoot([]), MatSnackBarModule]
  }));

  it('should be created', () => {
    const service: UserManagementService = TestBed.get(UserManagementService);
    expect(service).toBeTruthy();
  });
});
