import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {forkJoin, Observable} from 'rxjs';
import {SimpleUser} from '../../core/models/user.models';
import {first, mapTo, switchMap, tap} from 'rxjs/operators';
import {ConfigService} from '../../core/services/config/config.service';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {DatabaseDao} from '../../core/database/database.dao';

@Injectable({
    providedIn: 'root'
})
export class UserManagementService {

    apiurl = '/user/api';

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json',
        })
    };

    constructor(
        private http: HttpClient,
        private config: ConfigService,
        private router: Router,
        private snackBar: MatSnackBar,
        private databaseDao: DatabaseDao,
    ) {
    }

    /**
     * Method to log-in a user.
     * @param user      username of the user
     * @param pwd       password of the user
     */
    login(user: string, pwd: string): Observable<SimpleUser> {

        const json = {
            username: user,
            password: pwd,
        };

        return this.http.post<SimpleUser>(this.apiurl + '/login', json, this.httpOptions).pipe(
            tap({
                next: u => {
                    this.config.setUser(u);
                    if (localStorage.getItem('wasLoggedIn-' + user) !== 'true') {
                        this.router.navigate(['/confirm']);
                    } else {
                        this.router.navigate(['/']);
                    }
                },
                error: () => {
                    this.config.setUser(undefined);
                },
            }),
        );

    }

    /**
     * Method to register a new user.
     * @param user      username of the new user
     * @param pwd       password of the new user
     */
    register(user: string, pwd): Observable<SimpleUser> {

        const json = {
            username: user,
            password: pwd,
        };

        return this.http.post<SimpleUser>(this.apiurl + '/register', json, this.httpOptions).pipe(
            tap({
                next: u => {
                    this.config.setUser(u);
                    localStorage.setItem('wasLoggedIn-' + user, 'true');
                    this.router.navigate(['/']);
                },
                error: () => {
                    this.config.setUser(undefined);
                },
            }),
        );
    }

    /**
     * Method to logout the current user.
     */
    logout(): Observable<void> {
        return this.http.get<void>(this.apiurl + '/logout', this.httpOptions).pipe(
            tap({
                next: () => {
                    this.config.setUser(undefined);
                    this.router.navigate(['/login']);
                },
            }),
        );
    }

    /**
     * Method for requesting the logged-in user.
     */
    whoAmI(): Observable<SimpleUser> {
        return this.http.get<SimpleUser>(this.apiurl + '/whoami', this.httpOptions).pipe(
            tap({
                next: u => {
                    this.config.setUser(u);
                },
                error: () => {
                    this.config.setUser(undefined);
                }
            })
        );
    }

    /**
     * Method to permanently delete all related account-data to the currently logged-in user, including keys.
     */
    deleteAccountData(): Observable<void> {
        return forkJoin(
            this.http.get(this.apiurl + '/keys/unregister', this.httpOptions),
            this.http.get<void>(this.apiurl + '/unregister', this.httpOptions).pipe(
                switchMap(() => {
                    return this.config.getUser();
                }),
                first(),
                switchMap(user => {
                    return this.databaseDao.clearDatabase().pipe(
                        mapTo(user)
                    );
                }),
                first(),
                tap({
                    next: user => {
                        localStorage.removeItem('wasLoggedIn-' + user.username);
                        this.config.setUser(undefined);
                        window.location.href = '/';
                    },
                    error: () => {
                        this.snackBar.open('Something went wrong, please try again!', 'OK',
                            {duration: 2000});
                    }
                }))
        ).pipe(mapTo(undefined));
    }

}
