import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LandingpageRoutingModule} from './landingpage-routing.module';
import {RegisterComponent} from './components/register/register.component';
import {
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatInputModule,
    MatProgressSpinnerModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoginComponent} from './components/login/login.component';
import {MultiLoginConfirmComponent} from "./components/multi-login-confirm/multi-login-confirm.component";

export const DECLARATIONS = [
    LoginComponent,
    RegisterComponent,
    MultiLoginConfirmComponent,
];

export const IMPORTS = [
    CommonModule,
    LandingpageRoutingModule,
    MatDialogModule,
    MatInputModule,
    MatButtonModule,
    FormsModule,
    MatCardModule,
    MatProgressSpinnerModule,
    ReactiveFormsModule,
];

@NgModule({
    declarations: DECLARATIONS,
    imports: IMPORTS,
})
export class LandingpageModule {
}
