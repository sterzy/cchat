import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {IsRegisteredGuard} from './core/guards/is-registered.guard';
import {IsNotRegisteredGuard} from './core/guards/is-not-registered.guard';

export const ROUTES = {
    LANDING_PAGE: 'login',
};

const routes: Routes = [
    {path: '', canActivate: [IsRegisteredGuard], loadChildren: './client/client.module#ClientModule'},
    {path: '', canActivate: [IsNotRegisteredGuard], loadChildren: './landingpage/landingpage.module#LandingpageModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
