# Crypto Chat
This is a project for the Advanced Software Engineering course at TU Vienna in the summer semester 2019.

- [GitLab Project](https://gitlab.com/sterzy/cchat)
- [Redmine Issue Tracker (Not Public)](https://reset.inso.tuwien.ac.at/redmine/projects/proj_ss19_ase_inso_03/issues)
- [Redmine Wiki (Not Public)](https://reset.inso.tuwien.ac.at/redmine/projects/proj_ss19_ase_inso_03/wiki)

## How to Build & Run the Project
You can either run the services locally or you can user docker to build the services.

### Using Docker
To start all necessary containers you can simply execute the following command.

```bash
docker-compose up --build -d
```

After `docker-compose` has finished (this might take a while) it will take a couple of minutes until all services have been properly started. During this time the front-end will already be reachable, but you will not be able to log in or register, as the services handeling these actions aren't done starting up yet. When everything is up and running, you can visit the following pages:

- Client: [https://localhost:8080/](https://localhost:8000/)
- RabbitMQ Management Front-end: [http://localhost:15672/](http://localhost:15672/)
- Keycloak Front-end: [http://localhost:8081/](http://localhost:8080/)

*Note:* For this one you do not need anything else, but `docker`. We achieve this by using multi-staged docker builds.

*Note 2:* If the `mongodb` container crashes or you get an error in the `messaging` or `user` service telling you that the authentication failed, you might need to remove the `_data` folder that was created. MongoDB stores its data in that folder and will not update the credentials needed to connect to said database, if it detects that one exists already.

### Using `mvn` and `npm`
For the `user` and `messaging` service you can simply execute the following to start the services locally (these have to be executed in the respective directories):

```bash
mvn clean install
java -jar target/app.jar
```

For the `client` you can do the following:

```bash
npm install
npm start
```

#### Requirements:

- Java 11
- Maven 3.6
- Node 10
- NPM 6

## Test Coverage
| Service/Component | Coverage |
|:------------------|:--------:|
| `messaging` | [![messaging coverage](https://gitlab.com/sterzy/cchat/badges/develop/coverage.svg?job=test_messaging)](https://gitlab.com/sterzy/cchat/tree/develop/messaging) |
| `user` | [![user coverage](https://gitlab.com/sterzy/cchat/badges/develop/coverage.svg?job=test_user)](https://gitlab.com/sterzy/cchat/tree/develop/user) |
| `client` | [![client coverage](https://gitlab.com/sterzy/cchat/badges/develop/coverage.svg?job=test_client)](https://gitlab.com/sterzy/cchat/tree/develop/client) |
| `sticker` | [![sticker coverage](https://gitlab.com/sterzy/cchat/badges/develop/coverage.svg?job=test_sticker)](https://gitlab.com/sterzy/cchat/tree/develop/sticker) |
| `bot` | [![sticker coverage](https://gitlab.com/sterzy/cchat/badges/develop/coverage.svg?job=test_bot)](https://gitlab.com/sterzy/cchat/tree/develop/bot) |
